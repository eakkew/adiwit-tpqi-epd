<?php namespace Tpqi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Request;

class Answer extends Model {
	use SoftDeletes;
	
	protected	$connection		= 'adiwit_tpqi';
	protected	$table			= 'fact_answers';
	protected	$fillable		= [
		'id',
		'question_id',
		'content',
		'is_correct_answer'
	];

    /**
     * Relationship
     */
    public function question() {
    	return $this->belongsTo('Tpqi\Question', 'id', 'question_id');
    }
}