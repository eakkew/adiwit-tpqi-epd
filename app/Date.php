<?php

namespace Tpqi;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Date extends Model
{
    protected	$connection		= 'adiwit_tpqi';
	protected	$table			= 'dim_dates';
	public		$timestamps		= false;
	protected	$fillable		= ['day','month','year'];

	public static function generate($year, $month, $day) {
		$date = Date::find(Date::max('id'));
		$diff = abs(strtotime($year.'-'.$month.'-'.$day) - strtotime($date->year.'-'.$date->month.'-'.$date->day));
		$diffDays = floor($diff/(60*60*24));
		$carbon = Carbon::createFromDate($date->year, $date->month, $date->day);
		$carbon->addDay();
		$i = 0;
		while($i < $diffDays){
			Date::create([
                'day'   =>  $carbon->day,
                'month' =>  $carbon->month,
                'year'  =>  $carbon->year
            ]);
            $carbon->addDay();
			$i++;
		}
	}
}
