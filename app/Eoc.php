<?php namespace Tpqi;

use Illuminate\Database\Eloquent\Model;

class Eoc extends Model {
	
	protected	$connection	=	'tpqi_occ';
	protected	$table		=	'professional_standard';
	protected	$primaryKey	=	'stdID';
	public		$timestamps	=	false;

	/**
	 * Custom Attributes
	 */
	public function getUocIdAttribute() {
		return $this->attributes['parent'];
	}

	/**
	 * Relationships
	 */
	public function levelCompetence() {
		return $this->hasOne('Tpqi\LevelCompetence', 'elementID', 'stdID');
	}
	public function uoc() {
		return $this->hasOne('Tpqi\Uoc', 'stdID', 'parent');
	}
	public function question() {
		return $this->hasMany('Tpqi\Question', 'eoc_id', 'stdID');
	}
	public function industryGroup() {
		return $this->hasOne('Tpqi\Industry', 'indGRPID', 'indID');
	}
	public function industry() {
		return $this->hasOne('Tpqi\IndustryGroupX', 'indID', 'indID');
	}
}