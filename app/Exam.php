<?php

namespace Tpqi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Exam extends Model
{
	use SoftDeletes;

    protected	$connection		=	'adiwit_tpqi';
	protected	$table			=	'dim_exams';
	protected	$fillable		= [
		'date_id',
		'level_competence_name',
		'template_id',
		'cb_id',
		'set',
		'subset',
		'used_pdf',
		'hour',
		'minute',
		'location',
		'type'
	];

	protected static function boot() {
        parent::boot();

        static::deleting(function($exam) {
             $exam->examQuestions()->delete();
        });
    }
    // getter
    public function getCreateAtThaiAttribute() {
    	$dates = explode(' ', $this->created_at);
    	$date = '';
    	if(count($dates) > 1){
    		$date = explode('-', $dates[0]);
	    	$date = $date[2] . '/' . $date[1] . '/' . ($date[0]+543);
	    	// $date = $date[2] . '/' . $date[1] . '/' . ($date[0]+543) . ' ' . $dates[1];
    	}
		return $date;
	}

	public function getLastestTHAttribute() {
    	$dates = explode(' ', $this->lastest);
    	$date = '';
    	if(count($dates) > 1){
    		$date = explode('-', $dates[0]);
	    	$date = $date[2] . '/' . $date[1] . '/' . ($date[0]+543);
	    	// $date = $date[2] . '/' . $date[1] . '/' . ($date[0]+543) . ' ' . $dates[1];
    	}
		return $date;
	}

	public function getDeleteAtThaiAttribute() {
    	$dates = explode(' ', $this->deleted_at);
    	$date = '';
    	if(count($dates) > 1){
    		$date = explode('-', $dates[0]);
	    	$date = $date[2] . '/' . $date[1] . '/' . ($date[0]+543);
	    	// $date = $date[2] . '/' . $date[1] . '/' . ($date[0]+543) . ' ' . $dates[1];
    	}
		return $date;
	}

	//Relationship
	public function examQuestions() {
		return $this->hasMany('Tpqi\ExamQuestion', 'exam_id', 'id');
	}

	public function examQuestionsTrash() {
		return $this->hasMany('Tpqi\ExamQuestion', 'exam_id', 'id')->withTrashed();
	}

	public function lavelCompetence() {
		return $this->belongsTo('Tpqi\LevelCompetence', 'level_competence_name', 'levelName');
	}

	public function templateHeader() {
    return $this->belongsTo('Tpqi\TemplateHeader', 'template_id' ,'id')->withTrashed();
		// return $this->belongsTo('Tpqi\TemplateHeader', 'template_id' ,'id');
	}

	public function templateHeaderTrash() {
		return $this->belongsTo('Tpqi\TemplateHeader', 'template_id' ,'id')->withTrashed();
	}

	public function templateItems() {
		return $this->hasMany('Tpqi\TemplateItem', 'template_id', 'template_id')->withTrashed();
	}

	public function date() {
		return $this->belongsTo('Tpqi\Date', 'date_id', 'id');
	}

	public function organization() {
		return $this->belongsTo('Tpqi\Organization', 'cb_id', 'orgID');
	}

	public function examTransections() {
		return $this->hasMany('Tpqi\ExamTransection', 'exam_id', 'id');
	}
}
