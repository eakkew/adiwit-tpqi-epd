<?php

namespace Tpqi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExamAnswer extends Model
{
	use SoftDeletes;
	
    protected	$connection		=	'adiwit_tpqi';
	protected	$table			=	'fact_exam_answers';
	protected	$fillable		= 	[
		'exam_question_id',
		'answer_id',
		'correct_answer'
	];
	
	//Relations
	public function examQuestion() {
		return $this->belongsTo('Tpqi\ExamQuestion', 'exam_question_id', 'id');
	}

	public function answer() {
		return $this->belongsTo('Tpqi\Answer', 'answer_id', 'id');
	}	
}
