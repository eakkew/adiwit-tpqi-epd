<?php

namespace Tpqi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExamQuestion extends Model
{
	use SoftDeletes;
	
    protected	$connection		=	'adiwit_tpqi';
	protected	$table			=	'fact_exam_questions';
	protected	$fillable		= 	[
		'exam_id',
		'question_group_id',
		'question_id'
	];

    protected static function boot() {
        parent::boot();

        static::updating(function($examQuestion) {
             $examQuestion->examAnswers()->delete();
        });
    }
	
	//Relations
	public function exam() {
		return $this->belongsTo('Tpqi\Exam', 'exam_id', 'id');
	}
	public function examTrash() {
		return $this->belongsTo('Tpqi\Exam', 'exam_id', 'id')->withTrashed();
	}
	public function question() {
		return $this->belongsTo('Tpqi\Question', 'question_id', 'id');
	}
	public function questionTrash() {
		return $this->belongsTo('Tpqi\Question', 'question_id', 'id')->withTrashed();
	}
	public function examAnswers() {
		return $this->hasMany('Tpqi\ExamAnswer', 'exam_question_id', 'id');
	}
}
