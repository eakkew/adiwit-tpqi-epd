<?php

namespace Tpqi;

use Illuminate\Database\Eloquent\Model;

class ExamTransection extends Model
{
    protected	$connection	=	'adiwit_tpqi';
	protected	$table		=	'fact_exam_transection';
	protected	$fillable		= 	[
		'type',
		'exam_id',
		'user_id'
	];

	// Relationships
	public function tpqiUser() {
		return $this->hasOne('Tpqi\TpqiUser', 'personID', 'user_id');
	}

	public function exam() {
		return $this->hasOne('Tpqi\Exam', 'id', 'exam_id');
	}
}
