<?php

namespace Tpqi\Http\Controllers\Ajax;

use Illuminate\Http\Request;
use Tpqi\Http\Requests;
use Tpqi\Http\Controllers\Controller;
use Tpqi\Exam;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        // dd($request->all());
        $type = $request->input('type');
        $type = ($type == 1)? 'level' : (($type == 2)? 'uoc' : 'eoc');
        $exams = Exam::where('type', $type)->where('level_competence_name', $id);
        if($request->input('from') && $request->input('to'))
        {
            $exams = $exams->whereBetween('created_at',[$request->input('from'), $request->input('to')]);
        }
        $exams = $exams->groupBy('set')->withTrashed()->with('templateHeaderTrash')->get();
        return $exams;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
