<?php namespace Tpqi\Http\Controllers\Ajax;

use Tpqi\Http\Requests;
use Tpqi\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Tpqi\Industry;
use Tpqi\IndustryGroupX;
use Tpqi\LevelCompetence;
use Tpqi\ProfessionalStandard;
use Tpqi\Uoc;
use Tpqi\Eoc;
use Tpqi\FactTemplate;
use Tpqi\DimTemplates;

class FilterController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		switch ($id) {
			case 'industries':
			break;
			case 'profession':
			break;
			case 'qualification':
			break;
			case 'uoc':
			break;
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit()
	{

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy()
	{

	}

	public function selectedFilter(Request $request)
	{
		// dd($request->all());
		$request->session()->forget($request->input('nextTo'));
		// if(sizeof($request->input('eocs'))) {
		if(strpos($request->input('nextTo'), 'report') !== false){
			$fromDate = [];
			$toDate = [];
			session([$request->input('nextTo').'.filterSelected.industriesID' => $request->input('industry_id')]);
			session([$request->input('nextTo').'.filterSelected.levelsID' => $request->input('qualification')]);
			session([$request->input('nextTo').'.filterSelected.eocsID' => $request->input('eocs')]);
			session([$request->input('nextTo').'.filterSelected.type' => $request->input('type')]);
			session([$request->input('nextTo').'.filterSelected.typeData' => $request->input('typeData')]);
			session([$request->input('nextTo').'.filterSelected.typeReport' => $request->input('typeReport')]);
			if($request->input('masterExam')) session([$request->input('nextTo').'.filterSelected.masterExam' => $request->input('masterExam')]);
			if($request->input('orderType')) session([$request->input('nextTo').'.filterSelected.orderType' => $request->input('orderType')]);


			if($request->input('fromDate') && $request->input('toDate')){
				$explodeDate = explode("/", $request->input('fromDate'));
				$fromDate['day'] = $explodeDate[0];
				$fromDate['month'] = $explodeDate[1];
				$fromDate['year'] = $explodeDate[2];
				$explodeDate = explode("/", $request->input('toDate'));
				$toDate['day'] = $explodeDate[0];
				$toDate['month'] = $explodeDate[1];
				$toDate['year'] = $explodeDate[2];
				session([$request->input('nextTo').'.filterSelected.fromDate' => $fromDate]);
				session([$request->input('nextTo').'.filterSelected.toDate' => $toDate]);
			}
		}else{
			$industriesID = $request->input('industry_id');
			$levelsID = $request->input('qualification');
			$eocsID = $request->input('eocs');
			$type = $request->input('type');
			$fromDate = [];
			$toDate = [];
			
			session([$request->input('nextTo').'.filterSelected.industriesID' => $industriesID]);
			session([$request->input('nextTo').'.filterSelected.levelsID' => $levelsID]);
			session([$request->input('nextTo').'.filterSelected.eocsID' => $eocsID]);
			session([$request->input('nextTo').'.filterSelected.type' => $type]);


			if($request->input('fromDate') && $request->input('toDate')){
				$explodeDate = explode("/", $request->input('fromDate'));
				$fromDate['day'] = $explodeDate[0];
				$fromDate['month'] = $explodeDate[1];
				$fromDate['year'] = $explodeDate[2];
				$explodeDate = explode("/", $request->input('toDate'));
				$toDate['day'] = $explodeDate[0];
				$toDate['month'] = $explodeDate[1];
				$toDate['year'] = $explodeDate[2];
				session([$request->input('nextTo').'.filterSelected.fromDate' => $fromDate]);
				session([$request->input('nextTo').'.filterSelected.toDate' => $toDate]);
			}
		}
		// }
		// dd($request->session()->all());

		if(strpos($request->input('nextTo'), 'report') !== false){
			return redirect('/tpqi_epd/report/'.$request->input('nextTo'));
		}else{
			return redirect('/tpqi_epd/'.$request->input('nextTo'));
			// return redirect('/'.$request->input('nextTo'));
		}
	}

	public function postFilterValue(Request $request)
	{
		if ($request->input('dataName') == 'industryID'){
			$industries	= $request->input('parameter');
			if(gettype($industries) == 'array'){
				$professionIDs = ProfessionalStandard::whereIn('indID',$industries)->levelType()->lists('stdID');
			}else{
				$professionIDs = ProfessionalStandard::where('indID', '=', $industries)->levelType()->lists('stdID');
			}
			$levelCompetences = LevelCompetence::whereIn('stdID',$professionIDs)->groupBy('levelName')->get();

			return $levelCompetences;
		} else if ( $request->input('dataName') == 'qualification'){
			$qualification =  $request->input('parameter');
			if(gettype($qualification) == 'array'){
				$uocIDs = LevelCompetence::whereIn('levelName',$qualification)->groupBy('uocID')->lists('uocID');
			}else{
				$uocIDs = LevelCompetence::where('levelName', '=', $qualification)->groupBy('uocID')->lists('uocID');
			}
			if($request->input('group') == 'Y'){
				$uocs = Uoc::whereIn('stdID', $uocIDs)->with('eocs')->with('uocTpqi')->get();
			}else{
				$uocs = Uoc::whereIn('stdID', $uocIDs)->with('uocTpqi')->get();
			}

			return $uocs;
		} else if ($request->input('dataName') == 'uoc'){
			$uoc =  $request->input('parameter');
			if(gettype($uoc) == 'array'){
				$eocs = ProfessionalStandard::eocType()->whereIn('parent', $uoc)->get();
			}else{
				$eocs = ProfessionalStandard::eocType()->where('parent', '=', $uoc)->get();
			}

			return $eocs;
		} else if ($request->input('dataName') == 'dimtemplate'){
			$templateID =  $_GET["dimtemplate"]["templateID"];
			$eocID =  $_GET["dimtemplate"]["eocID"];
			$templateItems = FactTemplate::where('template_id', $templateID)->where('eoc_id', $eocID)->get();

			return $templateItems;
		} else if ($request->input('dataName') == 'getOnlyUOC'){
			$qualification =  $request->input('parameter');
			if(gettype($qualification) == 'array'){
				$uocIDs = LevelCompetence::whereIn('levelName', $qualification)->groupBy('uocID')->lists('uocID');
			}else{
				$uocIDs = LevelCompetence::where('levelName', '=', $qualification)->groupBy('uocID')->lists('uocID');
			}
			$uocs = Uoc::whereIn('stdID', $uocIDs)->get();

			return $uocs;
		}
	}

}
