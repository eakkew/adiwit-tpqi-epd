<?php namespace Tpqi\Http\Controllers\Ajax;

use Tpqi\Http\Requests;
use Tpqi\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Tpqi\Industry;
use Tpqi\IndustryGroupX;
use Tpqi\LevelCompetence;
use Tpqi\ProfessionalStandard;
use Tpqi\UOC;
use Tpqi\EOC;
use Tpqi\FactTemplate;
use Tpqi\DimTemplates;

class IndustryController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (array_key_exists('industryID', $_GET)){
			$industrys	= $_GET["industryID"];
			$professionIDs = ProfessionalStandard::whereIn('indID',$industrys)->levelType()->lists('stdID');
			$levelCompetences = LevelCompetence::whereIn('stdID',$professionIDs)->groupBy('levelName')->get();

			return $levelCompetences;
		} else if (array_key_exists('qualification', $_GET)){
			$qualification =  $_GET["qualification"];
			$uocIDs = LevelCompetence::whereIn('levelName',$qualification)->groupBy('uocID')->lists('uocID');
			if($qualification['group'] == 'Y'){
				$uocs = UOC::whereIn('stdID', $uocIDs)->with('eocs')->get();
			}else{
				$uocs = UOC::whereIn('stdID', $uocIDs)->get();
			}

			return $uocs;
		} else if (array_key_exists('uoc', $_GET)){
			$uoc =  $_GET["uoc"];
			$eocs = ProfessionalStandard::eocType()->whereIn('parent', $uoc)->get();

			return $eocs;
		} else if (array_key_exists('dimtemplate', $_GET)){
			$templateID =  $_GET["dimtemplate"]["templateID"];
			$eocID =  $_GET["dimtemplate"]["eocID"];
			$templateItems = FactTemplate::where('template_id', $templateID)->where('eoc_id', $eocID)->get();

			return $templateItems;
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		switch ($id) {
			case 'industries':
			break;
			case 'profession':
			break;
			case 'qualification':
			break;
			case 'uoc':
			break;
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit()
	{

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy()
	{

	}

}
