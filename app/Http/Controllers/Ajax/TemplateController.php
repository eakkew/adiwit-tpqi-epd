<?php

namespace Tpqi\Http\Controllers\Ajax;

use Illuminate\Http\Request;

use Tpqi\Http\Requests;
use Tpqi\Http\Controllers\Controller;
use Tpqi\TemplateHeader;

class TemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id, Request $request)
    {
        $txt = [];
        if($request->input('type') == 'getQuestionAmount'){
            $templateHeader = TemplateHeader::find($id);
            $flagAmount = 1;
            foreach ($templateHeader->templateItems as $templateItems) {
                if($templateHeader->max_difficulty > 0){
                    $flagAmount = ($templateItems->question()->where('difficulty', '<=', $templateHeader->max_difficulty)->count() < $templateItems->amount)? 0 : $flagAmount;
                }else{
                    $flagAmount = ($templateItems->question->count() < $templateItems->amount)? 0 : $flagAmount;
                }
                // foreach ($templateItems->question as $question) {
                //     $txt[$templateItems->eoc_id] = $templateHeader->max_difficulty . ' : ' . $question->difficulty;
                // }
            }
            return $flagAmount;
        }else{
            return TemplateHeader::where('level_competence_name', '=', $id)->get();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}