<?php namespace Tpqi\Http\Controllers;

use Tpqi\Http\Requests;
use Tpqi\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Tpqi\Eoc;
use Tpqi\Uoc;
use Tpqi\Industry;
use Tpqi\TemplateItem;
use Tpqi\IndustryGroupX;
use Tpqi\TemplateHeader;
use Tpqi\LevelCompetence;
use Tpqi\ProfessionalStandard;
use Tpqi\Organization;
use Tpqi\Qualificationscope;
use Tpqi\Question;
use Tpqi\Answer;
use Tpqi\Date;
use Tpqi\Exam;
use Tpqi\ExamQuestion;
use Tpqi\ExamAnswer;
use Tpqi\TpqiUser;
use Auth;


class ExamController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		// dd(TpqiUser::where('personID', $request->session()->get('tpqi.person.personID'))->first());
		// session selected filter
		$sessionEocID = ($request->session()->get('exam.filterSelected.eocsID'))? $request->session()->get('exam.filterSelected.eocsID') : [];
		$sessionLevelsID = ($request->session()->get('exam.filterSelected.levelsID'))? $request->session()->get('exam.filterSelected.levelsID') : [];
		$sessionIndustriesID = ($request->session()->get('exam.filterSelected.industriesID'))? $request->session()->get('exam.filterSelected.industriesID') : [];
		$sessionFromDate = ($request->session()->get('exam.filterSelected.fromDate'))? $request->session()->get('exam.filterSelected.fromDate') : [];
		$sessionToDate = ($request->session()->get('exam.filterSelected.toDate'))? $request->session()->get('exam.filterSelected.toDate') : [];
		$sessiontype = ($request->session()->get('exam.filterSelected.type'))? $request->session()->get('exam.filterSelected.type') : '';

		// load from selected Filter
		$professionalsID = ProfessionalStandard::whereIn('indID', $sessionIndustriesID)->levelType()->lists('stdID');
		$qualifications = LevelCompetence::whereIn('stdID', $professionalsID)->groupBy('levelName')->get();
		$uocsID = LevelCompetence::whereIn('levelName', $sessionLevelsID)->groupBy('uocID')->lists('uocID');
		$uocs = UOC::whereIn('stdID', $uocsID)->with('eocs')->get();

		// page
		$perPage = 10;
        $currentPage = ($request->input('page'))? $request->input('page') : 1;
        $skip = ($currentPage)? ($perPage*$currentPage)-$perPage : 0;

		// row data
		if($sessiontype == 1){ // type by level
			if($sessionLevelsID) {
	    		if($sessionFromDate && $sessionToDate){
					$from = ($sessionFromDate && $sessionToDate)? ($sessionFromDate['year']-543).'-'.$sessionFromDate['month'].'-'.$sessionFromDate['day'] : '';
		    		$to = ($sessionFromDate && $sessionToDate)? ($sessionToDate['year']-543).'-'.$sessionToDate['month'].'-'.$sessionToDate['day'] : '';
	    			$examQuestionsID = ExamQuestion::whereHas('questionTrash',
						function($q) use ($sessionLevelsID) {
							$q->whereIn('level_competence_name', $sessionLevelsID )->where('type', 'level');
							// ($sessionFromDate && $sessionToDate)? $q->whereIn('eoc_id', $sessionEocID ) : $q->whereIn('eoc_id', $sessionEocID );
						})->whereBetween('created_at',[$from, $to])
						->groupBy('exam_id')
						->lists('exam_id');
	    		}else{
	    			$examQuestionsID = ExamQuestion::whereHas('questionTrash',
	    				function($q) use ($sessionLevelsID) {
							$q->whereIn('level_competence_name', $sessionLevelsID )->where('type', 'level');
						})->groupBy('exam_id')
	    				->lists('exam_id');
	    		}
				$exams = Exam::whereIn('id', $examQuestionsID)->orderBy('id', 'ASC');
				// $exams = Exam::withTrashed()->whereIn('id', $examQuestionsID)->orderBy('id', 'ASC');
			} else {
				$exams = Exam::orderBy('id', 'ASC');
				// $exams = Exam::withTrashed()->orderBy('id', 'ASC');
			}
		}elseif($sessiontype == 2){ // type by uoc
			if($sessionEocID) {
	    		if($sessionFromDate && $sessionToDate){
					$from = ($sessionFromDate && $sessionToDate)? ($sessionFromDate['year']-543).'-'.$sessionFromDate['month'].'-'.$sessionFromDate['day'] : '';
		    		$to = ($sessionFromDate && $sessionToDate)? ($sessionToDate['year']-543).'-'.$sessionToDate['month'].'-'.$sessionToDate['day'] : '';
	    			$examQuestionsID = ExamQuestion::whereHas('questionTrash',
						function($q) use ($sessionEocID) {
							$q->whereIn('eoc_id', $sessionEocID )->where('type', 'uoc');
							// ($sessionFromDate && $sessionToDate)? $q->whereIn('eoc_id', $sessionEocID ) : $q->whereIn('eoc_id', $sessionEocID );
						})->whereBetween('created_at',[$from, $to])
						->groupBy('exam_id')
						->lists('exam_id');
	    		}else{
	    			$examQuestionsID = ExamQuestion::whereHas('questionTrash',
	    				function($q) use ($sessionEocID) {
							$q->whereIn('eoc_id', $sessionEocID )->where('type', 'uoc');
						})->groupBy('exam_id')
	    				->lists('exam_id');
	    		}
				$exams = Exam::whereIn('id', $examQuestionsID)->orderBy('id', 'ASC');
				// $exams = Exam::withTrashed()->whereIn('id', $examQuestionsID)->orderBy('id', 'ASC');
			} else {
				$exams = Exam::orderBy('id', 'ASC');
				// $exams = Exam::withTrashed()->orderBy('id', 'ASC');
			}
		}else{ // type by eoc
			if($sessionEocID) {
	    		if($sessionFromDate && $sessionToDate){
					$from = ($sessionFromDate && $sessionToDate)? ($sessionFromDate['year']-543).'-'.$sessionFromDate['month'].'-'.$sessionFromDate['day'] : '';
		    		$to = ($sessionFromDate && $sessionToDate)? ($sessionToDate['year']-543).'-'.$sessionToDate['month'].'-'.$sessionToDate['day'] : '';
	    			$examQuestionsID = ExamQuestion::whereHas('questionTrash',
						function($q) use ($sessionEocID) {
							$q->whereIn('eoc_id', $sessionEocID )->where('type', 'eoc');
							// ($sessionFromDate && $sessionToDate)? $q->whereIn('eoc_id', $sessionEocID ) : $q->whereIn('eoc_id', $sessionEocID );
						})->whereBetween('created_at',[$from, $to])
						->groupBy('exam_id')
						->lists('exam_id');
	    		}else{
	    			$examQuestionsID = ExamQuestion::whereHas('questionTrash',
	    				function($q) use ($sessionEocID) {
							$q->whereIn('eoc_id', $sessionEocID )->where('type', 'eoc');
						})->groupBy('exam_id')
	    				->lists('exam_id');
	    		}
				$exams = Exam::whereIn('id', $examQuestionsID)->orderBy('id', 'ASC');
				// $exams = Exam::withTrashed()->whereIn('id', $examQuestionsID)->orderBy('id', 'ASC');
			} else {
				$exams = Exam::orderBy('id', 'ASC');
				// $exams = Exam::withTrashed()->orderBy('id', 'ASC');
			}
		}

		$allDataCount = $exams->count();

		// statistic
		// $limitStatistic = 20;
		// $industriesStatistic = [];
		// foreach ($exams->get() as $exam) {
		// 	if($exam->templateHeader->IndustryData->indName) {
		// 		if(!isset($industriesStatistic[$exam->templateHeader->IndustryData->indName])) $industriesStatistic[$exam->templateHeader->IndustryData->indName] = 0;
		// 		$industriesStatistic[$exam->templateHeader->IndustryData->indName] += 1;
		// 	}elseif($exam->templateHeader->IndustryData->indGRPName){
		// 		if(!isset($industriesStatistic[$exam->templateHeader->IndustryData->indGRPName])) $industriesStatistic[$exam->templateHeader->IndustryData->indGRPName] = 0;
		// 		$industriesStatistic[$exam->templateHeader->IndustryData->indGRPName] += 1;
		// 	}
		// }
		// arsort($industriesStatistic);
		// ksort($industriesStatistic);
		// arsort($industriesStatistic);
		// $industriesStatistic = array_slice($industriesStatistic, 0, $limitStatistic);

		// user permission
		$authUser = Auth::user();
		$userType = ($authUser->hasRole(['view']))? 'view' : (($authUser->hasRole(['creator']))? 'creator' : (($authUser->hasRole(['master']))? 'master' : '')) ;
		return view('exam.index', [
			'industries'			=> Industry::with('industryGroupX')->orderBy('indGRPID')->get(),
			// 'industriesStatistic'	=> $industriesStatistic,
			'qualifications'		=> $qualifications,
			'uocs'					=> $uocs,
			'exams'					=> $exams->orderBy('id', 'desc')->skip($skip)->take($perPage)->get(),
			'industriesID'			=> $sessionIndustriesID,
			'levelsID'				=> $sessionLevelsID,
			'eocsID'				=> $sessionEocID,
			'fromDate'				=> $sessionFromDate,
			'toDate'				=> $sessionToDate,
			'allDataCount'			=> $allDataCount,
			'page'			        => $currentPage,
			'perPage'			    => $perPage,
			'userType'				=> $userType,
			'type'					=> $sessiontype,
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		if(Auth::user()->hasRole(['view'])) return redirect()->action('ExamController@index');

		return view('exam.create_coverpage')->with([
			'industries' => Industry::with('industryGroupX')->orderBy('indGRPID')->get()
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function genExamByDifficulty(Request $request)
	{
		$templateHeader = TemplateHeader::find($request->input('template_id'));
		$listQuestionsID = [];
		$examQuestions = [];
		$avgDifficulty = 0;
		// Generate Questions to ExamQuestion
		foreach ($templateHeader->templateItems as $templateItem) {
			if($templateItem->templateHeader->type == 'level'){
				if($templateHeader->max_difficulty > 0){
					$questionsID = Question::where('level_competence_name', '=', $templateItem->templateHeader->level_competence_name);
					// ->where('difficulty', '<=', $templateHeader->max_difficulty);
				}else{
					$questionsID = Question::where('level_competence_name', 'like', '%'.$templateItem->templateHeader->level_competence_name.'%');

				}
				$questionsID = $questionsID->where('type', 'level')->orderByRaw("RAND()")->limit($templateItem->amount)->get();
			}elseif($templateItem->templateHeader->type == 'uoc'){
				if($templateHeader->max_difficulty > 0){
					$questionsID = Question::where('eoc_id', '=', $templateItem->uoc->stdID);
					// ->where('difficulty', '<=', $templateHeader->max_difficulty);
				}else{
					$questionsID = Question::where('eoc_id', '=', $templateItem->uoc->stdID);
				}
				$questionsID = $questionsID->where('type', 'uoc')->orderByRaw("RAND()")->limit($templateItem->amount)->get();
			}else{
				if($templateHeader->max_difficulty > 0){
					$questionsID = Question::where('eoc_id', '=', $templateItem->eoc->stdID);
					// ->where('difficulty', '<=', $templateHeader->max_difficulty);
				}else{
					$questionsID = Question::where('eoc_id', '=', $templateItem->eoc->stdID);
				}
				$questionsID = $questionsID->where('type', 'eoc')->orderByRaw("RAND()")->limit($templateItem->amount)->get();
			}
			foreach($questionsID as $questionID){
				$avgDifficulty += $questionID->difficulty;
				$examQuestions[] = new ExamQuestion([ 'question_id'	=>	$questionID->id ]);
				$listQuestionsID[] = $questionID->id;
			}
		}
		// dd($avgDifficulty . ' : ' . count($examQuestions) . ' : ' . $avgDifficulty/count($examQuestions));
		// dd('-----');
		if(sizeof($examQuestions) > 0)
        	{
			$returnData['avg'] = $avgDifficulty/count($examQuestions);
			$returnData['data'] = $examQuestions;
			$returnData['listQuestionsID'] = $listQuestionsID;
		}
		else
		{
			$returnData['avg'] = 0.00;
			$returnData['data'] = $examQuestions;
			$returnData['listQuestionsID'] = $listQuestionsID;
		}
	
		// $returnData['listQuestionsID'] = implode(',', $listQuestionsID);
		return $returnData;
	}

	public function postCreateAutomatic(Request $request)
	{
		if(Auth::user()->hasRole(['view'])) return redirect()->action('ExamController@index');
		// dd($request->all());
		$examDate = explode('/', $request->input('examDate'));
		$allInputs = $request->all();
		$dateID = Date::where('day', '=', $examDate[0])->where('month', '=', $examDate[1])->where('year', '=', $examDate[2]-543)->first();
		if(!isset($dateID)){
        	Date::generate($examDate[2]-543, $examDate[1], $examDate[0]);
        	$dateID = Date::where('day', '=', $examDate[0])->where('month', '=', $examDate[1])->where('year', '=', $examDate[2]-543)->first();
		}
		$allInputs['date_id'] = $dateID->id;
		$templateHeader = TemplateHeader::find($request->input('template_id'));
		// dd($templateHeader->toArray());

		$i = 0;
		// dd($allInputs['amount']);
		$allInputs['set'] = count(Exam::where('template_id', '=', $request->input('template_id'))->groupBy('set')->lists('id'))+1;

		// new
		$allInputs['subset'] = 1;
		$allInputs['type'] = ($templateHeader->type == 'level')? 1 : (($templateHeader->type == 'uoc')? 2 : 3);
		$forloop = 10;
		if($request->input('nocheck')) $forloop = 1;
		for($x=0;$x<10;$x++){
			$examData = $this->genExamByDifficulty($request);
			$examData['flag'] = false;
			$difficulty = 0;
			if($examData['avg'] >= 0.60 && $examData['avg'] <= 0.80)
			{
				$difficulty = 1;
			}
			else if($examData['avg'] >= 0.40 && $examData['avg'] < 0.60)
			{
				$difficulty = 2;
			}
			else if($examData['avg'] >= 0.20 && $examData['avg'] < 0.40)
			{
				$difficulty = 3;
			}
			if($examData['avg'] == 0.00 && $difficulty == 0)
			{
				break;
			}
			//echo $examData['avg'] . ' : ' . $difficulty . ' : ' . $templateHeader->max_difficulty . ' || ';
			if($difficulty == $templateHeader->max_difficulty || $templateHeader->max_difficulty == 0){
				$examData['flag'] = true;
				break;
			}
		}

		// echo !$examData['flag'] . ' : ' . ($request->input('nocheck') == 'false');
		// dd($examData['listQuestionsID']->toArray());
		if(!$examData['flag'] && $request->input('nocheck') == 'false'){
			return 'no';
		}
		// dd($examData['listQuestionsID']);
		foreach($examData['listQuestionsID'] as $questionID){
			$ansCount = Answer::where('question_id', $questionID)->count();
			$limitAnswer = ($ansCount > 5)? 5 : $ansCount;
			$arrAnswers = Answer::where('question_id', $questionID)->orderByRaw('RAND()')->take($limitAnswer)->get();
			$flagCorrect = false;
			foreach($arrAnswers as $answer){
				$flagCorrect = ($answer->is_correct_answer)? true : $flagCorrect;
				$examData['listAnswersID'][$questionID][] = $answer->id;
			}
			if(!$flagCorrect){
				$correctAnswer = Answer::where('question_id', $questionID)->where('is_correct_answer', '=', '1')->first();
				$randomCorrect = rand(0,($limitAnswer-1));
				$examData['listAnswersID'][$questionID][$randomCorrect] = $correctAnswer->id;
			}
			$examData['listAnswersID'][$questionID] = implode(',', $examData['listAnswersID'][$questionID]);
		}
		return $examData;
		// $exam = Exam::create($allInputs);
		// $exam->examQuestions()->saveMany($examData['data']);

		// Generate Answer to ExamAnswer
		// foreach($exam->examQuestions as $examQuestion){
		// 	$examAnswersData = [];
		// 	$limitAnswer = ($examQuestion->question->answers->count() > 5)? 5 : $examQuestion->question->answers->count();
		// 	$arrAnswers = $examQuestion->question->answers->random($limitAnswer);
		// 	$flagCorrect = false;
		// 	foreach($arrAnswers as $answer){
		// 		$flagCorrect = ($answer->is_correct_answer)? true : $flagCorrect;
		// 		$examAnswersData[] = new ExamAnswer([
		// 			'answer_id' => $answer->id ,
		// 			'correct_answer' => $answer->is_correct_answer
		// 		]);
		// 	}
		// 	if(!$flagCorrect){
		// 		$correctAnswer = $examQuestion->question->answers()->where('is_correct_answer', '=', '1')->first();
		// 		$randomCorrect = rand(0,($limitAnswer-1));
		// 		$examAnswersData[$randomCorrect] = new ExamAnswer([
		// 			'answer_id' => $correctAnswer->id,
		// 			'correct_answer' => $correctAnswer->is_correct_answer
		// 		]);
		// 	}
		// 	$examQuestion->examAnswers()->saveMany($examAnswersData);
		// }


		// START : keep
		// while($i < $allInputs['amount']){
		// 	$allInputs['subset'] = $i+1;
		// 	$allInputs['type'] = ($templateHeader->type == 'level')? 1 : (($templateHeader->type == 'uoc')? 2 : 3);
		// 	if($i == 0){
		// 		$forloop = 10;
		// 		if($request->input('nocheck')) $forloop = 1;
		// 		for($x=0;$x<10;$x++){
		// 			$examData = $this->genExamByDifficulty($request);
		// 			$examData['flag'] = false;
		// 			if($examData['avg'] >= 0.60 && $examData['avg'] <= 0.80)
		// 			{
		// 				$difficulty = 1;
		// 			}
		// 			else if($examData['avg'] >= 0.40 && $examData['avg'] < 0.60)
		// 			{
		// 				$difficulty = 2;
		// 			}
		// 			else if($examData['avg'] >= 0.20 && $examData['avg'] < 0.40)
		// 			{
		// 				$difficulty = 3;
		// 			}
		// 			// echo $examData['avg'] . ' : ' . $difficulty . ' : ' . $templateHeader->max_difficulty . ' || ';
		// 			if($difficulty == $templateHeader->max_difficulty || $templateHeader->max_difficulty == 0){
		// 				$examData['flag'] = true;
		// 				break;
		// 			}
		// 		}
		// 		// echo !$examData['flag'] . ' : ' . ($request->input('nocheck') == 'false');
		// 		if(!$examData['flag'] && $request->input('nocheck') == 'false'){
		// 			return 'no';
		// 		}

		// 		$exam = Exam::create($allInputs);
		// 		$exam->examQuestions()->saveMany($examData['data']);

		// 		// Generate Answer to ExamAnswer
		// 		// dd('gen ans');
		// 		foreach($exam->examQuestions as $examQuestion){
		// 			$examAnswersData = [];
		// 			$limitAnswer = ($examQuestion->question->answers->count() > 5)? 5 : $examQuestion->question->answers->count();
		// 			$arrAnswers = $examQuestion->question->answers->random($limitAnswer);
		// 			$flagCorrect = false;
		// 			foreach($arrAnswers as $answer){
		// 				$flagCorrect = ($answer->is_correct_answer)? true : $flagCorrect;
		// 				$examAnswersData[] = new ExamAnswer([
		// 					'answer_id' => $answer->id ,
		// 					'correct_answer' => $answer->is_correct_answer
		// 				]);
		// 			}
		// 			if(!$flagCorrect){
		// 				$correctAnswer = $examQuestion->question->answers()->where('is_correct_answer', '=', '1')->first();
		// 				$randomCorrect = rand(0,($limitAnswer-1));
		// 				$examAnswersData[$randomCorrect] = new ExamAnswer([
		// 					'answer_id' => $correctAnswer->id,
		// 					'correct_answer' => $correctAnswer->is_correct_answer
		// 				]);
		// 			}
		// 			$examQuestion->examAnswers()->saveMany($examAnswersData);
		// 		}

		// 		$firstExam = $exam;
		// 	}else{
		// 		$exam = Exam::create($allInputs);
		// 		// dd('else');
		// 		$examQuestions = [];
		// 		// Generate Question to ExamQuestion from exam first set
		// 		foreach($firstExam->examQuestions->shuffle() as $examQuestion){
		// 			$examQuestions[] = new ExamQuestion([ 'question_id'	=>	$examQuestion->question_id ]);
		// 		}
		// 		$exam->examQuestions()->saveMany($examQuestions);

		// 		// Generate Answer to ExamAnswer
		// 		foreach($firstExam->examQuestions as $examQuestion){
		// 			$examAnswersData = [];
		// 			foreach($examQuestion->examAnswers->shuffle() as $answer){
		// 				$examAnswersData[] = new ExamAnswer([
		// 					'answer_id' => $answer->answer_id,
		// 					'correct_answer' => $answer->correct_answer
		// 				]);
		// 			}
		// 			$exam->examQuestions->where('question_id', $examQuestion->question_id)->first()->examAnswers()->saveMany($examAnswersData);
		// 		}
		// 	}

		// 	$i++;
		// }
		// return 'yes';
		// END : keep

		// return redirect()->action('ExamController@index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function postCreateManual(Request $request)
	{
		// dd($request->all());
		$examDate = explode('/', $request->input('examDate'));
		$questionsID = [];
		$answersID = [];
		if($request->input('questionsID')) $questionsID = explode(',', $request->input('questionsID'));
		if($request->input('listAnswersID')){
			foreach($request->input('listAnswersID') as $questionID => $listAnswerID){
				$answersID[$questionID] = explode(',', $listAnswerID);
			}
		}
		$allInputs = $request->all();
		$dateID = Date::where('day', '=', $examDate[0])->where('month', '=', $examDate[1])->where('year', '=', $examDate[2]-543)->first();
		if(!isset($dateID)){
        	Date::generate($examDate[2]-543, $examDate[1], $examDate[0]);
        	$dateID = Date::where('day', '=', $examDate[0])->where('month', '=', $examDate[1])->where('year', '=', $examDate[2]-543)->first();
		}

		return view('exam.create')->with([
			'templateHeader' => TemplateHeader::find($request->input('template_id')),
			'dateID' => $dateID,
			'inputData' => $request->all(),
			'questionsID' => $questionsID,
			'answersID' => $answersID,
		]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$allInputs = $request->all();
		// dd($request->all());
		// create Exam & ExamQuestion
		if($allInputs['amount'] > 1){
			$allInputs['set'] = count(Exam::where('template_id', '=', $request->input('template_id'))->groupBy('set')->lists('id'))+1;
			$i = 0;
			while($i < $allInputs['amount']){
				$allInputs['subset'] = $i+1;
				if($i == 0){
					$exam = Exam::create($allInputs);
					$examQuestions = [];
					foreach($request->input('question_id') as $questionID){
						$examQuestions[] = new ExamQuestion([ 'question_id'	=>	$questionID ]);
					}

					$exam->examQuestions()->saveMany($examQuestions);

					// Generate Answer to ExamAnswer
					$answers = $request->input('answer');
					foreach($exam->examQuestions as $examQuestion){
						$examAnswer = [];
						foreach ($answers[$examQuestion->question_id] as $answerID) {
							$correct = (strpos($request->input('correctAnswer'), $answerID))? 1 : 0;
							$examAnswer[] = new ExamAnswer([
								'answer_id' => $answerID,
								'correct_answer' => $correct
							]);
						}
						$examQuestion->examAnswers()->saveMany($examAnswer);
					}

					// foreach($exam->examQuestions as $examQuestion){
					// 	$examAnswersData = [];
					// 	$limitAnswer = ($examQuestion->question->answers->count() > 5)? 5 : $examQuestion->question->answers->count();
					// 	$arrAnswers = $examQuestion->question->answers->random($limitAnswer);
					// 	$flagCorrect = false;
					// 	foreach($arrAnswers as $answer){
					// 		$flagCorrect = ($answer->is_correct_answer)? true : $flagCorrect;
					// 		$examAnswersData[] = new ExamAnswer([
					// 			'answer_id' => $answer->id ,
					// 			'correct_answer' => $answer->is_correct_answer
					// 		]);
					// 	}
					// 	if(!$flagCorrect){
					// 		$correctAnswer = $examQuestion->question->answers()->where('is_correct_answer', '=', '1')->first();
					// 		$randomCorrect = rand(0,($limitAnswer-1));
					// 		$examAnswersData[$randomCorrect] = new ExamAnswer([
					// 			'answer_id' => $correctAnswer->id,
					// 			'correct_answer' => $correctAnswer->is_correct_answer
					// 		]);
					// 	}
					// 	$examQuestion->examAnswers()->saveMany($examAnswersData);
					// }

					$firstExam = $exam;
				}else{
					$exam = Exam::create($allInputs);
					// dd('else');
					$examQuestions = [];
					// Generate Question to ExamQuestion from exam first set
					foreach($firstExam->examQuestions->shuffle() as $examQuestion){
						$examQuestions[] = new ExamQuestion([ 'question_id'	=>	$examQuestion->question_id ]);
					}
					$exam->examQuestions()->saveMany($examQuestions);

					// Generate Answer to ExamAnswer
					foreach($firstExam->examQuestions as $examQuestion){
						$examAnswersData = [];
						foreach($examQuestion->examAnswers->shuffle() as $answer){
							$examAnswersData[] = new ExamAnswer([
								'answer_id' => $answer->answer_id,
								'correct_answer' => $answer->correct_answer
							]);
						}
						$exam->examQuestions->where('question_id', $examQuestion->question_id)->first()->examAnswers()->saveMany($examAnswersData);
					}
				}

				$i++;
			}
		}else{
			$allInputs['set'] = count(Exam::where('template_id', '=', $request->input('template_id'))->groupBy('set')->lists('id'))+1;
			$allInputs['subset'] = 1;
			$exam = Exam::create($allInputs);
			$examQuestions = [];
			foreach ($request->input('question_id') as $questionID) {
				$examQuestions[] = new ExamQuestion([ 'question_id'	=>	$questionID ]);
			}
			$exam->examQuestions()->saveMany($examQuestions);

			// create ExamAnswer
			$answers = $request->input('answer');
			foreach($exam->examQuestions as $examQuestion){
				$examAnswer = [];
				foreach ($answers[$examQuestion->question_id] as $answerID) {
					$correct = (strpos($request->input('correctAnswer'), $answerID))? 1 : 0;
					$examAnswer[] = new ExamAnswer([
						'answer_id' => $answerID,
						'correct_answer' => $correct
					]);
				}
				$examQuestion->examAnswers()->saveMany($examAnswer);
			}
		}

		return redirect()->action('ExamController@index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(Request $request, $id)
	{
		if($request->input('type') == 'getquestion'){
			return Question::where('eoc_id', '=', $id)->where('level_competence_name', $request->input('selectedLevel'))->whereNotIn('id', $request->input('selected'))->select('id', 'time_needed', 'content', 'difficulty')->get();
		}elseif($request->input('type') == 'getanswer'){
			if($request->input('selected')){
				return Answer::where('question_id', '=', $id)->whereNotIn('id', $request->input('selected'))->select('id', 'content', 'is_correct_answer')->get();
			}else{
				return Answer::where('question_id', '=', $id)->select('id', 'content', 'is_correct_answer')->get();
			}
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		if(Auth::user()->hasRole(['view'])) return redirect()->action('ExamController@index');
		$exam = Exam::find($id);
		// dd($exam->examQuestions->toArray());
		return view('exam.create')->with([
			'exam' => $exam,
			'dateID' => Date::find($exam->date_id)
		]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$exam = Exam::find($id);
		$inputQuestion = $request->input('question_id');
		$inputAnswer = $request->input('answer');

		// set array for scope query
		$questionLists = Question::whereIn('id', $inputQuestion)->get();
		foreach($questionLists as $questionList){
			$eocInputLists[$questionList->eoc_id][] = $questionList->id;
		}

		$questionLists = Question::whereIn('id', Exam::find($id)->examQuestions->lists('question_id')->toArray())->get();
		foreach($questionLists as $questionList){
			$eocUpdateLists[$questionList->eoc_id][] = $questionList->id;
		}
		$arrayDiffs = [];
		foreach($eocUpdateLists as $index => $eocUpdateList){
			if(array_diff($eocInputLists[$index], $eocUpdateList)) $arrayDiffs[$index] = array_diff($eocInputLists[$index], $eocUpdateList);
		}

		// update new examQuestion and examAnswer
		if($arrayDiffs){
			foreach($arrayDiffs as $index => $arrayDiff){
				sort($arrayDiff);
				$updateExamQuestions = [];
				foreach($eocUpdateLists[$index] as $examQuestionID){
					if(!in_array($examQuestionID, $eocInputLists[$index])) $updateExamQuestions[] = $examQuestionID;
				}
				$examQuestions = $exam->examQuestions()->whereIn('question_id', $updateExamQuestions)->get();
				foreach($examQuestions as $indexExam => $examQuestion){
					// update new question to exam
					$examQuestion->update([ 'question_id' => $arrayDiff[$indexExam] ]);

					// add new answer to exam
					$examAnswer = [];
					foreach ($inputAnswer[$examQuestion->question_id] as $answerID) {
						$correct = (strpos($request->input('correctAnswer'), $answerID))? 1 : 0;
						$examAnswer[] = new ExamAnswer([
							'answer_id' => $answerID,
							'correct_answer' => $correct
						]);
					}
					$examQuestion->examAnswers()->saveMany($examAnswer);
				}
			}
		}

		// update only new examAnswer
		foreach($exam->examQuestions as $qIndex => $examQuestion){
			foreach ($examQuestion->examAnswers as $aIndex => $examAnswer) {
				if(isset($inputAnswer[$examQuestion->question_id])){
					$answerID = $inputAnswer[$examQuestion->question_id][$aIndex];
					if($examAnswer->answer_id != $answerID){
						$examAnswer->update([
							'answer_id' => $answerID,
							'correct_answer' => (strpos($request->input('correctAnswer'), $answerID))? 1 : 0
						]);
					}
				}
			}
		}

		// foreach($exam->examQuestions as $qIndex => $examQuestion){
		// 	if($examQuestion->question_id != $inputQuestion[$qIndex]){
		// 		// update new question to exam
		// 		$examQuestion->update([
		// 			'question_id' => $inputQuestion[$qIndex]
		// 		]);

		// 		// add new answer to exam
		// 		$examAnswer = [];
		// 		foreach ($inputAnswer[$examQuestion->question_id] as $answerID) {
		// 			$correct = (strpos($request->input('correctAnswer'), $answerID))? 1 : 0;
		// 			$examAnswer[] = new ExamAnswer([
		// 				'answer_id' => $answerID,
		// 				'correct_answer' => $correct
		// 			]);
		// 		}
		// 		$examQuestion->examAnswers()->saveMany($examAnswer);
		// 	}else{
		// 		foreach ($examQuestion->examAnswers as $aIndex => $examAnswer) {
		// 			$answerID = $inputAnswer[$examQuestion->question_id][$aIndex];
		// 			if($examAnswer->answer_id != $answerID){
		// 				$examAnswer->update([
		// 					'answer_id' => $answerID,
		// 					'correct_answer' => (strpos($request->input('correctAnswer'), $answerID))? 1 : 0
		// 				]);
		// 			}
		// 		}
		// 	}
		// }

		// dd($request->all());
		return redirect()->action('ExamController@index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		if(Auth::user()->hasRole(['view'])) return redirect()->action('ExamController@index');
		$exam = Exam::find($id);
		if ($exam) $exam->delete();
		return redirect()->action('ExamController@index');
	}

	/**
	 * Shows the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getQuestion($id)
	{
		if(Auth::user()->hasRole(['creator'])) return redirect()->action('ExamController@index');
		return view('exam.question', [ 'exam' => Exam::find($id) ]);
	}

	/**
	 * Shows the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getAnswer($id)
	{
		if(Auth::user()->hasRole(['creator'])) return redirect()->action('ExamController@index');
		return view('exam.answer', [ 'exam' => Exam::find($id) ]);
	}

	/**
	 * Shows the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getSolution($id)
	{
		if(Auth::user()->hasRole(['creator'])) return redirect()->action('ExamController@index');
		return view('exam.solution', [ 'exam' => Exam::find($id) ]);
	}
}
