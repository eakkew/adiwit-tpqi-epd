<?php namespace Tpqi\Http\Controllers;

use Tpqi\Http\Controllers\Controller;
use Tpqi\Industry;
use Tpqi\IndustryGroupX;
use Tpqi\ProfessionalStandard;
use Tpqi\LevelCompetence;
use Tpqi\Question;
use Tpqi\TemplateHeader;
use Tpqi\Exam;
use Auth;

class MenuController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		// var_dump(Industry::all()->toArray());
		// die();
		// dd(Industry::all());

//---------------------

		// $industry = Industry::first();
		// $industies = Industry::all();

		// foreach ($industies as $index => $industry) {
		// 	var_dump($industry->toArray());
		// 	echo '<hr>';
		// }
		// die();

//---------------------

// $industies = Industry::with('industryGroupX')->get();
// $industies = Industry::with('industryGroupX')->first();
// dd($industies->toArray());


//---------------------

// $industies = Industry::with('industryGroupX')->get();
// $industies = Industry::with('industryGroupX')->where('indGRPID', 'like', 'ICT')->first();
// dd($industies->toArray());


		// session_start();

		// * function.php
		// 	* 
		// 	* function authenticated() {
		// 	*		$_SESSION['authen'];
		// 	*		Table::('tpqi_general.officer');
		// 	* }
		// 	*

		// if (isset($_SESSION['memAuthen']['memID'])) {
		// 	$person = Member::find($_SESSION['memAuthen']['memID'])->person;
		// } else if (isset($_SESSION['memAuthen']['memID'])) {
		// 	$person = Person::find($_SESSION['person']['personID']);
		// } else {
		// 	return redirect('http://xxxxxxx/tpqi/authen/');
		// }

		// dd(Industry::with('industryGroupX')->limit(5)->get()->toArray());
		// dd(IndustryGroupX::with('industry')->limit(5)->get()->toArray());
		// dd(ProfessionalStandard::where('stdID', '=', 6897)->with('uoc')->first()->toArray());
		// dd(LevelCompetence::where('stdID', '=', 5968)->with('ProfessionalStandard', 'uoc')->get()->toArray());
		// dd(Auth::user()->hasRole(['view']));
		return view('menu.index', [
			'question'			=> Question::all()->count(), 
			'templateHeader'	=> TemplateHeader::all()->count(), 
			'exam'				=> Exam::all()->count(),

			// 'question'			=> Question::all(), 
			// 'templateHeader'	=> TemplateHeader::all(), 
			// 'exam'				=> Exam::all(),
		]);
		// return view('menu.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
