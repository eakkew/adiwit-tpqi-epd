<?php

namespace Tpqi\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Response;

use Knp\Snappy\Pdf as Pdf;
use Carbon\Carbon;

use Tpqi\Exam;
use Tpqi\Http\Requests;
use Tpqi\Http\Controllers\Controller;
use Tpqi\Industry;
use Tpqi\IndustryGroupX;
use Tpqi\TemplateHeader;


//use Kendu\Mpdf\Facades\Pdf as PDF;
class PDFController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function report(){
        $countEocs = [];
        $uocsID = [];
        $templateHeaders = TemplateHeader::whereHas('exams', function($query) {})->get();
        foreach ($templateHeaders as $templateHeader) {
            foreach ($templateHeader->templateItems as $templateItem) {
                if(!in_array($templateItem->uoc_id, $uocsID)) $uocsID[]=$templateItem->uoc_id;
                if(!isset($countEocs[$templateItem->eoc_id])) $countEocs[$templateHeader->industry_id][$templateItem->uoc_id][$templateItem->eoc_id] = 0;
                $countEocs[$templateHeader->industry_id][$templateItem->uoc_id][$templateItem->eoc_id] += $templateHeader->exams->count();
            }
        }

        if(sizeof($uocsID) > 0){

            $industries = Industry::whereHas('professionalStandards', function($query) use ($uocsID) {
                $query->whereIn('stdID',$uocsID);
            })->with([ 'professionalStandards' => function ($q) { $q->levelType(); } ])->get();

            $industryGroups = IndustryGroupX::whereHas('professionalStandards', function($query) use ($uocsID) {
                $query->whereIn('stdID',$uocsID);
            })->with([ 'professionalStandards' => function ($q) { $q->levelType(); } ])->get();

            $array = [
                'industries' => $industries,
                'industryGroups' => $industryGroups,
                'countEocs' => $countEocs
            ];
        }else{
            $array = [ 'industries' => [], 'industryGroups' => [], 'countEocs' => $countEocs ];


        }
        $snappy = new Pdf(base_path() . '/vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64');
        $html = view('pdf.report',$array)->render();
        $date = Carbon::now();
        $pathToFile = 'pdf/report/'.'report-'. $date->format('Y-m-d-H-i-s').'.pdf';
        $snappy->generateFromHtml($html,$pathToFile);
        return Response::download($pathToFile);
        // header('Content-Type: application/pdf');
        // header('Content-Disposition: attachment; filename="'.'report-'.$date->format('Y-m-d-H-i-s').'.pdf'.'"');
    }

    public function question($id){
        $exam = Exam::find($id);
        $exam->update([ 
            'used_pdf' => ($exam->used_pdf+1)
        ]);
        $snappy = new Pdf(base_path() . '/vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64');
        $data = "";
        $html = view('pdf.question',[ 'exam' => $exam,'data' => $data ])->render();
        $date = Carbon::now();
        $pathToFile = 'pdf/question/'.'question-'. $date->format('Y-m-d-H-i-s').'.pdf';
        $snappy->generateFromHtml($html,$pathToFile);
        // header('Content-Type: application/pdf');
        // header('Content-Disposition: attachment; filename="'.'question-'.$date->format('Y-m-d-H-i-s').'.pdf'.'"');
        return Response::download($pathToFile);
    }

    public function solution($id){
//        $snappy = new Pdf(base_path() . '/vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64');
//        $data = "";
//        $html = view('pdf.solution',[ 'exam' => Exam::find($id),'data' => $data  ])->render();
//        $date = Carbon::now();
//        $pathToFile = 'pdf/solution/'.'solution-'. $date->format('Y-m-d-H-i-s').'.pdf';
//        $snappy->generateFromHtml($html,$pathToFile);
//        // header('Content-Type: application/pdf');
//        // header('Content-Disposition: attachment; filename="'.'solution-'.$date->format('Y-m-d-H-i-s').'.pdf'.'"');
//        return Response::download($pathToFile);
    }

    public function answer($id){
        $snappy = new Pdf(base_path() . '/vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64');
        $data = "";
        $html = view('pdf.answer',[ 'exam' => Exam::find($id),'data' => $data  ])->render();
        $date = Carbon::now();
        $pathToFile = 'pdf/answer/'.'answer-'. $date->format('Y-m-d-H-i-s').'.pdf' ;
        $snappy->generateFromHtml($html,$pathToFile);
        // header('Content-Type: application/pdf');
        // header('Content-Disposition: attachment; filename="'.'answer-'.$date->format('Y-m-d-H-i-s').'.pdf'.'"');
        return Response::download($pathToFile);
    }
}
