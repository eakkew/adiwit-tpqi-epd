<?php namespace Tpqi\Http\Controllers;

use Tpqi\Http\Requests;
use Tpqi\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Tpqi\Eoc;
use Tpqi\Uoc;
use Tpqi\Industry;
use Tpqi\IndustryGroupX;
use Tpqi\LevelCompetence;
use Tpqi\ProfessionalStandard;
use Tpqi\Question;
use Tpqi\Answer;

class QuestionController extends Controller {

	private $request;

	public function __construct(Request $request) {
		$this->request = $request;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	// public function index(Request $request)
	public function index(Request $request)
	{
		// session selected filter
		$sessionEocID 			= ($request->session()->get('question.filterSelected.eocsID'))? $request->session()->get('question.filterSelected.eocsID') : [];
		$sessionLevelsID 		= ($request->session()->get('question.filterSelected.levelsID'))? $request->session()->get('question.filterSelected.levelsID') : [];
		$sessionIndustriesID 	= ($request->session()->get('question.filterSelected.industriesID'))? $request->session()->get('question.filterSelected.industriesID') : [];
		$sessionFromDate 		= ($request->session()->get('question.filterSelected.fromDate'))? $request->session()->get('question.filterSelected.fromDate') : [];
		$sessionToDate 			= ($request->session()->get('question.filterSelected.toDate'))? $request->session()->get('question.filterSelected.toDate') : [];
		$sessiontype 			= ($request->session()->get('question.filterSelected.type'))? $request->session()->get('question.filterSelected.type') : '';

		// statistic
		// $limitStatistic = 20;
		// $industriesStatistic = [];
		// $industriesID = Question::groupBy('industry_id')->lists('industry_id');
		// $countIndustry = Industry::allQuestionsIndustries($industriesID, $sessionEocID, $sessionFromDate, $sessionToDate);
		// $countIndustryGroupX = IndustryGroupX::allQuestionsIndustries($industriesID, $sessionEocID, $sessionFromDate, $sessionToDate);
		// if($countIndustry != 'none') $industriesStatistic += $countIndustry;
		// if($countIndustryGroupX != 'none') $industriesStatistic += $countIndustryGroupX;
		// if($industriesStatistic){
		// 	ksort($industriesStatistic);
		// 	arsort($industriesStatistic);
		// 	$industriesStatistic = array_slice($industriesStatistic, 0, $limitStatistic);
		// }

		// load from selected Filter
		$professionalsID = ProfessionalStandard::whereIn('indID', $sessionIndustriesID)->levelType()->lists('stdID');
		$qualifications = LevelCompetence::whereIn('stdID', $professionalsID)->groupBy('levelName')->get();
		$uocsID = LevelCompetence::whereIn('levelName', $sessionLevelsID)->groupBy('uocID')->lists('uocID');
		$uocs = UOC::whereIn('stdID', $uocsID)->with('eocs')->get();

		// page
		$perPage = 10;
        $currentPage = ($request->input('page'))? $request->input('page') : 1;
        $skip = ($currentPage)? ($perPage*$currentPage)-$perPage : 0;

        // row data
        if($sessiontype == 1){
        	$questions = Question::levelId($sessionLevelsID, $sessionFromDate, $sessionToDate);
        }elseif($sessiontype == 2){
        	$questions = Question::uocId($sessionEocID, $sessionFromDate, $sessionToDate);
        }else{
        	$questions = Question::eocId($sessionEocID, $sessionFromDate, $sessionToDate);
        }
        $allDataCount = $questions->count();

		return view('question.index', [
			'industries'			=> Industry::with('industryGroupX')->orderBy('indGRPID')->get(),
			'qualifications'		=> $qualifications,
			'uocs'					=> $uocs,
			'questions'				=> $questions->orderBy('id', 'desc')->skip($skip)->take($perPage)->get(),
			'industriesID'			=> $sessionIndustriesID,
			'levelsID'				=> $sessionLevelsID,
			'eocsID'				=> $sessionEocID,
			'fromDate'				=> $sessionFromDate,
			'toDate'				=> $sessionToDate,
			'allDataCount'			=> $allDataCount,
			'page'			        => $currentPage,
			'perPage'			    => $perPage,
			'type'					=> $sessiontype,
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		$industries = Industry::with('industryGroupX')->orderBy('indGRPID')->get();
		$lists = [];
		if($request->all()){
			if($request->input('type') == 3){
				$professionIDs = ProfessionalStandard::where('indID', '=', $request->input('industry_id'))->levelType()->lists('stdID');
				$lists['level'] = LevelCompetence::whereIn('stdID',$professionIDs)->groupBy('levelName')->get();
				$uocID = LevelCompetence::where('levelName', $request->input('level_competence_name'))->lists('uocID');
				// $eocID = LevelCompetence::where('levelName', $request->input('level_competence_name'))->lists('elementID');
				$lists['uoc'] = Uoc::whereIn('stdID', $uocID)->get();
			}elseif($request->input('type') == 2){
				$professionIDs = ProfessionalStandard::where('indID', '=', $request->input('industry_id'))->levelType()->lists('stdID');
				$lists['level'] = LevelCompetence::whereIn('stdID',$professionIDs)->groupBy('levelName')->get();
				$uocID = LevelCompetence::where('levelName', $request->input('level_competence_name'))->lists('uocID');
				// $eocID = LevelCompetence::where('levelName', $request->input('level_competence_name'))->lists('elementID');
				$lists['uoc'] = Uoc::whereIn('stdID', $uocID)->get();
			}else{
				$professionIDs = ProfessionalStandard::where('indID', '=', $request->input('industry_id'))->levelType()->lists('stdID');
				$lists['level'] = LevelCompetence::whereIn('stdID',$professionIDs)->groupBy('levelName')->get();
			}

			return view('question.create')->with([
				'industries' => $industries,
				'getData' => $request->all(),
				'lists' => $lists,
			]);
		}else{
			return view('question.create')->with([
				'industries' => $industries,
			]);
		}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		// dd($request->all());
		$Question = Question::create($request->all());

		$answers = [];
		foreach ($request->input('answerContent') as $index => $answerContent) {
			if($answerContent){
				$pattern = "/font.*?:.+?;/";
				$answers[] = new Answer([
					'content'	=>	preg_replace($pattern, "", $answerContent),
					'is_correct_answer'	=>	(int) ($index == $request->input('is_correct_answer'))? 1 : 0,
				]);
			}
		}
		$Question->answers()->saveMany($answers);
		// return redirect()->action('QuestionController@index');
		$savedQuestion = [
			'question' => $Question,
			'answers' => $Question->answers,
		];
		return response()->json( $Question );
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$Question = Question::where('eoc_id', '=', $id)->with('answer')->orderBy('id')->get();
		return $Question;
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$question = Question::find($id);
		if($question->examQuestions->count()) return redirect()->action('QuestionController@index');
		return view('question.create', [
			'question'	=> $question,
		]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$Question = Question::find($id);

		if ($Question) {
			// Update Question
			$Question->update($request->all());
			$answersID = $request->input('answerID');
			$answers = [];
			foreach ($request->input('answerContent') as $index => $answerContent) {
				// check for new or old answer
				if(isset($answersID[$index])){
					if($answerContent){
						$answer = $Question->answers()->where('id', '=', $answersID[$index])->first();
						// Updata Answer
						$answer->update([
							'content'	=>	$answerContent,
							'is_correct_answer' => ($index == $request->input('is_correct_answer'))? 1 : 0 //use this value to edit correct choice.
						]);
					}else{
						$answer = Answer::find($answersID[$index]);
						if ($answer) $answer->delete();
					}
				}else{
					// Add new answers
					if($answerContent){
						$answers[] = new Answer([
							'content'	=>	$answerContent,
							'is_correct_answer'	=> 0,
						]);
					}
				}
			}
			$Question->answers()->saveMany($answers);
		}
		return redirect()->action('QuestionController@index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$question = Question::find($id);
		if ($question) $question->delete();
		return redirect()->action('QuestionController@index');
	}

	// Load data from filter.
	public function getFromFilter(Request $request)
	{
		dd($request->all());
	}

}
