<?php namespace Tpqi\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Response;

use Tpqi\Http\Requests;
use Tpqi\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Tpqi\Exam;
use Tpqi\Eoc;
use Tpqi\TemplateItem;
use Tpqi\ProfessionalStandard;
use Tpqi\Industry;
use Tpqi\TemplateHeader;
use Tpqi\IndustryGroupX;
use Tpqi\LevelCompetence;
use Tpqi\Question;
use Tpqi\TpqiUser;
use Tpqi\Uoc;
use Tpqi\ExamTransection;
use Tpqi\Organize_staff;
use Auth;
use DB;
use File;

use Knp\Snappy\Pdf as Pdf;
use Carbon\Carbon;


class ReportController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if(Auth::user()->hasRole(['creator'])) return redirect()->action('ExamController@index');
        return redirect()->action('ReportController@reportEPDData');
        // return redirect()->action('ReportController@getUocFrequency');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(Request $request, $id)
	{
		// this if for :: report/reportEPDData
		if($request->input('type') && $request->input('typeData')){
			if($request->input('typeData') == 'question'){
				$returnQuestions = [];
				if($request->input('activeData') != 'false'){
					$questions = Question::whereIn('id', $request->input('id'))->get();
				}else{
					$questions = Question::whereIn('id', $request->input('id'))->withTrashed()->get();
				}
				foreach($questions as $question){
					$returnQuestions[] = [
						// 'name' => str_limit($question->content, 40),
						'name' => $question->content,
						'status' => ($question->deleted_at)? false : true,
						'used' => $question->examQuestions->count(),
						'id' => $question->id,
					];
				}
				return $returnQuestions;
			}elseif($request->input('typeData') == 'template'){
				$returnTemplates = [];
				if($request->input('activeData') != 'false'){
					$templates = TemplateHeader::whereIn('id', $request->input('id'))->get();
				}else{
					$templates = TemplateHeader::whereIn('id', $request->input('id'))->withTrashed()->get();
				}
				foreach($templates as $template){
					$returnTemplates[] = [
						'name' => $template->name,
						'status' => ($template->deleted_at)? false : true,
						'used' => $template->exams()->count(),
						'id' => $template->id,
					];
				}
				return $returnTemplates;
			}else{
				$returnExams = [];
				if($request->input('activeData') != 'false'){
					$exams = Exam::whereIn('id', $request->input('id'))->get();
				}else{
					$exams = Exam::whereIn('id', $request->input('id'))->withTrashed()->get();
				}
				foreach($exams as $exam){
					$returnExams[] = [
						'name' => $exam->templateHeader->name . ' พิมพ์ครั้งที่ ' . $exam->set . ' ชุดที่ ' . $exam->subset,
						'status' => ($exam->deleted_at)? false : true,
						'used' => $exam->used_pdf,
						'id' => $exam->id,
					];
				}
				return $returnExams;
			}
		}

		if(Auth::user()->hasRole(['creator'])) return redirect()->action('ExamController@index');
		switch ($request->input('type')) {
		    case 'getFooter':
		        return $this->reportUocFrequencyPDF();
		        break;
		    case 'questionPDF':
		        $userID = TpqiUser::where('personID', $request->session()->get('tpqi.person.personID'))->lists('personID')->first();
		        if(isset($_SESSION['permission']) && $_SESSION['permission']['perID'] == 9) {
		        	$org_staff = Organize_staff::where('FK_personID', $_SESSION['person']['personID'])->first();
		        	$exam = Exam::find($id);
		        	if($exam && $org_staff) {
		        		if($org_staff->FK_orgID != $exam->cb_id) return redirect('/');// Access Deny
		        	}else {
		        		return redirect('/');// Access Deny
		        	}
		        }
				if($userID){
					$transection = ExamTransection::create(array(
						'type' => 1,
						'exam_id' => $id,
						'user_id' => $userID,
					));
					return $this->questionPDF($id, $transection);
				}else{
					return redirect()->action('ExamController@index');
				}
		        break;
		    case 'answerPDF':
		        $userID = TpqiUser::where('personID', $request->session()->get('tpqi.person.personID'))->lists('personID')->first();
		        if(isset($_SESSION['permission']) && $_SESSION['permission']['perID'] == 9) {
		        	$org_staff = Organize_staff::where('FK_personID', $_SESSION['person']['personID'])->first();
		        	$exam = Exam::find($id);
		        	if($exam && $org_staff) {
		        		if($org_staff->FK_orgID != $exam->cb_id) return redirect('/');// Access Deny
		        	}else {
		        		return redirect('/');// Access Deny
		        	}
		        }
				if($userID){
					$transection = ExamTransection::create(array(
						'type' => 2,
						'exam_id' => $id,
						'user_id' => $userID,
					));
					return $this->answerPDF($id, $transection);
				}else{
					return redirect()->action('ExamController@index');
				}
		        break;
		    case 'solutionPDF':
		        $userID = TpqiUser::where('personID', $request->session()->get('tpqi.person.personID'))->lists('personID')->first();
		        if(isset($_SESSION['permission']) && $_SESSION['permission']['perID'] == 9) {
		        	$org_staff = Organize_staff::where('FK_personID', $_SESSION['person']['personID'])->first();
		        	$exam = Exam::find($id);
		        	if($exam && $org_staff) {
		        		if($org_staff->FK_orgID != $exam->cb_id) return redirect('/');// Access Deny
		        	}else {
		        		return redirect('/');// Access Deny
		        	}
		        }
				if($userID){
					$transection = ExamTransection::create(array(
						'type' => 3,
						'exam_id' => $id,
						'user_id' => $userID,
					));
					return $this->solutionPDF($id, $transection);
				}else{
					return redirect()->action('ExamController@index');
				}
		        break;
		    case 'reportDiffPDF':
		    	return $this->reportDiffPDF($id, $request);
		        break;
		    case 'reportNumberExamByLevel':
		    	// $master
		    	break;
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function reportDiffPDF(Request $request)
	{
		$masterExam = Exam::find($request->input('id'));
		$from = ($request->input('from'))? $request->input('from.year')-543 . '-' . $request->input('from.month') . '-' . $request->input('from.day') : '';
		$to = ($request->input('to'))? $request->input('to.year')-543 . '-' . $request->input('to.month') . '-' . $request->input('to.day') : '';
		$exams = Exam::where('type', $masterExam->type)
					->where('id', '!=', $masterExam->id)
					->where('subset', 1)
					->where('template_id', $masterExam->template_id)
					->where('level_competence_name',$masterExam->level_competence_name);
		if($from && $to) $exams->whereBetween('created_at',[$from, $to]);
		$exams = $exams->withTrashed()->groupBy('set')->get();
		foreach($exams as $exam)
		{
			$diff = $this->compareDiffQuestion($masterExam, $exam);
			$exam['diffPercent'] = $diff['percent'];
		}
		$exams->sortByDesc('diffPercent');
		// for ($i=0; $i < 100; $i++) { 
		// 	$exams[] = $exams[0];
		// }

		// gentoPDF
		$from = ($from)? explode('-', $from) : '';
		$to = ($to)? explode('-', $to) : '';
		$reportName = 'รายงานแสดงการเปรียบเทียบความต่างกันของชุดข้อสอบ';
        $date = Carbon::now();
        $monthName = array("1"=>"มกราคม","2"=>"กุมภาพันธ์","3"=>"มีนาคม","4"=>"เมษายน","5"=>"พฤษภาคม","6"=>"มิถุนายน","7"=>"กรกฎาคม","8"=>"สิงหาคม","9"=>"กันยายน","10"=>"ตุลาคม","11"=>"พฤศจิกายน","12"=>"ธันวาคม");
	    $pathToFile = 'pdf/report/'.'rptdiffexam'. $date->format('YmdHis').'.pdf';
	    $cover = view('pdf.report.cover',[ 'masterExam' => $masterExam, 'exams' => $exams, 'from' => $from, 'to' => $to, 'reportName' => $reportName, 'date' => $date, 'monthName' => $monthName])->render();
	    $header = view('pdf.report.header',[ 'masterExam' => $masterExam, 'exams' => $exams, 'from' => $from, 'to' => $to, 'reportName' => $reportName, 'date' => $date, 'monthName' => $monthName])->render();
	    $footer = view('pdf.report.footer',[ 'masterExam' => $masterExam, 'exams' => $exams, 'from' => $from, 'to' => $to, 'reportName' => $reportName, 'date' => $date, 'monthName' => $monthName])->render();
	    $body = view('pdf.report.reportDiff',[ 'masterExam' => $masterExam, 'exams' => $exams, 'from' => $from, 'to' => $to, 'reportName' => $reportName, 'date' => $date, 'monthName' => $monthName])->render();

        // $snappy = new Pdf(base_path() . '/vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64');
        $snappy = new Pdf('/usr/local/bin/wkhtmltopdf');
        $snappy->setOption('cover', $cover);
        $snappy->setOption('header-html', $header);
        $snappy->setOption('header-spacing', 5);
        $snappy->setOption('footer-html', $footer);
        $snappy->setOption('footer-spacing', 5);
        $snappy->generateFromHtml($body,$pathToFile,[],$overwrite = true);

        return Response::make(File::get($pathToFile), 200)->header('Content-Type', 'application/pdf');
        // return Response::download($pathToFile);
	}

	public function reportFrequencyQuestionPDF(Request $request)
	{
		$type = ($request->input('type') == 1)? 'level' : (($request->input('type') == 2)? 'uoc' : (($request->input('type') == 3)? 'eoc' : ''));
		$from = ($request->input('from'))? $request->input('from.year')-543 . '-' . $request->input('from.month') . '-' . $request->input('from.day') : '';
		$to = ($request->input('to'))? $request->input('to.year')-543 . '-' . $request->input('to.month') . '-' . $request->input('to.day') : '';
		$questions = Question::where('type', $type);
		if($from && $to) $questions->whereBetween('fact_questions.created_at',[$from, $to]);
		if($request->input('qualification')) $questions->whereIn('level_competence_name',$request->input('qualification'));
		if($request->input('eocs')) $questions->whereIn('eoc_id',$request->input('eocs'));
		$questions = $questions->withTrashed()
			->leftjoin('fact_exam_questions', 'fact_exam_questions.question_id', '=', 'fact_questions.id')
			->groupBy('fact_questions.id')
			->select('fact_questions.*', DB::raw('count(fact_exam_questions.id) as cnt'))
			->orderBy('cnt', 'desc')
			->get();
		
		// gentoPDF
		$from = ($from)? explode('-', $from) : '';
		$to = ($to)? explode('-', $to) : '';
		$reportName = 'รายงานแสดงการเปรียบเทียบความต่างกันของชุดข้อสอบ';

        $date = Carbon::now();
        $monthName = array("1"=>"มกราคม","2"=>"กุมภาพันธ์","3"=>"มีนาคม","4"=>"เมษายน","5"=>"พฤษภาคม","6"=>"มิถุนายน","7"=>"กรกฎาคม","8"=>"สิงหาคม","9"=>"กันยายน","10"=>"ตุลาคม","11"=>"พฤศจิกายน","12"=>"ธันวาคม");
	    $pathToFile = 'pdf/report/'.'rptfqcquestion'. $date->format('YmdHis').'.pdf';
	    $cover = view('pdf.report.cover',[ 'from' => $from, 'to' => $to, 'reportName' => trans('menu.reportFrequencyQuestion'), 'date' => $date, 'monthName' => $monthName])->render();
	    $header = view('pdf.report.header',[ 'from' => $from, 'to' => $to, 'reportName' => trans('menu.reportFrequencyQuestion'), 'date' => $date, 'monthName' => $monthName])->render();
	    $footer = view('pdf.report.footer',[ 'from' => $from, 'to' => $to, 'reportName' => trans('menu.reportFrequencyQuestion'), 'date' => $date, 'monthName' => $monthName])->render();
	    $body = view('pdf.report.reportFrequencyQuestion',[ 'questions' => $questions, 'from' => $from, 'to' => $to, 'reportName' => trans('menu.reportFrequencyQuestion'), 'date' => $date, 'monthName' => $monthName])->render();

        // $snappy = new Pdf(base_path() . '/vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64');
        $snappy = new Pdf('/usr/local/bin/wkhtmltopdf');
	    $snappy->setOption('cover', $cover);
	    $snappy->setOption('header-html', $header);
	    $snappy->setOption('footer-html', $footer);
	    $snappy->setOption('header-spacing', 5);
	    $snappy->setOption('footer-spacing', 5);
     	$snappy->generateFromHtml($body,$pathToFile,[],$overwrite = true);
        return Response::make(File::get($pathToFile), 200)->header('Content-Type', 'application/pdf');
        // return Response::download($pathToFile);
	}

	public function reportNumberExamByLevelPDF(Request $request)
	{
		$type = ($request->input('type') == 1)? 'level' : (($request->input('type') == 2)? 'uoc' : (($request->input('type') == 3)? 'eoc' : ''));
		$from = ($request->input('from'))? $request->input('from.year')-543 . '-' . $request->input('from.month') . '-' . $request->input('from.day') : '';
		$to = ($request->input('to'))? $request->input('to.year')-543 . '-' . $request->input('to.month') . '-' . $request->input('to.day') : '';

		// query
		$examsTable = [];
		$exams = Exam::where('type', $type);
		if($from && $to) $exams->whereBetween('created_at',[$from, $to]);
		if($request->input('qualification')) $exams->whereIn('level_competence_name',$request->input('qualification'));
		$examsTable = $exams->groupBy('level_competence_name')
			->select(DB::raw('*, count(*) as count, MAX(created_at) as lastest'))
			->orderBy('count', 'desc')
			->get();
		// for ($i=0; $i < 100; $i++) { 
		// 	$examsTable[] = $examsTable[0];
		// }

		// gentoPDF
		$from = ($from)? explode('-', $from) : '';
		$to = ($to)? explode('-', $to) : '';
        $date = Carbon::now();
        $monthName = array("1"=>"มกราคม","2"=>"กุมภาพันธ์","3"=>"มีนาคม","4"=>"เมษายน","5"=>"พฤษภาคม","6"=>"มิถุนายน","7"=>"กรกฎาคม","8"=>"สิงหาคม","9"=>"กันยายน","10"=>"ตุลาคม","11"=>"พฤศจิกายน","12"=>"ธันวาคม");
	    $pathToFile = 'pdf/report/'.'rptnumberexamlv.pdf';
	    $cover = view('pdf.report.cover',[ 'from' => $from, 'to' => $to, 'reportName' => trans('menu.reportNumberExamByLevel'), 'date' => $date, 'monthName' => $monthName])->render();
	    $header = view('pdf.report.header',[ 'from' => $from, 'to' => $to, 'reportName' => trans('menu.reportNumberExamByLevel'), 'date' => $date, 'monthName' => $monthName])->render();
	    $body = view('pdf.report.reportNumberExamByLevel',[ 'examsTables' => $examsTable, 'from' => $from, 'to' => $to, 'reportName' => trans('menu.reportNumberExamByLevel'), 'date' => $date, 'monthName' => $monthName])->render();
	    $footer = view('pdf.report.footer',[ 'from' => $from, 'to' => $to, 'reportName' => trans('menu.reportNumberExamByLevel'), 'date' => $date, 'monthName' => $monthName])->render();

        // $snappy = new Pdf(base_path() . '/vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64');
        $snappy = new Pdf('/usr/local/bin/wkhtmltopdf');
        $snappy->setOption('cover', $cover);
        $snappy->setOption('header-html', $header);
        $snappy->setOption('header-spacing', 5);
        $snappy->setOption('footer-html', $footer);
        $snappy->setOption('footer-spacing', 5);
        $snappy->generateFromHtml($body,$pathToFile,[],$overwrite = true);
        return Response::make(File::get($pathToFile), 200)->header('Content-Type', 'application/pdf');
        // return Response::download($pathToFile);	
	}

	public function reportNumberExamByCBDPDF(Request $request)
	{
		$type = ($request->input('type') == 1)? 'level' : (($request->input('type') == 2)? 'uoc' : (($request->input('type') == 3)? 'eoc' : ''));
		$from = ($request->input('from'))? $request->input('from.year')-543 . '-' . $request->input('from.month') . '-' . $request->input('from.day') : '';
		$to = ($request->input('to'))? $request->input('to.year')-543 . '-' . $request->input('to.month') . '-' . $request->input('to.day') : '';

		// query
		$examsTable = [];
		$exams = Exam::where('type', $type);
		if($from && $to) $exams->whereBetween('created_at',[$from, $to]);
		if($request->input('qualification')) $exams->whereIn('level_competence_name',$request->input('qualification'));
		$examsTable = $exams->groupBy('cb_id')
			->select(DB::raw('*, count(*) as count, MAX(created_at) as lastest'))
			->orderBy('count', 'desc')
			->get();
		// for ($i=0; $i < 100; $i++) { 
		// 	$examsTable[] = $examsTable[0];
		// }

		// gentoPDF
		$from = ($from)? explode('-', $from) : '';
		$to = ($to)? explode('-', $to) : '';
        $date = Carbon::now();
        $monthName = array("1"=>"มกราคม","2"=>"กุมภาพันธ์","3"=>"มีนาคม","4"=>"เมษายน","5"=>"พฤษภาคม","6"=>"มิถุนายน","7"=>"กรกฎาคม","8"=>"สิงหาคม","9"=>"กันยายน","10"=>"ตุลาคม","11"=>"พฤศจิกายน","12"=>"ธันวาคม");
	    $pathToFile = 'pdf/report/'.'rptnumberexamcbd'. $date->format('YmdHis').'.pdf';
	    $cover = view('pdf.report.cover',[ 'from' => $from, 'to' => $to, 'reportName' => trans('menu.reportNumberExamByCBD'), 'date' => $date, 'monthName' => $monthName])->render();
	    $header = view('pdf.report.header',[ 'from' => $from, 'to' => $to, 'reportName' => trans('menu.reportNumberExamByCBD'), 'date' => $date, 'monthName' => $monthName])->render();
	    $body = view('pdf.report.reportNumberExamByCBD',[ 'examsTables' => $examsTable, 'from' => $from, 'to' => $to, 'reportName' => trans('menu.reportNumberExamByCBD'), 'date' => $date, 'monthName' => $monthName])->render();
	    $footer = view('pdf.report.footer',[ 'from' => $from, 'to' => $to, 'reportName' => trans('menu.reportNumberExamByCBD'), 'date' => $date, 'monthName' => $monthName])->render();

        // $snappy = new Pdf(base_path() . '/vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64');
        $snappy = new Pdf('/usr/local/bin/wkhtmltopdf');
        $snappy->setOption('cover', $cover);
        $snappy->setOption('header-html', $header);
        $snappy->setOption('header-spacing', 5);
        $snappy->setOption('footer-html', $footer);
        $snappy->setOption('footer-spacing', 5);
        $snappy->generateFromHtml($body,$pathToFile,[],$overwrite = true);
        return Response::make(File::get($pathToFile), 200)->header('Content-Type', 'application/pdf');
        // return Response::download($pathToFile);	
	}

	public function reportEPDDataPDF(Request $request)
	{
		// dd($request->all());
		$type = ($request->input('type') == 1)? 'level' : (($request->input('type') == 2)? 'uoc' : (($request->input('type') == 3)? 'eoc' : ''));
		$typeData = $request->input('typeData');
		$from = ($request->input('from'))? $request->input('from.year')-543 . '-' . $request->input('from.month') . '-' . $request->input('from.day') : '';
		$to = ($request->input('to'))? $request->input('to.year')-543 . '-' . $request->input('to.month') . '-' . $request->input('to.day') : '';
		$selectedLevelIds = $request->input('qualification');

		// query Data
		$graphsData = [];
		$drillDownsData = [];
		$listGraphsCode = [];
		$listGraphsName = [];
		$listDrillDownsName = [];
		$listGraphsID = [];
		if($typeData == 'question'){
			if($type == 'level'){
				$i = 0;
	        	foreach($selectedLevelIds as $levelID){
	        		$questions = Question::where('type', 'level')->where('level_competence_name', $levelID);
	        		if($from && $to) $questions->whereBetween('created_at',[$from, $to]);
	        		$cntActive = $questions->count();
	        		$activeID = $questions->lists('id');
	        		$cntDeleted = $questions->onlyTrashed()->count();
	        		$deletedID = $questions->lists('id');
	        		if($cntActive || $cntDeleted){
	        			$graphsData[$i]['active'] = $cntActive;
	        			$graphsData[$i]['deleted'] = $cntDeleted;
	        			$listGraphsCode[$i] = $levelID;
	        			$listGraphsName[$i] = $levelID;
	        			$listGraphsID[$i]['active'] = $activeID;
	        			$listGraphsID[$i]['deleted'] = $deletedID;
	        			$i++;
	        		}
	        	}
	        }elseif($type == 'uoc'){
	        	$uocID = LevelCompetence::whereIn('levelName', $selectedLevelIds)->lists('uocID');
	        	foreach($uocID as $id){
	        		$questions = Question::where('type', 'uoc')->where('eoc_id', $id);
	        		if($from && $to) $questions->whereBetween('created_at',[$from, $to]);
	        		$cntActive = $questions->count();
	        		$activeID = $questions->lists('id');
	        		$cntDeleted = $questions->onlyTrashed()->count();
	        		$deletedID = $questions->lists('id');
	        		if($cntActive || $cntDeleted){
	        			$uoc = Uoc::where('stdID', $id)->first();
	        			$graphsData[$id]['active'] = $cntActive;
	        			$graphsData[$id]['deleted'] = $cntDeleted;
	        			$listGraphsCode[$id] = $uoc->uocTpqi->uocCode;
	        			$listGraphsName[$id] = $uoc->stdName;
	        			$listGraphsID[$id]['active'] = $activeID;
	        			$listGraphsID[$id]['deleted'] = $deletedID;
	        		}
	        	}
	        }else{
	        	$uocID = LevelCompetence::whereIn('levelName', $selectedLevelIds)->lists('uocID');
	        	$uocs = Uoc::whereIn('stdID', $uocID)->get();
	        	foreach($uocs as $uoc){
	        		$listeocsID = Eoc::where('stdType', 'E')->where('parent', $uoc->stdID)->lists('stdID');
	        		$questions = Question::where('type', 'eoc')->whereIn('eoc_id', $listeocsID);
	        		if($from && $to) $questions->whereBetween('created_at',[$from, $to]);
	        		$cntActive = $questions->count();
	        		$activeID = $questions->lists('id');
	        		$cntDeleted = $questions->onlyTrashed()->count();
	        		$deletedID = $questions->lists('id');
	        		if($cntActive || $cntDeleted){
	        			$graphsData[$uoc->stdID]['active'] = $cntActive;
			        	$graphsData[$uoc->stdID]['deleted'] = $cntDeleted;
			        	$listGraphsCode[$uoc->stdID] = $uoc->uocTpqi->uocCode;
			        	$listGraphsName[$uoc->stdID] = $uoc->stdName;
			        	$listGraphsID[$uoc->stdID]['active'] = $activeID;
	        			$listGraphsID[$uoc->stdID]['deleted'] = $deletedID;
		        		foreach($uoc->eocs as $eoc){
		        			$cntDrilldownActive = Question::where('type', 'eoc')->where('eoc_id', $eoc->stdID)->count();
		        			$cntDrilldownDeleted = Question::where('type', 'eoc')->where('eoc_id', $eoc->stdID)->onlyTrashed()->count();
		        			if($cntDrilldownActive || $cntDrilldownDeleted){
		        				$drillDownsData[$uoc->stdID]['active'][$eoc->stdID] = $cntDrilldownActive;
		        				$drillDownsData[$uoc->stdID]['deleted'][$eoc->stdID] = $cntDrilldownDeleted;
		        				$listDrillDownsName[$eoc->stdID] = $eoc->stdName;
		        			}
		        		}
	        		}
	        	}
	        }
		}elseif($typeData == 'template'){
			$i = 0;
			$byType = $type;
			// $byType = ($type == 3)? 'eoc' : (($type == 2)? 'uoc' : 'level');
			foreach($selectedLevelIds as $levelID){
        		$templateHeaders = templateHeader::where('type', $byType)->where('level_competence_name', $levelID);
        		if($from && $to) $templateHeaders->whereBetween('created_at',[$from, $to]);
        		$cntActive = $templateHeaders->count();
				$activeID = $templateHeaders->lists('id');
				$cntDeleted = $templateHeaders->onlyTrashed()->count();
        		$deletedID = $templateHeaders->lists('id');
				if($cntActive || $cntDeleted){
					$graphsData[$levelID]['active'] = $cntActive;
        			$graphsData[$levelID]['deleted'] = $cntDeleted;
        			$listGraphsID[$levelID]['active'] = $activeID;
	        		$listGraphsID[$levelID]['deleted'] = $deletedID;
        			$listGraphsCode[$levelID] = $levelID;
        			$listGraphsName[$levelID] = $levelID;
        			$i++;
				}
			}
		}else{
			// $i = 0;
			$byType = $type;
			// $byType = ($type == 3)? 'eoc' : (($type == 2)? 'uoc' : 'level');
			foreach($selectedLevelIds as $levelID){
				$exams = Exam::where('type', $byType)->where('level_competence_name', $levelID);
        		if($from && $to) $exams->whereBetween('created_at',[$from, $to]);
        		$cntActive = $exams->count();
				$activeID = $exams->lists('id');
				$cntDeleted = $exams->onlyTrashed()->count();
        		$deletedID = $exams->lists('id');
				if($cntActive || $cntDeleted){
					$graphsData[$levelID]['active'] = $cntActive;
        			$graphsData[$levelID]['deleted'] = $cntDeleted;
        			$listGraphsID[$levelID]['active'] = $activeID;
	        		$listGraphsID[$levelID]['deleted'] = $deletedID;
        			$listGraphsCode[$levelID] = $levelID;
        			$listGraphsName[$levelID] = $levelID;
        			// $i++;
				}
			}
		}
		// dd($graphsData);
		// dd($drillDownsData);
		// dd($listGraphsName);
		// dd($listDrillDownsName);

		// gentoPDF
		$graphName = ($typeData == 'exam')? 'รายงานสถานะชุดข้อสอบในคลังข้อสอบ' : ( ($typeData == 'template')? 'รายงานสถานะแม่แบบในคลังข้อสอบ' : 'รายงานสถานะคำถามในคลังข้อสอบ');
		$graphThing = ($typeData == 'exam')? 'ชุดข้อสอบ' : ( ($typeData == 'template')? 'แม่แบบ' : 'คำถาม');
		$from = ($from)? explode('-', $from) : '';
		$to = ($to)? explode('-', $to) : '';
        $date = Carbon::now();
        $monthName = array("1"=>"มกราคม","2"=>"กุมภาพันธ์","3"=>"มีนาคม","4"=>"เมษายน","5"=>"พฤษภาคม","6"=>"มิถุนายน","7"=>"กรกฎาคม","8"=>"สิงหาคม","9"=>"กันยายน","10"=>"ตุลาคม","11"=>"พฤศจิกายน","12"=>"ธันวาคม");
	    $pathToFile = 'pdf/report/'.'rptepddata.pdf';
	    $cover = view('pdf.report.cover',[ 'from' => $from, 'to' => $to, 'landscape' => true, 'reportName' => trans('menu.reportEPDData'), 'date' => $date, 'monthName' => $monthName])->render();
	    $header = view('pdf.report.header',[ 'from' => $from, 'to' => $to, 'reportName' => trans('menu.reportEPDData'), 'date' => $date, 'monthName' => $monthName])->render();
	    $footer = view('pdf.report.footer',[ 'from' => $from, 'to' => $to, 'reportName' => trans('menu.reportEPDData'), 'date' => $date, 'monthName' => $monthName])->render();
		$body = view('pdf.report.reportEPDDataPDF', [ 'typeData' => $typeData, 'type' => $type, 'listGraphsCode' => $listGraphsCode, 'listGraphsName' => $listGraphsName, 'listGraphsID' => $listGraphsID, 'graphsData' => $graphsData, 'graphName' => $graphName, 'graphThing' => $graphThing, 'from' => $from, 'to' => $to, 'date' => $date, 'monthName' => $monthName ])->render();

        // $snappy = new Pdf(base_path() . '/vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64');
        $snappy = new Pdf('/usr/local/bin/wkhtmltopdf');
        $snappy->setOption('cover', $cover);
        $snappy->setOption('header-html', $header);
        $snappy->setOption('header-spacing', 5);
        $snappy->setOption('footer-html', $footer);
        $snappy->setOption('footer-spacing', 5);
        $snappy->setOption('orientation', 'Landscape');
        $snappy->generateFromHtml($body,$pathToFile,[],$overwrite = true);
        return Response::make(File::get($pathToFile), 200)->header('Content-Type', 'application/pdf');
        // return Response::download($pathToFile);	
	}

	public function compareDiffQuestion($masterExam, $compareExam)
	{
		$masterQID = $masterExam->examQuestions->lists('question_id')->toArray();
		$compareQID = $compareExam->examQuestions()->lists('question_id')->toArray();
		$diff['diffQuestions'] = array_diff($masterQID,$compareQID);
		$diff['sameQuestions'] = array_intersect($masterQID,$compareQID);
		// $diff['diffQuestions'] = Question::whereIn('id', $diff['questions'])->withTrashed()->get();
		// $diff['sameQuestions'] = Question::whereIn('id', $diff['questions'])->withTrashed()->get();
		$diff['percent'] = (count($diff['diffQuestions']) * 100 ) / count($masterQID);
		return $diff;
	}

	public function reportDiffExam(Request $request)
	{
		// selected filter
		// dd($request->session()->all());
		$sessionLevelsID 		= ($request->session()->get('reportDiffExam.filterSelected.levelsID'))? $request->session()->get('reportDiffExam.filterSelected.levelsID') : '';
		$sessionIndustriesID 	= ($request->session()->get('reportDiffExam.filterSelected.industriesID'))? $request->session()->get('reportDiffExam.filterSelected.industriesID') : [];
		$sessionFromDate 		= ($request->session()->get('reportDiffExam.filterSelected.fromDate'))? $request->session()->get('reportDiffExam.filterSelected.fromDate') : [];
		$sessionToDate 			= ($request->session()->get('reportDiffExam.filterSelected.toDate'))? $request->session()->get('reportDiffExam.filterSelected.toDate') : [];
		$sessiontype 			= ($request->session()->get('reportDiffExam.filterSelected.typeReport'))? $request->session()->get('reportDiffExam.filterSelected.typeReport') : '';
		$from = ($sessionFromDate && $sessionToDate)? ($sessionFromDate['year']-543).'-'.$sessionFromDate['month'].'-'.$sessionFromDate['day'] : '';
	    $to = ($sessionFromDate && $sessionToDate)? ($sessionToDate['year']-543).'-'.$sessionToDate['month'].'-'.$sessionToDate['day'] : '';
	    $sessionMasterExam	= ($request->session()->get('reportDiffExam.filterSelected.masterExam'))? $request->session()->get('reportDiffExam.filterSelected.masterExam') : '';

		// load from selected Filter
		$professionalsID = ProfessionalStandard::whereIn('indID', $sessionIndustriesID)->levelType()->lists('stdID');
		$qualifications = LevelCompetence::whereIn('stdID', $professionalsID)->groupBy('levelName')->get();
		$masterExam = Exam::find($sessionMasterExam);

		// query
		$type = ($sessiontype == 1)? 'level' : (($sessiontype == 2)? 'uoc' : (($sessiontype == 3)? 'eoc' : ''));
		// $exams = Exam::where('type', $type)->where('id', '!=', $sessionMasterExam);
		$exams = Exam::where('type', $type)->where('id', '!=', $sessionMasterExam)->where('subset', 1);
		if($masterExam) $exams = $exams->where('template_id', $masterExam->template_id);
		if($from && $to) $exams->whereBetween('created_at',[$from, $to]);
		if($sessionLevelsID) $exams->where('level_competence_name',$sessionLevelsID);
		// $exams = $exams->withTrashed()->get();
		$exams = $exams->withTrashed()->groupBy('set')->get();
		foreach($exams as $exam)
		{
			$diff = $this->compareDiffQuestion($masterExam, $exam);
			$exam['diffPercent'] = $diff['percent'];
			$exam['diffQuestions'] = $diff['diffQuestions'];
			$exam['sameQuestions'] = $diff['sameQuestions'];
		}
		$exams->sortByDesc('diffPercent');

		// calculate amont of question for reccomend in view
		$questionCount = 0;
		if(count($exams)) {
	    	if($exams->first()->type == 'eoc') {
				foreach($exams->first()->templateItems as $templateItem) {
					$questionCount += $templateItem->question->count();
				}
			} elseif ($exams->first()->type == 'uoc') {
				foreach($exams->first()->templateItems as $templateItem) {
					$questionCount += $templateItem->questionByUoc->count();
				}
			} else {
				$questionCount += $exams->first()->templateHeader->questionsByLevel->count();
			}
		}

		return view('report.reportDiffExam', [
			'exams'					=> $exams,
			'industries'			=> Industry::with('industryGroupX')->orderBy('indGRPID')->get(), 
			'qualifications'		=> $qualifications,
			'type'					=> $sessiontype,
			'industriesID'			=> $sessionIndustriesID,
			'levelsID'				=> $sessionLevelsID,
			'masterExam'			=> $masterExam,
			'fromDate'				=> $sessionFromDate,
			'toDate'				=> $sessionToDate,
			'questionCount'			=> $questionCount,
		]);
	}

	public function reportFrequencyQuestion(Request $request)
	{
		// selected filter
		// dd($request->session()->all());
		$sessionEocID 			= ($request->session()->get('reportFrequencyQuestion.filterSelected.eocsID'))? $request->session()->get('reportFrequencyQuestion.filterSelected.eocsID') : [];
		$sessionLevelsID 		= ($request->session()->get('reportFrequencyQuestion.filterSelected.levelsID'))? $request->session()->get('reportFrequencyQuestion.filterSelected.levelsID') : [];
		$sessionIndustriesID 	= ($request->session()->get('reportFrequencyQuestion.filterSelected.industriesID'))? $request->session()->get('reportFrequencyQuestion.filterSelected.industriesID') : [];
		$sessionFromDate 		= ($request->session()->get('reportFrequencyQuestion.filterSelected.fromDate'))? $request->session()->get('reportFrequencyQuestion.filterSelected.fromDate') : [];
		$sessionToDate 			= ($request->session()->get('reportFrequencyQuestion.filterSelected.toDate'))? $request->session()->get('reportFrequencyQuestion.filterSelected.toDate') : [];
		$sessiontype 			= ($request->session()->get('reportFrequencyQuestion.filterSelected.type'))? $request->session()->get('reportFrequencyQuestion.filterSelected.type') : '';
		$sessiontypeData 		= ($request->session()->get('reportFrequencyQuestion.filterSelected.typeData'))? $request->session()->get('reportFrequencyQuestion.filterSelected.typeData') : '';
		$sessionorderType 		= ($request->session()->get('reportFrequencyQuestion.filterSelected.orderType'))? $request->session()->get('reportFrequencyQuestion.filterSelected.orderType') : '';
		$from = ($sessionFromDate && $sessionToDate)? ($sessionFromDate['year']-543).'-'.$sessionFromDate['month'].'-'.$sessionFromDate['day'] : '';
	    $to = ($sessionFromDate && $sessionToDate)? ($sessionToDate['year']-543).'-'.$sessionToDate['month'].'-'.$sessionToDate['day'] : '';

		// load from selected Filter
		$professionalsID = ProfessionalStandard::whereIn('indID', $sessionIndustriesID)->levelType()->lists('stdID');
		$qualifications = LevelCompetence::whereIn('stdID', $professionalsID)->groupBy('levelName')->get();
		$uocsID = LevelCompetence::whereIn('levelName', $sessionLevelsID)->groupBy('uocID')->lists('uocID');
		$uocs = UOC::whereIn('stdID', $uocsID)->with('eocs')->get();

		// query
		$type = ($sessiontype == 1)? 'level' : (($sessiontype == 2)? 'uoc' : (($sessiontype == 3)? 'eoc' : ''));
		$questions = Question::where('type', $type);
		if($from && $to) $questions->whereBetween('fact_questions.created_at',[$from, $to]);
		if($sessionLevelsID) $questions->whereIn('level_competence_name',$sessionLevelsID);
		if($sessionEocID) $questions->whereIn('eoc_id',$sessionEocID);
		// $questions = $questions->withTrashed()->get()->sortByDesc(function($q){
		// 	return $q->examQuestionsTrash->count();
		// });;
		if($sessionorderType != 'none'){
			$questions = $questions->withTrashed()
				->leftjoin('fact_exam_questions', 'fact_exam_questions.question_id', '=', 'fact_questions.id')
				->groupBy('fact_questions.id')
				->select('fact_questions.*', DB::raw('count(fact_exam_questions.id) as cnt'))
				->orderBy('cnt', $sessionorderType)
				->paginate(10);
		}else{
			$questionsUsed = Question::where('type', $type);
			if($from && $to) $questions->whereBetween('fact_questions.created_at',[$from, $to]);
			if($sessionLevelsID) $questions->whereIn('level_competence_name',$sessionLevelsID);
			if($sessionEocID) $questions->whereIn('eoc_id',$sessionEocID);
			$questionsUsed = $questionsUsed->withTrashed()
				->join('fact_exam_questions', 'fact_exam_questions.question_id', '=', 'fact_questions.id')
				->groupBy('fact_questions.id')
				->select('fact_questions.*', DB::raw('count(fact_exam_questions.id) as cnt'))
				->lists('id');

			$questions = $questions->withTrashed()
				->whereNotIn('fact_questions.id', $questionsUsed)
				->paginate(10);	
		}

		return view('report.reportFrequencyQuestion', [
			'questions'				=> $questions,
			'industries'			=> Industry::with('industryGroupX')->orderBy('indGRPID')->get(), 
			'qualifications'		=> $qualifications,
			'uocs'					=> $uocs,
			'eocsID'				=> $sessionEocID,
			'type'					=> $sessiontype,
			'typeData'				=> $sessiontypeData,
			'industriesID'			=> $sessionIndustriesID,
			'levelsID'				=> $sessionLevelsID,
			'fromDate'				=> $sessionFromDate,
			'toDate'				=> $sessionToDate,
			'orderType'				=> $sessionorderType,
		]);
	}

	public function reportEPDData(Request $request)
	{
		// selected filter
		// dd($request->session()->all());
		$sessionEocID 			= ($request->session()->get('reportEPDData.filterSelected.eocsID'))? $request->session()->get('reportEPDData.filterSelected.eocsID') : [];
		$sessionLevelsID 		= ($request->session()->get('reportEPDData.filterSelected.levelsID'))? $request->session()->get('reportEPDData.filterSelected.levelsID') : [];
		$sessionIndustriesID 	= ($request->session()->get('reportEPDData.filterSelected.industriesID'))? $request->session()->get('reportEPDData.filterSelected.industriesID') : [];
		$sessionFromDate 		= ($request->session()->get('reportEPDData.filterSelected.fromDate'))? $request->session()->get('reportEPDData.filterSelected.fromDate') : [];
		$sessionToDate 			= ($request->session()->get('reportEPDData.filterSelected.toDate'))? $request->session()->get('reportEPDData.filterSelected.toDate') : [];
		$sessiontype 			= ($request->session()->get('reportEPDData.filterSelected.type'))? $request->session()->get('reportEPDData.filterSelected.type') : '';
		$sessiontypeData 		= ($request->session()->get('reportEPDData.filterSelected.typeData'))? $request->session()->get('reportEPDData.filterSelected.typeData') : '';
		$from = ($sessionFromDate && $sessionToDate)? ($sessionFromDate['year']-543).'-'.$sessionFromDate['month'].'-'.$sessionFromDate['day'] : '';
	    $to = ($sessionFromDate && $sessionToDate)? ($sessionToDate['year']-543).'-'.$sessionToDate['month'].'-'.$sessionToDate['day'] : '';

		// load from selected Filter
		$professionalsID = ProfessionalStandard::whereIn('indID', $sessionIndustriesID)->levelType()->lists('stdID');
		$qualifications = LevelCompetence::whereIn('stdID', $professionalsID)->groupBy('levelName')->get();
		$uocsID = LevelCompetence::whereIn('levelName', $sessionLevelsID)->groupBy('uocID')->lists('uocID');
		$uocs = UOC::whereIn('stdID', $uocsID)->get();

		// query Data
		$graphsData = [];
		$drillDownsData = [];
		$listGraphsCode = [];
		$listGraphsName = [];
		$listDrillDownsName = [];
		$listGraphsID = [];
		if($sessiontypeData == 'question'){
			if($sessiontype == 1){
				$i = 0;
	        	foreach($sessionLevelsID as $levelID){
	        		$questions = Question::where('type', 'level')->where('level_competence_name', $levelID);
	        		if($from && $to) $questions->whereBetween('created_at',[$from, $to]);
	        		$cntActive = $questions->count();
	        		$activeID = $questions->lists('id');
	        		$cntDeleted = $questions->onlyTrashed()->count();
	        		$deletedID = $questions->lists('id');
	        		if($cntActive || $cntDeleted){
	        			$graphsData[$i]['active'] = $cntActive;
	        			$graphsData[$i]['deleted'] = $cntDeleted;
	        			$listGraphsCode[$i] = $levelID;
	        			$listGraphsName[$i] = $levelID;
	        			$listGraphsID[$i]['active'] = $activeID;
	        			$listGraphsID[$i]['deleted'] = $deletedID;
	        			$i++;
	        		}
	        	}
	        }elseif($sessiontype == 2){
	        	// $uocID = LevelCompetence::whereIn('levelName', $sessionLevelsID)->lists('uocID');
	        	foreach($sessionEocID as $id){
	        		$questions = Question::where('type', 'uoc')->where('eoc_id', $id);
	        		if($from && $to) $questions->whereBetween('created_at',[$from, $to]);
	        		$cntActive = $questions->count();
	        		$activeID = $questions->lists('id');
	        		$cntDeleted = $questions->onlyTrashed()->count();
	        		$deletedID = $questions->lists('id');
	        		if($cntActive || $cntDeleted){
	        			$uoc = Uoc::where('stdID', $id)->first();
	        			$graphsData[$id]['active'] = $cntActive;
	        			$graphsData[$id]['deleted'] = $cntDeleted;
	        			$listGraphsCode[$id] = $uoc->uocTpqi->uocCode;
	        			$listGraphsName[$id] = $uoc->stdName;
	        			$listGraphsID[$id]['active'] = $activeID;
	        			$listGraphsID[$id]['deleted'] = $deletedID;
	        		}
	        	}
	        }else{
	        	// $uocID = LevelCompetence::whereIn('levelName', $sessionLevelsID)->lists('uocID');
	        	$uocsData = Uoc::whereIn('stdID', $sessionEocID)->get();
	        	foreach($uocsData as $uoc){
	        		$listeocsID = Eoc::where('stdType', 'E')->where('parent', $uoc->stdID)->lists('stdID');
	        		$questions = Question::where('type', 'eoc')->whereIn('eoc_id', $listeocsID);
	        		if($from && $to) $questions->whereBetween('created_at',[$from, $to]);
	        		$cntActive = $questions->count();
	        		$activeID = $questions->lists('id');
	        		$cntDeleted = $questions->onlyTrashed()->count();
	        		$deletedID = $questions->lists('id');
	        		if($cntActive || $cntDeleted){
	        			$graphsData[$uoc->stdID]['active'] = $cntActive;
			        	$graphsData[$uoc->stdID]['deleted'] = $cntDeleted;
			        	$listGraphsCode[$uoc->stdID] = $uoc->uocTpqi->uocCode;
			        	$listGraphsName[$uoc->stdID] = $uoc->stdName;
			        	$listGraphsID[$uoc->stdID]['active'] = $activeID;
	        			$listGraphsID[$uoc->stdID]['deleted'] = $deletedID;
		        		foreach($uoc->eocs as $eoc){
		        			$cntDrilldownActive = Question::where('type', 'eoc')->where('eoc_id', $eoc->stdID)->count();
		        			$cntDrilldownDeleted = Question::where('type', 'eoc')->where('eoc_id', $eoc->stdID)->onlyTrashed()->count();
		        			if($cntDrilldownActive || $cntDrilldownDeleted){
		        				$drillDownsData[$uoc->stdID]['active'][$eoc->stdID] = $cntDrilldownActive;
		        				$drillDownsData[$uoc->stdID]['deleted'][$eoc->stdID] = $cntDrilldownDeleted;
		        				$listDrillDownsName[$eoc->stdID] = $eoc->stdName;
		        			}
		        		}
	        		}
	        	}
	        }
		}elseif($sessiontypeData == 'template'){
			$i = 0;
			$byType = ($sessiontype == 3)? 'eoc' : (($sessiontype == 2)? 'uoc' : 'level');
			foreach($sessionLevelsID as $levelID){
        		$templateHeaders = templateHeader::where('type', $byType)->where('level_competence_name', $levelID);
        		if($from && $to) $templateHeaders->whereBetween('created_at',[$from, $to]);
        		$cntActive = $templateHeaders->count();
				$activeID = $templateHeaders->lists('id');
				$cntDeleted = $templateHeaders->onlyTrashed()->count();
        		$deletedID = $templateHeaders->lists('id');
				if($cntActive || $cntDeleted){
					$graphsData[$levelID]['active'] = $cntActive;
        			$graphsData[$levelID]['deleted'] = $cntDeleted;
        			$listGraphsID[$levelID]['active'] = $activeID;
	        		$listGraphsID[$levelID]['deleted'] = $deletedID;
        			$listGraphsCode[$levelID] = $levelID;
        			$listGraphsName[$levelID] = $levelID;
        			$i++;
				}
			}
		}else{
			// $i = 0;
			$byType = ($sessiontype == 3)? 'eoc' : (($sessiontype == 2)? 'uoc' : 'level');
			foreach($sessionLevelsID as $levelID){
				$exams = Exam::where('type', $byType)->where('level_competence_name', $levelID);
        		if($from && $to) $exams->whereBetween('created_at',[$from, $to]);
        		$cntActive = $exams->count();
				$activeID = $exams->lists('id');
				$cntDeleted = $exams->onlyTrashed()->count();
        		$deletedID = $exams->lists('id');
				if($cntActive || $cntDeleted){
					$graphsData[$levelID]['active'] = $cntActive;
        			$graphsData[$levelID]['deleted'] = $cntDeleted;
        			$listGraphsID[$levelID]['active'] = $activeID;
	        		$listGraphsID[$levelID]['deleted'] = $deletedID;
        			$listGraphsCode[$levelID] = $levelID;
        			$listGraphsName[$levelID] = $levelID;
        			// $i++;
				}
			}
		}
		// dd($graphsData);
		// dd($drillDownsData);
		// dd($listGraphsName);
		// dd($listDrillDownsName);



		return view('report.reportEPDData', [
			'industries'			=> Industry::with('industryGroupX')->orderBy('indGRPID')->get(), 
			'qualifications'		=> $qualifications,
			'type'					=> $sessiontype,
			'typeData'				=> $sessiontypeData,
			'industriesID'			=> $sessionIndustriesID,
			'levelsID'				=> $sessionLevelsID,
			'fromDate'				=> $sessionFromDate,
			'toDate'				=> $sessionToDate,
			'graphsData'			=> $graphsData,
			'listGraphsName'		=> $listGraphsName,
			'listGraphsCode'		=> $listGraphsCode,
			'drillDownsData'		=> $drillDownsData,
			'listDrillDownsName'	=> $listDrillDownsName,
			'listGraphsID'			=> $listGraphsID,
			'uocs'					=> $uocs,
			'eocsID'				=> $sessionEocID,
		]);
	}

	public function reportNumberExamByLevel(Request $request)
	{
		// selected filter
		// dd($request->session()->all());
		$sessionLevelsID 		= ($request->session()->get('reportNumberExamByLevel.filterSelected.levelsID'))? $request->session()->get('reportNumberExamByLevel.filterSelected.levelsID') : [];
		$sessionIndustriesID 	= ($request->session()->get('reportNumberExamByLevel.filterSelected.industriesID'))? $request->session()->get('reportNumberExamByLevel.filterSelected.industriesID') : [];
		$sessionFromDate 		= ($request->session()->get('reportNumberExamByLevel.filterSelected.fromDate'))? $request->session()->get('reportNumberExamByLevel.filterSelected.fromDate') : [];
		$sessionToDate 			= ($request->session()->get('reportNumberExamByLevel.filterSelected.toDate'))? $request->session()->get('reportNumberExamByLevel.filterSelected.toDate') : [];
		$sessiontype 			= ($request->session()->get('reportNumberExamByLevel.filterSelected.type'))? $request->session()->get('reportNumberExamByLevel.filterSelected.type') : '';
		$from = ($sessionFromDate && $sessionToDate)? ($sessionFromDate['year']-543).'-'.$sessionFromDate['month'].'-'.$sessionFromDate['day'] : '';
	    $to = ($sessionFromDate && $sessionToDate)? ($sessionToDate['year']-543).'-'.$sessionToDate['month'].'-'.$sessionToDate['day'] : '';

		// load from selected Filter
		$professionalsID = ProfessionalStandard::whereIn('indID', $sessionIndustriesID)->levelType()->lists('stdID');
		$qualifications = LevelCompetence::whereIn('stdID', $professionalsID)->groupBy('levelName')->get();

		// query
		$examDatas = [];
		$examsTable = [];
		$type = ($sessiontype == 1)? 'level' : (($sessiontype == 2)? 'uoc' : (($sessiontype == 3)? 'eoc' : ''));
		$exams = Exam::where('type', $type);
		if($from && $to) $exams->whereBetween('created_at',[$from, $to]);
		if($sessionLevelsID) $exams->whereIn('level_competence_name',$sessionLevelsID);
		$examDatas = $exams->withTrashed()->get();
		$examsTable = $exams->groupBy('level_competence_name')
			->select(DB::raw('*, count(*) as count, MAX(created_at) as lastest'))
			->orderBy('count', 'desc')
			->paginate(20);
		// dd($examsTable);


		return view('report.reportNumberExamByLevel', [
			'industries'			=> Industry::with('industryGroupX')->orderBy('indGRPID')->get(), 
			'qualifications'		=> $qualifications,
			'type'					=> $sessiontype,
			'industriesID'			=> $sessionIndustriesID,
			'levelsID'				=> $sessionLevelsID,
			'fromDate'				=> $sessionFromDate,
			'toDate'				=> $sessionToDate,
			'examsTables'			=> $examsTable,
			'exams'					=> $examDatas,
		]);
	}

	public function reportNumberExamByCBD(Request $request)
	{
		// selected filter
		// dd($request->session()->all());
		$sessionLevelsID 		= ($request->session()->get('reportNumberExamByCBD.filterSelected.levelsID'))? $request->session()->get('reportNumberExamByCBD.filterSelected.levelsID') : [];
		$sessionIndustriesID 	= ($request->session()->get('reportNumberExamByCBD.filterSelected.industriesID'))? $request->session()->get('reportNumberExamByCBD.filterSelected.industriesID') : [];
		$sessionFromDate 		= ($request->session()->get('reportNumberExamByCBD.filterSelected.fromDate'))? $request->session()->get('reportNumberExamByCBD.filterSelected.fromDate') : [];
		$sessionToDate 			= ($request->session()->get('reportNumberExamByCBD.filterSelected.toDate'))? $request->session()->get('reportNumberExamByCBD.filterSelected.toDate') : [];
		$sessiontype 			= ($request->session()->get('reportNumberExamByCBD.filterSelected.type'))? $request->session()->get('reportNumberExamByCBD.filterSelected.type') : '';
		$from = ($sessionFromDate && $sessionToDate)? ($sessionFromDate['year']-543).'-'.$sessionFromDate['month'].'-'.$sessionFromDate['day'] : '';
	    $to = ($sessionFromDate && $sessionToDate)? ($sessionToDate['year']-543).'-'.$sessionToDate['month'].'-'.$sessionToDate['day'] : '';

		// load from selected Filter
		$professionalsID = ProfessionalStandard::whereIn('indID', $sessionIndustriesID)->levelType()->lists('stdID');
		$qualifications = LevelCompetence::whereIn('stdID', $professionalsID)->groupBy('levelName')->get();

		// query
		$examDatas = [];
		$examsTable = [];
		$type = ($sessiontype == 1)? 'level' : (($sessiontype == 2)? 'uoc' : (($sessiontype == 3)? 'eoc' : ''));
		$exams = Exam::where('type', $type);
		if($from && $to) $exams->whereBetween('created_at',[$from, $to]);
		if($sessionLevelsID) $exams->whereIn('level_competence_name',$sessionLevelsID);
		$examDatas = $exams->withTrashed()->get();
		$examsTable = $exams->groupBy('cb_id')
			->select(DB::raw('*, count(*) as count, MAX(created_at) as lastest'))
			->orderBy('count', 'desc')
			->paginate(20);
		// dd($examsTable);


		return view('report.reportNumberExamByCBD', [
			'industries'			=> Industry::with('industryGroupX')->orderBy('indGRPID')->get(), 
			'qualifications'		=> $qualifications,
			'type'					=> $sessiontype,
			'industriesID'			=> $sessionIndustriesID,
			'levelsID'				=> $sessionLevelsID,
			'fromDate'				=> $sessionFromDate,
			'toDate'				=> $sessionToDate,
			'examsTables'			=> $examsTable,
			'exams'					=> $examDatas,
		]);
	}

	public function getUocFrequency()
	{
		$countEocs = [];
		$uocsID = [];
		$templateHeaders = TemplateHeader::whereHas('exams', function($query) {})->get();
		foreach ($templateHeaders as $templateHeader) {
			foreach ($templateHeader->templateItems as $templateItem) {
				if(!in_array($templateItem->uoc_id, $uocsID)) $uocsID[]=$templateItem->uoc_id;
				if(!isset($countEocs[$templateItem->eoc_id])) $countEocs[$templateHeader->industry_id][$templateItem->uoc_id][$templateItem->eoc_id] = 0;
				$countEocs[$templateHeader->industry_id][$templateItem->uoc_id][$templateItem->eoc_id] += $templateHeader->exams->count();
			}
		}

		if(sizeof($uocsID) > 0){

			$industries = Industry::whereHas('professionalStandards', function($query) use ($uocsID) {
				$query->whereIn('stdID',$uocsID);
			})->with([ 'professionalStandards' => function ($q) { $q->levelType(); } ])->get();

			$industryGroups = IndustryGroupX::whereHas('professionalStandards', function($query) use ($uocsID) {
				$query->whereIn('stdID',$uocsID);
			})->with([ 'professionalStandards' => function ($q) { $q->levelType(); } ])->get();

			return view('report.uoc', [ 
				'industries' => $industries, 
				'industryGroups' => $industryGroups, 
				'countEocs' => $countEocs 
			]);
		}else{
			return view('report.uoc', [ 'industries' => [], 'industryGroups' => [], 'countEocs' => $countEocs ]);
		}
	}

	/**
	 * Download to PDF
	 */
	private function reportUocFrequencyPDF(){
        $countEocs = [];
        $uocsID = [];
        $templateHeaders = TemplateHeader::whereHas('exams', function($query) {})->get();
        foreach ($templateHeaders as $templateHeader) {
            foreach ($templateHeader->templateItems as $templateItem) {
                if(!in_array($templateItem->uoc_id, $uocsID)) $uocsID[]=$templateItem->uoc_id;
                if(!isset($countEocs[$templateItem->eoc_id])) $countEocs[$templateHeader->industry_id][$templateItem->uoc_id][$templateItem->eoc_id] = 0;
                $countEocs[$templateHeader->industry_id][$templateItem->uoc_id][$templateItem->eoc_id] += $templateHeader->exams->count();
            }
        }

        if(sizeof($uocsID) > 0){

            $industries = Industry::whereHas('professionalStandards', function($query) use ($uocsID) {
                $query->whereIn('stdID',$uocsID);
            })->with([ 'professionalStandards' => function ($q) { $q->levelType(); } ])->get();

            $industryGroups = IndustryGroupX::whereHas('professionalStandards', function($query) use ($uocsID) {
                $query->whereIn('stdID',$uocsID);
            })->with([ 'professionalStandards' => function ($q) { $q->levelType(); } ])->get();

            $array = [
                'industries' => $industries,
                'industryGroups' => $industryGroups,
                'countEocs' => $countEocs
            ];
        }else{
            $array = [ 'industries' => [], 'industryGroups' => [], 'countEocs' => $countEocs ];


        }
        $snappy = new Pdf(base_path() . '/vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64');
        // $snappy = new Pdf('/usr/local/bin/wkhtmltopdf');
        $html = view('pdf.report',$array)->render();
        $date = Carbon::now();
        $pathToFile = 'pdf/report/'.'report-'. $date->format('Y-m-d-H-i-s').'.pdf';
        $snappy->generateFromHtml($html,$pathToFile);
        return Response::download($pathToFile);
    }

    private function questionPDF($id, $examTransection){
    	// dd($examTransection->toArray());
        $exam = Exam::find($id);
        $exam->update([ 
            'used_pdf' => ($exam->used_pdf+1)
        ]);

        $date = Carbon::now();
        $monName = array("1"=>"มกราคม","2"=>"กุมภาพันธ์","3"=>"มีนาคม","4"=>"เมษายน","5"=>"พฤษภาคม","6"=>"มิถุนายน","7"=>"กรกฎาคม","8"=>"สิงหาคม","9"=>"กันยายน","10"=>"ตุลาคม","11"=>"พฤศจิกายน","12"=>"ธันวาคม");
	    $pathToFile = 'pdf/question/'.'question'.$examTransection->id.'.pdf';
	    $cover = view('pdf.coverpdf',[ 'exam' => $exam, 'type' => 'question', 'examTransection' => $examTransection, 'monName' => $monName, 'date' => $date ])->render();
	    $header = view('pdf.headerpdf',[ 'exam' => $exam, 'type' => 'question', 'examTransection' => $examTransection, 'monName' => $monName, 'date' => $date ])->render();
	    $html = view('pdf.question',[ 'exam' => $exam, 'type' => 'question', 'monName' => $monName, 'date' => $date ])->render();
	    $footer = view('pdf.footer',[ 'exam' => $exam, 'type' => 'question', 'examTransection' => $examTransection, 'monName' => $monName, 'date' => $date ])->render();

        // echo $cover;
        // echo $header;
        // echo $html;
        // echo $footer;

        // $snappy = new Pdf(base_path() . '/vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64');
        $snappy = new Pdf('/usr/local/bin/wkhtmltopdf');
        $snappy->setOption('footer-html', $footer);
        $snappy->setOption('header-html', $header);
        $snappy->setOption('cover', $cover);
        $snappy->setOption('margin-top', '30mm');
        $snappy->generateFromHtml($html,$pathToFile);

        // return $snappy->stream($pathToFile);
        return Response::make(File::get($pathToFile), 200)->header('Content-Type', 'application/pdf');
        // return Response::download($pathToFile);
    }

    private function solutionPDF($id, $examTransection){
    	$exam = Exam::find($id);
    	$date = Carbon::now();
        $monName = array("1"=>"มกราคม","2"=>"กุมภาพันธ์","3"=>"มีนาคม","4"=>"เมษายน","5"=>"พฤษภาคม","6"=>"มิถุนายน","7"=>"กรกฎาคม","8"=>"สิงหาคม","9"=>"กันยายน","10"=>"ตุลาคม","11"=>"พฤศจิกายน","12"=>"ธันวาคม");
	    $pathToFile = 'pdf/question/'.'solution'.$examTransection->id.'.pdf';
	    $cover = view('pdf.coverpdf',[ 'exam' => $exam, 'type' => 'solution', 'examTransection' => $examTransection, 'monName' => $monName, 'date' => $date ])->render();
	    $header = view('pdf.headerpdf',[ 'exam' => $exam, 'type' => 'solution', 'examTransection' => $examTransection, 'monName' => $monName, 'date' => $date ])->render();
	    $amountPage = ($exam->examQuestions()->count() / 150);
    	$n = $amountPage;
    	for($i=0; $i<100; $i++){
    		if($n < $i){
    			$amountPage = $i;
    			break;
    		}
    	}
    	$startFrom = (150 * $amountPage)-149;
    	$exam['choice'] = $startFrom;
    	// dd($exam->examQuestions->toArray());
    	$html = '';
	    for($i=0;$i<$amountPage;$i++){
	    	$html .= view('pdf.solution',[ 'exam' => $exam, ])->render();
	    }
	    $footer = view('pdf.footer',[ 'exam' => $exam, 'type' => 'solution', 'examTransection' => $examTransection, 'monName' => $monName, 'date' => $date ])->render();

	    // echo $cover;
     //    echo $header;
     //    echo $html;
     //    echo $footer;

        // $snappy = new Pdf(base_path() . '/vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64');
        $snappy = new Pdf('/usr/local/bin/wkhtmltopdf');
        $snappy->setOption('footer-html', $footer);
        $snappy->setOption('header-html', $header);
        $snappy->setOption('cover', $cover);
        $snappy->setOption('margin-top', '30mm');
        $snappy->generateFromHtml($html,$pathToFile);

        return Response::make(File::get($pathToFile), 200)->header('Content-Type', 'application/pdf');
        // return Response::download($pathToFile);
    }

    private function answerPDF($id, $examTransection){
    	$exam = Exam::find($id);
        $date = Carbon::now();

        $monName = array("1"=>"มกราคม","2"=>"กุมภาพันธ์","3"=>"มีนาคม","4"=>"เมษายน","5"=>"พฤษภาคม","6"=>"มิถุนายน","7"=>"กรกฎาคม","8"=>"สิงหาคม","9"=>"กันยายน","10"=>"ตุลาคม","11"=>"พฤศจิกายน","12"=>"ธันวาคม");
	    $pathToFile = 'pdf/question/'.'answer'.$examTransection->id.'.pdf';
	    $amountPage = ($exam->examQuestions()->count() / 150);
    	$n = $amountPage;
    	for($i=0; $i<100; $i++){
    		if($n < $i){
    			$amountPage = $i;
    			break;
    		}
    	}
    	$startFrom = (150 * $amountPage)-149;
    	$exam['choice'] = $startFrom;
    	$html = '';
	    for($i=0;$i<$amountPage;$i++){
	    	$html .= view('pdf.answer',[ 'exam' => $exam, ])->render();
	    }
	    $cover = view('pdf.coverpdf',[ 'exam' => $exam, 'type' => 'answer', 'examTransection' => $examTransection, 'monName' => $monName, 'date' => $date ])->render();
	    $header = view('pdf.headerpdf',[ 'exam' => $exam, 'type' => 'answer', 'examTransection' => $examTransection, 'monName' => $monName, 'date' => $date ])->render();
	    $footer = view('pdf.footer',[ 'exam' => $exam, 'type' => 'answer', 'examTransection' => $examTransection, 'monName' => $monName, 'date' => $date ])->render();

        // echo $cover;
        // echo $header;
        // echo $html;
        // echo $footer;

        // $snappy = new Pdf(base_path() . '/vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64');
        $snappy = new Pdf('/usr/local/bin/wkhtmltopdf');
        $snappy->setOption('footer-html', $footer);
        $snappy->setOption('header-html', $header);
        $snappy->setOption('cover', $cover);
        $snappy->setOption('margin-top', '30mm');
        $snappy->generateFromHtml($html,$pathToFile);

        return Response::make(File::get($pathToFile), 200)->header('Content-Type', 'application/pdf');
        // return Response::download($pathToFile);
    }

    private function getFooter($id){
    	return view('pdf.footer')->render();
    }

}
