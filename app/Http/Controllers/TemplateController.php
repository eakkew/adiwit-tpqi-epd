<?php namespace Tpqi\Http\Controllers;

use Tpqi\Http\Requests;
use Tpqi\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Tpqi\Eoc;
use Tpqi\Uoc;
use Tpqi\Industry;
use Tpqi\TemplateItem;
use Tpqi\IndustryGroupX;
use Tpqi\TemplateHeader;
use Tpqi\LevelCompetence;
use Tpqi\ProfessionalStandard;

class TemplateController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		// session selected filter
		$sessionEocID = ($request->session()->get('template.filterSelected.eocsID'))? $request->session()->get('template.filterSelected.eocsID') : [];
		$sessionLevelsID = ($request->session()->get('template.filterSelected.levelsID'))? $request->session()->get('template.filterSelected.levelsID') : [];
		$sessionIndustriesID = ($request->session()->get('template.filterSelected.industriesID'))? $request->session()->get('template.filterSelected.industriesID') : [];
		$sessionFromDate = ($request->session()->get('template.filterSelected.fromDate'))? $request->session()->get('template.filterSelected.fromDate') : [];
		$sessionToDate = ($request->session()->get('template.filterSelected.toDate'))? $request->session()->get('template.filterSelected.toDate') : [];
		$sessiontype = ($request->session()->get('template.filterSelected.type'))? $request->session()->get('template.filterSelected.type') : '';

		// statistic
		// $limitStatistic = 20;
		// $industriesStatistic = [];
		// $industriesID = TemplateHeader::groupBy('industry_id')->lists('industry_id');
		// $countIndustry = Industry::allTemplatesIndustries($industriesID, $sessionEocID, $sessionFromDate, $sessionToDate);
		// $countIndustryGroupX = IndustryGroupX::allTemplatesIndustries($industriesID, $sessionEocID, $sessionFromDate, $sessionToDate);
		// if($countIndustry != 'none') $industriesStatistic += $countIndustry;
		// if($countIndustryGroupX != 'none') $industriesStatistic += $countIndustryGroupX;
		// ksort($industriesStatistic);
		// arsort($industriesStatistic);
		// $industriesStatistic = array_slice($industriesStatistic, 0, $limitStatistic);

		// load from selected Filter
		$professionalsID = ProfessionalStandard::whereIn('indID', $sessionIndustriesID)->levelType()->lists('stdID');
		$qualifications = LevelCompetence::whereIn('stdID', $professionalsID)->groupBy('levelName')->get();
		$uocsID = LevelCompetence::whereIn('levelName', $sessionLevelsID)->groupBy('uocID')->lists('uocID');
		$uocs = UOC::whereIn('stdID', $uocsID)->with('eocs')->get();
		
		// page
		$perPage = 10;
		$currentPage = ($request->input('page'))? $request->input('page') : 1;
        $skip = ($currentPage)? ($perPage*$currentPage)-$perPage : 0;

        // row data
        if($sessiontype == 1){
        	$templateHeaders = TemplateHeader::levelID($sessionLevelsID, $sessionFromDate, $sessionToDate);
        }elseif($sessiontype == 2){
        	$templateHeaders = TemplateHeader::uocID($sessionEocID, $sessionFromDate, $sessionToDate);
        }else{
        	$templateHeaders = TemplateHeader::eocID($sessionEocID, $sessionFromDate, $sessionToDate);
        }
        $allDataCount = $templateHeaders->count();

		return view('template.index', [
			'industries'			=> Industry::with('industryGroupX')->orderBy('indGRPID')->get(), 
			// 'industriesStatistic'	=> $industriesStatistic,
			'qualifications'		=> $qualifications,
			'uocs'					=> $uocs,
			'templateHeaders'		=> $templateHeaders->orderBy('id', 'desc')->skip($skip)->take($perPage)->get(),
			'industriesID'			=> $sessionIndustriesID,
			'levelsID'				=> $sessionLevelsID,
			'eocsID'				=> $sessionEocID,
			'fromDate'				=> $sessionFromDate,
			'toDate'				=> $sessionToDate,
			'allDataCount'			=> $allDataCount,
			'page'			        => $currentPage,
			'perPage'			    => $perPage,
			'type'					=> $sessiontype,
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$industries = Industry::with('industryGroupX')->orderBy('indGRPID')->get();
		return view('template.create')->with(['industries' => $industries]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		// dd($request->all());
		$templateHeader = TemplateHeader::create($request->all());

		$templateItems = [];
		if($request->input('type') == 1){
			if($request->input('levelAmount') > 0){
				$templateItems[] = new TemplateItem([
					'amount'	=>	$request->input('levelAmount'),
				]);
			}
		}elseif($request->input('type') == 2){
			foreach ($request->input('uoc_id') as $index => $uocID) {
				if($request->input('amount.' . $index) > 0){
					$templateItems[] = new TemplateItem([
						'uoc_id'	=>	$uocID,
						'amount'	=>	$request->input('amount.' . $index),
					]);
				}
			}
		}elseif($request->input('type') == 3){
			foreach ($request->input('eoc_id') as $index => $eocID) {
				if($request->input('amount.' . $index) > 0){
					$templateItems[] = new TemplateItem([
						'eoc_id'	=>	$eocID,
						'uoc_id'	=>	Eoc::find($eocID)->uocID,
						'amount'	=>	$request->input('amount.' . $index),
					]);
				}
			}
		}
		$templateHeader->templateItems()->saveMany($templateItems);
		return redirect()->action('TemplateController@index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$templateHeader = TemplateHeader::find($id);
		if($templateHeader->exams->count()) return redirect()->action('TemplateController@index');
		$uocIDs = LevelCompetence::where('levelName', '=', $templateHeader->level_competence_name)->groupBy('uocID')->lists('uocID');
		$uocs = UOC::whereIn('stdID', $uocIDs)->get();

		return view('template.create', [
			'templateHeader'	=> $templateHeader,
			'uocs'				=> $uocs
		]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id) {
		$templateHeader = TemplateHeader::find($id);
		// dd($request->all());
		if ($templateHeader) {
			// Update Header
			$templateHeader->update($request->all());
			if($templateHeader->type == 'eoc'){
				foreach ($request->input('eoc_id') as $index => $eocID) {
					$templateItem = $templateHeader->templateItems()->where('eoc_id', '=', $eocID)->first();

					if ($templateItem) {// Update TemplateItem (amount)
						if ($request->input('amount.' . $index) > 0) {
							$templateItem->update([
								'amount' => $request->input('amount.' . $index)
							]);
						}else {// If (amount == 0) delete the item
							$templateItem->delete();
						}
					}

					// New TemplateItem
					elseif ($request->input('amount.' . $index) > 0) {
						TemplateItem::create([
							'eoc_id' => $eocID,
							'uoc_id' => Eoc::find($eocID)->uocID,
							'amount' => $request->input('amount.' . $index),
							'template_id' => $id,
						]);
					}
				}
			}elseif($templateHeader->type == 'uoc'){
				foreach ($request->input('uoc_id') as $index => $uocID) {
					$templateItem = $templateHeader->templateItems()->where('uoc_id', $uocID)->first();

					// Update TemplateItem (amount)
					if ($templateItem) {
						if ($request->input('amount.' . $index) > 0) {
							$templateItem->update([
								'amount' => $request->input('amount.' . $index)
							]);
						}else { // If (amount == 0) delete this item
							$templateItem->delete();
						}
					}elseif ($request->input('amount.' . $index) > 0) { // New TemplateItem
						TemplateItem::create([
							'eoc_id' => 0,
							'uoc_id' => $uocID,
							'amount' => $request->input('amount.' . $index),
							'template_id' => $id,
						]);
					}
				}
			}elseif($templateHeader->type == 'level'){
				if($templateHeader->templateItems){
					if($request->input('amount') > 0) $templateHeader->templateItems()->first()->update(['amount' => $request->input('amount')]);
				}
			}
		}

		return redirect()->action('TemplateController@index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$templateHeader = TemplateHeader::find($id);
		if ($templateHeader) $templateHeader->delete();
		return redirect()->action('TemplateController@index');
	}

}
