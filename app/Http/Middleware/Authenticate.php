<?php namespace Tpqi\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\DB;

use Tpqi\MenuPermission;
use Tpqi\MenuDetail;
use Tpqi\Organize_staff;

class Authenticate {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	// ['middleware' => 'auth']
	public function handle($request, Closure $next)
	{
		session_start();
if (isset($_POST["user"]) && isset($_POST["password"]) ) {

		$user=$_POST["user"];
		$password=$_POST["password"];

		$query="
		select	*
		from	tpqi_general.officer
		where	(
		user='$user' and
		password=md5('$password')
		)
		";

		$result=DB::connection('tpqi_general')->select($query);
		$result=json_decode(json_encode($result), true);

		if($result) {
			$row=$result[0];
			$_SESSION["authen"]=$row;

			$queryPerson="
			select  *
			from    tpqi_general.person
			where   `personID`='".$row["personID"]."'
			";
			// $resultPerson=  mysql_query($queryPerson);
			$resultPerson=DB::connection('tpqi_general')->select($queryPerson);
			$resultPerson=json_decode(json_encode($resultPerson), true);
			// $rowPerson=  mysql_fetch_array($resultPerson);
			$rowPerson=$resultPerson[0];

			$_SESSION["person"]=$rowPerson;

			$queryPermission="
			select  *
			from    tpqi_general.permission
			where   perID='".$row["perID"]."'
			";
			// $resultPermission=  mysql_query($queryPermission);
			$resultPermission=DB::connection('tpqi_general')->select($queryPermission);
			$resultPermission=json_decode(json_encode($resultPermission), true);
			// $_SESSION["permission"]=  mysql_fetch_array($resultPermission);
			$_SESSION["permission"]=$resultPermission[0];
		}
}

		if(!isset($_SESSION['permission']) && !isset($_SESSION['person'])) return redirect('tpqi/authen');
		$permission = $_SESSION['permission'];
		$person = $_SESSION['person'];
		// $permission = [ "perID" => "4", "perName" => "ผู้ดูแลคลังข้อสอบ", "detail" => null, "menuType" => "EPD" ];
		// $person 	= [ "personID" => "6426", "citizenID" => "0900000000045", "givenName" => "ผู้ดูแล", "familyName" => "คลังข้อสอบ", "birthDate" => "0000-00-00", "birthJurisdictionCountrySubDivision" => "", "nationality" => "", "gender" => "", "religion" => "", "prefix" => "", "addressID" => "", "buildingNumber" => "", "floorID" => "", "buildingName" => "", "townshipNumber" => "", "townshipName" => "", "subLane" => "", "lane" => "", "streetName" => "", "subDistrict" => "อนุสาวรีย์", "district" => "เขตบางเขน", "province" => "1", "postcode" => "10220", "country" => "", "telephone" => "02-9999999, 02-1111111", "fax" => "", "mobile" => "", "email" => "info@adiwit.co.th", "emailAlternative" => "", "registDate" => "2015-09-17 13:48:59", "age" => "", "givenNameEn" => null, "familyNameEn" => null, "prefixEn" => null ];
		$_SESSION['printURL'] = $_SERVER["REQUEST_URI"];
		if ($this->auth->guest())
		{
		    if($permission['menuType'] == 'EPD' || $permission['perID'] == 9){
		    	session(['tpqi.permission' => $permission]);
				session(['tpqi.person' => $person]);
		    	$member = MenuPermission::where('perID', $permission['perID'])->first();
				if($permission['perID'] == 9){
			    	$member = MenuPermission::where('perID', $permission['perID'])->where('menuID', 'EP05')->first();
				}
				if (!$member) {
					$this->auth->logout();
					$this->auth->loginUsingId(2);
					return $next($request);
				}elseif($member->ins && $member->upd && $member->prt){
					$this->auth->loginUsingId(3, true);
				}elseif($member->ins && $member->upd){
					$this->auth->loginUsingId(1);
				}elseif($member->prt){
					$this->auth->loginUsingId(2);
				}else{
					$request->session()->forget($request->input('tpqi'));
		    		return redirect('/');// Access Deny
		    	}
		    	return redirect()->action('MenuController@index');
		    }else{
		    	$request->session()->forget($request->input('tpqi'));
		    	return redirect('/');// Access Deny
		    }

		    // Reject AJAX without login first
			if ($request->ajax()) return response('Unauthorized.', 401);
		}
		else
		{
		    if($permission['menuType'] == 'EPD' || $permission['perID'] == 9){
		    	session(['tpqi.permission' => $permission]);
				session(['tpqi.person' => $person]);
		    	$member = MenuPermission::where('perID', $permission['perID'])->first();
		    	if ( $permission['perID'] == 9 ) {
		    		$member = MenuPermission::where('perID', $permission['perID'])->where('menuID', 'EP05')->first();
		    	}
		    	if (!$member) {
		    		$this->auth->logout();
		    		$this->auth->loginUsingId(2);
		    		return $next($request);
		    	}elseif($member->ins && $member->upd && $member->prt){
		    		if($this->auth->user()->id == 3){
		    			return $next($request);
		    		}else{
		    			$this->auth->logout();
		    			$this->auth->loginUsingId(3);
		    		}
		    	}elseif($member->ins && $member->upd){
		    		if($this->auth->user()->id == 1){
		    			return $next($request);
		    		}else{
		    			$this->auth->logout();
		    			$this->auth->loginUsingId(1);
		    		}
		    	}elseif($member->prt){
		    		if($this->auth->user()->id == 2){
		    			if ($permission['perID'] == 9) {
		    				$getAction = $request->route()->getAction();
		    				$getAction['as'] = $_SESSION['printURL'];
		    				$request->route()->setAction($getAction);
		    			}
		    			return $next($request);
		    		}else{
		    			$this->auth->logout();
		    			$this->auth->loginUsingId(2);
		    		}
		    	}else{
		    		$this->auth->logout();
		    		$request->session()->forget($request->input('tpqi'));
		    		return redirect('/');// Access Deny
		    	}
		    }else{
		    	$this->auth->logout();
		    	$request->session()->forget($request->input('tpqi'));
		    	return redirect('/');// Access Deny
		    }
		}
	}

}
