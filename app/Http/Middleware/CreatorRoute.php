<?php

namespace Tpqi\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class CreatorRoute
{
  /**
   * Create a new filter instance.
   *
   * @param  Guard  $auth
   * @return void
   */
  public function __construct(Guard $auth)
  {
      $this->auth = $auth;
  }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$this->auth->user()->hasRole(['creator', 'master'])) return redirect()->action('MenuController@index');
        return $next($request);
    }
}
