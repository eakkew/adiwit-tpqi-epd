<?php namespace Tpqi\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\RedirectResponse;

class RedirectIfAuthenticated {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */

	// ['middleware' => 'guest']
	public function handle($request, Closure $next)
	{
		// dd('middleware guest');
		// if ($this->auth->check())
		// {
			// dd('member');
			// return redirect()->action('MenuController@index');
			// Auth::user() == $this->auth->user();
			// if ($tpqi-session->userID == $this->auth->user()->id) {
				// dashboard
				// return new RedirectResponse(url('/home'));
			// } else {
				// Auth::logout();
				// return redirect('/');
			// }
		// }

		return $next($request);
	}

}
