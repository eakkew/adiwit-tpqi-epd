<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
URL::forceRootUrl('http://tpqi-net.tpqi.go.th/');

Route::get('/test', function() {
});

Route::group(['middleware' => 'auth'], function() {
	Route::group(['prefix' => '/tpqi_epd'], function () {
		Route::resource('/', 'MenuController');// Dash board
		Route::get('/logout', 'Auth\AuthController@getLogout'); // Logout in tpqi_epd first brfore redirect to tpqi

		// Route::resource('organization', (Request::ajax()) ? 'Ajax\OrganizationController' : 'OrganizationController');
		// Route::resource('template', (Request::ajax()) ? 'Ajax\TemplateController' : 'TemplateController');

		// Ajax
		Route::post('filter', 'Ajax\FilterController@postFilterValue');
		Route::post('filterSelected', 'Ajax\FilterController@selectedFilter');
		if(Request::ajax()){
			Route::resource('organization', 'Ajax\OrganizationController');
			Route::resource('ajax/exam', 'Ajax\ExamController');
		}
		
		Route::group(['middleware' => 'creator'], function() {
			Route::post('exam/create/automatic', 'ExamController@postCreateAutomatic');
			Route::post('exam/create/manual', 'ExamController@postCreateManual');

			Route::resource('question', 'QuestionController');
			Route::resource('template', (Request::ajax())? 'Ajax\TemplateController' : 'TemplateController');
		});

		Route::group(['middleware' => 'view'], function() {
			Route::get('report/reportDiffExam', 'ReportController@reportDiffExam');
			Route::get('report/reportFrequencyQuestion', 'ReportController@reportFrequencyQuestion');
			Route::get('report/reportNumberExamByLevel', 'ReportController@reportNumberExamByLevel');
			Route::get('report/reportNumberExamByCBD', 'ReportController@reportNumberExamByCBD');
			Route::get('report/reportEPDData', 'ReportController@reportEPDData');

			Route::get('exam/{id}/question', 'ExamController@getQuestion');
			Route::get('exam/{id}/answer', 'ExamController@getAnswer');
			Route::get('exam/{id}/solution', 'ExamController@getSolution');

			Route::any('report/{id}', 'ReportController@show');
			Route::resource('report', 'ReportController');
			Route::post('reportDiffPDF', 'ReportController@reportDiffPDF');
			Route::post('reportFrequencyQuestionPDF', 'ReportController@reportFrequencyQuestionPDF');
			Route::post('reportNumberExamByLevel', 'ReportController@reportNumberExamByLevelPDF');
			Route::post('reportNumberExamByCBDPDF', 'ReportController@reportNumberExamByCBDPDF');
			Route::post('reportEPDDataPDF', 'ReportController@reportEPDDataPDF');
			// Route::resource('template', 'TemplateController');
		});

		Route::resource('exam', 'ExamController');
	});
});
