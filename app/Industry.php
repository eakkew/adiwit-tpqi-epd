<?php namespace Tpqi;

use Illuminate\Database\Eloquent\Model;

class Industry extends Model {

	protected	$connection		=	'tpqi_occ';
	protected	$table			=	'industry';
	public		$timestamps		=	false;
	
	/**
	 * Custom Attributes
	 */
	public function getCountQuestionAttribute($type)
	{
	    return ($this->countByQuestion)? $this->countByQuestion->count : 0;
	}

	public function getCountTemplateAttribute($type)
	{
	    return ($this->countByTemplate)? $this->countByTemplate->count : 0;
	}

	//Relations
	public function industryGroupX() {
		return $this->hasMany('Tpqi\IndustryGroupX', 'indGRPID', 'indGRPID');
	}

	public function professionalStandards() {
		return $this->hasMany('Tpqi\ProfessionalStandard', 'indID', 'indGRPID');
	}

	public function questions() {
		return $this->hasMany('Tpqi\Question', 'industry_id', 'indGRPID');
	}
	public function templateHeaders() {
		return $this->hasMany('Tpqi\TemplateHeader', 'industry_id', 'indGRPID');
	}

	// Count Relations
	public function countByQuestion($eocID, $from, $to)
	{
		if($eocID){
			if($from && $to){
				return $this->hasMany('Tpqi\Question', 'industry_id', 'indGRPID')->selectRaw('count(*) as count')->whereIn('eoc_id', $eocID)->whereBetween('created_at',[$from, $to])->groupBy('industry_id');
			}else{
				return $this->hasMany('Tpqi\Question', 'industry_id', 'indGRPID')->selectRaw('count(*) as count')->whereIn('eoc_id', $eocID)->groupBy('industry_id');
			}
		}else{
	    	return $this->hasMany('Tpqi\Question', 'industry_id', 'indGRPID')->selectRaw('count(*) as count')->groupBy('industry_id');
	    }
	}
	
	public function countByTemplate($industriesID, $from, $to)
	{
		if($industriesID){
			if($from && $to){
				return $this->hasMany('Tpqi\TemplateHeader', 'industry_id', 'indGRPID')->selectRaw('count(*) as count')->whereIn('industry_id', $industriesID)->whereBetween('created_at',[$from, $to])->groupBy('industry_id');
			}else{
				return $this->hasMany('Tpqi\TemplateHeader', 'industry_id', 'indGRPID')->selectRaw('count(*) as count')->whereIn('industry_id', $industriesID)->groupBy('industry_id');
			}
		}else{
			return $this->hasMany('Tpqi\TemplateHeader', 'industry_id', 'indGRPID')->selectRaw('count(*) as count')->groupBy('industry_id');
		}
	}

	// Scope
	public function scopeAllQuestionsIndustries($query, $industriesID, $sessionEocID, $fromDate, $toDate)
	{
		$from = ($fromDate && $toDate)? ($fromDate['year']-543).'-'.$fromDate['month'].'-'.$fromDate['day'] : '';
	    $to = ($fromDate && $toDate)? ($toDate['year']-543).'-'.$toDate['month'].'-'.$toDate['day'] : '';
    	foreach($query->whereIn('indGRPID',$industriesID)->get() as $industry)
		{
			$countQuestion = $industry->countByQuestion($sessionEocID, $from, $to)->count();
			if($countQuestion) $industriesStatistic[$industry->indGRPName] = $countQuestion;
		}
    	if(isset($industriesStatistic)){
			return $industriesStatistic;
		}else{
			return 'none';
		}
	}

	public function scopeAllTemplatesIndustries($query, $industriesID, $sessionEocID, $fromDate, $toDate)
	{
		$from = ($fromDate && $toDate)? ($fromDate['year']-543).'-'.$fromDate['month'].'-'.$fromDate['day'] : '';
	    $to = ($fromDate && $toDate)? ($toDate['year']-543).'-'.$toDate['month'].'-'.$toDate['day'] : '';
		foreach($query->whereIn('indGRPID',$industriesID)->get() as $industry)
		{
			foreach($industry->templateHeaders as $templateHeader)
			{
				$countTemplate = $templateHeader->countByTemplateItems($sessionEocID, $from, $to)->count();
				if($countTemplate){
					if(!isset($industriesStatistic[$industry->indGRPName])) $industriesStatistic[$industry->indGRPName] = 0;
					$industriesStatistic[$industry->indGRPName] += 1;
				}
			}
		}
		if(isset($industriesStatistic)){
			return $industriesStatistic;
		}else{
			return 'none';
		}
	}

}