<?php namespace Tpqi;

use Illuminate\Database\Eloquent\Model;

class IndustryGroupX extends Model {

	protected	$connection	=	'tpqi_occ';
	protected	$table			=	'industry_groupx';
	public		$timestamps	=	false;

	/**
	 * Custom Attributes
	 */
	public function getCountQuestionAttribute()
	{
		return ($this->countByQuestion)? $this->countByQuestion->count : 0;
	}

	public function getCountTemplateAttribute($type)
	{
	    return ($this->countByTemplate)? $this->countByTemplate->count : 0;
	}

	//Relations
	public function industry() {
		return $this->belongsTo('Tpqi\Industry', 'indGRPID', 'indGRPID')->orderBy('indGRPName');
	}

	public function professionalStandards() {
		return $this->hasMany('Tpqi\ProfessionalStandard', 'indID', 'indID');
	}

	public function questions() {
		return $this->hasMany('Tpqi\Question', 'industry_id', 'indID');
	}

	public function templateHeaders() {
		return $this->hasMany('Tpqi\TemplateHeader', 'industry_id', 'indID');
	}

	// Count Relations
	public function countByQuestion($eocID, $from, $to)
	{
		if($eocID){
			if($from && $to){
				return $this->hasMany('Tpqi\Question', 'industry_id', 'indID')->selectRaw('count(*) as count')->whereIn('eoc_id', $eocID)->whereBetween('created_at',[$from, $to])->groupBy('industry_id');
			}else{
				return $this->hasMany('Tpqi\Question', 'industry_id', 'indID')->selectRaw('count(*) as count')->whereIn('eoc_id', $eocID)->groupBy('industry_id');
			}
		}else{
			return $this->hasMany('Tpqi\Question', 'industry_id', 'indID')->selectRaw('count(*) as count')->groupBy('industry_id');
		}
	}
	public function countByTemplate($industriesID, $from, $to)
	{
		if($industriesID){
			if($from && $to){
				return $this->hasMany('Tpqi\TemplateHeader', 'industry_id', 'indID')->selectRaw('count(*) as count')->whereIn('industry_id', $industriesID)->whereBetween('created_at',[$from, $to])->groupBy('industry_id');
			}else{
				return $this->hasMany('Tpqi\TemplateHeader', 'industry_id', 'indID')->selectRaw('count(*) as count')->whereIn('industry_id', $industriesID)->groupBy('industry_id');
			}
		}else{
			return $this->hasMany('Tpqi\TemplateHeader', 'industry_id', 'indID')->selectRaw('count(*) as count')->groupBy('industry_id');
		}
	}

	// Scope
	public function scopeAllQuestionsIndustries($query, $industriesID, $sessionEocID, $fromDate, $toDate)
	{
		$from = ($fromDate && $toDate)? ($fromDate['year']-543).'-'.$fromDate['month'].'-'.$fromDate['day'] : '';
	    $to = ($fromDate && $toDate)? ($toDate['year']-543).'-'.$toDate['month'].'-'.$toDate['day'] : '';
    	foreach($query->whereIn('indID',$industriesID)->get() as $industryGroupX)
		{
			$countQuestion = $industryGroupX->countByQuestion($sessionEocID, $from, $to)->count();
			if($countQuestion) $industriesStatistic[$industryGroupX->indName] = $countQuestion;
		}
		if(isset($industriesStatistic)){
			return $industriesStatistic;
		}else{
			return 'none';
		}
	}

	public function scopeAllTemplatesIndustries($query, $industriesID, $sessionEocID, $fromDate, $toDate)
	{
		$from = ($fromDate && $toDate)? ($fromDate['year']-543).'-'.$fromDate['month'].'-'.$fromDate['day'] : '';
	    $to = ($fromDate && $toDate)? ($toDate['year']-543).'-'.$toDate['month'].'-'.$toDate['day'] : '';
		foreach($query->whereIn('indID',$industriesID)->get() as $industryGroupX)
		{
			foreach($industryGroupX->templateHeaders as $templateHeader)
			{
				$countTemplate = $templateHeader->countByTemplateItems($sessionEocID, $from, $to)->count();
				if($countTemplate){
					if(!isset($industriesStatistic[$industryGroupX->indName])) $industriesStatistic[$industryGroupX->indName] = 0;
					$industriesStatistic[$industryGroupX->indName] += 1;
				}
			}
		}
		if(isset($industriesStatistic)){
			return $industriesStatistic;
		}else{
			return 'none';
		}
	}
}