<?php namespace Tpqi;

use Illuminate\Database\Eloquent\Model;

class LevelCompetence extends Model {

	protected	$connection	=	'tpqi_occ';
	protected	$table		=	'level_competence';
	public		$timestamps	=	false;
	
	// Relationships
	public function professionalStandard() {
		return $this->belongsTo('Tpqi\professionalStandard', 'stdID', 'stdID');
	}

	public function uocs() {
		return $this->hasMany('Tpqi\Uoc', 'stdID', 'uocID');
	}

	public function uoc() {
		return $this->hasOne('Tpqi\Uoc', 'stdID', 'uocID');
	}

	public function eoc() {
		return $this->hasMany('Tpqi\Eoc', 'stdID', 'elementID');
	}

	// scope
	public function scopeUocID($query, $uocsID){
		return $query->whereIn('uocID', $uocsID);
	}
	public function scopeEocID($query, $eocsID){
		return $query->whereIn('elementID', $uocsID);
	}
}