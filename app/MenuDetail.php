<?php

namespace Tpqi;

use Illuminate\Database\Eloquent\Model;

class MenuDetail extends Model
{
    protected	$connection	=	'tpqi_general';
	protected	$table		=	'menu_detail';
	public		$timestamps	=	false;
}
