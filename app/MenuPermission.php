<?php

namespace Tpqi;

use Illuminate\Database\Eloquent\Model;

class MenuPermission extends Model
{
    protected	$connection	=	'tpqi_general';
	protected	$table		=	'menu_permission';
	public		$timestamps	=	false;
}
