<?php namespace Tpqi;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model {
	
	protected	$connection	=	'tpqi_cbd';
	protected	$table		=	'organization';
	public		$timestamps	=	false;

	// Custom Attributes
	public function getOrgNameAttribute() {
		return $this->attributes['orgCode'].' '.$this->attributes['orgName'];
	}

	public function getDescriptionAttribute() {
		return $this->attributes['qualification_scope']->description;
	}

	// Relationships
	public function qualificationScope() {
		return $this->hasMany('Tpqi\Qualificationscope', 'FK_orgID', 'orgID')->where('status', '=', 'active');
	}
}