<?php

namespace Tpqi;

use Illuminate\Database\Eloquent\Model;

class Organize_staff extends Model
{
    protected	$connection	=	'tpqi_cbd';
	protected	$table		=	'org_staff';
	public		$timestamps	=	false;
}
