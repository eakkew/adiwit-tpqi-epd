<?php

namespace Tpqi;

use Zizaco\Entrust\EntrustPermission;

class Permission extends EntrustPermission
{
  //public		$timestamps = false;
  protected $fillable = array('name');
}
