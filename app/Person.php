<?php namespace Tpqi;

use Illuminate\Database\Eloquent\Model;
use \Auth;

class Person extends Model {

  protected $table = 'person';
  protected $primaryKey = 'personID';

  // Computed Properties
  public function getFullNameAttribute() {
    if (Auth::check()) {
      return Auth::user()->givenName . ' ' . Auth::user()->familyName;
    } else {
      return 'UAuthorised Person';
    }
  }
}