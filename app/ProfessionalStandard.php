<?php namespace Tpqi;

use Illuminate\Database\Eloquent\Model;

class ProfessionalStandard extends Model {
	
	protected	$connection	=	'tpqi_occ';
	protected	$table			=	'professional_standard';
	public		$timestamps	=	false;

	// Relationships
	public function levelCompetence() {
		// stdType = 'P' is LevelCompetence
		return $this->hasMany('Tpqi\LevelCompetence', 'stdID', 'stdID');
	}

	public function industryGroup() {
		return $this->belongsTo('Tpqi\Industry', 'indID', 'indGRPID');
	}

	public function industry() {
		return $this->belongsTo('Tpqi\IndustryGroupX', 'indID', 'indID');
	}	

	public function uoc() {
		return $this->hasMany('Tpqi\UOC', 'parent', 'stdID')->where('stdType', '=', 'U');
	}

	public function eoc() {
		return $this->hasMany('Tpqi\EOC', 'parent', 'stdID')->where('stdType', '=', 'E');
	}

	// Scope
	public function scopeUocType($query){
		return $query->where('stdType', '=', 'U');
	}
	public function scopeEocType($query){
		return $query->where('stdType', '=', 'E');
	}
	public function scopeLevelType($query){
		return $query->where('stdType', '=', 'P');
	}
}