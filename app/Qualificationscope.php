<?php

namespace Tpqi;

use Illuminate\Database\Eloquent\Model;

class Qualificationscope extends Model
{
    protected	$connection	=	'tpqi_cbd';
	protected	$table		=	'org_qualificationscope';
	public		$timestamps	=	false;

	// Relationships
	public function qualificationScope() {
		return $this->belongsTo('Tpqi\Organization', 'orgID', 'FK_orgID');
	}
}
