<?php namespace Tpqi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Request;

class Question extends Model {
	use SoftDeletes;
	
	protected	$connection		= 'adiwit_tpqi';
	protected	$table			= 'fact_questions';
	protected	$fillable		= [
		'id',
		'industry_id',
		'level_competence_name',
		'eoc_id',
		'question_group_id',
		'content',
		'difficulty',
		'type',
		'time_needed',
		'is_approved'
	];

    protected static function boot() {
        parent::boot();

        // static::creating(function($question) {
        // 	$date = Date::find($question->date_id);
        // 	if ($date == null) Date::generate(2020);
        // });

        static::deleting(function($question) {
             $question->answers()->delete();
        });
    }
    // getter
    public function getCountExamQuestionsTrashAttribute() {
        return $this->examQuestionsTrash->count();
    }

    public function getCreateAtThaiAttribute() {
        $dates = explode(' ', $this->created_at);
        $date = '';
        if(count($dates) > 1){
            $date = explode('-', $dates[0]);
            $date = $date[2] . '/' . $date[1] . '/' . ($date[0]+543); 
            // $date = $date[2] . '/' . $date[1] . '/' . ($date[0]+543) . ' ' . $dates[1]; 
        }
        return $date;
    }

    public function getDeleteAtThaiAttribute() {
        $dates = explode(' ', $this->deleted_at);
        $date = '';
        if(count($dates) > 1){
            $date = explode('-', $dates[0]);
            $date = $date[2] . '/' . $date[1] . '/' . ($date[0]+543) . ' ' . $dates[1]; 
        }
        return $date;
    }

    public function getLastestTHAttribute() {
        $dates = explode(' ', $this->lastest);
        $date = '';
        if(count($dates) > 1){
            $date = explode('-', $dates[0]);
            $date = $date[2] . '/' . $date[1] . '/' . ($date[0]+543) . ' ' . $dates[1]; 
        }
        return $date;
    }

    /**
     * Relationship
     */
    public function answers() {
    	return $this->hasMany('Tpqi\Answer', 'question_id', 'id');
    }

    public function answersTrash() {
    	return $this->hasMany('Tpqi\Answer', 'question_id', 'id')->withTrashed();
    }

	public function industryGroup() {
		return $this->belongsTo('Tpqi\Industry', 'industry_id', 'indGRPID');
	}

	public function industry() {
		return $this->belongsTo('Tpqi\IndustryGroupX', 'industry_id', 'indID');
	}

	public function levelCompetence() {
		return $this->belongsTo('Tpqi\LevelCompetence', 'level_competence_name', 'levelName');
	}

    public function uoc() {
        return $this->belongsTo('Tpqi\Uoc', 'eoc_id', 'stdID');
    }

	public function eoc() {
		return $this->belongsTo('Tpqi\Eoc', 'eoc_id', 'stdID');
	}

	public function examQuestions() {
		return $this->hasMany('Tpqi\ExamQuestion', 'question_id', 'id');
	}

    public function examQuestionsTrash() {
        return $this->hasMany('Tpqi\ExamQuestion', 'question_id', 'id')->withTrashed();
    }

	// Scope
	public function scopeEocId($query, $eocID, $fromDate, $toDate)
    {
    	$from = ($fromDate && $toDate)? ($fromDate['year']-543).'-'.$fromDate['month'].'-'.$fromDate['day'] : '';
	    $to = ($fromDate && $toDate)? ($toDate['year']-543).'-'.$toDate['month'].'-'.$toDate['day'] : '';
    	if($eocID){
    		if($from && $to){
    			return $query->whereIn('eoc_id', $eocID)->where('type', 'eoc')->whereBetween('created_at',[$from, $to]);
    		}else{
    			return $query->whereIn('eoc_id', $eocID)->where('type', 'eoc');
    		}
    	}else{
    		return $query;
    	}
    }

    public function scopeUocId($query, $uocID, $fromDate, $toDate)
    {
    	$from = ($fromDate && $toDate)? ($fromDate['year']-543).'-'.$fromDate['month'].'-'.$fromDate['day'] : '';
	    $to = ($fromDate && $toDate)? ($toDate['year']-543).'-'.$toDate['month'].'-'.$toDate['day'] : '';
	    if($uocID){
	    	if($from && $to){
    			return $query->whereIn('eoc_id', $uocID)->where('type', 'uoc')->whereBetween('created_at',[$from, $to]);
    		}else{
    			return $query->whereIn('eoc_id', $uocID)->where('type', 'uoc');
    		}
	    }else{
	    	return $query;
	    }
    }

    public function scopeLevelId($query, $levelID, $fromDate, $toDate)
    {
    	$from = ($fromDate && $toDate)? ($fromDate['year']-543).'-'.$fromDate['month'].'-'.$fromDate['day'] : '';
	    $to = ($fromDate && $toDate)? ($toDate['year']-543).'-'.$toDate['month'].'-'.$toDate['day'] : '';
    	if($levelID){
    		if($from && $to){
    			return $query->whereIn('level_competence_name', $levelID)->where('type', 'level')->whereBetween('created_at',[$from, $to]);
    		}else{
    			return $query->whereIn('level_competence_name', $levelID)->where('type', 'level');
    		}
    	}else{
    		return $query;
    	}
    }
}