<?php

namespace Tpqi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Request;

class QuestionEocs extends Model
{
    use SoftDeletes;
	
	protected	$connection		= 'adiwit_tpqi';
	protected	$table			= 'fact_questions';
	protected	$fillable		= [
		'id',
		'question_id',
		'uoc_id',
		'eoc_id',
	];

	// Relation
	public function question() {
    	return $this->belongsTo('Tpqi\Question', 'id', 'question_id');
    }

	public function eoc() {
		return $this->belongsTo('Tpqi\Eoc', 'eoc_id', 'stdID');
	}

	// Scope
	public function aa($difficulty) {
		return 'aa';
	}
}
