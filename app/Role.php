<?php

namespace Tpqi;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
   public		$timestamps = false;
   protected	$fillable = ['name'];
}
