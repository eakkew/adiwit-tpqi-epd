<?php namespace Tpqi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Request;

class TemplateHeader extends Model {
	use SoftDeletes;
	
	protected	$connection		= 'adiwit_tpqi';
	protected	$table			= 'dim_templates';
	protected	$fillable		= [
		'id',
		'name',
		'max_difficulty',
		'max_time_needed',
		'industry_id',
		'level_competence_name',
		'type'
	];

    protected static function boot() {
        parent::boot();

        static::deleting(function($templateHeader) {
             $templateHeader->templateItems()->delete();
        });
    }

    /**
	 * Custom Attributes
	 */
	public function getCountExamAttribute()
	{
		return ($this->countByExam)? $this->countByExam->count : 0;
	}

	public function getIndustryDataAttribute()
	{
		if($this->industry){
			return $this->industry;
		}elseif($this->industryGroup){
			return $this->industryGroup;
		}
	}

    /**
     * Relationship
     */
	public function templateItems() {
		return $this->hasMany('Tpqi\TemplateItem', 'template_id', 'id');
	}

	public function templateItemsTrash() {
		return $this->hasMany('Tpqi\TemplateItem', 'template_id', 'id')->withTrashed();
	}

	public function industryGroup() {
		return $this->belongsTo('Tpqi\Industry', 'industry_id', 'indGRPID');
	}

	public function industry() {
		return $this->belongsTo('Tpqi\IndustryGroupX', 'industry_id', 'indID');
	}

	public function levelCompetence() {
		return $this->belongsTo('Tpqi\LevelCompetence', 'level_competence_name', 'levelName');
	}

	public function exams() {
		return $this->hasMany('Tpqi\Exam', 'template_id', 'id');	
	}

	public function questionsByLevel() {
		return $this->hasMany('Tpqi\Question', 'level_competence_name', 'level_competence_name')->where('type', 'level');
	}

	/**
	 * Count Relations
	 */
	public function countByExam()
	{
	    return $this->hasMany('Tpqi\Exam', 'template_id', 'id')->selectRaw('count(*) as count')->groupBy('template_id');
	}
	
	public function countByTemplateItems($eocID, $from, $to)
	{
		// dd($from . " : " . $to);
		if($eocID){
			if($from && $to){
				return $this->hasMany('Tpqi\TemplateItem', 'template_id', 'id')->whereIn('eoc_id', $eocID)->whereBetween('created_at',[$from, $to])->groupBy('template_id');
			}else{
				return $this->hasMany('Tpqi\TemplateItem', 'template_id', 'id')->whereIn('eoc_id', $eocID)->groupBy('template_id');
			}
		}else{
			return $this->hasMany('Tpqi\TemplateItem', 'template_id', 'id')->groupBy('template_id');
		}
	}

	// Scope
	public function scopeEocId($query, $eocID, $fromDate, $toDate)
    {
    	$from = ($fromDate && $toDate)? ($fromDate['year']-543).'-'.$fromDate['month'].'-'.$fromDate['day'] : '';
	    $to = ($fromDate && $toDate)? ($toDate['year']-543).'-'.$toDate['month'].'-'.$toDate['day'] : '';
    	if($eocID){
    		if($from && $to){
    			return $query->where('type', 'eoc')->whereHas('templateItems', function($q) use ($eocID, $from, $to) {
					$q->whereIn('eoc_id', $eocID)->whereBetween('created_at',[$from, $to]);
				});
    		}else{
    			return $query->where('type', 'eoc')->whereHas('templateItems', function($q) use ($eocID) {
					$q->whereIn('eoc_id', $eocID);
				});
    		}
    	}else{
    		return $query;
    	}
    }

    public function scopeUocId($query, $uocID, $fromDate, $toDate)
    {
    	$from = ($fromDate && $toDate)? ($fromDate['year']-543).'-'.$fromDate['month'].'-'.$fromDate['day'] : '';
	    $to = ($fromDate && $toDate)? ($toDate['year']-543).'-'.$toDate['month'].'-'.$toDate['day'] : '';
    	if($uocID){
    		if($from && $to){
    			return $query->where('type', 'uoc')->whereHas('templateItems', function($q) use ($uocID, $from, $to) {
					$q->whereIn('uoc_id', $uocID)->whereBetween('created_at',[$from, $to]);
				});
    		}else{
    			return $query->where('type', 'uoc')->whereHas('templateItems', function($q) use ($uocID) {
					$q->whereIn('uoc_id', $uocID);
				});
    		}
    	}else{
    		return $query;
    	}
    }

    public function scopeLevelId($query, $levelID, $fromDate, $toDate)
    {
    	$from = ($fromDate && $toDate)? ($fromDate['year']-543).'-'.$fromDate['month'].'-'.$fromDate['day'] : '';
	    $to = ($fromDate && $toDate)? ($toDate['year']-543).'-'.$toDate['month'].'-'.$toDate['day'] : '';
    	if($levelID){
    		if($from && $to){
    			return $query->where('type', 'level')->whereIn('level_competence_name', $levelID)->whereBetween('created_at',[$from, $to]);
    		}else{
    			return $query->where('type', 'level')->whereIn('level_competence_name', $levelID);
    		}
    	}else{
    		return $query;
    	}
    }
}