<?php namespace Tpqi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\ServiceProvider;

class TemplateItem extends Model {
	use SoftDeletes;
	
	protected	$connection		= 'adiwit_tpqi';
	protected	$table			= 'fact_template_items';
	protected	$fillable		= [
			'template_id',
			'uoc_id',
			'eoc_id',
			'amount'
		];

	/**
	 * Relationships
	 */
	public function templateHeader(){
		return $this->hasOne('Tpqi\TemplateHeader', 'id', 'template_id');
	}

	public function uoc() {
		return $this->hasOne('Tpqi\Uoc', 'stdID', 'uoc_id');
	}

	public function eoc() {
		return $this->hasOne('Tpqi\Eoc', 'stdID', 'eoc_id');
	}

	public function question() {
		return $this->hasMany('Tpqi\Question', 'eoc_id', 'eoc_id');	
	}

	public function questionByUoc() {
		return $this->hasMany('Tpqi\Question', 'eoc_id', 'uoc_id');	
	}
	
}