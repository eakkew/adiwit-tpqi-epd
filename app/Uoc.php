<?php namespace Tpqi;

use Illuminate\Database\Eloquent\Model;

class Uoc extends Model {
	
	protected	$connection	=	'tpqi_occ';
	protected	$table		=	'professional_standard';
	protected	$primaryKey	=	'stdID';
	public		$timestamps	=	false;

	/**
	 * Custom Attributes
	 */
	public function getEocAttributes() {
		return $this->levelCompetence->eoc;
	}

	/**
	 * Relationships
	 */
	public function levelCompetence() {
		return $this->belongsTo('Tpqi\LevelCompetence', 'stdID', 'uocID');
	}

	public function uocTpqi() {
		return $this->hasOne('Tpqi\uocTpqi', 'uocID', 'stdID');
	}

	public function eocs() {
		return $this->hasMany('Tpqi\Eoc', 'parent', 'stdID');
	}

	public function questionsUOC() {
		return $this->hasMany('Tpqi\Question', 'eoc_id', 'stdID')->where('type', 'uoc');
	}

	public function questionsUOCTrash() {
		return $this->hasMany('Tpqi\Question', 'eoc_id', 'stdID')->where('type', 'uoc')->withTrashed();
	}

	public function questionsEOC() {
		return $this->hasMany('Tpqi\Question', 'eoc_id', 'stdID')->where('type', 'eoc');
	}

	// Scope
	public function scopeUocType($query){
		return $query->where('stdType', '=', 'U');
	}

}