<?php 
##### generate question to Exam #####
if(Auth::user()->hasRole(['view'])) return redirect()->action('ExamController@index');
// dd($request->all());
$examDate = explode('/', $request->input('examDate'));
$allInputs = $request->all();
$dateID = Date::where('day', '=', $examDate[0])->where('month', '=', $examDate[1])->where('year', '=', $examDate[2]-543)->first();
if(!isset($dateID)){
	Date::generate($examDate[2]-543, $examDate[1], $examDate[0]);
	$dateID = Date::where('day', '=', $examDate[0])->where('month', '=', $examDate[1])->where('year', '=', $examDate[2]-543)->first();
}
$allInputs['date_id'] = $dateID->id;
$templateHeader = TemplateHeader::find($request->input('template_id'));
// dd($templateHeader->toArray());

$i = 0;
while($i < $allInputs['amount']){ // loop number of set u want to generate

	// find number of set of generate in this time , find type of exam and create dim_exam
	$allInputs['set'] = count(Exam::where('template_id', '=', $request->input('template_id'))->lists('id'))+1;
	$allInputs['type'] = ($templateHeader->type == 'level')? 1 : (($templateHeader->type == 'uoc')? 2 : 3);
	$exam = Exam::create($allInputs);

	if($i == 0){ // generate question from fact_question only first loop ( only first set of this time )
		$examQuestions = [];
		// Generate Questions to ExamQuestion
		/**
		 * This loop order by levelCompetence, Uoc or Eoc from templateItem
		 * templateHeader is one - to - many with templateItem
		 * 1 templateItem has 1 Uoc/Eoc
		 */
		foreach ($templateHeader->templateItems as $templateItem) {
			// Get array of questionID by random
			if($templateItem->templateHeader->type == 'level'){

				if($templateHeader->max_difficulty > 0){
					$questionsID = Question::where('level_competence_name', '=', $templateItem->templateHeader->level_competence_name)->where('difficulty', '<=', $templateHeader->max_difficulty);
				}else{
					$questionsID = Question::where('level_competence_name', '=', $templateItem->templateHeader->level_competence_name);
				}
				$questionsID = $questionsID->where('type', 'level')->orderByRaw("RAND()")->limit($templateItem->amount)->get();
			}elseif($templateItem->templateHeader->type == 'uoc'){
				if($templateHeader->max_difficulty > 0){
					$questionsID = Question::where('eoc_id', '=', $templateItem->uoc->stdID)->where('difficulty', '<=', $templateHeader->max_difficulty);
				}else{
					$questionsID = Question::where('eoc_id', '=', $templateItem->uoc->stdID);
				}
				$questionsID = $questionsID->where('type', 'uoc')->orderByRaw("RAND()")->limit($templateItem->amount)->get();
			}else{
				if($templateHeader->max_difficulty > 0){
					$questionsID = Question::where('eoc_id', '=', $templateItem->eoc->stdID)->where('difficulty', '<=', $templateHeader->max_difficulty);
				}else{
					$questionsID = Question::where('eoc_id', '=', $templateItem->eoc->stdID);
				}
				$questionsID = $questionsID->where('type', 'eoc')->orderByRaw("RAND()")->limit($templateItem->amount)->get();
			}

			// insert questionID to examQuestion
			foreach($questionsID as $questionID){
				$examQuestions[] = new ExamQuestion([ 'question_id'	=>	$questionID->id ]);
			}
			$exam->examQuestions()->saveMany($examQuestions);
		}

		// Generate Answer to ExamAnswer
		foreach($exam->examQuestions as $examQuestion){
			$examAnswersData = [];
			$limitAnswer = ($examQuestion->question->answers->count() > 5)? 5 : $examQuestion->question->answers->count();
			$arrAnswers = $examQuestion->question->answers->random($limitAnswer);
			$flagCorrect = false;
			foreach($arrAnswers as $answer){
				$flagCorrect = ($answer->is_correct_answer)? true : $flagCorrect;
				$examAnswersData[] = new ExamAnswer([
					'answer_id' => $answer->id ,
					'correct_answer' => $answer->is_correct_answer
				]);
			}
			if(!$flagCorrect){
				$correctAnswer = $examQuestion->question->answers()->where('is_correct_answer', '=', '1')->first();
				$randomCorrect = rand(0,($limitAnswer-1));
				$examAnswersData[$randomCorrect] = new ExamAnswer([
					'answer_id' => $correctAnswer->id,
					'correct_answer' => $correctAnswer->is_correct_answer
				]);
			}
			$examQuestion->examAnswers()->saveMany($examAnswersData);
		}

		$firstExam = $exam;
	}else{ // generate other set by re-order from question of first loop ( first set of this time )
		$examQuestions = [];
		// Generate Question to ExamQuestion from exam first set
		foreach($firstExam->examQuestions->shuffle() as $examQuestion){
			$examQuestions[] = new ExamQuestion([ 'question_id'	=>	$examQuestion->question_id ]);
		}
		$exam->examQuestions()->saveMany($examQuestions);

		// Generate Answer to ExamAnswer
		foreach($firstExam->examQuestions as $examQuestion){
			$examAnswersData = [];
			foreach($examQuestion->examAnswers->shuffle() as $answer){
				$examAnswersData[] = new ExamAnswer([
					'answer_id' => $answer->answer_id,
					'correct_answer' => $answer->correct_answer
				]);
			}
			$exam->examQuestions->where('question_id', $examQuestion->question_id)->first()->examAnswers()->saveMany($examAnswersData);
		}
	}

	$i++;
}