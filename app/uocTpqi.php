<?php namespace Tpqi;

use Illuminate\Database\Eloquent\Model;

class UocTpqi extends Model {
	
	protected	$connection	=	'tpqi_occ';
	protected	$table		=	'uoc_tpqi';
	public		$timestamps	=	false;

	/**
	 * Relationships
	 */
	public function uoc() {
		return $this->belongsTo('Tpqi\Uoc', 'uocID', 'stdID');
	}

}