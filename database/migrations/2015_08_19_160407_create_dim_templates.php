<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimTemplates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('industry_id', 3);
            $table->string('level_competence_name');
            // $table->tinyInteger('type'); // 1 : level , 2 : uoc , 3 : eoc
            $table->enum('type', ['level', 'uoc', 'eoc']);
            $table->decimal('max_difficulty', 5, 2);
            $table->integer('max_time_needed');
            $table->integer('buffer_time');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dim_templates');
    }
}
