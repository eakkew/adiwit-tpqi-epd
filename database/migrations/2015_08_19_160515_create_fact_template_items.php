<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactTemplateItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_template_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('template_id')->foreign('template_id')->references('id')->on('dim_templates');
            $table->integer('uoc_id');
            $table->integer('eoc_id');
            $table->integer('amount');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fact_template_items');
    }
}
