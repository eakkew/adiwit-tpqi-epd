<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimExams extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_exams', function (Blueprint $table) {
            $table->increments('id');
            $table->string('level_competence_name');
            $table->enum('type', ['level', 'uoc', 'eoc']);
            $table->integer('template_id')->foreign('template_id')->references('id')->on('dim_templates');
            $table->string('cb_id', 50);
            $table->integer('set');
            $table->integer('subset');
            $table->integer('used_pdf');
            $table->integer('date_id');
            $table->integer('hour');
            $table->integer('minute');
            $table->integer('print_count');
            $table->string('location', 85);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dim_exams');
    }
}
