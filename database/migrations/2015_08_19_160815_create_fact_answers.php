<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactAnswers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('question_id')->foreign('question_id')->references('id')->on('fact_questions');
            $table->longText('content');
            $table->boolean('is_correct_answer');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fact_answers');
    }
}
