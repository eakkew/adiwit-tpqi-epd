<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactExamQuestions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // exam_id
        // question_group_id
        // question_id
        Schema::create('fact_exam_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('exam_id')->foreign('exam_id')->references('id')->on('dim_exams');
            $table->integer('question_group_id')->foreign('question_group_id')->references('id')->on('dim_question_groups');
            $table->integer('question_id')->foreign('question_id')->references('id')->on('fact_questions');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fact_exam_questions');
    }
}
