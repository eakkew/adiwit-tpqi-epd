<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactQuestions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('fact_questions', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->integer('question_id');
        //     $table->integer('uoc_id');
        //     $table->integer('eoc_id');
        //     $table->timestamps();
        //     $table->softDeletes();
        // });
        Schema::create('fact_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('industry_id', 3);
            $table->string('level_competence_name');
            $table->integer('eoc_id');
            $table->longText('content');
            $table->decimal('difficulty', 5, 2);
            // $table->decimal('difficulty', 3, 2);
            // $table->enum('difficulty', ['none', 'easy', 'medium', 'hard']);
            // $table->tinyInteger('difficulty');
            // $table->tinyInteger('type'); // 1 : level , 2 : uoc , 3 : eoc
            $table->enum('type', ['level', 'uoc', 'eoc']);
            $table->integer('time_needed');
            $table->boolean('is_approved');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fact_questions');
    }
}
