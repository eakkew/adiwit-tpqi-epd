<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Carbon\Carbon;
use Tpqi\Date;

class CreateDimDates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_dates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('day');
            $table->integer('month');
            $table->integer('year');
        });

        $carbon = Carbon::createFromDate(date("Y"), 01, 01);
        for ($i=0; $i < 2190; $i++) { 
            Date::create([
                'day'   =>  $carbon->day,
                'month' =>  $carbon->month,
                'year'  =>  $carbon->year
            ]);
            $carbon->addDay();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dim_dates');
    }
}
