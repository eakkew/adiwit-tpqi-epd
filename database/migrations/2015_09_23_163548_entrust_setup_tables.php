<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

use Tpqi\Role;
use Tpqi\Permission;
use Tpqi\User;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Zizaco\Entrust\EntrustRole;

class EntrustSetupTables extends Migration
{
    use EntrustUserTrait;
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        // Create table for storing roles
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('display_name')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
        });

        // Create table for associating roles to users (Many-to-Many)
        Schema::create('role_user', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->integer('role_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on('roles')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['user_id', 'role_id']);
        });

        // Create table for storing permissions
        Schema::create('permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('display_name')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
        });

        // Create table for associating permissions to roles (Many-to-Many)
        Schema::create('permission_role', function (Blueprint $table) {
            $table->integer('permission_id')->unsigned();
            $table->integer('role_id')->unsigned();

            $table->foreign('permission_id')->references('id')->on('permissions')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on('roles')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['permission_id', 'role_id']);
        });

        // Role
        $masterRole     = Role::create(['name' => 'master']);   // All
        $creatorRole    = Role::create(['name' => 'creator']);  // Create | Update
        $viewRole       = Role::create(['name' => 'view']);     // Print

        // Role Menu
        $questionIndexPermission    = Permission::create(['name' => 'question-index']);
        $questionCreatePermission   = Permission::create(['name' => 'question-create']);

        $templateIndexPermission    = Permission::create(['name' => 'template-index']);
        $templateCreatePermission   = Permission::create(['name' => 'template-create']);

        $examIndexPermission        = Permission::create(['name' => 'exam-index']);
        $examCreatePermission       = Permission::create(['name' => 'exam-create']);
        $examPrintPermission        = Permission::create(['name' => 'exam-print']);

        $reportIndexPermission      = Permission::create(['name' => 'report-index']);
        $reportPrintPermission      = Permission::create(['name' => 'report-print']);

        // Access Control
        $masterRole->attachPermission($questionIndexPermission);
        $masterRole->attachPermission($questionCreatePermission);
        $masterRole->attachPermission($templateIndexPermission);
        $masterRole->attachPermission($templateCreatePermission);
        $masterRole->attachPermission($examIndexPermission);
        $masterRole->attachPermission($examCreatePermission);
        $masterRole->attachPermission($examPrintPermission);
        $masterRole->attachPermission($reportIndexPermission);
        $masterRole->attachPermission($reportPrintPermission);

        $creatorRole->attachPermission($questionIndexPermission);
        $creatorRole->attachPermission($questionCreatePermission);
        $creatorRole->attachPermission($templateIndexPermission);
        $creatorRole->attachPermission($templateCreatePermission);
        $creatorRole->attachPermission($examIndexPermission);
        $creatorRole->attachPermission($examCreatePermission);
        $creatorRole->attachPermission($reportIndexPermission);

        $viewRole->attachPermission($examIndexPermission);
        $viewRole->attachPermission($examPrintPermission);
        $viewRole->attachPermission($reportIndexPermission);
        $viewRole->attachPermission($reportPrintPermission);

        // Attach Role to User :: 3 => master, 2 => view, 1 => creator
         // Call to undefined method Illuminate\Database\Query\Builder::attachRole()
        User::find(1)->attachRole(Role::where('name', '=', 'creator')->first());
        User::find(2)->attachRole(Role::where('name', '=', 'view')->first());
        User::find(3)->attachRole(Role::where('name', '=', 'master')->first());
        

    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('permission_role');
        Schema::drop('permissions');
        Schema::drop('role_user');
        Schema::drop('roles');
    }
}
