<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
        // $this->call(DimTemplatesTableSeeder::class);
        // $this->call(FactTemplateItemsTableSeeder::class);
        // $this->call(FactQuestionsTableSeeder::class); //use only this seeder.
        // $this->call(FactAnswersTableSeeder::class);
        // $this->call(DimExamTableSeeder::class);
        // $this->call(FactExamQuestionsTableSeeder::class);
        // $this->call(FactExamAnswerTableSeeder::class);
        Model::reguard();
	}

}
