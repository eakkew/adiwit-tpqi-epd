<?php

use Illuminate\Database\Seeder;

use Tpqi\TemplateHeader;
use Tpqi\Organization;
use Tpqi\Exam;

class DimExamTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker\Factory::create();
    	$templates = TemplateHeader::all();
		foreach($templates as $template){
			$limitExam = rand(0,5);
			$i = 0;
			while($i < $limitExam){
				$cbID = Organization::whereHas('qualificationScope', function ($query) use ($template) {
				            $query->where('description', '=', $template->level_competence_name);
				        })->orderByRaw('RAND()')->first();
				if($cbID){
					$id = $cbID->orgID;
				}else{
					$id = '';
				}
				Exam::create(array(
		          	'date_id' => rand(1,365),
		          	'level_competence_name' => $template->level_competence_name,
					'template_id' => $template->id,
					'cb_id' => $id,
					'set' => $i+1,
					'hour' => rand(0,24),
					'minute' => rand(0,59),
					'location' => $faker->address,
		        ));
		        $i++;
			}
		}
    }
}
