<?php

use Illuminate\Database\Seeder;

use Tpqi\LevelCompetence;
use Tpqi\TemplateHeader;

class DimTemplatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$limit = 20;
    	$faker = Faker\Factory::create();
    	$levelCompetences = LevelCompetence::orderByRaw('RAND()')->groupBy('uocID')->limit($limit)->get();
    	foreach ($levelCompetences as $levelCompetence) {
    		TemplateHeader::create(array(
				'name' => $faker->domainWord,
				'industry_id' => $levelCompetence->professionalStandard->indID,
				'level_competence_name' => $levelCompetence->levelName,
                'max_difficulty' => 0,
                'type' => rand(1,3),
				// 'max_difficulty' => rand(7,10)/10,
				'max_time_needed' => rand(60, 120),
			));
    	}
    }
}
