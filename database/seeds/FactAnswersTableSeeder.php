<?php

use Illuminate\Database\Seeder;

use Tpqi\Question;
use Tpqi\Answer;

class FactAnswersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker\Factory::create();
    	$questionsID = Question::all();
    	foreach($questionsID as $questionID)
    	{
    		$limitAnswer = rand(4,10);
    		$flagAnswer = rand(1,$limitAnswer);
    		foreach( range(1,$limitAnswer) as $i)
    		{
                $flagPicture = rand(0,4);
    			$correct = ($flagAnswer == $i)? 1 : 0;
    			Answer::create(array(
					'question_id' => $questionID->id,
                    'content' => ($flagPicture)? $faker->text : '<img src="http://lorempixel.com/120/100" />' ,
					'is_correct_answer' => $correct,
				));
    		}
    	}
    }
}
