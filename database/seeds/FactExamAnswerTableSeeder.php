<?php

use Illuminate\Database\Seeder;

use Tpqi\ExamQuestion;
use Tpqi\ExamAnswer;

class FactExamAnswerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $examQuestions = ExamQuestion::all();
		foreach($examQuestions as $examQuestion){
			$examAnswersData = [];
			$limitAnswer = ($examQuestion->question->answers->count() > 5)? 5 : $examQuestion->question->answers->count();
			$arrAnswers = $examQuestion->question->answers->random($limitAnswer);
			$flagCorrect = false;
			foreach($arrAnswers as $answer){
				$flagCorrect = ($answer->is_correct_answer)? true : $flagCorrect;
				$examAnswersData[] = new ExamAnswer([
					'answer_id' => $answer->id ,
					'correct_answer' => $answer->is_correct_answer
				]);
			}
			if(!$flagCorrect){
				$correctAnswer = $examQuestion->question->answers()->where('is_correct_answer', '=', '1')->first();
				$randomCorrect = rand(0,($limitAnswer-1));
				$examAnswersData[$randomCorrect] = new ExamAnswer([
					'answer_id' => $correctAnswer->id,
					'correct_answer' => $correctAnswer->is_correct_answer
				]);
			}
			$examQuestion->examAnswers()->saveMany($examAnswersData);
		}
        
    }
}
