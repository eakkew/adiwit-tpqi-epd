<?php

use Illuminate\Database\Seeder;

use Tpqi\Exam;
use Tpqi\TemplateHeader;
use Tpqi\Question;
use Tpqi\ExamQuestion;

class FactExamQuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$exams = Exam::orderByRaw('RAND()')->get();
		foreach($exams as $exam){
			$templateHeader = TemplateHeader::find($exam->template_id);
			foreach($templateHeader->templateItems as $templateItem){
				// if($templateHeader->max_difficulty > 0){
					// $questions = Question::where('eoc_id', '=', $templateItem->eoc_id)->where('difficulty', '<=', $templateHeader->max_difficulty)->orderByRaw('RAND()')->limit($templateItem->amount)->get();
				// }else{
					$questions = Question::where('eoc_id', '=', $templateItem->eoc_id)->orderByRaw('RAND()')->limit($templateItem->amount)->get();
				// }
				foreach($questions as $question){
					ExamQuestion::create(array(
				      	'exam_id' => $exam->id,
						'question_id' => $question->id
				    ));
				}
			}
		}
    }
}
