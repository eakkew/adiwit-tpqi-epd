<?php

use Illuminate\Database\Seeder;

use Tpqi\TemplateItem;
use Tpqi\TemplateHeader;
use Tpqi\Question;
use Tpqi\Answer;

class FactQuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// for test data
        // $limit = rand(4,10);
        // $faker = Faker\Factory::create();
        // $TemplateItems = TemplateItem::all();

        // foreach ($TemplateItems as $TemplateItem) {
        //     if($TemplateItem->amount > $limit) $limit = $TemplateItem->amount;
        //     foreach (range(1,$limit) as $i) 
        //     {
        //         $flagPicture = rand(0,4);
        //         Question::create(array(
        //             'industry_id' => $TemplateItem->templateHeader->industry_id,
        //             'level_competence_name' => $TemplateItem->templateHeader->level_competence_name,
        //             'eoc_id' => $TemplateItem->eoc_id,
        //             'content' => ($flagPicture)? $faker->catchPhrase : '<img src="http://lorempixel.com/400/200" />' ,
        //             'type' => rand(1,3),
        //             'difficulty' => $difficulties[rand(0,2)],
        //             'time_needed' => rand(1,5),
        //             'is_approved' => rand(0,1),
        //         ));
        //     }
        // }

        // for UAT
        $exQuestions = [];
        $exAnswers = [];
        // exampleQuestion
        $exQuestions['exampleQuestion'] = [
            'รูปเรขาคณิตสามมิติใดเป็นพีระมิด',
            'มีผู้โดยสารเครื่องบินระหว่างประเทศ สองร้อยเจ็ดสิบล้านเก้าหมื่นสี่พันหนึ่งร้อยหกสิบห้าคน จำนวนผู้โดยสารเขียนเป็นตัวเลขได้อย่างไร',
            '<p><img src="http://www.trueplookpanya.com/data/product/blog/images/upload_4/folder_King_Math/upconts/o-net(Math)P6_2553(3).jpg" style="width: 450px;"></p>จากรูปข้างบน วิรัชต้องการสร้างปริซึมห้าเหลี่ยม แต่รูปเรขาคณิตสองมิติที่มีอยู่ยังไม่เพียงพอ วิรัชต้องหารูปในข้อใดมาเพิ่มอีกจึงจะสร้างได้',
            'มีลูกอมรสนม 5 เม็ด รสส้ม 7 เม็ด รสสตรอว์เบอร์รี 4 เม็ด และรสมะนาว 3 เม็ด ถ้าสุ่มหยิบลูกอมครั้งละ 6 เม็ด จะมีโอกาสหยิบลูกอมชนิดใดได้เหมือนกันทั้ง 6 เม็ด',
            'ค่าประมาณใกล้เคียงจำนวนเต็มล้านของระยะทางจากดาวอังคารถึงดวงอาทิตย์ ซึ่งห่างกัน 227,940,000 กิโลเมตร เป็นเท่าใด',
            '<p><img src="http://www.trueplookpanya.com/data/product/blog/images/upload_4/folder_King_Math/upconts/o-net(Math)P6_2553(6).jpg" style="width: 450px;"></p>ผลบวกของส่วนที่ระบายสี เป็นเท่าไร',
            '0.175 เขียนเป็นเศษส่วนอย่างต่ำได้เท่าใด',
            'เรือสองลำแล่นออกจากท่าเรือเดียวกันพร้อมกัน เรือแต่ละลำแล่นด้วยความเร็วสม่ำเสมอชั่วโมงละ 100 กิโลเมตร เรือลำแรกออกจากท่าเรือไปทางทิศเหนือ เมื่อเวลาผ่านไปครึ่งชั่วโมงจึงเลี้ยวไปทางทิศตะวันออก ส่วนเรือลำที่สองออกจากท่าเรือไปทางทิศตะวันออก เมื่อเรือทั้งสองลำแล่นไปได้ 1 ชั่วโมง เรือลำแรกอยู่ทางทิศใดของเรือลำที่สอง',
            'วันดีนำแผ่นตารางเซนติเมตรมาตัด แล้วเรียงต่อกันเป็นรูปต่างกัน 3 แบบ ข้อใดแสดงการเปรียบเทียบพื้นที่ของรูปทั้ง 3 แบบได้อย่างถูกต้อง<img style="width: 450px;" src="http://www.trueplookpanya.com/data/product/blog/images/upload_4/folder_King_Math/upconts/o-net(Math)P6_2553(9).jpg">',
            '<p><span style="color: rgb(0, 0, 0); font-family: Times; line-height: normal;"><br></span><img src="http://www.trueplookpanya.com/data/product/blog/images/upload_4/folder_King_Math/upconts/o-net(Math)P6_2553(10).jpg" style="line-height: 1.42857; width: 198.5px; height: 127.481px;"></p>',
            'จำนวนใดเมื่อตัดตัวเลขศูนย์ออกแล้ว จำนวนนั้นมีค่าเพิ่มขึ้น',
            '<p><img style="width: 311.5px; height: 92.7578px;" src="http://www.trueplookpanya.com/data/product/blog/images/upload_4/folder_King_Math/upconts/o-net(Math)P6_2553(12).jpg"></p>',
            'วิรัตน์กู้เงิน 50,000 บาท เสียดอกเบี้ยอัตราร้อยละ 9 ต่อปี ถ้ากู้เงิน 292 วัน จะส่งต้นและดอกเบี้ยรวมเป็นเงินเท่าไร',
            'เอื้อยนำกระดาษรูปสี่เหลี่ยมผืนผ้ากว้าง 12 เซนติเมตร ยาว 15 เซนติเมตร วางเรียงติดกันให้เป็นรูปสี่เหลี่ยมจัตุรัสที่มีขนาดเล็กที่สุด รูปสี่เหลี่ยมจัตุรัสรูปนั้นมีขนาดเท่าใด',
            '<p><span style="color: rgb(0, 0, 0); font-family: Times; line-height: normal;">จำนวนผู้ใช้รถประจำทางในเวลา 6 เดือนแรกของปีจำนวนผู้โดยสาร (คน)</span></p>',
            'ออร์แกเนลล์ใดที่พบในเซลล์ของสิ่งมีชีวิตทุกอาณาจักร',
            'สิ่งมีชีวิตในข้อใดที่อาศัยอยู่ในสภาพแวดล้อมที่เป็นน้ำและมีความเข้มข้นของสารละลายในร่างกายใกล้เคียงกับของสิ่งแวดล้อมมากที่สุด',
            'การตรวจดูเส้นเลือดที่บริเวณหางของปลาด้วยกล้องจุลทรรศน์ เกณฑ์ในข้อใดใช้จำแนกเส้นเลือดใด เป็นเส้นอาร์เตอรีหรือเส้นเวนได้ดีที่สุด',
            'ข้าวโพดมีโครโมโซม 2n = 20 จำนวนโครโมโซมที่พบในโพลาร์นิวเคลียส สเปิร์มนิวเคลียสและไมโครสปอร์มาเทอร์เซลล์เป็นข้อใดตามลำดับ',
            'การส่งกระแสประสาทไปยังปลายแอกซอนเกี่ยวข้องกับการเคลื่อนที่ของประจุชนิดใดไปตามผิวด้านใดของเยื่อหุ้มเซลล์ประสาท',
            'โมเลกุลของสาร ATP มีความเหมาะสมในการลำเลียงพลังงานเนื่องจากข้อใด',
            'ในเซลล์ที่มีการสังเคราะห์โปรตีนปริมาณสูง พบสารใดมีปริมาณสูงด้วย ก. mRNA    ข. tRNA ค. rRNA ง. DNA',
            'แบบแผนใดที่แสดงได้เหมาะสมกับเพดดีกรีของครอบครัวที่แสดง',
            'การสืบพันธุ์ตามธรรมชาติของสิ่งมีชีวิต กลุ่มใดก่อให้เกิดการโคลน (cloning) ก. พารามีเซียม    ข. ไฮดรา ค. ปลาดาว',
            'จากการผสมระหว่าง Aa BB Dd กับ Aa bb Ce DD โอกาสที่จะได้ลูกมีฟีโนไทป์เด่นทั้งหมดเท่าใด',
            'กล้องใดที่มองเห็นเซลล์เม็ดเลือดแดงขนาดเท่า ๆ กัน',
            'เซลล์ชนิดใดไม่มี microfilament ก. E. coli  ข. Anabaena (bluegreen algae) ค. Spirogyra  ง. Amoeba',
            'ลักษณะสีขนของกระต่ายถูกควบคุมด้วย 4 แอลลีล แอลลีลที่ควบคุมสีขนในเซลล์ผิวหนัง และเซลล์สืบพันธุ์มีจำนวนเท่าใด',
            'ข้อใดถูกต้องเกี่ยวกับเซลล์ร่างกายของสิ่งมีชีวิตชั้นสูง',
            'ข้อสรุปในข้อใดเป็นจริง',
            'ในการยกน้ำหนักดังภาพ แรงที่เกิดจากการหดตัวของกล้ามเนื้อเท่ากับเท่าใด และเกิดจากการทำงานของกล้ามเนื้อใด',
            'หอยทากชนิดหนึ่งมีเปลือกสีน้ำตาลเป็นลักษณะด้อย เปลือกสีแดงเป็นลักษณะเด่น ถ้าประชากรหอยทาก 1,000 ตัว มีหอยทากเปลือกสีน้ำตาล 90 ตัว จำนวนหอยทากที่เป็นเฮเทอโรไซโกตมีจำนวนเท่าใด เมื่อเข้าสู่สมดุล ฮาร์ดี-ไวน์เบอร์ก',
            'การหมุนเวียนของธาตุชนิดใดในระบบนิเวศ มีต้นกำเนิดมาจากหิน ก. ไนโรเจน    ข. ฟอสฟอรัส ค. คาร์บอน  ง. กำมะถัน',
            'ข้อใดจัดเป็นตัวอย่างของการแยกกันทางการสืบพันธุ์หลังระยะไซโกต ก. ลูกผสมที่เป็นหมัน โดยเกิดมาจากการผสมพันธุ์ของสิ่งมีชีวิต 2 ชนิด ในจีนัสเดียวกัน ข. ลูกผสมที่เกิดมาแล้วตายหลังคลอด โดยเกิดมาจากการผสมพันธุ์ของสิ่งมีชีวิต 2 ชนิด ในจีนัสเดียวกัน ค. ไม้พุ่มในจีนัสเดียวกัน กลุ่มหนึ่งอาศัยในดินเปรี้ยวอีกกลุ่มอาศัยอยู่ในดินต่างหรือดินเค็ม ง. กบ 2 ชนิด มีเสียงร้องเรียกการผสมพันธุ์แตกต่างกัน',
            'กลุ่มสิ่งมีชีวิตในข้อใดไม่จัดเป็นผู้ผลิตเบื้องต้น',

            'เซลล์กัลวานิกในข้อใดสามารถเกิดขึ้นได้เอง',
            'พิจาราณการให้เหตุผลเกี่ยวกับการรบกวนสมดุลของปฏิกิริยา A(g) + B(g) = C(g) ในระบบปิด 1)เพราะว่าการเพิ่มแก๊สมีตระกูลเช่นฮีเลียมทำให้ความดันรวมของระบบสูงขึ้น 2)ทำให้มีผลต่อสมดุลโดยทำให้สมดุลเลื่อนไปทางฝั่งที่มีโมลของแก๊สมากกว่า การให้เหตุผลข้างต้นมีข้อบกพร่องอย่างไร',
            'จากภาพเป็นกลไกการทำปฏิกิริยาของ LiAlH4 ในการเปลี่ยนสารประกอบคาร์บอนิลเป็นแอลกอฮอล์ รีเอเจนต์ที่ใกล้เคียงกับ LiAlH4 คือ LiBH4 และ NaAlH4 ตัวรีเอเจนต์จัดว่าเป็นตัวรีดิวซ์หรือออกซิไดซ์และข้อใดเปรียบเทียบความแรงของ รีเอเจนต์ได้ถูกต้องที่สุด',
            'ปรอท Hg (Z=80) เป็นโลหะชนิดเดียวที่เป็นของเหลวเหตุผลที่ใช้ในการอธิบายเหตุการ์ณนี้คืออิเล็กตรอนชั้นนอกสุดอยู่ใน ออร์บิทัลในคาบหลัง ๆ (เช่น 4f 4d ...) ซึ่งสามารถกันแรงกระทำของนิวเคลียสได้น้อยแต่ยังมีอีกเหตุผลที่ต้องใช้อธิบายประกอบเพราะว่ายังมีธาตุอีกจำนวนหนึ่งที่มีลักษณะนี้แต่ยังคงเป็นของแข็งอยู่ อยากทราบว่าอีกเหตุผลนั้นคืออะไรและทำไม 2 เหตุผลนี้จึงสามารถอธิบายได้ว่าทำไมปรอทเป็นโลหะที่เป็นของเหลว',
            'ทฤษฎี : ไอออนบวกที่มีขนาดใหญ่จะสร้างพันธะกับไอออนลบที่มีขนาดใหญ่เพราะมีการ Polarize หรือ การบิดเบี้ยวของรูปร่างจากการกระจายของประจุในลักษณะคล้ายกันจึงมีรูปร่างที่เหมาะสมในการสร้างพันธะกันในทำนองเดียวกันไอออนบวกที่มีขนาดเล็กจะสร้างพันธะกับไอออนลบที่มีขนาดเล็ก คำถาม Li Na K เกิดเป็นสารประกอบออกไซด์ที่เสถียรได้แตกต่างกันทั้งสามตัว (อาจเป็น oxide peroxide superoxide) สารประกอบที่น่าจะเกิดขึ้นมีสูตรโมเลกุลอย่างไร',
            'หากสังเกตเราจะพบว่าขวดที่เก็บสารละลายกรดไนตริกจะเป็นสีชาเพราะแสงจะเร่งให้เกิดปฏิกิริยาการสลายตัว ข้อใดเป็นปฏิกิริยาการสลายตัวดังกล่าว',
            'ในการศึกษาปฏิกิริยาหนึ่งพบว่าเมื่อเพิ่มอุณหภูมิ อัตราการเกิดปฏิกิริยาจะเกิดเร็วขึ้น ข้อกล่าวถูกต้องเกี่ยวกับปฏิกิริยานี้',
            'ในภาชนะที่บรรจุสารละลายผสมของ Fe2+ และ Fe3+ โดยเราใช้สารละลาย SnCl2 เป็นตัวเปลี่ยนให้มีไอออนเหล็กเพียงชนิดเดียวสังเกตโดยสีของสารละลายเปลี่ยนแต่ไม่เกิดตะกอนแต่ปริมาณ SnCl2 ที่เหลือจะทำให้การวัดปริมาณเหล็กผิดพลาดได้จึงต้องกำจัดโดยการใส่สารละลาย HgCl2 ลงไปซึ่งจะเกิดตะกอนสีขาวขึ้นแต่หากใส่ SnCl2 มากเกินไปตั้งแต่แรกก็จะทำให้เกิดตะกอนสีเทาแทน จากการวิเคราะห์ในครั้งนี้ สารใดบ้างเป็นตัวรีดิวซ์',
            'ให้ X (M.W. = 94) เป็นสารอินทรีย์ประเภทวงชนิดหนึ่งสารนี้ไม่มีโซ่ข้างหรือหมู่แทนที่เป็นหมู่ Alkyl เลยนำสาร X มาทำปฏิกิริยาเคมีต่าง ๆ ได้ผลดังนี้ 1) ทำปฏิกิริยากับ Na 2) เกิดปฏิกิริยากับ NaOH แต่ไม่เกิดกับ NaHCO3 3) ทำปฏิกิริยา Bromination ในอัตราส่วน 1 โมล X : 3 โมล Br2 เกิดเป็นตะกอน Y ขึ้นและเมื่อนำกระดาษลิมัสชื้นสีน้ำเงินไปอังปากหลอดทดลองกระดาษจะเปลี่ยนสีเป็นสีแดง ตะกอน Y มีมวลโมเลกุลเท่าไหร่ (Br=80)',
            'จากข้อ 9 หากนำ X ไปทำปฏิกิริยา hydrogenation ในสภาวะที่เหมาะสมได้ผลิตภัณฑ์ผสม 2 ชนิดคือ A (M.W. = 100) และ B (M.W. = 98) ถ้าต้องการแยกสาร A และ B โดยการกลั่นลำดับส่วนจะได้สารใดออกมาก่อน พร้อมบอกเหตุผลที่คิดว่ามีส่วนเกี่ยวข้องมากที่สุด ถ้าสารทั้งสองทำปฏิกิริยา Bromination ในอัตราส่วน 1 โมลสาร : 1 โมล Br2 เกิดแก๊สที่นำกระดาษลิมัสชื้นสีน้ำเงินไปอังปากหลอดทดลองกระดาษจะเปลี่ยนสีเป็นสีแดงเหมือนกัน',
        ];
        $exAnswers['exampleQuestion'] = [
            0 => [ '<p><img style="width: 186.601px; height: 182px;" src="http://www.trueplookpanya.com/data/product/blog/images/upload_4/folder_King_Math/upconts/o-net(Math)P6_2553(1-1).jpg"><br></p>', '<p><img style="width: 186.601px; height: 182px;" src="http://www.trueplookpanya.com/data/product/blog/images/upload_4/folder_King_Math/upconts/o-net(Math)P6_2553(1-2).jpg"><br></p>', '<p><img style="width: 214.5px; height: 125.984px;" src="http://www.trueplookpanya.com/data/product/blog/images/upload_4/folder_King_Math/upconts/o-net(Math)P6_2553(1-3).jpg"><br></p>', '<p><img style="width: 259.5px; height: 120.688px;" src="http://www.trueplookpanya.com/data/product/blog/images/upload_4/folder_King_Math/upconts/o-net(Math)P6_2553(1-4).jpg"><br></p>', 'ผิดทุกข้อ' ],
            1 => [ '207,094,165', '217,904,165', '270,904,165', '270,094,165', 'ผิดทุกข้อ', ],
            2 => [ '<p><img style="width: 281px;" src="http://www.trueplookpanya.com/data/product/blog/images/upload_4/folder_King_Math/upconts/o-net(Math)P6_2553(3-1).jpg"><br></p>', '<p><img style="width: 415px;" src="http://www.trueplookpanya.com/data/product/blog/images/upload_4/folder_King_Math/upconts/o-net(Math)P6_2553(3-2).jpg"><br></p>', '<p><img style="width: 284px;" src="http://www.trueplookpanya.com/data/product/blog/images/upload_4/folder_King_Math/upconts/o-net(Math)P6_2553(3-3).jpg"><br></p>', '<p><img style="width: 275px;" src="http://www.trueplookpanya.com/data/product/blog/images/upload_4/folder_King_Math/upconts/o-net(Math)P6_2553(3-4).jpg"><br></p>', 'ผิดทุกข้อ', ],
            3 => [ 'รสนม', 'รสส้ม', 'รสมะนาว', 'รสสตรอว์เบอร์รี', 'ผิดทุกข้อ', ],
            4 => [ '230,000,000', '229,000,000', '228,000,000', '208,000,000', 'ผิดทุกข้อ', ],
            5 => [ '3/9', '1/2', '9/18', '3/3', 'ผิดทุกข้อ', ],
            6 => [ '7/45', '7/40', '1/5', '1/4', 'ผิดทุกข้อ', ],
            7 => [ 'ตะวันตกเฉียงเหนือ', 'ตะวันตกเฉียงใต้', 'ตะวันออกเฉียงเหนือ', 'ตะวันออกเฉียงใต้', 'ผิดทุกข้อ', ],
            8 => [ 'รูปที่ 2 มีพื้นที่มากที่สุด', 'รูปที่ 3 มีพื้นที่น้อยที่สุด', 'ทั้งสามรูปมีพื้นที่เท่ากัน', 'รูปที่ 1 และรูปที่ 3 มีพื้นที่เท่ากัน แต่น้อยกว่ารูปที่ 2', 'ผิดทุกข้อ', ],
            9 => [ 'มุม COF ใหญ่กว่า มุม AOD', 'มุม BOE เล็กกว่า มุม AOD', 'มุม AOD ไม่เท่ากับ มุม BOE และ มุม COF', 'มุม AOD เท่ากับ มุม BOE และ มุม COF', 'ผิดทุกข้อ', ],
            10 => [ '0.75', '7.05', '7.50', '750', 'ผิดทุกข้อ', ],
            11 => [ '<p><img style="width: 91px;" src="http://www.trueplookpanya.com/data/product/blog/images/upload_4/folder_King_Math/upconts/o-net(Math)P6_2553(12-1).jpg"><br></p>', '<p><img style="width: 131px;" src="http://www.trueplookpanya.com/data/product/blog/images/upload_4/folder_King_Math/upconts/o-net(Math)P6_2553(12-2).jpg"><br></p>', '<p><img style="width: 171px;" src="http://www.trueplookpanya.com/data/product/blog/images/upload_4/folder_King_Math/upconts/o-net(Math)P6_2553(12-3).jpg"><br></p>', '<p><img style="width: 215px;" src="http://www.trueplookpanya.com/data/product/blog/images/upload_4/folder_King_Math/upconts/o-net(Math)P6_2553(12-4).jpg"><br></p>', 'ผิดทุกข้อ', ],
            12 => [ '53,200 บาท', '53,500 บาท', '53,600 บาท', '54,500 บาท', 'ผิดทุกข้อ', ],
            13 => [ '12 เซนติเมตร × 12 เซนติเมตร', '15 เซนติเมตร × 15 เซนติเมตร', '60 เซนติเมตร × 60 เซนติเมตร', '120 เซนติเมตร × 120 เซนติเมตร', 'ผิดทุกข้อ', ],
            14 => [ '1,500 คน', '2,000 คน', '2,500 คน', '3,000 คน', 'ผิดทุกข้อ', ],
            15 => ['Mitochondria', 'Endoplasmic reticulum', 'Golgi apparatus', 'Ribosome'],
            16 => ['ปลากระดูกแข็งในทะเล', 'ปลากระดูกแข็งในน้ำจืด', 'เต่าทะเล', 'แมงกะพรุนน้ำเค็ม'],
            17 => ['เลือดในเส้นอาร์เตอร์ไหลเร็วกว่าในเส้นเวน', 'เส้นเวนนำเลือดกลับสู่หัวใจ ส่วนเส้นอาร์เตอรีนำเลือดออกจากหัวใจ', 'เลือดในเส้นเวนไหลไปเรื่อยๆ ช้าๆ ส่วนเลือดในเส้นอาร์เตอร์ไหลแรงเป็นจังหวะๆ', 'เลือดในเส้นเวนจะไหลเข้าสู่เส้นเลือดที่ใหญ่กว่า ส่วนเส้นเลือดที่มีเลือดไหลไปยังเส้นเลือดที่เล็กลง คือ เส้นอาร์เตอรี'],
            18 => ['10, 10, 10', '20, 10, 20', '20, 10, 10', '10, 10, 20'],
            19 => ['ชนิดประจุ : ประจุลบ ตำแหน่ง : ผิวด้านใน', 'ชนิดประจุ : ประจุลบ ตำแหน่ง : ผิวด้านนอก', 'ชนิดประจุ : ประจุบวก ตำแหน่ง : ผิวด้านใน', 'ชนิดประจุ : ประจุบวก ตำแหน่ง : ผิวด้านนอก'],
            20 => ['การยึดเกาะระหว่างหมู่ฟอสเฟตอาศัยพันธะไอออนิกซึ่งแตกสลายง่าย', 'มีพันธะโคเวเลนต์ที่แข็งแรงมากจำนวน 3 พันธะเก็บสะสมพลังงาน', 'มีพันธะที่แตกสลายง่ายและปลดปล่อยพลังงานจำนวนมาก', 'มีพลังงานเก็บสะสมไว้จำนวนมากที่โมเลกุลของน้ำตาลไรโบส'],
            21 => ['ก', 'ก, ข', 'ก, ข, ค', 'ก, ข, ค, ง'],
            22 => ['Autosomal recessive', 'Autosomal dominant', 'Sex-linked recessive', 'Sex-linked dominant'],
            23 => ['ก, ข', 'ก, ค', 'ข, ค', 'ก, ข, ค'],
            24 => ['ก, ข', 'ก, ค', 'ข, ค', 'ก, ข, ค'],
            25 => ['ก, ข', 'ก, ค', 'ก, ง', 'ค, จ'],
            26 => ['ก, ข', 'ข, ค', 'ค, ง', 'ก, ง'],
            27 => ['4 และ 2', '2 และ 2', '2 และ 1', '4 และ 4'],
            28 => ['ทุกเซลล์มีจำนวนยีนเท่ากัน แต่สร้าง mRNA เหมือนกัน', 'ทุกเซลล์มีจำนวนยีนเท่ากัน แต่สร้าง mRNA ต่างกัน', 'ทุกเซลล์มีจำนวนยีนต่างกัน แต่สร้าง mRNA ต่างกัน', 'ทุกเซลล์มีจำนวนยีนต่างกัน แต่สร้าง mRNA และ tRNA ต่างกัน'],
            29 => ['ความสัมพันธ์ระหว่างหนูกับกระต่าย : Parasitism ปริมาณชีวมวลน้อยที่สุด : งู จำนวนน้อยที่สุด : กิ้งก่า', 'ความสัมพันธ์ระหว่างหนูกับกระต่าย : Mutualism ปริมาณชีวมวลน้อยที่สุด : ตั๊กแตน จำนวนน้อยที่สุด : งู', 'ความสัมพันธ์ระหว่างหนูกับกระต่าย : Predation ปริมาณชีวมวลน้อยที่สุด : กิ้งก่า จำนวนน้อยที่สุด : หนู', 'ความสัมพันธ์ระหว่างหนูกับกระต่าย : Competition ปริมาณชีวมวลน้อยที่สุด : เหยี่ยว จำนวนน้อยที่สุด : เหยี่ยว'],
            30 => ['ขนาดแรง : น้อยกว่า 10 กิโลกรัม กล้ามเนื้อ P : หดตัว กล้ามเนื้อ M : คลายตัว', 'ขนาดแรง : น้อยกว่า 10 กิโลกรัม กล้ามเนื้อ P : คลายตัว กล้ามเนื้อ M : หดตัว', 'ขนาดแรง : มากกว่า 10 กิโลกรัม กล้ามเนื้อ P : หดตัว กล้ามเนื้อ M : คลายตัว', 'ขนาดแรง : มากกว่า 10 กิโลกรัม กล้ามเนื้อ P : คลายตัว กล้ามเนื้อ M : หดตัว'],
            31 => ['420 ตัว', '490 ตัว', '582 ตัว', '700 ตัว'],
            32 => ['ก, ข', 'ค, ง', 'ก, ค', 'ข, ง'],
            33 => ['ก', 'ก, ข', 'ก, ข, ค', 'ข, ค, ง'],
            34 => ['แพลงก์ตอนพืช', 'แบคทีเรียสังเคราะห์ด้วยแสง', 'มอสและเฟิร์น', 'เห็ด-รา'],
            35 => ['แพะรับบาป', 'ไก่รองบ่อน', 'ตัวตายตัวแทน', 'หนังหน้าไฟ'],
            36 => ['ส่วนที่ 1', 'ส่วนที่ 2', 'ส่วนที่ 3', 'ส่วนที่ 4'],
            37 => ['วรรคที่ 1', 'วรรคที่ 2', 'วรรคที่ 3', 'วรรคที่ 4'],
            38 => ['2 3 1 4', '1 2 3 4', '4 1 3 2 ', '3 4 1 2'],
            39 => ['Mg | Mg2+(1 M) || Ca2+ (1 M) | Ca', 'Ca | Ca2+(1 M) || Mg2+ (1 M) | Mg', 'F2 | F- (1 M) || Cl- (1 M) | Cl2', 'Cl2 | Cl- (1 M) || F- (1 M) | F2'],
            40 => ['ผิดที่ข้อ 1) เพราะการเพิ่มแก๊สฮีเลียมไม่ทีผลต่อความดันรวมของระบบสูงขึ้น', 'ผิดที่ข้อ 1) เพราะการเพิ่มแก๊สฮีเลียมทำให้ความดันรวมของระบบต่ำลง', 'ผิดที่ข้อ 2) เพราะปริมาตรคงที่ทำให้ความเข้มข้นของสารแต่ละชนิดเท่าเดิม', 'ผิดที่ข้อ 2) เพราะความดัยรวมที่สูงขึ้นทำให้สมดุลเลื่อนไปทางฝั่งที่มีโมลของแก๊สน้อยกว่า'],
            41 => ['ตัวรีดิวซ์    – NaBH4 แรงกว่า NaAlH4 เพราะ B มีค่า E.N. สูงกว่า Al', 'ตัวออกซิไดซ์    – NaBH4 แรงกว่า NaAlH4 เพราะ B มีค่า E.N. สูงกว่า Al', 'ตัวออกซิไดซ์ – NaAlH4 แรงกว่า NaBH4 เพราะ B มีค่า E.N. สูงกว่า Al', 'ตัวออกซิไดซ์ – NaAlH4 แรงกว่า NaBH4 เพราะ B มีค่า E.N. สูงกว่า Al'],
            42 => ['ปรอทมีการบรรจุอิเล็กตรอนเป็น Half-filled  – ทำให้อะตอมมีความเป็นประจุมากขึ้นจึงผลึกกัน', 'ปรอทมีการบรรจุอิเล็กตรอนเป็น Half-filled    – ทำให้พันธะโลหะอ่อนแอลง', 'ปรอทมีการบรรจุอิเล็กตรอนเป็น Full-filled    – ทำให้อะตอมมีความเป็นประจุมากขึ้นจึงผลึกกัน', 'ปรอทมีการบรรจุอิเล็กตรอนเป็น Full-filled    – ทำให้พันธะโลหะอ่อนแอลง'],
            43 => ['Li2O2 NaO2 K2O', 'Li2O2   Na2O KO2', 'Li2O    Na2O2 KO2', 'Li2O   NaO2 Na2O2'],
            44 => ['HNO3 + O2 → NO2 + H2O', 'HNO3 → NO + H2O', 'HNO3 →H2O + OH- + NO2+', 'HNO3 →NO2 + H2O + O2'],
            45 => ['เป็นปฏิกิริยาประเภทคายพลังงาน', 'เป็นปฏิกิริยาประเภทดูดพลังงาน', 'หลังจากเพิ่มอุณหภูมิค่าพลังงานกระตุ้น (Ea) มีค่าสูงขึ้น', 'พลังงานจลน์เฉลี่ยของอนุภาคในระบบมีค่ามากขึ้น'],
            46 => ['SnCl2 เท่านั้น', 'SnCl2 และ ตะกอนสีขาว', 'SnCl2 และ ตะกอนสีเทา', 'ตะกอนสีขาว และ ตะกอนสีเทา'],
            47 => ['329', '331', '412', '493'],
            48 => ['A เพราะมีสภาพขั้วแรงกว่า B', 'B เพราะมีสภาพขั้วแรงกว่า A', 'A เพราะเป็นสารที่อิ่มตัวมากกว่า B', 'B เพราะเป็นสารที่อิ่มตัวมากกว่า A'],
        ];

        // testQuestion
        $testQuestions['testQuestion'] = [];
        $testAnswers['testQuestion'] = [];
        $i = 0;
        while($i < 700){
            $txtQuestion = 'คำถามสำหรับการทำสอบระบบ คำถามที่ ' . ((string) ($i+1));
            $testQuestions['testQuestion'][] = $txtQuestion;
            for($x=0;$x<rand(4, 7);$x++){
                $txtAnswer = 'คำตอบสำหรับทดสอบ คำตอบที่ ' . ((string) ($x+1));
                $testAnswers['testQuestion'][$i][] = $txtAnswer; 
            }
            $i++;
        }

        $difficulties = [0.8, 0.6, 0.2];
        
        foreach($exQuestions as $name => $questions){
            $levelName = [
                'สาขาวิชาชีพเทคโนโลยีสารสนเทศและการสื่อสาร และดิจิตอลคอนเทนต์ สาขาการบริหารโครงการสารสนเทศ (Project Management)  อาชีพนักบริหารโครงการสารสนเทศ ชั้น 3',
                'สาขาวิชาชีพเทคโนโลยีสารสนเทศและการสื่อสาร และดิจิตอลคอนเทนต์ สาขาการบริหารโครงการสารสนเทศ (Project Management)  อาชีพนักบริหารโครงการสารสนเทศ ชั้น 6',
            ];
            $eocID[$levelName[0]] = [6236,6237,6238,6248,6249];
            $eocID[$levelName[1]] = [6246,6245,6267,6268,6269,6270];
            $uocID[$levelName[1]] = [6244,6266];
            $questionType = ['level', 'uoc', 'eoc'];

            $exName = $name;
            foreach($questions as $index => $question){
                $type = $questionType[rand(0,2)];
                // $randLevelName = $levelName[rand(0, (count($levelName)-1))];
                $randLevelName = $levelName[1];
                $randUocIndex = rand(0,(count($uocID[$randLevelName])-1));
                $randEocIndex = rand(0,(count($eocID[$randLevelName])-1));
                if($type == 'level'){
                    $question = Question::create(array(
                        'industry_id' => 'PJM',
                        'level_competence_name' => $randLevelName,
                        'eoc_id' => 0,
                        'content' => $question,
                        'type' => $type,
                        'difficulty' => $difficulties[rand(0,2)],
                        'time_needed' => rand(1,5),
                        'is_approved' => rand(0,1),
                    ));
                }elseif($type == 'uoc'){
                    $question = Question::create(array(
                        'industry_id' => 'PJM',
                        'level_competence_name' => $randLevelName,
                        'eoc_id' => $uocID[$randLevelName][$randUocIndex],
                        'content' => $question,
                        'type' => $type,
                        'difficulty' => $difficulties[rand(0,2)],
                        'time_needed' => rand(1,5),
                        'is_approved' => rand(0,1),
                    ));
                }else{
                    $question = Question::create(array(
                        'industry_id' => 'PJM',
                        'level_competence_name' => $randLevelName,
                        'eoc_id' => $eocID[$randLevelName][$randEocIndex],
                        'content' => $question,
                        'type' => $type,
                        'difficulty' => $difficulties[rand(0,2)],
                        'time_needed' => rand(1,5),
                        'is_approved' => rand(0,1),
                    ));
                }
                $flagAnswer = rand(1,count($exAnswers[$exName][$index]));
                $answers = [];
                $i = 1;
                foreach ($exAnswers[$exName][$index] as $answer) {
                    if($answer){
                        $answers[] = new Answer([
                            'content'   =>  $answer,
                            'is_correct_answer' =>  ($flagAnswer == $i)? 1 : 0,
                        ]);
                        $i++;
                    }
                }
                $question->answers()->saveMany($answers);
            }
        }

        foreach($testQuestions as $name => $questions){
            $levelName = [
                'สาขาวิชาชีพเทคโนโลยีสารสนเทศและการสื่อสาร และดิจิตอลคอนเทนต์ สาขาการบริหารโครงการสารสนเทศ (Project Management)  อาชีพนักบริหารโครงการสารสนเทศ ชั้น 7',
            ];


            $eocID[$levelName[0]] = [6234,6233,6232,6243,6242,6241,6252,6251,6322,6321,6320,6319,6318];
            // $eocID[$levelName[0]] = [6232,6233,6234,6241,6242,6243,6251,6252,6318,6319,6320];
            $exName = $name;
            foreach($questions as $index => $question){
                $randLevelName = $levelName[rand(0, (count($levelName)-1))];
                $randEocIndex = rand(0,(count($eocID[$randLevelName])-1));
                $question = Question::create(array(
                    'industry_id' => 'PJM',
                    'level_competence_name' => $randLevelName,
                    'eoc_id' => $eocID[$randLevelName][$randEocIndex],
                    'content' => $question,
                    'type' => 3,
                    'difficulty' => $difficulties[rand(0,2)],
                    'time_needed' => rand(1,5),
                    'is_approved' => rand(0,1),
                ));
                $flagAnswer = rand(1,count($testAnswers[$exName][$index]));
                $answers = [];
                $i = 1;
                foreach ($testAnswers[$exName][$index] as $answer) {
                    if($answer){
                        $answers[] = new Answer([
                            'content'   =>  $answer,
                            'is_correct_answer' =>  ($flagAnswer == $i)? 1 : 0,
                        ]);
                        $i++;
                    }
                }
                $question->answers()->saveMany($answers);
            }
        }
    }
}
