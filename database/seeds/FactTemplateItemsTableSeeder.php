<?php

use Illuminate\Database\Seeder;

use Tpqi\TemplateHeader;
use Tpqi\LevelCompetence;
use Tpqi\TemplateItem;
use Tpqi\EOC;

class FactTemplateItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

		$limit = 20;
		$eocsID = [];
		$templateHeaders = TemplateHeader::all();
		foreach ($templateHeaders as $templateHeader) {
			$eocsID = LevelCompetence::where('levelName', '=', $templateHeader->level_competence_name)->orderByRaw('RAND()')->limit($limit)->lists('elementID');
			$eocs = EOC::whereIn('stdID', $eocsID)->get();
			foreach($eocs as $index => $eoc){
				$amount = rand(0,6);
				if($index == 0) $amount = rand(1,2);
				if($amount > 0){
					TemplateItem::create(array(
						'template_id' => $templateHeader->id,
						'uoc_id' => $eoc->uoc->stdID,
						'eoc_id' => $eoc->stdID,
						'amount' => $amount,
					));
				}
			}
		}
    }
}
