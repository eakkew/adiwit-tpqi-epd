// set value from  first load create.blade.php 
var selectedQuestions = [];
var selectedAnswers = [];

// flag to check disable submit button
var haveSelectedQuestion = [];
var haveSelectedAnswer = [];
var haveCollectAnswers = [];
var correctAnswers = [];
var flagTimeLeft = false;
var LimitTime = 0;
var maxDifficulty = 0;

function getQuestion(eocID, questionNumber){
	haveSelectedQuestion[eocID][questionNumber] = true;

	var selectedParameter = '';
	var allSelected = [];
	$.each($('select[selection="question"][eocID="'+eocID+'"]'), function(key, select){
		var value = parseInt($(select).val());
		allSelected.push(value);
		selectedParameter += 'selected['+key+']='+value+'&';
	});
	selectedParameter += ('selectedLevel=' + $('#level_competence_name').val());
	
	$.ajax({
		url: '/tpqi_epd/exam/'+eocID+'/?type=getquestion&'+selectedParameter,
		// url: '/exam/'+eocID+'/?type=getquestion&'+selectedParameter,
		type: 'get',
		success: function(response){
			var questions = response;
			var selects = $('select[selection="question"][eocID="'+eocID+'"]');
			$.each(selects, function( index, select) {
				var selectedValue = $(select).val();
				var thisID = $(select).attr('id');
				$('#'+thisID+' option').each(function(){
					if($(this).val() != selectedValue){
						$(this).remove();	
					}else{
						$(this).attr("selected", "selected");
						$(select).attr('timeNeed', $(this).attr('timeNeed'));
						$(select).attr('difficulty', $(this).attr('difficulty'));
						setEocTime(eocID);
					}
				});
				$.each( questions, function( index, question ) { 
					// console.log(question.difficulty + ' : ' + maxDifficulty);
					// if(question.difficulty <= maxDifficulty || maxDifficulty == 0){
						$(select).append( $("<option data-content='<p class=\"text-right\"><code>ระดับความยาก "+question.difficulty+"</code> <code>เวลาที่กำหนด "+question.time_needed+" นาที</code></p><p><b>"+question.content+"</b></p>' difficulty="+question.difficulty+" timeNeed='"+question.time_needed+"'></option>").val(question.id) );
					// }
				});
				if($('#'+thisID).attr('class').indexOf('selectpicker') >= 0) $('#'+thisID).selectpicker('refresh');
			});
			setExamTime();
		}
	});
}

function getAnswer(obj){
	var thisQuestion = $(obj).val();
	var thisSelected = (selectedAnswers[thisQuestion])? selectedAnswers[thisQuestion] : [] ;
	var eocID = $(obj).attr('eocID');
	var questionNumber = $(obj).attr('questionNumber');
	var answerSelection = [];
	$.ajax({
		url: '/tpqi_epd/exam/'+thisQuestion+'/?type=getanswer',
		// url: '/exam/'+thisQuestion+'/?type=getanswer',
		type: 'get',
		success: function(response){
			var answers = response;
			var cntAns = (answers.length > 5)? 5 : answers.length;
			$('#showAnswer_'+eocID+'_'+questionNumber).html('');
			for(var i=0; i<cntAns; i++){
				if(typeof haveSelectedAnswer[eocID] == 'undefined') haveSelectedAnswer[eocID] = [];
				if(typeof haveSelectedAnswer[eocID][questionNumber] == 'undefined') haveSelectedAnswer[eocID][questionNumber] = [];
				if(typeof haveSelectedAnswer[eocID][questionNumber] == 'undefined') haveSelectedAnswer[eocID][questionNumber][(i+1)] = false;
				haveSelectedAnswer[eocID][questionNumber][(i+1)] = false;

                var AnswerChoice = ["ก","ข","ค","ง","จ","ฉ"];
				var AnswerID = '#answer_'+thisQuestion+'_'+(i+1);
				var tagAnswer = $('#cloneAnswer').html();
				tagAnswer = tagAnswer.replace(/\xxchoicexx/g,AnswerChoice[i]);
				tagAnswer = tagAnswer.replace(/\xxnumberxx/g,i+1);
				tagAnswer = tagAnswer.replace(/\xxquestionIDxx/g,thisQuestion);
				$('#showAnswer_'+eocID+'_'+questionNumber).append(tagAnswer);
				$(AnswerID).attr('class', 'selectpicker');
				$(AnswerID).attr('eocID', eocID);
				$(AnswerID).attr('questionID', thisQuestion);
				$(AnswerID).attr('questionNumber', questionNumber);
				$(AnswerID).attr('answerNumber', (i+1));
				$(AnswerID).attr('onchange', 'genAnswerToOtherNumber(this),checkCorrectAnswer(this),checkSubmitButton()');
				for(var index in answers){
					var answer = answers[index];

					if(parseInt(answer.is_correct_answer)){
						if($('#correctAnswer').val().indexOf(answer.id) < 0 ) $('#correctAnswer').val( $('#correctAnswer').val()+','+answer.id );
					}

					if((i in thisSelected) && thisSelected[i] == answer.id){
						haveSelectedAnswer[eocID][questionNumber][(i+1)] = true;
						var txt = '';
						txt = (answer.is_correct_answer == 1)? '<p class=\"text-right\"><code>คำตอบที่ถูกต้อง</code></p>' : '';
						$(AnswerID).append( $("<option data-content='"+txt+"<p class=select-custom>"+answer.content+"</p>' is_correct_answer='"+answer.is_correct_answer+"' selected></option>").val(answer.id) );
					}else if($.inArray(answer.id, thisSelected) == -1){
						var txt = '';
						txt = (answer.is_correct_answer == 1)? '<p class=\"text-right\"><code>คำตอบที่ถูกต้อง</code></p>' : '';
						$(AnswerID).append( $("<option data-content='"+txt+"<p class=select-custom>"+answer.content+"</p>' is_correct_answer='"+answer.is_correct_answer+"'></option>").val(answer.id) );
					}
				}
				if($(AnswerID).attr('class').indexOf('selectpicker') >= 0) $(AnswerID).selectpicker('refresh');
			}

			checkCorrectAnswer(obj);
			checkSubmitButton();
		}
	});
}

function genAnswerToOtherNumber(obj){
	haveSelectedAnswer[$(obj).attr('eocID')][$(obj).attr('questionNumber')][$(obj).attr('answerNumber')] = true;
	var questionID = $(obj).attr('questionID');
	var thisID = $(obj).attr('id');
	var selectedParameter = '';
	var allSelected = [];
	$.each($('select[selection="answer"][questionID="'+questionID+'"]'), function(key, select){
		var value = parseInt($(select).val());
		allSelected.push(value);
		selectedParameter += 'selected['+key+']='+value+'&';
	});

	$.ajax({
		url: '/tpqi_epd/exam/'+questionID+'/?type=getanswer&'+selectedParameter,
		// url: '/exam/'+questionID+'/?type=getanswer&'+selectedParameter,
		type: 'get',
		success: function(response){
			var answers = response;
			var selectes = $('select[questionID="'+questionID+'"][id!="'+thisID+'"]');
			$.each(selectes, function(indexSelect, select){
				var selectID = $(select).attr('id');
				var selectedValue = $(select).val();
				// remove option
				$('#'+selectID+' option').each(function(){
					if($(this).val() != selectedValue){
						$(this).remove();	
					}else{
						$(this).attr("selected", "selected");
					}
				});
				// add new option
				$.each(answers, function(indexAnswer, answer){
					var txt = '';
					txt = (answer.is_correct_answer == 1)? '<p class=\"text-right\"><code>คำตอบที่ถูกต้อง</code></p>' : '';
					$(select).append( $("<option data-content='"+txt+"<p class=select-custom>"+answer.content+"</p>' is_correct_answer='"+answer.is_correct_answer+"'></option>").val(answer.id) );
				});
				if($(select).attr('class').indexOf('selectpicker') >= 0) $(select).selectpicker('refresh');
			});
		}
	});
}

function setDefaultExamQuestion(selectedQuestions){
	var allEocID = [];
	$.each($('select[selection="question"]'), function(key, select){
		var flagGenAnswer = false;
		var eocID = $(select).attr('eocID');
		var thisID = $(select).attr('id');
		var thisNumber = $(select).attr('questionNumber');

		// for check all question number is selected
		if(typeof haveSelectedQuestion[eocID] == 'undefined') haveSelectedQuestion[eocID] = [];
		if(typeof haveSelectedQuestion[eocID][thisNumber] == 'undefined') haveSelectedQuestion[eocID][thisNumber] = false;

		// var selectedValue = parseInt($('#'+thisID+' option:selected').val()); //this line for tpqi-test
		var selectedValue = $('#'+thisID+' option:selected').val();
		$('#'+thisID+' option').each(function(){
			var thisValue = $(this).val();
			if(typeof thisValue == 'string'){
				thisValue = parseInt(thisValue);
			}
			// var thisValue = parseInt($(this).val()); //this line for tpqi-test
			if($.inArray(thisValue, selectedQuestions[eocID]) > -1){
				if($(this).val() != selectedValue){
					$(this).remove();
				}else{
					$(select).attr('timeNeed', $(this).attr('timeNeed'));
					$(select).attr('difficulty', $(this).attr('difficulty'));
					haveSelectedQuestion[eocID][thisNumber] = true;
					flagGenAnswer = true;
				}
			}
		});
		if(flagGenAnswer){
			getAnswer(select);
		}
		if($.inArray(eocID, allEocID)) allEocID.push(eocID);
	});
	for(index in allEocID){
		setEocTime(allEocID[index]);
	}
	setExamTime();
}

function setEocTime(eocID){
	var totalExamTime = LimitTime;
	var totalTimeEOC = 0;
	$.each($('select[selection="question"][eocID="'+eocID+'"]'), function(index, question){
		if($(question).attr('timeNeed')) totalTimeEOC += parseInt($(question).attr('timeNeed'));
		$('#'+eocID).html(totalTimeEOC);
		$('#'+eocID).width( ((totalTimeEOC*100)/totalExamTime) + '%' );
	});
}

function setExamTime(){
	var totalExamTime = LimitTime;
	var txtDifficulty = "";
	var totalQuestionTime = 0;
	var totalDifficulty = 0;
	var avgDifficulty = 0;
	var amountQuestion = 0;
	var timeLeft = 0;
	$.each($('select[selection="question"]'), function(index, question){
		if($(question).attr('timeNeed')) totalQuestionTime += parseInt($(question).attr('timeNeed'));
		if($(question).attr('difficulty')){
			totalDifficulty += parseFloat($(question).attr('difficulty'));
			amountQuestion++;
		}
	});
	avgDifficulty = totalDifficulty/amountQuestion;
	if(avgDifficulty >= 0.6){
		txtDifficulty = '<p>ง่าย</p><p>(0.6 - 0.8)</p>';
	}else if(avgDifficulty >= 0.4){
		txtDifficulty = '<p>ปานกลาง</p><p>(0.4 - 0.59)</p>';
	}else{
		txtDifficulty = '<p>ยาก</p><p>(0.2 - 0.39)</p>';
	}
	if(totalDifficulty == 0){
		avgDifficulty = '';
		txtDifficulty = '';	
	}else{
		$('#txtDifficulty').html(txtDifficulty);
		$('#avgDifficulty').attr('avgDifficulty', avgDifficulty);
		$('#avgDifficulty').html( parseFloat(avgDifficulty).toFixed(2) );
	}
	
	timeLeft = totalQuestionTime;
	// timeLeft = totalExamTime - totalQuestionTime;
	flagTimeLeft = (timeLeft > 10)? false : (timeLeft < -10)? false : true;
	$('#timer').html(timeLeft);
	$('#timer').attr('totalTime', timeLeft);
	if( parseInt($('#timer').attr('totalTime')) >= parseInt($('#alertTime').attr('timeNeed')) && parseInt($('#alertTime').attr('timeNeed')) > 0 ){
		// $('#alertTime').show();
	}
	checkSubmitButton();
}

function checkCorrectAnswer(obj){
	var haveCorrectAnswer = false;
	var eocID = $(obj).attr('eocID');
	var questionNumber = $(obj).attr('questionNumber');
	var questionID = ($(obj).attr('selection') == 'question')? $(obj).val() : $(obj).attr('questionID');
	var answers = $('select[selection="answer"][id^="answer_'+questionID+'"]');

	$.each( answers, function(answerIndex, answer) {
		var thisID = '#' + $(answer).attr('id');
		$(thisID+' option:selected').each(function(){
			// for check all answer number is selected
			if($(this).attr('is_correct_answer') == 1) haveCorrectAnswer = true;
		});
	});
	if(typeof haveCollectAnswers[eocID] == 'undefined') haveCollectAnswers[eocID] = [];
	if(typeof haveCollectAnswers[eocID][questionNumber] == 'undefined') haveCollectAnswers[eocID][questionNumber] = false;
	haveCollectAnswers[eocID][questionNumber] = (haveCorrectAnswer)? true : false;
}

function checkSubmitButton(){
	var allQuestionSelected = true;
	var allAnswerSelected = true;
	var allHaveCorrectAnswer = true;
	for(var eocID in haveSelectedQuestion){
		for(var questionNumber in haveSelectedQuestion[eocID]){
			// all question is selected
			if(!haveSelectedQuestion[eocID][questionNumber]) allQuestionSelected = false;

			// all selected answer have correct answer
			if(typeof haveCollectAnswers[eocID] == 'undefined'){
				allHaveCorrectAnswer = false;
			}else{
				if(typeof haveCollectAnswers[eocID][questionNumber] == 'undefined'){
					allHaveCorrectAnswer = false;
				}else{
					if(!haveCollectAnswers[eocID][questionNumber]) allHaveCorrectAnswer = false;
				}
			}

			// all answer is selected
			if(typeof haveSelectedAnswer[eocID] == 'undefined'){
				allAnswerSelected = false;
			}else{
				if(typeof haveSelectedAnswer[eocID][questionNumber] == 'undefined'){
					allAnswerSelected = false;
				}else{
					var answers = haveSelectedAnswer[eocID][questionNumber];
					for(var answerNumber in haveSelectedAnswer[eocID][questionNumber]){
						if(!haveSelectedAnswer[eocID][questionNumber][answerNumber]) allAnswerSelected = false;
					}
				}
			}
		}
	}
	// console.log(allQuestionSelected +' : '+ allAnswerSelected +' : '+ allHaveCorrectAnswer +' : '+ flagTimeLeft)
	// if(allQuestionSelected && allAnswerSelected && allHaveCorrectAnswer && flagTimeLeft){
	if(allQuestionSelected && allAnswerSelected && allHaveCorrectAnswer){
		$('#submitBtn').attr('disabled',false);
	}else{
		$('#submitBtn').attr('disabled',true);
	}
}

$(function() {
	setDefaultExamQuestion(selectedQuestions);

	$('#testbtn').click(function(){
		checkSubmitButton();
	});

	$('select[selection="question"]').change(function(){
		var eocID = $(this).attr('eocID');
		var questionNumber = $(this).attr('questionNumber');
		getQuestion(eocID, questionNumber);
		getAnswer(this);
	});
});