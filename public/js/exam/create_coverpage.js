function checkDisable(){
	var flagInput = ( $('input[name="location"]').val() == '' )? false : true ;
		flagInput = ( $('input[name="examDate"]').val() == '' )? false : true ;
	var flagSelect = true;
	var flaHaveQuestion = false;

	var selects = $('select');
	for(var i=0; i<selects.length; i++){
		var select = selects[i];
		if($(select).val() == '') flagSelect = false;
	}

	var templateID = $('select[id="templateHeader"]').val();
	if(templateID){
		$.ajax({
			url: "/tpqi_epd/template/"+templateID+"?type=getQuestionAmount",
			// url: "/template/"+templateID+"?type=getQuestionAmount",
			type: "get",
			success: function(response){
				if(!parseInt(response)){
					flaHaveQuestion = false;
					$('#notEnoughQuestion').attr('class', 'text-danger');
				}else{
					flaHaveQuestion = true;
					$('#notEnoughQuestion').attr('class', 'hidden text-danger');
				}
				var buttons = $('button[checkDisable="1"]');
				for(var i=0; i<buttons.length; i++){
					var btn = buttons[i];
					$(btn).prop('disabled', (flagInput && flagSelect && flaHaveQuestion)? false : true);
				}
			}
		});
	}else{
		$('#notEnoughQuestion').attr('class', 'hidden text-danger');

		var buttons = $('button[checkDisable="1"]');
		for(var i=0; i<buttons.length; i++){
			var btn = buttons[i];
			$(btn).prop('disabled', (flagInput && flagSelect && flaHaveQuestion)? false : true);
		}
	}
}

$(function() {
	checkDisable();
	$('#qualification').change(function (){
		$('#templateHeader').val('');
		$('#templateHeader').setFilterList('N', '#templateHeader', '');
		$('#cbdID').val('');
		$('#cbdID').setFilterList('N', '#cbdID', '');
		checkDisable();

		$.ajax({
			url: "/tpqi_epd/template/"+$(this).val(),
			// url: "/template/"+$(this).val(),
			type: "get",
			success: function(response){
				$(this).setFilterList('N', '#templateHeader', response);
			}
		});
		$.ajax({
			url: "/tpqi_epd/organization/"+$(this).val(),
			// url: "/organization/"+$(this).val(),
			type: "get",
			success: function(response){
				$(this).setFilterList('N', '#cbdID', response);
			}
		});
	})

	$('#industryGroup').change(function (){
		$('#qualification').val('');
		$('#templateHeader').val('');
		$('#templateHeader').setFilterList('N', '#templateHeader', '');
		$('#cbdID').val('');
		$('#cbdID').setFilterList('N', '#cbdID', '');
		checkDisable();	
	});

	$('#examDate').change(function (){
		checkDisable();
	})

	if($.datepicker){
		$.datepicker.regional['th'] ={
   			changeYear: true,
	        yearOffSet: 543,
	        // numberOfMonths: 2,
	        dateFormat: 'dd/mm/yy',
	        dayNames: ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'],
	        dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
	        monthNames: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
	        monthNamesShort: ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'],
	        yearRange: '-3:+3',
		};
		$.datepicker.setDefaults($.datepicker.regional['th']);
		$( "#examDate" ).datepicker({
			beforeShow: function( selectedDate ) {
				$( "#examDate" ).datepicker( "option", "minDate", thisDate );
			}
		});
	}

	$('#notCheck').click(function (){
		$('#nocheck').val('true');
	});

	$('#checkAgain').click(function (){
		$('#notSuccess').modal('toggle');
	});
});