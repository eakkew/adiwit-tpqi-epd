$(function() {
	if($.datepicker){
		$.datepicker.regional['th'] ={
			changeYear: true,
	        yearOffSet: 543,
	        // numberOfMonths: 2,
	        dateFormat: 'dd/mm/yy',
	        dayNames: ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'],
	        dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
	        monthNames: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
	        monthNamesShort: ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'],
	        // yearRange: '-10:+10',
		};
		$.datepicker.setDefaults($.datepicker.regional['th']);

		// var selectedFromDate = '';
		// var selectedToDate = '';
		if(typeof selectedFromDate != "undefined" && typeof selectedToDate != "undefined"){
			if(selectedFromDate.day && selectedToDate.day){
				$( "#fromDate" ).val(selectedFromDate.day+'/'+selectedFromDate.month+'/'+selectedFromDate.year);
				$( "#toDate" ).val(selectedToDate.day+'/'+selectedToDate.month+'/'+selectedToDate.year);
			}

			$( "#fromDate" ).datepicker({
				beforeShow: function( selectedDate ) {
					$( "#fromDate" ).datepicker( "option", "maxDate", $( "#toDate" ).val() );
				}
			});

			$( "#toDate" ).datepicker({
				beforeShow: function( selectedDate ) {
					$( "#toDate" ).datepicker( "option", "minDate", $( "#fromDate" ).val() );
				}
			});
		}
	}
	
	$('.multiselect').multiselect({
		enableClickableOptGroups: true,
		includeSelectAllOption: true,
		buttonWidth: '100%',
		maxHeight: 400,
		enableFiltering: true,
		nonSelectedText: $(this).data('nonSelectedText'),
	});

	var FilterNames = ['industryGroup', 'qualification', 'uoc'];

	$.fn.disableSearchButton = function (){
		var flagDisable = false;
		var type = $('#types input:radio:checked').val();
		$.each(FilterNames, function (index, filterID){
			var filter = '#'+filterID;
			if(!$(filter).val()) flagDisable = true;
			// console.log(type + ' : ' + filterID + ' : ' + !flagDisable);
			if(type == 1 && filterID == 'qualification' && !flagDisable){
				flagDisable = false;
				return false;	
			} 
			// if(type == 1 && filterID == 'qualification' && !flagDisable) return false;
		})
		$('button[id="filterBtn"]').prop('disabled', flagDisable);
	}

	$.fn.getAjaxFilter = function (groupflag, parameterName, arrID, callback){
		$.ajax({
			url: "/tpqi_epd/filter",
			// url: "/filter",
			type: "post",
			data: {
				module : 'filter',
				parameter : arrID,
				group : groupflag,
				dataName : parameterName,
				'_token' : $(this).attr('data-csrf')
			},
			success: function(response){
				callback(response);
			}
		});
	}

	$.fn.setFilterList = function (groupflag, id, arrData, levelName){
		// var start_pos = arrData.indexOf('Start JSON |') + 12;
		// var end_pos = arrData.indexOf('| End JSON',start_pos);
		// textJSON = arrData.substring(start_pos,end_pos);
		// arrData = JSON.parse(textJSON);

		if(typeof arrData == 'object' && $(id).attr('class')){
			$(id).empty();
			if(groupflag == 'Y'){
				$.each(arrData, function(groupkey, groupdata) {
					$.each(groupdata.eocs, function(key, data) {
						var idValue = (data.stdID)? data.stdID : (data.indID)? data.indID : data.id;
						var nameValue = (data.stdName)? data.stdName : (data.indName)? data.indName : (data.levelName)? data.levelName : data.name;
						var groupid = (groupdata.stdID)? groupdata.stdID : groupdata.indID;
						$(id).append( 
				    		$('<option class="'+groupdata.stdID+'"></option>').val(idValue).html(nameValue)
				    	);
					});
					$(id+' option.'+groupdata.stdID).wrapAll('<optgroup label="'+groupdata.stdName+'" />')
				});
			}else{
				$.each(arrData, function(key, data) {
			    	switch(id) {
						case '#cbdID':
							var idValue = data.orgID;
							var nameValue = data.orgCode+" "+data.orgName;
							// var nameValue = data.orgID+" : "+data.orgName+"<small class='text-muted'> "++" </small>";
							break;
						case '#templateHeader':
							var idValue = data.id;
							var nameValue = data.name;
							break;
						default:
							var idValue = (levelName == 'Y')? data.levelName : (data.stdID)? data.stdID : data.indID;
							var nameValue = (data.stdName)? data.stdName : (data.indName)? data.indName : data.levelName;
							break;
					}
					$(id).append( $('<option></option>').val(idValue).html(nameValue) );
				});
			}
			if($(id).attr('class').indexOf('multiselect') >= 0){
				$(id).prop('disabled', false);
		    	$(id).multiselect('rebuild');
			}else if($(id).attr('class').indexOf('selectpicker') >= 0){
				$(id).prop('disabled', false);
				$(id).selectpicker('refresh');
			}
		}else if($(id).attr('class')){
			if($(id).attr('class').indexOf('multiselect') >= 0){
				$(id).empty();
				$(id).prop('disabled', true);
				$(id).multiselect('rebuild');
				$(id).multiselect('disable');
			}else if($(id).attr('class').indexOf('selectpicker') >= 0){
				$(id).empty();
				$(id).prop('disabled', true);
				$(id).selectpicker('refresh');
			}
		}
	}

	$.fn.getPageData = function (urlName, industriesID, levelsID, eocsID, page){
		$.ajax({
			url: "/tpqi_epd/"+urlName,
			// url: "/"+urlName,
			type: "post",
			data: {
				industriesID : industriesID,
				levelsID : levelsID,
				eocsID : eocsID,
				page : page,
				'_token' : $(this).attr('data-csrf')
			},
			success: function(response){
				callback(response);
			}
		});
	}

	////////// industries -> profession -> qualification -> uoc -> eoc
	$(this).disableSearchButton();

	$('#industryGroup').change(function(){
		if($(this).attr('endfilter') != 'Y'){
			var arrID = $(this).val();
			$(this).getAjaxFilter('N', 'industryID', arrID, function(response) {
				$(this).setFilterList('N', '#qualification',response, 'Y');
				$(this).setFilterList('N', '#uoc','');
			});
			if( $('#questionType').val() != undefined && !$('#industryGroup').attr('nocheck')) CheckSubmitButton('submitbtn');
			$(this).disableSearchButton();
		}
	});

	$('#qualification').change(function(){   
		if($(this).attr('endfilter') != 'Y'){
			var arrID = $(this).val();
			if($('#questionType').val() == 2 || $('#types input:radio:checked').val() == 2){
				$(this).getAjaxFilter('Y', 'getOnlyUOC', arrID, function(response) {
					$(this).setFilterList('N', '#uoc',response);
					if( $('#questionType').val() != undefined && !$('#qualification').attr('nocheck')) CheckSubmitButton('submitbtn');
				});
			}else{
				if($('#questionType').val() == 3 || $('#types input:radio:checked').val() == 3){
					$(this).getAjaxFilter('Y', 'qualification', arrID, function(response) {
						$(this).setFilterList('Y', '#uoc',response);
						if( $('#questionType').val() != undefined && !$('#qualification').attr('nocheck')) CheckSubmitButton('submitbtn');
					});
				}
				$(this).disableSearchButton();
			}
		}
		if( $('#questionType').val() != undefined && !$('#qualification').attr('nocheck')) CheckSubmitButton('submitbtn');
		$(this).disableSearchButton();
	});

	$('#uoc').change(function(){
		// if(!$(this).attr('nocheck')) {
			if( $('#questionType').val() != undefined && !$('#uoc').attr('nocheck')) CheckSubmitButton('submitbtn');
			$(this).disableSearchButton();
		// }
	});

	$("input[name=type]:radio").change(function(){
		if($(this).attr('endfilter') != 'Y'){
			var arrID = $('#qualification').val();
			if($(this).val() == 1){
				$(this).setFilterList('N', '#uoc', '');
				$(this).disableSearchButton();
			}else if($(this).val() == 2){
				$(this).getAjaxFilter('Y', 'getOnlyUOC', arrID, function(response) {
					$(this).setFilterList('N', '#uoc',response);
					$(this).disableSearchButton();
				});
			}else{
				$(this).getAjaxFilter('Y', 'qualification', arrID, function(response) {
					$(this).setFilterList('Y', '#uoc',response);
					$(this).disableSearchButton();
				});
			}
		}
	});

	$('#resetFilter').click(function(){
		$( "#fromDate" ).val('');
		$( "#toDate" ).val('');
		var selects = $('select');
		selects.each(function(){
			$(this).val([]);
			$(this).multiselect('rebuild');
			if($(this).attr('id') != 'industryGroup' && $(this).attr('class').indexOf('multiselect') >= 0){
				$(this).empty();
				$(this).multiselect('rebuild');
				$(this).multiselect('disable');
			}
		});

		$('#filter').submit();
	});

});


