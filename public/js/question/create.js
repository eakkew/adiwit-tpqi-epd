function disableEdit(){
	var answerNumber = $("input[type='radio']:checked").val();
	if(answerNumber){
		var answer = $("#answer-"+answerNumber);
	    var editArea = $(answer).next().children('.note-editable');
	    $(editArea).attr('contenteditable', false);
	    $('input[type="radio"]').prop('disabled', true);
	}
}
function setSummernote(id){
	if(id){
		$('#'+id).summernote({
			minHeight: 280,
			height: 300,
			lang: 'th-TH',
			toolbar: [
			      ['style',['style']],
			      ['color', ['color']],
			      // ['fontname', ['fontname']],
			      ['fontsize', ['fontsize']],
			      ['supersub', ['superscript','subscript']],
			      ['font', ['bold', 'italic', 'strikethrough', 'underline', 'clear']],
			      ['para', ['ul', 'ol', 'paragraph']],
			      ['table', ['table']],
			      ['insert', ['picture']],
			      // ['insert', ['link', 'picture']],
			      ['other',['help','hide']]
		    ]
		});
	}else{
		$('.summernote').summernote({
			minHeight: 280,
			height: 300,
			lang: 'th-TH',
			toolbar: [
			      ['style',['style']],
			      ['color', ['color']],
			      // ['fontname', ['fontname']],
			      ['fontsize', ['fontsize']],
			      ['supersub', ['superscript','subscript']],
			      ['font', ['bold', 'italic', 'strikethrough', 'underline', 'clear']],
			      ['para', ['ul', 'ol', 'paragraph']],
			      ['table', ['table']],
			      ['insert', ['picture']],
			      // ['insert', ['link', 'picture']],
			      ['other',['help','hide']]
		    ]
		});
	}
}
function addAnswer (fromID, toID, row){
	var uoc = $('#uoc').val();
	var qualification = $('#qualification').val();
	var industryGroup = $('#industryGroup').val();

	var answer = $('#'+fromID).html();
	answer = answer.replace(/\xxixx/g, row);
	$("#"+toID).append(answer);
	$('#answer-'+row).attr('name','answerContent[]');
	$('#answer-'+row).attr('class','summernote');
	CheckSubmitButton('submitbtn');
}
function CheckSubmitButton(name){
	var submitFlag = true;
	var difficulty = $('#difficulty').val();
	var executionTime = $('#executionTime').val();
	var questionContent = $('#questionContent').code();
	var answerRows = $('#answerlist textarea[id^="answer-"]').length;

	var industryGroup = '';
	var qualification = '';
	var uoc = '';
	if($('#industryGroup').get(0).tagName == 'SELECT'){
		$( "#industryGroup option:selected" ).each(function() {
			// industryGroup = ($( this ).attr('selected'))? $( this ).val() : '';
			industryGroup = $( this ).val();
		});
		$( "#qualification option:selected" ).each(function() {
			// qualification = ($( this ).attr('selected'))? $( this ).val() : '';
			qualification = $( this ).val();
		});
		$( "#uoc option:selected" ).each(function() {
			// uoc = ($( this ).attr('selected'))? $( this ).val() : '';
			uoc = $( this ).val();
		});
	}else{
		industryGroup = $('#industryGroup').attr('value');
		qualification = $('#qualification').attr('value');
		uoc = $('#uoc').attr('value');
	}
	var questionType = $('#questionType').val();
	var checkCondition = false;
	if(questionType == 1){
		if(qualification != '' && industryGroup != '' && questionContent != '') checkCondition = true;
	}else if(questionType == 2 || questionType == 3){
		if(uoc != '' && qualification != '' && industryGroup != '' && questionContent != '') checkCondition = true;
	}

	if(checkCondition){
	// if(uoc != '' && qualification != '' && industryGroup != '' && questionContent != ''){
	// if(difficulty > 0 && executionTime > 0 && uoc != '' && qualification != '' && industryGroup != '' && questionContent != ''){
		var allRowsHaveContent = true;
		var haveCorrectAnswer = false;
		var countHaveAnswer = 0;
		if(answerRows > 0){
			for(var i=0; i<answerRows; i++){
				if($('#answer-'+i).code() != '') countHaveAnswer++;
				allRowsHaveContent = ($('#answer-'+i).code() == '')? false : allRowsHaveContent;
			}
			for(var i=0; i<answerRows; i++){
				if($('#correctAnswer-'+i+':checked').prop("checked") == true && $('#answer-'+i).code() != '') haveCorrectAnswer = true;
			}
		}else{
			allRowsHaveContent = false;
			haveCorrectAnswer = false;
		}

		if((allRowsHaveContent || (countHaveAnswer >= 2)) && haveCorrectAnswer){
			$('#'+name).prop('disabled', false);
		}else{
			$('#'+name).prop('disabled', true);
		}
	}else{
		$('#'+name).prop('disabled', true);
	}
}
function CheckQuestionType(obj){
	if($(obj).val()){
		$('#industryGroup').prop('disabled', false);
		$('#qualification').prop('disabled', false);
		if($(obj).val() == 1){
			$('#uoc').empty();
			$('#selectUoc').hide();
			// var length = $('#uoc').options.length;
			// for (i = 0; i < length; i++) {
			//   $('#uoc').options[i] = null;
			// }
			$('#uoc').prop('disabled', true);
			$('#qualification').attr('endfilter', 'Y');
		}else{
			$('#selectUoc').show();
			$('#qualification').attr('endfilter', 'N');
			$('#uoc').prop('disabled', false);
		}
	}else{
		$('#industryGroup').prop('disabled', true);
		$('#qualification').prop('disabled', true);
		$('#uoc').prop('disabled', true);
	}
	$('#industryGroup').selectpicker('refresh');
	$('#qualification').selectpicker('refresh');
	$('#uoc').selectpicker('refresh');
}


$(function() {
	// $('select').multiselect({
	// 	maxHeight: 400,
	// 	buttonWidth: '100%',
	// 	enableClickableOptGroups: true,
	// 	enableFiltering: true,
	// 	nonSelectedText: $(this).data('nonSelectedText'),
	// });

	// Default on load
	setSummernote();
	// disableEdit(); //Use this function to disable solution choice
	CheckSubmitButton('submitbtn');

	if($('#answerlist textarea[id^="answer-"]').length == 0){
		addAnswer('newAnswer','answerlist', 0);
		addAnswer('newAnswer','answerlist', 1);
		setSummernote();
	}
	// Event
	$('#questionType').change(function (){
		CheckQuestionType(this);

		if($(this).val() == 2){
			var arrID = $('#qualification').val();
			$(this).getAjaxFilter('Y', 'getOnlyUOC', arrID, function(response) {
				$(this).setFilterList('N', '#uoc',response);
				CheckSubmitButton('submitbtn');
			});
		}else if($(this).val() == 3){
			var arrID = $('#qualification').val();
			$(this).getAjaxFilter('Y', 'qualification', arrID, function(response) {
				$(this).setFilterList('Y', '#uoc',response);
				CheckSubmitButton('submitbtn');
			});
		}
	})

	$('#addAnswer').click(function (){
		var row = $('#answerlist textarea[id^="answer-"]').length;
		addAnswer('newAnswer','answerlist', row);
		setSummernote();
	});

	$('#difficulty').change(function (){
		CheckSubmitButton('submitbtn');
	})
	$('#executionTime').change(function (){
		CheckSubmitButton('submitbtn');
	})

	$('#answerlist').keyup(function (){
		CheckSubmitButton('submitbtn');
	})

	$('#questionlist').keyup(function (){
		CheckSubmitButton('submitbtn');
	})

	$('#formCreate').on('submit', function () {
		if($('#flagEdit').val() == 0){
			return ajaxToStore($(this));
		}
	});

	function ajaxToStore(formQuestion){
		var answers = [];
		// $('[name="answerContent[]"]').each(function (index) {
		// var numAnswer = $('#answerlist textarea[id^="answer-"]').length;
		$( "#formCreate" ).find('[name="answerContent[]"]').each(function (index) {
			answers.push($(this).code());
		});

		$.ajax({
			url: $( "form" ).attr('action'),
			type: "post",
			data: {
				'_token' : $('input[name="_token"]').val(),
				"difficulty" : $('[name="difficulty"]').val(),
				"time_needed" : $('[name="time_needed"]').val(),
				"type" : $('[name="type"]').val(),
				"industry_id" : $('[name="industry_id"]').val(),
				"level_competence_name" : $('[name="level_competence_name"]').val(),
				"eoc_id" : $('[name="eoc_id"]').val(),
				"is_approved" : $('[name="is_approved"]').val(),
				"content" : $('[name="content"]').val(),
				"answerContent" : answers,
				"is_correct_answer" : $('[name="is_correct_answer"]:checked').val(),
			},
			success: function(response){
				var defaultModal = $('#saveModal').html();
				var question = response;
				var answers = $('#modalAnswer').html();
				var i = 0;
				var trueAnswer = '';
				while (i < question.answers.length) {
				    answers += (i+1) + ' : ' + question.answers[i].content + '<br>';
				    if( parseInt(question.answers[i].is_correct_answer) ){
				    	trueAnswer = $('#modalSolution').html() + (i+1) + ' : ' + question.answers[i].content;
				    }
				    i++;
				}
				var editQuestion = $('#editQuestion').attr('url');
				editQuestion = editQuestion + '/' + question.id + '/edit';
				$('#modalQuestion').html($('#modalQuestion').html() + question.content);
				$('#modalAnswer').html(answers);
				$('#modalSolution').html(trueAnswer);
				$('#editQuestion').attr('href', editQuestion);
				$('#nextQuestion').attr('href', $('#nextQuestion').attr('url') + '/create/?'
					+ 'difficulty=' + question.difficulty + '&'
					+ 'eoc_id=' + question.eoc_id + '&'
					+ 'industry_id=' + question.industry_id + '&'
					+ 'level_competence_name=' + question.level_competence_name + '&'
					+ 'time_needed=' + question.time_needed + '&'
					+ 'type=' + question.type + '&'
				);

				$('#showModal').html( $('#saveModal').html() );
				$('#saveModal').html( defaultModal );
			}
		});
		return false;
	}

});
