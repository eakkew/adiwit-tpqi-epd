function setModal(data, typeData, name) 
{	
	var name = name.substring(0, 50);;
	var idModal = 'questionModal';
	var defaultModal = $('#'+idModal).html();
	var newModal = '';
	if(typeData == 'question'){
		headHTML = 'รายงานคำถาม : ' + name;
	}else if(typeData == 'template'){
		headHTML = 'รายงานแม่แบบ : ' + name;
	}else{
		headHTML = 'รายงานชุดข้อสอบ : ' + name;
	}
	
	var bodyHTML = "";
	for(var i=0; i<data.length; i++){
		bodyHTML += "<tr>";
		var txt = (data[i].status)? 'เปิดใช้งาน' : 'ยกเลิกการใช้งาน';
		bodyHTML += "<td><a style='max-width: 350px; overflow: hidden;' class=btn btn-default' href='/tpqi_epd/"+typeData+"/" + data[i].id + "/edit' role='button'>" + data[i].name + "</a></td>";
		// bodyHTML += "<td><a style='max-width: 350px; overflow: hidden;' class=btn btn-default' href='/"+typeData+"/" + data[i].id + "/edit' role='button'>" + data[i].name + "</a></td>";
		bodyHTML += "<td style='text-align: center;'>" + txt + "</td>";
		bodyHTML += "<td style='text-align: center;'>" + data[i].used + "</td>";
		bodyHTML += "</tr>";
	}
	$('#header').html(headHTML);
	$('#body').html(bodyHTML);
	newModal = $('#'+idModal).html();

	$('#graphModal').html(newModal);
	$('#'+idModal).html(defaultModal);
	$('#graphModal').modal('toggle');
}



$(function() {
	$.fn.checkFilterBtn = function ()
	{
		var disableFlag = false;
		if(!$('#typeData').val()) disableFlag = true;
		if(!$('#type').val()) disableFlag = true;
		if(!$('#industryGroup').val()) disableFlag = true;
		if(!$('#qualification').val()) disableFlag = true;
		if($('#typeData').val() != 'question') {
			$('#qualification').attr('endfilter', 'Y')
			$('#uoc').empty();
			$('#uoc').prop('disabled', true);
			$('#uoc').selectpicker('refresh');
		}else{
			if($('#type').val() == 1) {
				$('#qualification').attr('endfilter', 'Y')
				$('#uoc').empty();
				$('#uoc').prop('disabled', true);
				$('#uoc').selectpicker('refresh');
			} else {
				$('#qualification').attr('endfilter', 'N')
				$('#uoc').prop('disabled', false);
				$('#uoc').selectpicker('refresh');
			}
		}
		$('#filterBtn').prop('disabled', disableFlag)
	}

	$(this).checkFilterBtn();

	$("select").change(function(){
		$(this).checkFilterBtn();
	});

});