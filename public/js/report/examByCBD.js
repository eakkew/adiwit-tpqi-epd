$(function() {
	$.fn.checkFilterBtn = function ()
	{
		var disableFlag = false;
		if(!$('#type').val()) disableFlag = true;
		if(!$('#industryGroup').val()) disableFlag = true;
		if(!$('#qualification').val()) disableFlag = true;
		$('#filterBtn').prop('disabled', disableFlag)
	}

	$(this).checkFilterBtn();

	$("select").change(function(){
		$(this).checkFilterBtn();
	});

});