$(function() {
	$.fn.checkFilterBtn = function ()
	{
		var disableFlag = false;
		if(!$('#typeReport input:radio:checked').val()) disableFlag = true;
		if(!$('#industryGroup').val()) disableFlag = true;
		if(!$('#qualification').val()) disableFlag = true;
		if(!$('#masterExam').val()) disableFlag = true;
		$('#filterBtn').prop('disabled', disableFlag)
	}

	$.fn.listMasterExam = function (selectedExam)
	{

		$('#masterExam').html('');
		var level = $('#qualification').val();
		var from = '';
		var to = '';
		if($('#fromDate').val() && $('#toDate').val()){
			from = $('#fromDate').val().split('/');
			from = from[2]-543 + '-' + from[1] + '-' + from[0];
			to = $('#toDate').val().split('/');
			to = to[2]-543 + '-' + to[1] + '-' + to[0]; 
		}
		var examType = $('#typeReport input:radio:checked').val();
		if(examType && level)
		{
			$.ajax({
				url: "/tpqi_epd/ajax/exam/"+level,
				// url: "/ajax/exam/"+level,
				type: "get",
				data:{
					from: from,
					to: to,
					type: examType,
				},
				success: function(response){
					// masterExam
					for(var i=0; i<response.length; i++)
					{
						var selected = ( response[i].id == selectedExam )? 'selected' : '';
						$('#masterExam').append( $('<option ' + selected + ' ></option>').val(response[i].id).html(response[i].template_header_trash.name + ' พิมพ์ครั้งที่ ' + response[i].set) );
					}
					$('#masterExam').selectpicker('refresh');
					$(this).checkFilterBtn();
					// $(this).setFilterList('N', '#cbdID', response);
				}
			});
		}

	}

	$(this).checkFilterBtn();
	$(this).listMasterExam(masterExam);

	$("select").change(function(){
		$(this).checkFilterBtn();
		if($(this).attr('id') != 'masterExam') $(this).listMasterExam(masterExam);
	});

	$("#fromDate").change(function(){
		if($(this).attr('id') != 'masterExam' && $("#toDate").val()) $(this).listMasterExam(masterExam);
		$(this).checkFilterBtn();
	});

	$("#toDate").change(function(){
		if($(this).attr('id') != 'masterExam' && $("#fromDate").val()) $(this).listMasterExam(masterExam);
		$(this).checkFilterBtn();
	});

	$("#typeReport").change(function(){
		$(this).checkFilterBtn();
		$(this).listMasterExam(masterExam);
	});

});