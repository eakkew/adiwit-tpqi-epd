function CheckSubmitButton(name){
	var templateType = $('#templateType').val();
	var TotalUOC = (templateType == 1)? parseInt($('#byLevelAmount').val()) : (((templateType == 2))? parseInt($('#totalUOC').html()) : parseInt($('#totalEOC').html()));
	var difficulty = $('#difficulty').val();
	var executionTime = $('#executionTime').val();
	var templateName = $('#templateName').val();
	var disableButton = true;
	// console.log(TotalUOC + ' : ' + templateName + ' : ' + $('#templateType').val());
	if(TotalUOC > 0 && templateName != ''){
	// if(TotalUOC > 0 && difficulty > 0 && executionTime > 0 && templateName != ''){
		if(templateType == 1){
			console.log($("#industryGroup").val() + ' : ' + $("#industryGroup").attr('value') + ' # ' + $("#qualification").val() + ' : ' + $("#qualification").attr('value'));
			if( ($("#industryGroup").val() || $("#industryGroup").attr('value') ) && ( $("#qualification").val() || $("#qualification").attr('value') ) ){
				disableButton = false;
			}else{
				disableButton = true;
			}
		}else{
			disableButton = false;
		}
	}else{
		disableButton = true;
	}
	$("#submitTemplateLevel").prop('disabled', disableButton);
	$("#submitTemplateUoc").prop('disabled', disableButton);
	$("#submitTemplateEoc").prop('disabled', disableButton);
}
function CheckAmountEOC(inputName){
	var TotalAmount = 0;
	var TotalEOC = [];
	var arr = $('*[id^="'+inputName+'"]');
	for(var i in arr){
		if(arr[i].value > 0) {
			TotalAmount += parseInt(arr[i].value);
			var id = arr[i].id.split('-');
			if(TotalEOC[id[3]]){
				TotalEOC[id[3]] += parseInt(arr[i].value);
			}else{
				TotalEOC[id[3]] = parseInt(arr[i].value);
			}
		}
	}

	//sumTotalUOC
	if($('#templateType').val() == 3){ // EOC
		$('#totalUOC').html(0);
		$('#totalEOC').html(TotalAmount);
	}else if($('#templateType').val() == 2){ // UOC
		$('#totalUOC').html(TotalAmount);
		$('#totalEOC').html(0);
	}

	//sumEOC
	for(var i in TotalEOC){
		$('#totalEOC-'+i).html(TotalEOC[i]);
	}

	//disable submit button
	CheckSubmitButton();
}

function SetAmount(templateID, eocID, number){า
	$.ajax({
		url: "/tpqi_epd/industry/?dimtemplate[templateID]="+templateID+"&dimtemplate[eocID]="+eocID,
		// url: "/industry/?dimtemplate[templateID]="+templateID+"&dimtemplate[eocID]="+eocID,
		type: "get",
		success: function(response){
			var templateItem = response;
			$('#EOCamount-'+templateItem[0].uoc_id+'-'+eocID+'-'+number).val(templateItem[0].amount);
			CheckAmountEOC('EOCamount');
		}
	});
}

$(function() {
	$.fn.DefaultUOC = function() {
		$("#templateLevellist").html('');
		$("#templateUOClist").html('');
		$("#templateEOClist").html('');
	}
	$.fn.SetLevelList = function(id) {
		var Qid = $('#qualification').val();
		var Edit = $('#qualification').attr('edit');
		var level = $('#newlevel').html();
		level = level.replace(/\xxLevelIDxx/g,Qid);
		$("#templateLevellist").append(level);
		$("#byLevelAmount").attr('name', 'levelAmount');

		if(Edit == 'Y') SetAmount($('#templateID').val(), GroupUOC[i].eocs[x].stdID, i);
	}
	$.fn.SetEOCList = function(id) {
		var Qid = $('#qualification').val();
		var Edit = $('#qualification').attr('edit');
		if(Qid){
			$(this).getAjaxFilter('Y', 'qualification', Qid, function(response) {
				var GroupUOC = response;
				var Template = "";
				for(var i in GroupUOC){ //UOC
					var uoc = $('#neweoc').html();
					uoc = uoc.replace(/\xxixx/g,i);
					uoc = uoc.replace(/\xxUOCdataxx/g,GroupUOC[i].stdName);
					uoc = uoc.replace(/\xxUOCidxx/g,GroupUOC[i].stdID);
					uoc = uoc.replace(/\xxUocCodexx/g,GroupUOC[i].uoc_tpqi.uocCode);
					uoc = '<div id="AddUOC'+i+'">'+uoc+'</div>';
					$("#templateEOClist").append(uoc);

					var row = $('#listeoc'+i).html();
					$('#AddUOC'+i+' #listeoc'+i).empty();
					for(var x in GroupUOC[i].eocs){ //EOC
						var eoc = row;
						eoc = eoc.replace(/\xxEOCdataxx/g,GroupUOC[i].eocs[x].stdName);
						eoc = eoc.replace(/\xxEOCidxx/g,GroupUOC[i].eocs[x].stdID);
						eoc = eoc.replace(/\xxidxx/g,i);
						eoc = eoc.replace(/\xxUOCidxx/g,GroupUOC[i].stdID);
						$('#AddUOC'+i+' #listeoc'+i).append(eoc)
						$('#EOCamount-'+GroupUOC[i].stdID+'-'+GroupUOC[i].eocs[x].stdID+'-'+i).attr('name', 'amount[]');
						$('#EOCid-'+GroupUOC[i].eocs[x].stdID).attr('name', 'eoc_id[]');
						if(Edit == 'Y') SetAmount($('#templateID').val(), GroupUOC[i].eocs[x].stdID, i);
					}
				}
				$("#submitTemplateEoc").prop('disabled', true);
				$('#totalEOC').html(0)
			});
		}
	}
	$.fn.SetUOCList = function() {
		var Qid = $('#qualification').val();
		console.log(Qid);
		// var Edit = $('#qualification').attr('edit');
		if(Qid){
			$(this).getAjaxFilter('Y', 'getOnlyUOC', Qid, function(response) {
				var GroupUOC = response;
				var Template = "";
				for(var i in GroupUOC){ //UOC
					var uoc = $('#newuoc').html();
					uoc = uoc.replace(/\xxidxx/g,i);
					uoc = uoc.replace(/\xxUOCdataxx/g,GroupUOC[i].stdName);
					uoc = uoc.replace(/\xxUOCidxx/g,GroupUOC[i].stdID);
					uoc = '<div id="AddUOC'+i+'">'+uoc+'</div>';
					$("#templateUOClist").append(uoc);
					$('#UOCamount-'+GroupUOC[i].stdID+'-'+i).attr('name', 'amount[]');
					$('#UOCid-'+GroupUOC[i].stdID).attr('name', 'uoc_id[]');
				}
				$("#submitTemplateUoc").prop('disabled', true);
				$('#totalUOC').html(0)
			});
		}
	}


	$('#qualification').change(function(){
		$(this).DefaultUOC();
		$('#btnaddUOC').css('display', '');
		var id = $('div[id^="AddUOC"]').length;
		if($('#templateType').val() == 3){
			$(this).SetEOCList(id);
		}else if($('#templateType').val() == 2){
			$(this).SetUOCList();
		}else{
			$(this).SetLevelList();
		}
		CheckSubmitButton();
	});
	$('#profession').change(function(){
		$(this).DefaultUOC();
		$('#btnaddUOC').css('display', 'none');
		CheckSubmitButton();
	});
	// $('#industry').change(function(){
	$('#industryGroup').change(function(){
		$(this).DefaultUOC();
		$('#btnaddUOC').css('display', 'none');
		CheckSubmitButton();
	});
	$('#templateType').change(function(){
		CheckSubmitButton();
	});


	$('#difficulty').change(function(){
		CheckSubmitButton();
	});
	$('#executionTime').change(function(){
		CheckSubmitButton();
	});
	$('#templateName').change(function(){
		CheckSubmitButton();
	});
	$('#templateType').change(function(){
		$(this).DefaultUOC();
		var id = $('div[id^="AddUOC"]').length;
		if($(this).val() == 1){
			$(this).SetLevelList();
			$('#templateEoc').hide();
			$('#templateUoc').hide();
			$('#templateLevel').show();
		}else if($(this).val() == 2){
			$(this).SetUOCList();
			$('#templateEoc').hide();
			$('#templateUoc').show();
			$('#templateLevel').hide();
		}else if($(this).val() == 3){
			$(this).SetEOCList(id);
			$('#templateEoc').show();
			$('#templateUoc').hide();
			$('#templateLevel').hide();
		}
	})

	if($('#templateType').val() == 3){
		CheckAmountEOC('EOCamount');
	}else if($('#templateType').val() == 2){
		CheckAmountEOC('UOCamount');
	}else{
		CheckSubmitButton();
	}
});