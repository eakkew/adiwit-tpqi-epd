$(function() {

	$('select').multiselect({
		enableClickableOptGroups: true,
		// includeSelectAllOption: true,
		buttonWidth: 200,
		maxHeight: 400,
		enableFiltering: true,
		nonSelectedText: $(this).data('nonSelectedText'),
	});

	$('#newEntryBottom').html($('#newEntryTop').html());
});