<?php

return [
	'automaticGeneration'					=> 'สร้างอัตโนมัติ',
	'create'								=> 'สร้างชุดคำถาม',
	'day'									=> 'วัน',
	'hour'									=> 'ชั่วโมง',
	'manualGeneration'						=> 'สร้างเอง',
	'minute'								=> 'นาที',
	'month'									=> 'เดือน',
	'set'									=> 'จำนวนชุด',
	'totaleQuestions'						=> 'จำนวน :count ข้อ',
	'year'									=> 'ปี',
	'notEnoughQuestion'						=> 'จำนวนคำถามตามเงื่อนไขของแม่แบบ ไม่เพียงพอต่อการสร้างชุดข้อสอบ',
];