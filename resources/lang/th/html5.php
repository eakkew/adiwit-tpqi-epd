<?php

return [
	'app'							=> 'โปรแกรม',
	'title'						=> 'TPQI',
	'settings'				=> 'เซ็ตติ้ง',
	'profile'					=> 'โปรไฟล์',
	'logout'					=> 'ล็อกเอ้าท์',
	'owner'						=> 'TPQI',
	'print'						=> 'Print',
	'pdf'							=> 'PDF',
];
