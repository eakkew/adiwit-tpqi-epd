<?php

return [
	'create'						=> 'เพิ่มคำถาม',
	'createQuestion'				=> 'สร้างคำถาม',
	'edit'							=> 'แก้ไขคำถาม',
	'difficulty'					=> 'ระดับความยาก',
	'difficulty-none'				=> 'ไม่ระบุ',
	'difficulty-easy'				=> 'ระดับง่าย',
	'difficulty-medium'				=> 'ระดับกลาง',
	'difficulty-hard'				=> 'ระดับยาก',
	'executionTime'					=> 'เวลาในการทำ',
	'executionTimeUnit' 			=> 'นาที',
	'trusted'						=> 'ผ่านการตรวจสอบความเชื่อมั่นของเครื่องมือ',
	'isCorrectAnswer'				=> 'คำตอบที่ถูกต้อง',
	'answer'						=> 'คำตอบ',
	'moreAnswers'					=> 'เพิ่มคำตอบ',
	'submit'						=> 'บันทึก',
	'deletedOn'						=> 'ถูกยกเลิกการใช้งานเมื่อวันที่ :date',
	'confirmDeleteBody'				=> '<div>คลิ๊กปุ่ม [ยืนยัน] เพื่อยกเลิกการใช้งาน :</div>:item',
	'type'							=> 'ประเภทคำถาม',

	// for question/index
	'templateTypelevel'				=> 'คำถามตามคุณวุฒิวิชาชีพ', 
	'templateTypeuoc'				=> 'คำถามตามหน่วยสมรรถนะ', 
	'templateTypeeoc'				=> 'คำถามตามหน่วยสมรรถนะย่อย', 

	// for field type in fact_question
	'questionType1'					=> 'คำถามตามคุณวุฒิ', 
	'questionType2'					=> 'คำถามตามหน่วยสมรรถนะ', 
	'questionType3'					=> 'คำถามตามหน่วยสมรรถนะย่อย', 

	'savedQuestion'					=> 'บันทึกคำถามแล้ว',
];