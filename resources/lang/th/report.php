<?php

return [
	'title'						=> 'รายงาน',
	'uocFrequency'		=> 'ความถี่ในการเรียกใช้งาน UOC',
	'frequency'			=> 'ความถี่',
	'times'			=> 'ครั้ง',
	'typeData'			=> 'ชนิดข้อมูล',
];