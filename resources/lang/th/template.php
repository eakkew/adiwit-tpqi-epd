<?php

return [
	'create'						=> 'เพิ่มแม่แบบ',
	'editTemplate'					=> 'แก้ไขแม่แบบ',
	'edit'							=> 'บันทึกการแก้ไข',
	'totalUOC'						=> 'UOC รวม',
	'averageDifficulty'				=> 'ความยากเฉลี่ย',
	'Difficulty'					=> 'ระดับความยาก',
	'difficulty-none'				=> 'ไม่ระบุ',
	'difficulty-easy'				=> 'ระดับง่าย',
	'difficulty-medium'				=> 'ระดับกลาง',
	'difficulty-hard'				=> 'ระดับยาก',	
	'maxExecutionTime'				=> 'เวลาในการทำข้อสอบ',
	'templateName'					=> 'ชื่อแม่แบบ',
	'questionCount'					=> 'จำนวนข้อ',
	'maxExecutionTimeUnit'			=> 'นาที',
	'submit'						=> 'เพิ่มแม่แบบ',
	'amount'						=> 'จำนวน',
	'amountUnit'					=> 'ข้อ',
	'moreUOCs'						=> 'เพิ่ม UOC',
	'type'							=> 'ประเภทแม่แบบ',

	// for template/index
	'templateTypelevel'					=> 'แม่แบบตามคุณวุฒิวิชาชีพ', 
	'templateTypeuoc'					=> 'แม่แบบตามหน่วยสมรรถนะ', 
	'templateTypeeoc'					=> 'แม่แบบตามหน่วยสมรรถนะย่อย', 

	// for field type in fact_question
	'templateType1'					=> 'แม่แบบตามคุณวุฒิวิชาชีพ', 
	'templateType2'					=> 'แม่แบบตามหน่วยสมรรถนะ', 
	'templateType3'					=> 'แม่แบบตามหน่วยสมรรถนะย่อย', 
];