@extends('layout.html5')

@section('head')
	<style>
		{!! File::get(public_path() . '/css/dataTables.bootstrap.css') !!}
		{!! File::get(public_path() . '/css/template/index.css') !!}
	</style>
	<script type="text/javascript">
		{!! File::get(public_path() . '/js/dataTables.min.js') !!}
		{!! File::get(public_path() . '/js/dataTables.bootstrap.js') !!}
		{!! File::get(public_path() . '/js/template/index.js') !!}
	</script>
@stop

@section('body')
	<div class="container">
		<div class="row">

			<div class="col-md-3">
			    <div class="panel panel-default">
			        <div class="panel-heading">
                        <label>สถิติ: </label>
			        </div>
			        <div class="panel-body" >

			        </div>
			        <div class="panel-footer">

			        </div>
			    </div>

			</div>

			<div class="col-md-9">

			</div>

		</div>
	</div>
@stop
