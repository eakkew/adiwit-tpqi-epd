<?php use Carbon\Carbon; ?>
@extends('layout.html5')

@section('head')
@stop

@section('body')
	<?php 
		$monName=array("1"=>"มกราคม","2"=>"กุมภาพันธ์","3"=>"มีนาคม","4"=>"เมษายน","5"=>"พฤษภาคม","6"=>"มิถุนายน","7"=>"กรกฎาคม","8"=>"สิงหาคม","9"=>"กันยายน","10"=>"ตุลาคม","11"=>"พฤศจิกายน","12"=>"ธันวาคม");
		$dt = Carbon::parse(Carbon::now());
		$page = 1;
		$col = 1;
		$createHeader = true;
		$createCol = true;
        $choices = hexdec('0E01'); // First character of the Thai Alphabet
        $maximumAnswer = 0
    ?>
	<div class="container" style="background-color: white; padding: 20px 40px;">
        <div class=" hidden-print">
            <div class="col-xs-12 text-right">
                <a target="_blank" href="{{ action('ReportController@show', [ 'id' => $exam->id, 'type' => 'answerPDF' ]) }}" class="btn btn-default"><i class="fa fa-print"></i> {{ trans('html5.print') }}</a>
            </div>
        </div>
		{{--@for($examQuestionIndex = 0 ; $examQuestionIndex<100 ;$examQuestionIndex++)--}}
		@foreach ($exam->examQuestions as $examQuestionIndex => $examQuestion)
		@if($createHeader)
	    <div class="text-right">
	        <div class="col-xs-12" >
	        <h4>
	        {{ trans('keyword.answer')}}
	        </h4>
	        </div>
	    </div>
	    <div class="row">
			<div class="col-xs-6">
				<img src="{{ asset('tpqi_epd/images/paper_header.jpg') }}" class="img img-responsive">
			</div>
			<div class="col-xs-6">
				<p class="col-xs-12 text-right">
					{{-- trans('keyword.examinationDate', array('date' => $exam->date->day . ' ' . $monName[$exam->date->month] . ' พ.ศ. ' . ($exam->date->year + 543))) --}}
				</p>
				<p class="col-xs-12 text-right">
					{{-- trans('keyword.location') . " : " . $exam->location --}}
				</p>
			</div>
			<div class="clearfix"></div>
			<h1 class="text-center">{{ trans('keyword.answer') . ' ' . trans('keyword.set') . ' ' . $exam->subset}}</h1>
		</div>
		@endif
		@if($createHeader) <div class="row"><div class="col-xs-12"><hr></div></div> @endif
        @if($createCol)
            @if($createHeader)
            <div class="row">
            <?php $createHeader = false; ?> @endif

            <div class="col-xs-6">
                <div class="row">
                    <div class="col-xs-2"></div>
                    @for ($choiceIndex = 0; $choiceIndex < 8; $choiceIndex++)
                        @if($choiceIndex != 2 && $choiceIndex != 4 && $choiceIndex != 5)
                            <div class="col-xs-2">
                                {{ json_decode('"\u0'.dechex($choices+$choiceIndex).'"') }}
                            </div>
                        @endif
                    @endfor
                </div>

        <?php $createCol = false; ?>
		@endif
		<!-- Real Data -->
			<div class="row">
				<div class="col-xs-2">
					<p class="form-control-static">
					{{ $examQuestionIndex+1 }})</p>
				</div>
				<?php $maxAnswer = rand(3, 11); ?>
				@foreach ($examQuestion->examAnswers as $answer)
					<div class="col-xs-2">
						<div class="checkbox">
							<label><input type="checkbox" value="{{ $answer->id }}"></label>
						</div>
					</div>
				@endforeach
			</div>
			<?php
			    if($page ==1){
                    if($examQuestionIndex%21 == 20){
                        $createCol = true;
                        if($col % 2 == 0){
                            $page ++;
                            echo "</div>";

                            $createHeader = true;
                        }
                        $col ++;
                        echo "</div>";
                    }
			    }
			    else{
                    if(($examQuestionIndex - 42)%23 == 22){
                        $createCol = true;
                        if($col % 2 == 0){
                            $createHeader = true;
                            $page ++;
                            echo "</div>";
                        }
                        $col ++;
                        echo "</div>";
                    }
			    }
			?>
		@endforeach
		{{--@endfor--}}
		    </div>
		</div>
		<hr>
		<div class="row">
			<div class="col-xs-4">
				{{ trans('command.printedBy', array('name' => Request::session()->get('tpqi.permission.perName'))) }}
			</div>
			<div class="col-xs-4 text-center">
				{{ trans('command.printedAt', array('location' => $exam->location)) }}
			</div>
			<div class="col-xs-4 text-right">
				{{ trans('command.printedOn', array('date' => $dt->day . ' ' . $monName[$dt->month] . ' ' . ($dt->year+543) . ' [' . $dt->toTimeString() .']' )) }}
			</div>
		</div>
        <div class="container hidden-print" style="margin-top: 20px;">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <a href="{{ action('ReportController@show', [ 'id' => $exam->id, 'type' => 'answerPDF' ]) }}" class="btn btn-default"><i class="fa fa-print"></i> {{ trans('html5.print') }}</a>
                </div>
            </div>
        </div>
	</div>
@stop