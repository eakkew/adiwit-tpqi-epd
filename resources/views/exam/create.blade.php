@extends('layout.html5')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/bootstrap-typeahead.css') }}" defer="defer">
    <link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/bootstrap-select.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/exam/create.css') }}">
    <script type="text/javascript" src="{{ asset('tpqi_epd/js/bootstrap-select.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('tpqi_epd/js/exam/create.js') }}"></script>
    <style>
        .p-style{
            white-space: initial;
        }
        p img{
        max-width: 100%;
        }
    </style>
@stop

@section('body')
    <?php
        $selectedQuestions = [];
        $selectedAnswers =  [];
        if(isset($answersID)) $selectedAnswers = $answersID;
        $correctAnswers = [];
        if(isset($exam)){
            $templateHeader = $exam->templateHeaderTrash;
            foreach($exam->examQuestions as $examQuestion){
                $selectedQuestions[$examQuestion->question->eoc_id][] = (int)$examQuestion->question_id;
                foreach($examQuestion->examAnswers as $examAnswer){
                    $selectedAnswers[$examQuestion->question_id][] = (int)$examAnswer->answer_id;
                    if($examAnswer->correct_answer) $correctAnswers[$examQuestion->question_id] = (int)$examAnswer->answer_id;
                }
            }
        }elseif($questionsID){
            $selectedQIndex = 0;
            // $selectedAIndex = 0;
            foreach($templateHeader->templateItemsTrash as $templateItem){
                for($i=0; $i<$templateItem->amount; $i++){
                    if($templateHeader->type == 'level'){
                        $selectedQuestions[0][] = (int)$questionsID[$selectedQIndex];
                    }elseif($templateHeader->type == 'uoc'){
                        $selectedQuestions[$templateItem->uoc_id][] = (int)$questionsID[$selectedQIndex];
                    }else{
                        $selectedQuestions[$templateItem->eoc_id][] = (int)$questionsID[$selectedQIndex];
                    }

                    $selectedQIndex++;
                    // $selectedAIndex++;
                }
            }
        }
    ?>
    <!-- set selected data to javascript -->
    <script type="text/javascript" defer="defer">
        selectedQuestions = JSON.parse('<?php echo json_encode($selectedQuestions); ?>');
        selectedAnswers = JSON.parse('<?php echo json_encode($selectedAnswers); ?>');
        correctAnswers = JSON.parse('<?php echo json_encode($correctAnswers); ?>');
        maxDifficulty = JSON.parse('<?php echo json_encode($templateHeader->max_difficulty); ?>');
        LimitTime = JSON.parse('<?php echo json_encode($templateHeader->max_time_needed); ?>');
    </script>
    <form action="{{ isset($exam)? action('ExamController@update', ['id' => $exam->id]) : action('ExamController@store') }}" method="post">
        @if (isset($exam))
            <input type="hidden" name="_method" value="put">
        @endif
        {!! csrf_field() !!}
        <div class="container" >
            @if(!isset($exam))
                <!-- input from coverPage -->
                <input type="hidden" class="form-control" name="date_id" value="{{ $dateID->id }}">
                <input type="hidden" class="form-control" id="level_competence_name" name="level_competence_name" value="{{ $inputData['level_competence_name'] }}">
                <input type="hidden" class="form-control" name="template_id" value="{{ $inputData['template_id'] }}">
                <input type="hidden" class="form-control" name="cb_id" value="{{ $inputData['cb_id'] }}">
                <input type="hidden" class="form-control" name="hour" value="{{ $inputData['hour'] }}">
                <input type="hidden" class="form-control" name="minute" value="{{ $inputData['minute'] }}">
                <input type="hidden" class="form-control" name="location" value="{{ $inputData['location'] }}">
                <input type="hidden" class="form-control" id="examType" levelCompetence="{{ $templateHeader->level_competence_name }}" name="type" value="{{ $templateHeader->type }}">
                @if($inputData['amount'] || $inputData['manualAmount'])
                    @if($inputData['questionsID'])
                        <input type="hidden" class="form-control" id="amount" name="amount" value="{{ $inputData['amount'] }}">
                    @else
                        <input type="hidden" class="form-control" id="amount" name="amount" value="{{ $inputData['manualAmount'] }}">
                    @endif
                @endif
            @else
                <input type="hidden" class="form-control" id="level_competence_name" name="level_competence_name" value="{{ $templateHeader->level_competence_name }}">
                <input type="hidden" class="form-control" id="examType" levelCompetence="{{ $templateHeader->level_competence_name }}" name="type" value="{{ $templateHeader->type }}">
            @endif
            <div class="container" style="background-color: white; padding: 20px; ">
                <h1 class="text-center">{{ trans('exam.create') }}</h1>
                <h3 class="text-center">{{ trans('keyword.template') . " : " . $templateHeader->name }}</h3>
                <!-- timer -->
                <div class="col-sm-5 col-md-3" >
                    {{--<div class="panel panel-default" data-spy="affix" data-offset-top="250" data-offset-bottom="100">--}}
                    <div class="panel panel-default" >
                        <div class="panel-heading">
                            {{--
                            @if(0)
                                เวลาที่เหลือ จากทั้งหมด <strong>{{ $templateHeader->max_time_needed }}</strong> นาที
                            @else
                                เวลาที่ใช้ทั้งหมด (นาที)
                            @endif
                            --}}
                            ระดับความยาก
                        </div>
                        <div class="panel-body text-center clock">
                            <p id='avgDifficulty' avgDifficulty='0'></p>
                            <h4 id='txtDifficulty'></h4>
                            <p id='alertTime' style="font-size: 15px; padding: 5px; color: #c7254e; background-color: #f9f2f4;" timeNeed='{{ $templateHeader->max_time_needed }}' hidden>
                                เวลาที่กำหนดในแม่แบบคือไม่เกิน {{ $templateHeader->max_time_needed }} นาที
                            </p>
                        </div>
                        <div class="panel-heading">
                            เวลาที่ใช้รวม (นาที)
                        </div>
                        <div class="panel-body text-center clock">
                            <p id='timer' totalTime='0'></p>
                        </div>
                        <input id='examTotalTime' value="{{ $templateHeader->max_time_needed }}" hidden>
                        <div class="panel-footer">
                            <p><strong> ข้อมูลแม่แบบ </strong></p>
                            <p><strong> ระดับความยาก : </strong>
                            @if($templateHeader && $templateHeader->max_difficulty && $templateHeader->max_difficulty == 3)
                                ยาก
                            @elseif($templateHeader && $templateHeader->max_difficulty && $templateHeader->max_difficulty == 2)
                                ปานกลาง
                            @elseif($templateHeader && $templateHeader->max_difficulty && $templateHeader->max_difficulty == 1)
                                ง่าย
                            @else
                                ไม่ระบุความยาก
                            @endif
                            </p>
                            <p><strong> เวลาที่กำหนด :</strong> {{ $templateHeader->max_time_needed }} นาที
                            <hr>
                            @if($templateHeader->type == 'eoc')
                                @foreach ($templateHeader->templateItemsTrash->groupBy('uoc_id') as $uocID => $templateItems)
                                    <p><strong>{{$templateItems->first()->uoc->stdName}}</strong> : {{ trans('exam.totaleQuestions', array('count' => $templateItems->sum('amount'))) }}</p>
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            @foreach ($templateItems as $templateItemIndex => $templateItem )
                                            <span>{{$templateItem->eoc->stdName}} </span> :  {{ trans('exam.totaleQuestions', array('count' => $templateItem->amount)) }}
                                            <div class="progress" hidden>
                                                <div id="{{$templateItem->eoc->stdID}}" showtype="eocTime" class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em; width: 0%;">
                                                    0
                                                </div>
                                            </div>
                                            <br>
                                            @endforeach
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
                <!-- row question -->
                <div class="col-sm-7 col-md-9">
                    <?php $countExamQuestionKey = 0 ?>
                    @if($templateHeader->type == 'level')
                        <div class="panel panel-default">
                            <div class="panel-heading clearfix">
                                <div class="col-sm-8">
                                    <b>{{ trans('keyword.qualification') . ' :: ' . $templateHeader->level_competence_name }} </b>
                                </div>
                                <div class="col-sm-4">
                                    <span style="float: right">{{ trans('exam.totaleQuestions', array('count' => $templateHeader->templateItemsTrash->first()->amount)) }}</span>
                                </div>
                            </div>
                            <div class="panel-body">
                                @for($i = 0; $i < $templateHeader->templateItemsTrash->first()->amount; $i++)
                                    <?php
                                        if(isset($exam)){
                                            $thisExamQuestion = $exam->examQuestions()->whereIn('question_id', $templateHeader->questionsByLevel->where('type', 'level')->lists('id'))->get();
                                        }
                                    ?>
                                    <!-- question -->
                                    <div class="col-xs-12 col-sm-12" style="    margin: 10px 0;">
                                        <select id="question_{{ '0' . '_' . $i }}" selection="question" eocID="{{ '0' }}" questionNumber="{{ $i }}" name="question_id[]" class="selectpicker" data-live-search="true" title="-- {{  trans('command.select').trans('keyword.question') }} --" data-width="100%" data-size="auto">
                                            @if(count($templateHeader->questionsByLevel->where('type', 'level')))
                                                @foreach($templateHeader->questionsByLevel->where('type', 'level') as $question)
                                                    @if($question->difficulty <= $templateHeader->max_difficulty || $templateHeader->max_difficulty == 0)
                                                        @if(isset($exam))
                                                            <option data-content="{{ ' <p class="text-right"><code>ระดับความยาก '.$question->difficulty.'</code> <code style=\'color: #4f9fcf;\'> เวลาที่กำหนด '.$question->time_needed.'นาที</code></p><p><b>' . $question->content . '</b></p>' }}" value="{{ $question->id }}" timeNeed="{{$question->time_needed}}" difficulty="{{$question->difficulty}}" {{ ($thisExamQuestion[$i]->question_id == $question->id)? 'selected' : '' }} ></option>
                                                        @elseif(count($questionsID) > 0)
                                                            <option data-content="{{ ' <p class="text-right"><code>ระดับความยาก '.$question->difficulty.'</code> <code style=\'color: #4f9fcf;\'> เวลาที่กำหนด '.$question->time_needed.'นาที</code></p><p><b>' . $question->content . '</b></p>' }}" value="{{ $question->id }}" timeNeed="{{$question->time_needed}}" difficulty="{{$question->difficulty}}" {{ ($questionsID[$countExamQuestionKey] == $question->id)? 'selected' : '' }} ></option>
                                                        @else
                                                            <option data-content="{{ ' <p class="text-right"><code>ระดับความยาก '.$question->difficulty.'</code> <code style=\'color: #4f9fcf;\'> เวลาที่กำหนด '.$question->time_needed.'นาที</code></p><p><b>' . $question->content . '</b></p>' }}" value="{{ $question->id }}" timeNeed="{{$question->time_needed}}" difficulty="{{$question->difficulty}}"></option>
                                                        @endif
                                                    @endif
                                                @endforeach
                                            @else
                                                <option data-content="-"></option>
                                            @endif
                                        </select>
                                    </div>
                                    <div id="{{ 'showAnswer_' . '0' . '_' . $i }}">

                                    </div>
                                    <?php $countExamQuestionKey++ ?>
                                @endfor

                            </div>
                            <div class="panel-footer"></div>
                        </div>
                    @elseif($templateHeader->type == 'uoc')
                        @foreach ($templateHeader->templateItemsTrash as $templateItem)
                            <div class="panel panel-default">
                                <div class="panel-heading clearfix">
                                    <div class="col-sm-8">
                                        <b>{{ trans('keyword.uoc') . ' :: ' . $templateItem->uoc->stdName }} </b>
                                    </div>
                                    <div class="col-sm-4">
                                        <span style="float: right">{{ trans('exam.totaleQuestions', array('count' => $templateItem->amount)) }}</span>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    @for($i = 0; $i < $templateItem->amount; $i++)
                                        <?php
                                            if(isset($exam)){
                                                $thisExamQuestion = $exam->examQuestions()->whereIn('question_id', $templateItem->questionByUoc->where('type', 'uoc')->lists('id'))->get();
                                            }
                                        ?>
                                        <!-- question -->
                                        <div class="col-xs-12 col-sm-12" style="    margin: 10px 0;">
                                            <select id="question_{{ $templateItem->uoc_id . '_' . $i }}" selection="question" eocID="{{ $templateItem->uoc_id }}" questionNumber="{{ $i }}" name="question_id[]" class="selectpicker" data-live-search="true" title="-- {{  trans('command.select').trans('keyword.question') }} --" data-width="100%" data-size="auto">
                                                @if(count($templateItem->question))
                                                    @foreach($templateItem->questionByUoc->where('type', 'uoc') as $question)
                                                        @if($question->difficulty <= $templateHeader->max_difficulty || $templateHeader->max_difficulty == 0)
                                                            @if(isset($exam))
                                                                <option data-content="{{ ' <p class="text-right"><code>ระดับความยาก '.$question->difficulty.'</code> <code style=\'color: #4f9fcf;\'>เวลาที่กำหนด '.$question->time_needed.'นาที</code></p><p><b>' . $question->content . '</b></p>' }}" value="{{ $question->id }}" timeNeed="{{$question->time_needed}}" difficulty="{{$question->difficulty}}" {{ ($thisExamQuestion[$i]->question_id == $question->id)? 'selected' : '' }} ></option>
                                                            @elseif(count($questionsID) > 0)
                                                                <option data-content="{{ ' <p class="text-right"><code>ระดับความยาก '.$question->difficulty.'</code> <code style=\'color: #4f9fcf;\'>เวลาที่กำหนด '.$question->time_needed.'นาที</code></p><p><b>' . $question->content . '</b></p>' }}" value="{{ $question->id }}" timeNeed="{{$question->time_needed}}" difficulty="{{$question->difficulty}}" {{ ($questionsID[$countExamQuestionKey] == $question->id)? 'selected' : '' }} ></option>
                                                            @else
                                                                <option data-content="{{ ' <p class="text-right"><code>ระดับความยาก '.$question->difficulty.'</code> <code style=\'color: #4f9fcf;\'>เวลาที่กำหนด '.$question->time_needed.'นาที</code></p><p><b>' . $question->content . '</b></p>' }}" value="{{ $question->id }}" timeNeed="{{$question->time_needed}}" difficulty="{{$question->difficulty}}"></option>
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                @else
                                                    <option data-content="-"></option>
                                                @endif
                                            </select>
                                        </div>
                                        <div id="{{ 'showAnswer_' . $templateItem->uoc_id . '_' . $i }}">

                                        </div>
                                        <?php $countExamQuestionKey++ ?>
                                    @endfor

                                </div>
                                <div class="panel-footer"></div>
                            </div>
                        @endforeach
                    @else
                        @foreach ($templateHeader->templateItemsTrash->groupBy('uoc_id') as $uocID => $templateItems)
                            <div class="panel panel-default">
                                <div class="panel-heading clearfix">
                                    <div class="col-sm-8">
                                        <b>{{ trans('keyword.uoc') . ' :: ' . $templateItems->first()->uoc->stdName }} </b>
                                    </div>
                                    <div class="col-sm-4">
                                        <span style="float: right">{{ trans('exam.totaleQuestions', array('count' => $templateItems->sum('amount'))) }}</span>
                                    </div>
                                </div>
                                <div class="panel-body">
                                @foreach ($templateItems as $templateItemIndex => $templateItem )
                                    <div class="col-xs-12 col-sm-8">
                                        <p class="form-control-static">
                                            {{ $templateItemIndex+1 }} ) {{ trans('keyword.eoc') . ' ' . $templateItem->eoc->stdName }}
                                        </p>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 text-right">
                                        <p class="form-control-static">{{ trans('exam.totaleQuestions', array('count' => $templateItem->amount)) }}</p>
                                    </div>
                                    @for($i = 0; $i < $templateItem->amount; $i++)
                                        <?php
                                            // $templateItem->eoc_id;
                                            $questionListID = $templateItem->question->lists('id')->toArray();
                                            if(isset($exam)){
                                                $thisExamQuestion = $exam->examQuestions()->whereIn('question_id', $questionListID)->get();
                                            }
                                        ?>
                                        <!-- question -->
                                        <div class="col-xs-12 col-sm-12" style="    margin: 10px 0;">
                                            <select id="question_{{ $templateItem->eoc_id . '_' . $i }}" selection="question" eocID="{{ $templateItem->eoc_id }}" questionNumber="{{ $i }}" name="question_id[]" class="selectpicker" data-live-search="true" title="-- {{  trans('command.select').trans('keyword.question') }} --" data-width="100%" data-size="auto">
                                                @if(count($templateItem->question))
                                                    @foreach($templateItem->question as $question)
                                                        @if($question->difficulty <= $templateHeader->max_difficulty || $templateHeader->max_difficulty == 0)
                                                            @if(isset($exam))
                                                                <option data-content="{{ ' <p class="text-right"><code>ระดับความยาก '.$question->difficulty.'</code> <code style=\'color: #4f9fcf;\'>เวลาที่กำหนด '.$question->time_needed.'นาที</code></p><p><b>' . $question->content . '</b></p>' }}" value="{{ $question->id }}" timeNeed="{{$question->time_needed}}" difficulty="{{$question->difficulty}}" {{ ($thisExamQuestion[$i]->question_id == $question->id)? 'selected' : '' }} ></option>
                                                            @elseif(count($questionsID) > 0)
                                                                <option data-content="{{ ' <p class="text-right"><code>ระดับความยาก '.$question->difficulty.'</code> <code style=\'color: #4f9fcf;\'>เวลาที่กำหนด '.$question->time_needed.'นาที</code></p><p><b>' . $question->content . '</b></p>' }}" value="{{ $question->id }}" timeNeed="{{$question->time_needed}}" difficulty="{{$question->difficulty}}" {{ ($questionsID[$countExamQuestionKey] == $question->id)? 'selected' : '' }} ></option>
                                                            @else
                                                                <option data-content="{{ ' <p class="text-right"><code>ระดับความยาก '.$question->difficulty.'</code> <code style=\'color: #4f9fcf;\'>เวลาที่กำหนด '.$question->time_needed.'นาที</code></p><p><b>' . $question->content . '</b></p>' }}" value="{{ $question->id }}" timeNeed="{{$question->time_needed}}" difficulty="{{$question->difficulty}}"></option>
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                @else
                                                    <option data-content="-"></option>
                                                @endif
                                            </select>
                                        </div>
                                        <div id="{{ 'showAnswer_' . $templateItem->eoc_id . '_' . $i }}">

                                        </div>
                                        <?php $countExamQuestionKey++ ?>
                                    @endfor
                                    <div class="clearfix"></div>
                                @endforeach
                                </div>
                                <div class="panel-footer">

                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
                <div class="row">
                    <div class="col-xs-12 text-right">
                        <?php
                            foreach ($correctAnswers as $value) {
                                $txtAnswerCorrect = $value.',';
                            }
                        ?>
                        <input id="correctAnswer" name="correctAnswer" value="{{ (isset($txtAnswerCorrect))? $txtAnswerCorrect : '' }}" hidden>
                        {{ trans('exam.totaleQuestions', array('count' => $countExamQuestionKey))}}
                    </div>
                </div>
            </div>
            <div id='cloneAnswer' style='display:none'>
                <div class="col-xs-12 col-sm-offset-1">
                    <p class="form-control-static">คำตอบ xxchoicexx )</p>
                </div>
                <div class="col-xs-12 col-sm-11 col-sm-offset-1">
                    <select id="answer_xxquestionIDxx_xxnumberxx" name='answer[xxquestionIDxx][]' selection="answer" data-live-search="true" title="-- {{  trans('command.select').trans('keyword.answerChoice') }} --" data-width="100%" data-size="auto">
                    </select>
                </div>
            </div>
            <div class="row" style="padding-top:2%">
                <div class="col-xs-12 text-center">
                    <button id="submitBtn" type="submit" class="btn btn-primary" disabled>{{ 'บันทึก' }}</button>
                </div>
            </div>
        </div>
        {!! csrf_field() !!}
    </form>
@stop
