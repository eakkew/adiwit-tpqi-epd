
@extends('layout.html5')

@section('head')
	<link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/template/create.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/question/create.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/bootstrap-select.min.css') }}">
	<script type="text/javascript" src="{{ asset('tpqi_epd/js/bootstrap-select.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('tpqi_epd/js/jquery-ui.js') }}"></script>
	<script type="text/javascript" src="{{ asset('tpqi_epd/js/exam/create_coverpage.js') }}"></script>
@stop

@section('body')
	<form id="examCover" action="{{ action('ExamController@postCreateAutomatic') }}" method="post">
		@if (isset($question))<input type="hidden" name="_method" value="put">@endif
		{!! csrf_field() !!}
		<div class="container">
			<div class="row">
				<h1 class="text-center">{{ trans('exam.create') }}</h1>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<label for="industry">
					@if( isset($question) && isset($question->industry->industry->indGRPName) )
						{{ $question->industry->industry->indGRPName }} :
					@else
						{{ trans('keyword.industry') }} :
					@endif
					</label>
					<label class="requiredField">*</label>
					@if (isset($industries))
						<select id="industryGroup" name="industry_id" data-csrf="{{ csrf_token() }}" class="selectpicker" data-live-search="true" title="-- {{  trans('command.select').trans('keyword.industry') }} --" data-width="100%" data-size="10">
							@foreach ($industries as $industry)
								<optgroup label="{{ $industry->indGRPName }}">
								@if(count($industry->industryGroupX) > 0)
									@foreach ($industry->industryGroupX as $industryGroupX)
										<option class="{{ $industry->indGRPID }}" value="{{ $industryGroupX->indID }}">{{ $industryGroupX->indName }}</option>
									@endforeach
								@else
									<option class="{{ $industry->indGRPID }}" value="{{ $industry->indGRPID }}">{{ $industry->indGRPName }}</option>
								@endif
								</optgroup>
							@endforeach
						</select>
					@else
						<p class="text-muted" id="industryGroup" name="industry_id" value="{{ $question->industry_id }}">{{ ($question->industry)? $question->industry->indName : $question->industryGroup->indGRPName }}</p>
					@endif
				</div>
				<div class="col-xs-12">
					<label class="control-label">{{ trans('keyword.qualification') }}</label>
					<label class="requiredField">*</label>
					@if (isset($industries))
						<select id="qualification" name="level_competence_name" data-csrf="{{ csrf_token() }}" onchange="checkDisable();" endfilter="Y" class="selectpicker" title="-- {{  trans('command.select').trans('keyword.qualification') }} --" data-live-search="true" data-width="100%" data-size="10" disabled></select>
					@else
						<p id="qualification" name="level_competence_name" class="text-muted" value="{{ $question->levelCompetence->levelName }}">{{ $question->levelCompetence->levelName }}</p>
					@endif

				</div>
				<div class="col-xs-12">
					<label class="control-label">{{ trans('keyword.template') }}</label>
					<label class="requiredField">*</label>
					<select id="templateHeader" name="template_id" onchange="checkDisable();" class="selectpicker" title="-- {{  trans('command.select').trans('keyword.qualification') }} --" data-live-search="true" data-width="100%" data-size="10" disabled >
					</select>
					<p id="notEnoughQuestion" class="hidden text-danger">{{ trans('exam.notEnoughQuestion') }}</p>
				</div>
				<div class="col-xs-12">
					<label class="control-label" >{{ trans('keyword.certifiedBody') }}</label>
					<label class="requiredField">*</label>
					<select id="cbdID" name="cb_id" onchange="checkDisable();" class="selectpicker" title="-- {{ trans('command.select') . trans('keyword.certifiedBody') }} --" data-live-search="true" data-width="100%" data-size="10" disabled >
					</select>
				</div>
				<div class="col-xs-12">
					<label class="control-label">{{ trans('keyword.location') }}</label>
					<input name="location" onchange="checkDisable();" type="text" class="form-control" placeholder="{{ trans('keyword.location') }}">
				</div>
				<div class="col-xs-12">
					<label class="control-label">{{ trans('keyword.examinationDate') }}</label>
					<label class="requiredField">*</label>
				</div>

				<div class="col-xs-2">
                    <input id="examDate" name="examDate" type="text" class="form-control date" placeholder="{{ trans('command.select') . trans('keyword.examinationDate') }}" style="width:100%">
                </div>
                <div class="col-xs-2">
					<select class="form-control" name="hour" onchange="checkDisable();">
						<option selected="selected" value="">-- {{ trans('command.select') . trans('exam.hour') }} --</option>
						@for ($examHour=8; $examHour<18; $examHour++)
							<option value="{{ $examHour }}">{{ str_pad($examHour, 2, 0, STR_PAD_LEFT) }}</option>
						@endfor
					</select>
				</div>
				<div class="col-xs-2">
					<select class="form-control" name="minute" onchange="checkDisable();">
						<option selected="selected" value="">-- {{ trans('command.select') . trans('exam.minute') }} --</option>
						@for ($examMinute=0; $examMinute<60; $examMinute++)
							<option value="{{ $examMinute }}">{{ str_pad($examMinute, 2, 0, STR_PAD_LEFT) }}</option>
						@endfor
					</select>
				</div>
				<!-- <div class="col-xs-2">
					<select class="form-control" name="day" onchange="checkDisable();">
						<option selected="selected" value="">-- {{ trans('command.select') . trans('keyword.examinationDate') }} --</option>
						@for ($examDay=1; $examDay<=31; $examDay++)
							<option value="{{ $examDay }}">{{ $examDay }}</option>
						@endfor
					</select>
				</div>
				<div class="col-xs-2">
					<select class="form-control" name="month" onchange="checkDisable();">
						<option selected="selected" value="">-- {{ trans('command.select') . trans('exam.month') }} --</option>
          				<?php $thaiMonth = array("มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"); ?>
						@for ($examMonth=0; $examMonth<12; $examMonth++)
							<option value="{{ $examMonth+1 }}">{{ $thaiMonth[$examMonth] }}</option>
						@endfor
					</select>
				</div>
				<div class="col-xs-2">
					<select class="form-control" name="year" onchange="checkDisable();">
						<option selected="selected" value="">-- {{ trans('command.select') . trans('exam.year') }} --</option>
						@for ($examYear=date('Y')+543+5; $examYear>=date('Y')+543; $examYear--)
							<option value="{{ $examYear }}">{{ $examYear }}</option>
						@endfor
					</select>
				</div> -->
				<div class="col-xs-12">
				</div>
				<div class="col-xs-12"><hr></div>
				<div class="col-xs-12 text-center">
					<button type="button" checkDisable="1" id="submitAuto" class="btn btn-primary" data-toggle="modal" data-target="#automatic">{{ trans('exam.automaticGeneration') }}</button>
					<button type="button" checkDisable="1" id="submitAuto" class="btn btn-primary" data-toggle="modal" data-target="#manual">{{ trans('exam.manualGeneration') }}</button>
					{{--<button type="submit" checkDisable="1" id="submitCover" class="btn btn-success" onclick="$(this).closest('form').attr('action', '{{ action('ExamController@postCreateManual') }}')">{{ trans('exam.manualGeneration') }}</button>--}}
				</div>
			</div>
		</div>
		{{-- Create Modal --}}
			<div id="automatic" tabindex="-1" class="modal fade">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title">{{ trans('exam.set') }}</h4>
						</div>
						<div class="modal-body">
							<label class="control-label">{{ trans('exam.set') }}</label>
							<input type="number" min="1" class="form-control" name="amount" placeholder="{{ trans('exam.set') }}" value='1'>
							<input id="questionsID" name="questionsID" hidden>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-primary" onclick="$(this).closest('form').attr('action', '{{ action('ExamController@postCreateAutomatic') }}')">{{ trans('exam.automaticGeneration') }}</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('command.close') }}</button>
						</div>
					</div>
				</div>
			</div>
			<div id="manual" tabindex="-1" class="modal fade">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title">{{ trans('exam.set') }}</h4>
						</div>
						<div class="modal-body">
							<label class="control-label">{{ trans('exam.set') }}</label>
							<input type="number" min="1" class="form-control" name="manualAmount" placeholder="{{ trans('exam.set') }}" value='1'>
						</div>
						<div class="modal-footer">
							<button type="submit" checkDisable="1" id="submitCover" class="btn btn-primary" onclick="$(this).closest('form').attr('action', '{{ action('ExamController@postCreateManual') }}')">{{ trans('exam.manualGeneration') }}</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('command.close') }}</button>
						</div>
					</div>
				</div>
			</div>
			<div id="notSuccess" tabindex="-1" class="modal fade">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-body">
							<label class="control-label">{{ 'ไม่สามารถสร้างชุดคำถามตามระดับความยากที่กำหนดได้ คลิ๊กปุ่ม \'ยืนยัน\' เพื่อยืนยันการสร้างชุดคำถาม ' }}</label>
							<input name='nocheck' id='nocheck' value='false' hidden>
						</div>
						<div class="modal-footer">
							<!-- <button type="submit" id="checkAgain" class="btn btn-primary">สุ่มใหม่</button> -->
							<button type="submit" id="notCheck" class="btn btn-success" onclick="$(this).closest('form').attr('action', '{{ action('ExamController@postCreateAutomatic') }}')">{{ 'สร้างชุดคำถาม' }}</button>
							<button type="button" class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
						</div>
					</div>
				</div>
			</div>
		{{-- End  Create Modal --}}
	</form>
	<script type="text/javascript">
		var thisDate = '<?php echo date("d/m/Y", strtotime("+543 years")); ?>';

		$("#examCover").submit(function(e) {
			var url = $(this).attr('action');
			// alert(url);

			var findPath = url.search("exam/create/manual");
			// alert(findPath);
			if(findPath == -1){
				$('#notCheck').prop("disabled", true);
				$('#submitCover').prop("disabled", true);
				$('#submitAuto').prop("disabled", true);

				// var url = "/exam/create/automatic"; // the script where you handle the form input.
				var url = "/tpqi_epd/exam/create/automatic"; // the script where you handle the form input.
			    $.ajax({
		            type: "POST",
		            url: url,
		            data: $("#examCover").serialize(), // serializes the form's elements.
		            success: function(data)
		            {
		            	if(data == 'no'){
		            		$('#notCheck').prop("disabled", false);
							$('#submitCover').prop("disabled", false);
							$('#submitAuto').prop("disabled", false);

		            		$('#automatic').modal('toggle');
		            		$('#notSuccess').modal('toggle');
		            	}else if(data == 'yes'){
		            		window.location="{{ action('ExamController@index') }}";
		            	}else{
		            		$('#examCover').attr('action', '{{ action("ExamController@postCreateManual") }}');
		            		$('#questionsID').val(data.listQuestionsID);
		            		$('#answersID').val(data.listQuestionsID);
		            		for(var i=0;i<data.listQuestionsID.length; i++)
		            		{
		            			$('<input name="listAnswersID['+data.listQuestionsID[i]+']" value="'+data.listAnswersID[data.listQuestionsID[i]]+'" hidden>').appendTo($('#automatic'));
		            		}
		            		$('#examCover').submit();
		            	}
		           		// console.log(data);
		               // alert(data); // show response from the php script.
		            }
		        });

			    e.preventDefault(); // avoid to execute the actual submit of the form.
			}
		});
	</script>
@stop
