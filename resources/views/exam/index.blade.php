<?php use Carbon\Carbon; ?>

@extends('layout.html5')

@section('head')
	<link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/dataTables.bootstrap.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/exam/index.css') }}">
	<script type="text/javascript" src="{{ asset('tpqi_epd/js/dataTables.min.js') }}" defer="defer"></script>
	<script type="text/javascript" src="{{ asset('tpqi_epd/js/dataTables.bootstrap.js') }}" defer="defer"></script>
	<script type="text/javascript" src="{{ asset('tpqi_epd/js/jquery-ui.js') }}"></script>
	<script type="text/javascript" src="{{ asset('tpqi_epd/js/exam/index.js') }}" defer="defer"></script>
@stop

@section('body')
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<form id="filter" class="form-inline" action="{{ action('Ajax\FilterController@selectedFilter') }}" method="post">
					<input name='nextTo' value='exam' hidden>
					{!! csrf_field() !!}
					@include('partials.filter')
				</form>
			</div>

			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<strong style="font-size: 20px;">{{ trans('keyword.exam') }}</strong>
						@if($userType != 'view')
							<a href="{{ action('ExamController@create') }}" class="btn btn-default newexam pull-right">{{ trans('command.add'). trans('keyword.exam') }}</a>
						@endif
					</div>
					<div class="panel-body">
						<div class="table-responsive">
							<?php
								$monName=array("1"=>"มกราคม","2"=>"กุมภาพันธ์","3"=>"มีนาคม","4"=>"เมษายน","5"=>"พฤษภาคม","6"=>"มิถุนายน","7"=>"กรกฎาคม","8"=>"สิงหาคม","9"=>"กันยายน","10"=>"ตุลาคม","11"=>"พฤศจิกายน","12"=>"ธันวาคม");
							?>
							@foreach ($exams as $exam)
                            <div class="panel panel-default">
								<div class="panel-body">
									<div class="row">
										<div class="col-sm-8">
											<h4> <strong>{{ 'ชุดข้อสอบ ' . $exam->templateHeaderTrash->name }}</strong> {{ trans('keyword.generateNumber') . ' ' . $exam->set . ' ' . trans('keyword.set') . ' : ' . $exam->subset }}</h4>
										</div>
										<div class="col-sm-4 text-right">
											@if($userType != 'creator')
												<span><a href="{{ action('ExamController@getQuestion', $exam->id) }}" class="btn btn-sm btn-primary">{{ trans('keyword.question') }}</a></span>
												<span><a href="{{ action('ExamController@getAnswer', $exam->id) }}" class="btn btn-sm btn-success">{{ trans('keyword.answer') }}</a></span>
												<span><a href="{{ action('ExamController@getSolution', $exam->id) }}" class="btn btn-sm btn-warning">{{ trans('keyword.solution') }}</a></span>
											@endif
										</div>
									</div>


									<div class="row">
										{{--
											@if($exam->templateHeader->industry)
												<div class="col-xs-5 col-sm-3">
													<strong>{{ trans('keyword.industry') }} :</strong>
												</div>
												<div class="col-xs-7 col-sm-9">
													{{ $exam->templateHeader->industry->industry->indGRPName }}
												</div>
											@endif
											<div class="col-xs-5 col-sm-3">
												<strong>{{ trans('keyword.profession') }} :</strong>
											</div>
											<div class="col-xs-7 col-sm-9">
												{{ ($exam->templateHeader->industry)? $exam->templateHeader->industry->indName : $exam->templateHeader->industryGroup->indGRPName }}
											</div>
										--}}
										<div class="col-xs-5 col-sm-3">
											<strong>{{ trans('keyword.qualification') }} :</strong>
										</div>
										<div class="col-xs-7 col-sm-9">
											{{ $exam->templateHeaderTrash->levelCompetence->levelName }}
										</div>
										<div class="col-xs-5 col-sm-3">
											<strong>{{ trans('keyword.certifiedBody') }} :</strong>
										</div>
										<div class="col-xs-7 col-sm-9">
											@if(isset($exam->organization->orgName))
												{{ $exam->organization->orgName }}
											@else
												{{ '-' }}
											@endif
										</div>
										<div class="col-xs-5 col-sm-3">
											<strong>{{ trans('keyword.examinationDate') }}:</strong>
										</div>
										<div class="col-xs-7 col-sm-9">
											<?php
												$dt = Carbon::parse
												(
													$exam->date->year.'-'
													.(($exam->date->month > 9)? $exam->date->month : '0'.$exam->date->month).'-'
													.(($exam->date->day > 9)? $exam->date->day : '0'.$exam->date->day).' '
													.(($exam->hour > 9)? $exam->hour : '0'.$exam->hour).':'
													.(($exam->minute > 9)? $exam->minute : '0'.$exam->minute).':00'
												);
											?>
											{{ $dt->day . ' ' . $monName[$dt->month] . ' พ.ศ. ' . ($dt->year+543) . ' (' . $dt->toTimeString() . ')' }}
										</div>
										<div class="col-xs-5 col-sm-3">
											<strong>{{ trans('keyword.location') }} :</strong>
										</div>
										<div class="col-xs-7 col-sm-9">
                                            {{ ($exam->location)? $exam->location : '-' }}
										</div>
										<div class="col-xs-5 col-sm-3">
											<strong>{{ 'วันที่ออกข้อสอบ' }} :</strong>
										</div>
										<div class="col-xs-7 col-sm-9">
											<?php $dt = Carbon::parse($exam->created_at); ?>
											{{ $dt->day . ' ' . $monName[$dt->month] . ' พ.ศ. ' . ($dt->year+543) }}
										</div>

									</div>
									<div class="row"><hr></div>
									<div class="row text-right">
										<div class="col-md-12">
											@if($userType != 'view')
												<span>
													@if($exam->used_pdf)
														<p class="btn btn-sm btn-info" title="ชุดข้อสอบนี้ถูกใช้งานแล้ว" disabled>{{ trans('command.edit') }}</p>
													@else
														<a href="{{ action('ExamController@edit', $exam->id) }}" class="btn btn-sm btn-info">{{ trans('command.edit') }}</a>
													@endif
												</span>
												<span>
													@if($exam->used_pdf)
														<p class="btn btn-sm btn-danger" title="ชุดข้อสอบนี้ถูกใช้งานแล้ว" disabled>{{ trans('command.delete') }}</p>
													@elseif(!$exam->deleted_at)
														<a href="#" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#d{{ $exam->id }}">{{ trans('command.delete') }}</a>
													@endif
												</span>
											@endif
										</div>
									</div>
								</div>
							</div>
							@endforeach
							<?php
								$url =  \Illuminate\Support\Facades\URL::current();
								$lastPage = floor($allDataCount/$perPage);
								$lastPage = ($allDataCount%$perPage)? $lastPage+1 : $lastPage;
							?>
							<ul class="pagination">
							    <li @if($page==1) class="disabled" @endif ><a href="{{$url}}/?page=1" rel="prev">«</a></li>
							    @foreach(range($page-5,$page-1) as $i)
							        @if($i >0)
							            <li><a href="{{$url}}/?page={{$i}}">{{$i}}</a></li>
							        @endif
							    @endforeach
							    <li class="active"><span>{{$page}}</span></li>
							    @foreach(range($page+1,$page+5) as $i)
	                                @if($i <= $lastPage || ($i <= $lastPage))
	                                    <li><a href="{{$url}}/?page={{$i}}" {{$allDataCount . ' | ' . $perPage . ' | ' . $lastPage}}>{{$i}}</a></li>
	                                @endif
	                            @endforeach
							    <li @if($page==$lastPage) class="disabled" @endif><a href="{{$url}}/?page={{$lastPage}}" rel="prev">»</a></li>
	                        </ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Popup Delete -->
	@foreach ($exams as $examIndex => $exam)
		<div class="modal fade" id="d{{ $exam->id }}">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">{{ trans('command.confirmDeleteHeader') }}
							<button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
						</h4>
					</div>
					<div class="modal-body">
						{{ trans('command.confirmDeleteBody', ['item' => $exam->templateHeaderTrash->name . ' : ' . trans('keyword.set') . ' ' . ($examIndex+1)]) }}
					</div>
					<div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('command.cancelButton') }}</button>
				        <a href="{{ action('ExamController@destroy', $exam->id) }}" data-csrf="{{ csrf_token() }}" data-method="delete" type="button" class="btn btn-primary">{{ trans('command.confirmButton') }}</a>
					</div>
				</div>
			</div>
		</div>
	@endforeach

@stop
