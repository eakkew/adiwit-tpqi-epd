<?php use Carbon\Carbon; ?>
@extends('layout.html5')

@section('head')
<style>
    .choice{
        margin-left: -15px;
    }
    .list-unstyled{
        padding: 5px 0;
    }
</style>
@stop

@section('body')
	<?php 
		$monName=array("1"=>"มกราคม","2"=>"กุมภาพันธ์","3"=>"มีนาคม","4"=>"เมษายน","5"=>"พฤษภาคม","6"=>"มิถุนายน","7"=>"กรกฎาคม","8"=>"สิงหาคม","9"=>"กันยายน","10"=>"ตุลาคม","11"=>"พฤศจิกายน","12"=>"ธันวาคม");
		$dt = Carbon::parse(Carbon::now());
	?>
	<div class="container" style="background-color: white; padding: 20px;">
	    <div class=" hidden-print">
            <div class="col-xs-12 text-right">
				<a target="_blank" href="{{ action('ReportController@show', [ 'id' => $exam->id, 'type' => 'questionPDF' ]) }}" class="btn btn-default"><i class="fa fa-print"></i> {{ trans('html5.print') }}</a>
            </div>
        </div>
		<?php
		// echo "<pre>";
		// print_r($exam->examQuestions->toArray());
		// echo "</pre>";
		?>
		<div class="row">
			<div class="col-xs-6">
				<img src="{{ asset('tpqi_epd/images/paper_header.jpg') }}" class="img img-responsive">
			</div>
			<div class="col-xs-6">
				<p class="col-xs-12 text-right">
					{{-- trans('keyword.examinationDate', array('date' => $exam->date->day . ' ' . $monName[$exam->date->month] . ' พ.ศ. ' . ($exam->date->year + 543))) --}}
				</p>
				<p class="col-xs-12 text-right">
					{{-- trans('keyword.location') . " : " . $exam->location --}}
				</p>
			</div>
			<div class="clearfix"></div>
			<h1 class="text-center">{{ trans('keyword.exam') . ' ' . trans('keyword.set') . ' ' . $exam->subset}}</h1>
		</div>
		<div class="row">
			<!-- Real Data -->
			@foreach ($exam->examQuestions as $examQuestionIndex => $examQuestion)
				<div class="col-xs-12">
					@if ($examQuestion->questionTrash->deleted_at)
						<div class="alert alert-danger hidden-print">
							<?php $dtDel = Carbon::parse($examQuestion->questionTrash->deleted_at) ?>
							{{ trans('question.deletedOn', array('date' => $dt->day . ' ' . $monName[$dt->month] . ' พ.ศ. ' . ($dt->year+543))) }}
						</div>
						<del class="text-muted">
					@endif
						<h4>
							<span style="float:left;">{!! $examQuestionIndex+1 . ') ' !!}</span>
							<span style="float:left; width: 96%; padding-left: 2%;">{!! $examQuestion->questionTrash->content !!}</span>
						</h4>
						<div class="clearfix"></div>
						<ul style="padding-top: 1%;">
							<?php $choices = hexdec('0E01'); // First character of the Thai Alphabet ?>
							@foreach ($examQuestion->examAnswers as $answerIndex => $examAnswer)
								<li class="list-unstyled">
									<?php
										if($answerIndex == 0){
											$choices = $choices;
										}else if($answerIndex == 2){
											$choices = $choices+2;
										}else if($answerIndex == 3){
											$choices = $choices+3;
										}else{
											$choices = $choices+1;
										}
									?>
									<span class="choice" style="float:left;">
    									{{ json_decode('"\u0'.dechex($choices).'"') }})
									</span>
									<span style="float:left; width: 96%; padding-left: 2%;">
                    <?php if($examAnswer->answer) echo $examAnswer->answer->content;?>
									</span>
									<div class="clearfix"></div>
								</li>
							@endforeach
						</ul>
						</del>
					@if ($examQuestion->questionTrash->deleted_at)
						<div class="alert alert-danger hidden-print">
							{{ trans('question.deletedOn', array('date' => $dt->day . ' ' . $monName[$dt->month] . ' พ.ศ. ' . ($dt->year+543))) }}
						</div>
						<del class="text-muted">
					@endif
					<hr>
				</div>
			@endforeach
		</div>
		<div class="row">
			<div class="col-xs-4">
				{{ trans('command.printedBy', array('name' => Request::session()->get('tpqi.permission.perName'))) }}
			</div>
			<div class="col-xs-4 text-center">
				{{ trans('command.printedAt', array('location' => $exam->location)) }}
			</div>
			<div class="col-xs-4 text-right">
				{{ trans('command.printedOn', array('date' => $dt->day . ' ' . $monName[$dt->month] . ' ' . ($dt->year+543) . ' [' . $dt->toTimeString() . ']')) }}
			</div>
		</div>
        <div class="container hidden-print" style="margin-top: 20px;">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <a href="{{ action('ReportController@show', [ 'id' => $exam->id, 'type' => 'questionPDF' ]) }}" class="btn btn-default"><i class="fa fa-print"></i> {{ trans('html5.print') }}</a>
                </div>
            </div>
        </div>
	</div>
@stop