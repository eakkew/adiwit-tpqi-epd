<div id="footer" class="hidden-print">
	<div id="footerContent" style="max-width: 1200px;margin-left: auto;margin-right: auto;text-align: left;">
		<div class="col-xs-4" style="padding-top: 10px;cursor: pointer;"
			onclick="window.open('http://www.tpqi.go.th','_blank')"
			>
			<table class="noSpacing" style="width: 100%;height: 100%;margin-top: 5px;">
				<tr>
					<td style="vertical-align: top;width: 80px;"><img src="{{ asset('tpqi_epd/images/tpqiLogo.png') }}" height="70"></td>
					<td style="font-size: 18px;line-height: 15px; font-style: italic;">
						<div>
							Thailand Professional Qualification Institute<br>
							TPQI SMART Information Service System
						<div>
						<br>
						<div style="font-size: 16px;line-height: 80%;">
							17th Floor, Sun Towers (Building B)<br>
							123 Vibhavadi-Rangsit Road Chomphon,<br>
							Chatuchak Bangkok 10900 Thailand
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div id="footerSocial" class="col-xs-5" style="padding-top: 10px;">
			
			<!-- <div id="fb-root"></div>
			<script>
				// (function(d, s, id) {
				//   var js, fjs = d.getElementsByTagName(s)[0];
				//   if (d.getElementById(id)) return;
				//   js = d.createElement(s); js.id = id;
				//   js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=550865598380344&version=v2.0";
				//   fjs.parentNode.insertBefore(js, fjs);
				// }(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-like-box" data-href="https://www.facebook.com/pages/Thailand-Professional-Qualification-Institute/307375519374416" data-width="450" data-height="200" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div> -->
		</div> 
		<div class="col-xs-3" style="padding-top: 10px;">
			<div class="col-xs-6" style="font-style: italic; ">
				<img src="{{ asset('tpqi_epd/images/contactUs.png') }}" height="25"> <span style="font-size: 18px;">Contact Us</span><br>
				<!-- <a href="mailto:info.smartdb@tpqi.go.th" style="font-size: 110%;">info.smartdb@tpqi.go.th</a>  -->
			</div>
			<div class="col-xs-6" style="text-align: right;line-height: 95%; padding-left: 5px; padding-right: 5px; font-size: 16px;">
				Site's Map<br>
				Account<br>
				Help<br>
				<br>
				User Agreement<br>
				<br>
				Terms of Use<br>
				Privacy Policy
			</div>
		</div>
		<div class="col-xs-12" style="font-size: 11px;text-align: center;padding: 10px; font-family: tahoma;">
			&copy;2004-2014 Thailand Professional Qualification Institute (Public Organization). All rights reserved.
		</div>
	</div>
</div>