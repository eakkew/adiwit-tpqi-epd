<DOCTYPE html>
<html lang="{{ Config::get('app.locale') }}">
	<head>
		@include('layout.head')
		@yield('head')
	</head>
	<body>
		<div class="hidden-print">

			<div id="headerDIV" style="
		        background-color: #122140;
		        background-image: url({{ asset('tpqi_epd/images/white.jpg') }});
		        background-size: 50% 100%;
		        background-repeat: no-repeat;
		    ">
				<div style="
			        margin-left: auto;
			        margin-right: auto;
			        max-width: 1200px;
			        position: relative;
			        background-color: #122140;
			        background-image: url({{ asset('tpqi_epd/images/bg_lang_white.png') }});
			        background-repeat: no-repeat;
			        height: 100%;
			        background-position: -370px center;
			    ">
					<table class="noSpacing" style="width: 100%;background: none;border: none;">
						<tr>
							<td style="padding-left: 20px;height: 83px;">
								<div style="position: absolute;right: 0px;top : 25px;display: none;">
									<a href="#"><img src="{{ asset('tpqi_epd/images/th.jpg') }}"></a>&nbsp;<a href="#"><img src="{{ asset('tpqi_epd/images/en.jpg') }}"></a>
								</div>
								<div style="position: relative;width: 100%;height: 100%; padding-top: 3px;">
									<table id="logoTable" class="noSpacing">
										<tr>
											<td style="vertical-align: middle;">
												<img class="menu_home" src="{{ asset('/tpqi/images/tpqiNetHeaderLogo.png') }}" style="height: 80px;">
											</td>
										</tr>
									</table>
								</div>
								<div id="topRightHeader" style="position: absolute;top: 0px;right: 0px;z-index: 1000">
									<img id="tpqiRibbon" class="menu_home" src="{{ asset('tpqi_epd/images/label_tpqi.png') }}" >
									<div style="position: absolute;width: 500px;height:90px;top:0px; right:50px;">
										<div id="memberProfile" style="margin-top: 10px;">
											<div>
												<a href="#"><span><b>TH</b></span></a>
												/
												<a href="#"><span>EN</span></a>
											</div>
											ยินดีต้อนรับ <b>{{ Request::session()->get('tpqi.permission.perName') }}</b><br>
											@if (sizeof(Auth::user()))
												<div style="font-weight: bold;">
													ผู้ดูแลคลังข้อสอบ
													<input type="button" class="form-control" value="ออกจากระบบ" onclick="window.open('/tpqi_epd/logout','_self')" style="float:right; width: auto; height: 25px; font-size: 80%; padding: 0px 10px 0px 10px;">
												</div>
											@else
												<div style="font-weight: bold;">
													<input class="form-control" type="button" value="ออกจากระบบ" onclick="window.open('/tpqi_epd/logout','_self')" style="float:right; width: auto; height: 25px; font-size: 80%; padding: 0px 10px 0px 10px;">
												</div>
											@endif
										</div>
									</div>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			
			<div id="mainMenuDIV">
				<div class="container">
					<ul  class="nav navbar-nav">
		                <?php
		                	// Mock up
		                    $array = [
		                    	["menu" => Lang::get('menu.question'), "name" => "question", "url" => action('QuestionController@index') , "submenus" => [] ],
		                    	["menu" => Lang::get('menu.template'), "name" => "template", "url" => action('TemplateController@index') , "submenus" => [] ],
		                    	["menu" => Lang::get('menu.exam'), "name" => "exam", "url" =>action('ExamController@index'), "submenus" => [] ],
		                    	["menu" => Lang::get('menu.report'), "name" => "report", "submenus" => [
		                    			[ "url" => action('ReportController@reportDiffExam'), "menu" => Lang::get('menu.reportDiffExam') ],
		                    			[ "url" => action('ReportController@reportFrequencyQuestion'), "menu" => Lang::get('menu.reportFrequencyQuestion') ],
		                    			[ "url" => action('ReportController@reportNumberExamByLevel'), "menu" => Lang::get('menu.reportNumberExamByLevel') ],
		                    			[ "url" => action('ReportController@reportNumberExamByCBD'), "menu" => Lang::get('menu.reportNumberExamByCBD') ],
		                    			[ "url" => action('ReportController@index'), "menu" => Lang::get('menu.reportEPDData') ],
		                    		]
		                    	]
		                    ];
		                    $menus = json_encode($array);
		                    $menus = json_decode($menus);
		                    // $aaa = Request::url();
		                    // $aaa = explode('/', $aaa);
		                    // print_r($aaa);
		                ?>
		                <li><a href="{{action('MenuController@index')}}" class="navbar-brand glyphicon glyphicon-home {{ (Request::is('/*'))? 'active' : '' }}"></a></li>
		                @foreach($menus as $menu)
		                    @if(count($menu->submenus) != 0)
			                    <li class="dropdown">
			                        <a href="#" class="dropdown-toggle {{ ( Request::is( $menu->name . '*') )? 'active' : '' }}" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{$menu->menu}} <span class="caret"></span></a>
			                        <ul class="dropdown-menu">
			                            @foreach($menu->submenus as $submenu)
			                            <li><a href="{{$submenu->url}}">{{$submenu->menu}}</a></li>
			                            @endforeach
			                        </ul>
			                    </li>
		                    @else
		                        <li><a href="{{$menu->url}}" class="{{ ( Request::is( $menu->name . '*') )? 'active' : '' }}">{{$menu->menu}}</a></li>
		                    @endif
		                @endforeach
					</ul>
				</div>
			</div>

		</div>
		@yield('body')
		@include('layout.footer')
	</body>
</html>