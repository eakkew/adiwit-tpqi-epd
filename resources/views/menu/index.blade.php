@extends('layout.html5')

@section('head')
  <link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/menu/index.css') }}">
@stop

@section('body')
<div class="container">
    <div class="row">

        <div class="col-md-3">
            <div class="row">
                <div class="col-md-12 col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-body" align="center">
                            <img src="{{ asset('tpqi_epd/images/tpqiLogo.png') }}">
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <label>ข้อมูลภาพรวม: </label>
                        </div>
                        <div class="panel-body" >
                            <div class="row">
                                <div class="col-xs-12"><strong>คำถามทั้งหมด :</strong> {{$question}} คำถาม</div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12"><strong>แม่แบบทั้งหมด :</strong> {{$templateHeader}} แม่แบบ</div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12"><strong>ข้อสอบทั้งหมด :</strong> {{$exam}} ชุด</div>
                            </div>

                        </div>
                        <div class="panel-footer">

                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-md-3">
                        <div class="thumbnail text-center">
                            <a href="{{ action('QuestionController@index') }}">
                                <h1 class="text-primary"><i class="fa fa-file-text-o"></i></h1></a>
                            <div class="caption">
                                <a href="{{ action('QuestionController@index') }}">
                                <h1>{{ Lang::get('menu.question') }}</h1></a>
                                {{--<p>{{ Lang::get('menu.questionDescription') }}</p>--}}
                                <p>
                                    <a href="{{ action('QuestionController@create') }}" class="btn btn-primary" role="button">&plus; {{ trans('command.add'). trans('keyword.question') }}</a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="thumbnail text-center">
                            <a href="{{ action('TemplateController@index') }}">
                            <h1 class="text-danger"><i class="fa fa-newspaper-o"></i></h1>
                            </a>

                            <div class="caption">
                                <a href="{{ action('TemplateController@index') }}">
                                <h1>{{ Lang::get('menu.template') }}</h1></a>
                                {{--<p>{{ Lang::get('menu.templateDescription') }}</p>--}}
                                <p>
                                <a href="{{ action('TemplateController@create') }}" class="btn btn-danger" role="button">&plus; {{ trans('command.add'). trans('keyword.template') }}</a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="thumbnail text-center">
                            <a href="{{ action('ExamController@index') }}"><h1 class="text-success"><i class="fa fa-copy"></i></h1></a>
                            <div class="caption">
                            <a href="{{ action('ExamController@index') }}">
                                <h1>{{ Lang::get('menu.exam') }}</h1></a>
                                {{--<p>{{ Lang::get('menu.examDescription') }}</p>--}}
                                <p>
                                <a href="{{ action('ExamController@create') }}" class="btn btn-success" role="button">&plus; {{ trans('command.add'). trans('keyword.exam') }}</a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="thumbnail text-center">
                            <a href="{{ action('ReportController@index') }}">

                            <h1 class="text-warning"><i class="fa fa-area-chart"></i></h1></a>
                            <div class="caption">
                                                        <a href="{{ action('ReportController@index') }}">

                                <h1>{{ Lang::get('menu.report') }}</h1></a>
                                {{--<p>{{ Lang::get('menu.reportDescription') }}</p>--}}
                                <p>
                                <a href="{{ action('ReportController@index') }}" class="btn btn-warning" role="button">{{ Lang::get('menu.go') }} &raquo;</a>
                                </p>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>

    </div>
</div>
  <div class="container text-center">
    <div class="row">


    </div>
  </div>
@stop
