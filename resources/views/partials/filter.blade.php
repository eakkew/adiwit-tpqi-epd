<link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/daterangepicker.css') }}">
<div class="panel panel-default">
	<div class="panel-heading">
		<label>{{ trans('command.filter') }}: </label>
	</div>
	<div class="panel-body">		
		<p>
			<label class="control-label">{{ trans('keyword.type') }} :</label><br />
			<div id='types' class="btn-group btn-group-justified" data-toggle="buttons">
                <label class="btn btn-default {{ ($type == 1)? 'active' : '' }}" >
                	<input data-csrf="{{ csrf_token() }}" type="radio" name="type" value="1" {{ ($type == 1)? 'checked' : '' }}>
                    {{ trans('keyword.typeLevel') }}
                </label>
                <label class="btn btn-default {{ ($type == 2)? 'active' : '' }}" >
                    <input data-csrf="{{ csrf_token() }}" type="radio" name="type" value="2" {{ ($type == 2)? 'checked' : '' }}>
                    {{ trans('keyword.typeUOC') }}
                </label>
                <label class="btn btn-default {{ ($type == 3 || $type == '')? 'active' : '' }}">
                    <input data-csrf="{{ csrf_token() }}" type="radio" name="type" value="3" {{ ($type == 3 || $type == '')? 'checked' : '' }}>
                    {{ trans('keyword.typeEOC') }}
                </label>
            </div>
		</p>
		<p> 
			<label class="control-label">{{ trans('keyword.industry') }} :</label><br />
			<select id="industryGroup" name="industry_id[]" data-csrf="{{ csrf_token() }}" class="multiselect" multiple="multiple" data-non-selected-text="-- {{  trans('command.select').trans('keyword.industry') }} --">
				@foreach ($industries as $industry)
					<optgroup label="{{ $industry->indGRPName }}">
					@if(count($industry->industryGroupX) > 0)				
						@foreach ($industry->industryGroupX as $industryGroupX)
							<option class="{{ $industry->indGRPID }}" value="{{ $industryGroupX->indID }}" {{ ($industriesID && $industriesID[array_search($industryGroupX->indID, $industriesID)] == $industryGroupX->indID)? 'selected' : '' }} >{{ $industryGroupX->indName }}</option>
						@endforeach
					@else
						<option class="{{ $industry->indGRPID }}" value="{{ $industry->indGRPID }}" {{ ($industriesID && $industriesID[array_search($industry->indGRPID, $industriesID)] == $industry->indGRPID)? 'selected' : '' }} >{{ $industry->indGRPName }}</option>
					@endif
					</optgroup>
				@endforeach
			</select>
		</p>
		<p>
			<label class="control-label">{{ trans('keyword.qualification') }} :</label><br />
			<select id="qualification" name="qualification[]" data-csrf="{{ csrf_token() }}" class="multiselect" multiple="multiple" data-non-selected-text="-- {{  trans('command.select').trans('keyword.qualification') }} --" <?=($qualifications && $qualifications != '[]')? '' : 'disabled' ?> >
				@if ($qualifications)
					@foreach ($qualifications as $qualification)
						<option value="{{ $qualification->levelName }}" {{ ($levelsID && $levelsID[array_search($qualification->levelName, $levelsID)] == $qualification->levelName)? 'selected' : '' }} >{{ $qualification->levelName }}</option>
					@endforeach
				@endif
			</select>
		</p>
		<p>
			<label class="control-label">{{ trans('keyword.uoc') }} / {{ trans('keyword.eoc') }} :</label><br />
			<select id="uoc" name="eocs[]" data-csrf="{{ csrf_token() }}" class="multiselect" multiple="multiple" data-non-selected-text="-- {{  trans('command.select') }} {{ trans('keyword.uoc') }} / {{ trans('keyword.eoc') }} --" <?=( ($uocs && $uocs != '[]') && $type != 1)? '' : 'disabled' ?> >
				@if ($type == 2)
					@if ($uocs)
						@foreach ($uocs as $uoc)
							<option value="{{ $uoc->stdID }}" {{ ($eocsID && $eocsID[array_search($uoc->stdID, $eocsID)] == $uoc->stdID)? 'selected' : '' }} >{{ $uoc->stdName }}</option>
						@endforeach
					@endif
				@elseif ($type == 3 || $type == '')
					@if ($uocs)
						@foreach ($uocs as $uoc)
							<optgroup label="{{ $uoc->stdName }}">
							@foreach ($uoc->eocs as $eoc)
								<option value="{{ $eoc->stdID }}" {{ ($eocsID && $eocsID[array_search($eoc->stdID, $eocsID)] == $eoc->stdID)? 'selected' : '' }} >{{ $eoc->stdName }}</option>
							@endforeach
							</optgroup>
						@endforeach
					@endif
				@endif
			</select>
		</p>
		<p>
			<label class="control-label">{{ trans('keyword.date') }} :</label><br />
			<div class="form-group" style="width:49%; margin-right: 1%">
				<input id="fromDate" name="fromDate" type="text" class="form-control date" placeholder="From Date" style="width:100%; background-color:#ffffff;" readonly>
			</div><div class="form-group" style="width:49%; margin-left: 1%;">
				<input id="toDate" name="toDate" type="text" class="form-control date"  placeholder="To Date" style="width:100%; background-color:#ffffff;" readonly>
			</div>
		</p>
		<hr>
		<div class="text-center">
			<button id='resetFilter' type="button" class="btn btn-danger"><i class="fa fa-times"></i> {{ trans('command.cancelButton') }}</button>
			<button type="submit" id="filterBtn" class="btn btn-primary"><i class="fa fa-search"></i> {{ trans('command.search') }}</button>
		</div>
	</div>
</div>
<script type="text/javascript">
	var selectedFromDate = JSON.parse('<?php echo json_encode($fromDate); ?>');
	var selectedToDate = JSON.parse('<?php echo json_encode($toDate); ?>');
</script>
{{--<script type="text/javascript" src="{{ asset('tpqi_epd/js/moment.min.js') }}"></script>--}}
{{--<script type="text/javascript" src="{{ asset('tpqi_epd/js/daterangepicker.js') }}"></script>--}}
<script>
    // $( ".date" ).datepicker();

//$('#date').daterangepicker({
//    'showDropdowns' : true,
//    'isBuddhist' : true
//}, function(start, end, label) {
//  console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
//});
</script>
