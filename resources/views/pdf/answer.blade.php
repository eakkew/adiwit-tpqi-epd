<?php use Carbon\Carbon; ?>
@extends('pdf.template')

@section('title')
@endsection

@section('sub-title')
@endsection
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/font-awesome.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/THSarabunPSK.css') }}">
        <meta http-equiv="Content-Type" content="text/html" charset="utf-8" >
        <style>
            table>tbody>tr>td,table>thead>tr>td{
                border: 1px solid #000;
                width: 25px;
                text-align: center;
                font-size: 14px;
            }
            h3.text-center{
                margin-top: 0px;
                margin-bottom: 0px;
            }
        </style>
    </head>

@section('content')
<?php
    $monName=array("1"=>"มกราคม","2"=>"กุมภาพันธ์","3"=>"มีนาคม","4"=>"เมษายน","5"=>"พฤษภาคม","6"=>"มิถุนายน","7"=>"กรกฎาคม","8"=>"สิงหาคม","9"=>"กันยายน","10"=>"ตุลาคม","11"=>"พฤศจิกายน","12"=>"ธันวาคม");
    $dt = Carbon::parse(Carbon::now());
    $page = 1;
    $col = 1;
    $createHeader = true;
    $createCol = true;
    $choices = hexdec('0E01'); // First character of the Thai Alphabet
    $maximumAnswer = 0
?>
<div class="container" style="background-color: white; padding: 0px 40px; font-family: 'THSarabunPSK';">
    @if($createHeader)
    <div class="col-xs-12 header" style="padding:0 20px;">
        @if($page == 1)
        <div class="clearfix"></div>
        <h3 class="text-center">{{ trans('keyword.answer') . ' ' . trans('keyword.set') . ' ' . $exam->subset}}</h3>
        @endif
    </div>

    @endif
    @if($createHeader) <div class="row"><div class="col-xs-12"><hr style="margin-top:10px;"></div></div> @endif
    @if($createCol)
        @if($createHeader)
            <div class="row">
                @for($a=0;$a<3;$a++)
                    <div class="col-xs-4">
                        <table>
                            <thead>
                                <tr>
                                    <td>ข้อ</td>
                                    <td>ก</td>
                                    <td>ข</td>
                                    <td>ค</td>
                                    <td>ง</td>
                                    <td>จ</td>
                                <tr>
                            </thead>
                            <tbody>
                                @for($i=0; $i<50; $i++)
                                    <tr>
                                        <td>{{ $exam->choice++ }}</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    <tr>
                                @endfor
                            </tbody>
                        </table>
                    </div>
                @endfor
            </div>
        @endif
    @endif
</div>
@endsection
</html>