<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/THSarabunPSK.css') }}">
        <meta http-equiv="Content-Type" content="text/html" charset="utf-8" >
        <style>
            .sarabun{
                font-family: 'THSarabunPSK';
            }
            .boxpage{
                height: 100%;
                width: 100%;
            }
            .box-point{
                border: 2px solid #000;
                width: 150px;
                height: 50px;
                border-radius: 10px;
            }
            .under-box{
                width: 150px;
                text-align: center;
                margin-right: 50px;
                padding-top: 10px;
                margin-bottom: 0px;
            }
            .img-center{
                position: relative;
                left: 0;
                right: 0;
                margin:auto;

            }
            .blue{
                margin-top: 20px;
                margin-left:15%;
                width: 70%;
                border: #5099D8 solid 3px;
                /*border-top: 50px;*/
                text-align: center;
                border-radius: 10px;
            }
            .blue p{
                /*border-top: #5099D8 solid 3px;*/
                /*width: 100%;*/
                padding: 10px;
            }
            .box-black{
                margin-top: 20px;
                margin-left:15%;
                border: #000 solid 3px;
                width: 70%;
                padding: 20px;
            }
            .practice{
                padding-top: 15px; 
                /*padding-left: 20px;*/
                font-size: 16px; 
                min-width: 120px;
            }
            .information{
                text-align: left;
            }
            .fill{
                border-bottom: 1px solid;
                height: 18px;
                text-align: center;
            }
            .center{
                text-align: center;
            }
            
        </style>
    </head>
    <body>
        <?php 
            use Carbon\Carbon; 
            $monName=array("1"=>"มกราคม","2"=>"กุมภาพันธ์","3"=>"มีนาคม","4"=>"เมษายน","5"=>"พฤษภาคม","6"=>"มิถุนายน","7"=>"กรกฎาคม","8"=>"สิงหาคม","9"=>"กันยายน","10"=>"ตุลาคม","11"=>"พฤศจิกายน","12"=>"ธันวาคม");
            $dt = Carbon::parse(Carbon::now());
        ?>
        <div class="boxpage sarabun">
            @if($type != 'question')
                <div class="row" style="padding-right: 50px;padding-top: 0px;">
                    <div class="box-point pull-right"></div>
                </div>
                <div class="row">
                    <p  class="under-box pull-right">
                        คะแนนที่ได้
                    </p>
                </div>
                <div class="row" style="padding-right: 50px;padding-top: 10px;">
                    <div class="box-point pull-right"></div>
                </div>
                <div class="row">
                    <p class="under-box pull-right">
                        ผ่าน/ไม่ผ่าน
                    </p>
                </div>
            @endif
            <div class="row text-center" @if($type == 'question') style="padding-top: 20%;" @endif>
                <img src="{{ asset('tpqi_epd/images/tpqiLogoTH.png') }}" style="max-height: 200px;" class="img img-responsive img-center">
            </div>

            <div class="row" style="padding-top: 40px; font-size: 16px;" align="center">
                <p>กระดาษข้อสอบประเมินสมรรถนะความรู้ ชุดข้อสอบที่ <u>{{ $exam->subset }}</u></p>
                <p>คุณวุฒิวิชาชีพ</p>
            </div>

            <div class="row" align="center">
                <div class="blue" style="max-width: 600px; font-size: 16px;">
                    <p>{{ $exam->level_competence_name }}</p>
                </div>
            </div>

            <div class="row" style="padding-top: 50px; font-size: 16px; min-width: 120px;" align="center">
                <div class="col-xs-8 col-xs-offset-2 information">
                    <div class="row">
                        <div class="col-xs-2">
                            วันที่สอบ
                        </div>
                        <div class="col-xs-5 fill">
                            {{ $exam->date->day . ' ' . $monName[$exam->date->month] . ' ' . ($exam->date->year+543) }}
                        </div>
                        <div class="col-xs-1">
                            เวลา
                        </div>
                        <div class="col-xs-4 fill">
                            {{ date('G:i', strtotime( $exam->hour . '.' . $exam->minute )) . ' - ' . date('G:i', strtotime("+".$exam->templateHeader->max_time_needed." minute", strtotime( $exam->hour . '.' . $exam->minute ))) }}  ({{ $exam->templateHeader->max_time_needed . ' นาที'}})</u>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2">
                            สถานที่สอบ
                        </div>
                        <div class="col-xs-5 fill">
                            {{ $exam->location }}
                        </div>
                        <div class="col-xs-2">
                            ห้องสอบ
                        </div>
                        <div class="col-xs-3 fill">
                            
                        </div>
                    </div>
                    @if($type == 'answer')
                        <div class="row">
                            <div class="col-xs-1">
                                ชื่อ
                            </div>
                            <div class="col-xs-5 fill">
                                
                            </div>
                            <div class="col-xs-1">
                                นามสกุล
                            </div>
                            <div class="col-xs-5 fill">
                                
                            </div>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-xs-3">
                            ลำดับที่ในใบรายชื่อ
                        </div>
                        <div class="col-xs-4 fill">
                            
                        </div>
                        <div class="col-xs-2">
                            หมายเลขที่นั่ง
                        </div>
                        <div class="col-xs-3 fill">
                            
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row" style="padding-top: 50px; font-size: 16px; min-width: 120px;">
            <!-- <div class="row practice" align="left"> -->
                <div class="col-xs-8 col-xs-offset-2">
                    <p><u>ข้อปฏิบัติ</u></p>
                </div>
                <div class="col-xs-8 col-xs-offset-2" style="font-size: 16px;">
                    1) เขียนชื่อ-นามสกุล และรหัสประจำตัวผู้สอบในข้อสอบและกระดาษคำตอบ<br>
                    2) ให้ทำลงในกระดาษคำตอบทุกข้อ และห้ามแยกกระดาษข้อสอบออกจากกัน<br>
                    3) ให้เลือกคำตอบที่ถูกที่สุดเพียงข้อเดียว โดยการกากบาทลงในช่องสี่เหลี่ยมในกระดาษคำตอบ<br>
                    4) ให้ปิดอุปกรณ์สื่อสารทุกชนิด
                </div>
                
            </div>
            
                
            </div>
            
            @if(isset($type))
                @if($type == 'answer')
                    <!-- <div class="row">
                        <div class="box-black">
                            <div class="row">
                                <div class="row">
                                    ชื่อ ............................................ นามสกุล ..............................
                                </div>
                                <div class="row">
                                    หน่วยงาน .............................................................................
                                </div>
                                <div class="row">
                                    ระดับการศึกษา .........................................................................
                                </div>
                            </div>
                        </div>
                    </div> -->
                @endif
            @endif
        </div>
    </body>
</html>