<style>
    .boxpage{
        height: 1010px;
        width: 100%;
    }
    .box-point{
        border: 2px solid #000;
        width: 150px;
        height: 50px;
    }
    .under-box{
        width: 150px;
        text-align: center;
        margin-right: 50px;
        padding-top: 10px;
    }
    .img-center{
        position: relative;
        left: 0;
        right: 0;
        margin:auto;

    }
    .blue{
        margin-top: 20px;
        margin-left:15%;
        width: 70%;
        border: #5099D8 solid 3px;
        border-top: 50px;
        text-align: center;
    }
    .blue p{
        border-top: #5099D8 solid 3px;
        width: 100%;
        padding: 10px;
    }
    .box-black{
        margin-top: 20px;
        margin-left:15%;
        border: #000 solid 3px;
        width: 70%;
        padding: 20px;
    }
</style>
<div class="boxpage">
    <div class="row" style="padding-right: 50px;padding-top: 100px;">
        <div class="box-point pull-right"></div>
    </div>
    <div class="row">
        <p  class="under-box pull-right">
            คะแนนที่ได้
        </p>
    </div>
    <div class="row" style="padding-right: 50px;padding-top: 10px;">
        <div class="box-point pull-right"></div>
    </div>
    <div class="row">
        <p class="under-box pull-right">
            ผ่าน/ไม่ผ่าน
        </p>
    </div>
    <div class="row text-center">
        <img src="{{ asset('tpqi_epd/images/tpqiLogo.png') }}" class="img img-responsive img-center">

    </div>
    <div class="row">
        <div class="blue">
            <p>ข้อสอบประเมินสมรรถนะด้านความรู้ ชุดที่ {{ $exam->subset }}</p>
            <p>
                คุณวุฒิ<br> {{ $exam->level_competence_name }}  <br><br>
                สาขาอาชีพ<br> {{ ($exam->templateHeader->industry->indName)? $exam->templateHeader->industry->indName : $exam->templateHeader->industryGroup->indGRPName }}
            </p>
        </div>
    </div>
    @if(isset($type))
        @if($type == 'answer')
            <div class="row">
                <div class="box-black">
                    <div class="row">
                        <div class="row">
                            ชื่อ ............................................ นามสกุล ..............................
                        </div>
                        <div class="row">
                            หน่วยงาน .............................................................................
                        </div>
                        <div class="row">
                            ระดับการศึกษา .........................................................................
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endif

</div>
