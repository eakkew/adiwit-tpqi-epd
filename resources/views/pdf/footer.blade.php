<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/THSarabunPSK.css') }}">
        <meta http-equiv="Content-Type" content="text/html" charset="utf-8" >
        <script>
          function subst() {
            var vars={};
            var x=window.location.search.substring(1).split('&');
            for (var i in x) {var z=x[i].split('=',2);vars[z[0]] = unescape(z[1]);}
            var x=['frompage','topage','page','webpage','section','subsection','subsubsection'];
            for (var i in x) {
              var y = document.getElementsByClassName(x[i]);
              for (var j=0; j<y.length; ++j) y[j].textContent = vars[x[i]];
            }
          }
        </script>
    </head>
    <body onload="subst()">
        <?php 
            use Carbon\Carbon; 
            $monName=array("1"=>"มกราคม","2"=>"กุมภาพันธ์","3"=>"มีนาคม","4"=>"เมษายน","5"=>"พฤษภาคม","6"=>"มิถุนายน","7"=>"กรกฎาคม","8"=>"สิงหาคม","9"=>"กันยายน","10"=>"ตุลาคม","11"=>"พฤศจิกายน","12"=>"ธันวาคม");
            $dt = Carbon::parse(Carbon::now());
        ?>
        <div class="row" style="font-family: 'THSarabunPSK'; padding-bottom:25px;">
            <div class="col-xs-4">
                {{ trans('command.printedBy', array('name' => Request::session()->get('tpqi.permission.perName'))) }}
            </div>
            <div class="col-xs-4 text-center">
                {{-- trans('command.printedAt', array('location' => $exam->location)) --}}
                {{ trans('command.printedOn', array('date' => $dt->day . ' ' . $monName[$dt->month] . ' ' . ($dt->year+543))) }}
            </div>
            <div class="col-xs-4 text-right">
                <!-- หน้าที่ <span class="page"></span> จาก <span class="topage"></span>   -->
                <span class="page"></span>
            </div>
        </div>
    </body>
</html>