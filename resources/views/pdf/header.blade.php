<div class="row">
    <div class="col-xs-8">
        <div class="row">
            <div class="col-md-12">
                <img src="{{ asset('tpqi_epd/images/paper_header.jpg') }}" class="img img-responsive">
            </div>
        </div>
    </div>
    <div class="col-xs-4 text-right">
        @if(isset($type))
            @if($type == 'answer')
                <div class="row">
                    <div class="col-md-12">
                        <strong> ชื่อ ___________________ </strong>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <strong> นามสกุล ___________________ </strong>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <strong> ระดับการศึกษา ___________________ </strong>
                    </div>
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    {{ 'วันที่สอบ : ' . $dt->day . ' ' . $monName[$dt->month] . ' ' . ($dt->year+543) }}
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    {{trans('keyword.location') . ' : ' . $exam->location }}
                </div>
            </div>
        @else
            <div class="row">
                <div class="col-md-12">
                    <strong> ชื่อ ___________________ </strong>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <strong> นามสกุล ___________________ </strong>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <strong> ระดับการศึกษา ___________________ </strong>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    {{ 'วันที่สอบ : ' . $dt->day . ' ' . $monName[$dt->month] . ' ' . ($dt->year+543) }}
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    {{trans('keyword.location') . ' : ' .  $exam->location }}
                </div>
            </div>
        @endif
    </div>
    {{--<div class="clearfix"></div>--}}
    {{--<h1 class="text-center">{{ trans('keyword.exam') . ' ' . trans('keyword.set') . ' ' . $exam->subset}}</h1>--}}
</div>