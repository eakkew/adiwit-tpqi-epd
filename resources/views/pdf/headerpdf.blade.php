<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/THSarabunPSK.css') }}">
        <meta http-equiv="Content-Type" content="text/html" charset="utf-8" >
        <style>
            .headerq{
                padding-top: 20px;
                font-family: 'THSarabunPSK';
            }
        </style>
    </head>
    <body>
        <div class="row headerq">
            <div class="col-xs-8">
                <div class="row">
                    <div class="col-md-12">
                        <img src="{{ asset('tpqi_epd/images/paper_header.jpg') }}" class="img img-responsive">
                    </div>
                </div>
            </div>
            <div class="col-xs-4 text-right">
                <?php
                    if(4 - strlen($examTransection->id) == 3){
                        $transectionID = '000' . $examTransection->id;
                    }elseif(4 - strlen($examTransection->id) == 2){
                        $transectionID = '00' . $examTransection->id;
                    }elseif(4 - strlen($examTransection->id) == 1){
                        $transectionID = '0' . $examTransection->id;
                    }else{
                        $transectionID = $examTransection->id;
                    }
                ?>
                <div class="row"><div class="col-md-12 pull-right">{{ $transectionID }}</div></div>
                @if(isset($type))
                    @if($type == 'answer')
                        <div class="row">
                            <div class="col-md-12">
                                ชื่อ ___________________
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                นามสกุล ___________________
                            </div>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-md-12">
                            {{ 'วันที่สอบ : ' . $date->day . ' ' . $monName[$date->month] . ' ' . ($date->year+543) }}
                        </div>
                    </div>
                   <!--  <div class="row">
                        <div class="col-md-12">
                            {{trans('keyword.location') . ' : ' . $exam->location }}
                        </div>
                    </div> -->
                @else
                    <div class="row">
                        <div class="col-md-12">
                            <strong> ชื่อ ___________________ </strong>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <strong> นามสกุล ___________________ </strong>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <strong> ระดับการศึกษา ___________________ </strong>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            {{ 'วันที่สอบ : ' . $date->day . ' ' . $monName[$date->month] . ' ' . ($date->year+543) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            {{trans('keyword.location') . ' : ' .  $exam->location }}
                        </div>
                    </div>
                @endif
            </div>
            {{--<div class="clearfix"></div>--}}
            {{--<h1 class="text-center">{{ trans('keyword.exam') . ' ' . trans('keyword.set') . ' ' . $exam->subset}}</h1>--}}
        </div>
    </body>
</html>