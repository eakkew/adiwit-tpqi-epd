<?php use Carbon\Carbon; ?>

@extends('pdf.template')

@section('content')
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/THSarabunPSK.css') }}">
        <meta http-equiv="Content-Type" content="text/html" charset="utf-8" >
    </head>
    <style>
        div, table, tr, td, th, tbody, thead, tfoot {
            page-break-inside: avoid !important;
        }
    </style>
    <body>
<div class="container" style="background-color: white; padding: 20px; font-family: 'THSarabunPSK';">
<?php
    $monName=array("1"=>"มกราคม","2"=>"กุมภาพันธ์","3"=>"มีนาคม","4"=>"เมษายน","5"=>"พฤษภาคม","6"=>"มิถุนายน","7"=>"กรกฎาคม","8"=>"สิงหาคม","9"=>"กันยายน","10"=>"ตุลาคม","11"=>"พฤศจิกายน","12"=>"ธันวาคม");
    $dt = Carbon::parse(Carbon::now());
?>
    <div class="row">
        <div class="col-xs-12"><strong style="font-size: 16px">จงเลือกคำตอบที่ถูกต้องที่สุด โดยเลือกตอบในกระดาษคำตอบ</strong></div>
        <!-- Real Data -->
        @foreach ($exam->examQuestions as $examQuestionIndex => $examQuestion)
            <div class="col-xs-12">
                @if ($examQuestion->questionTrash->deleted_at)
                    <div class="alert alert-danger hidden-print">
                        <?php $dtDel = Carbon::parse($examQuestion->questionTrash->deleted_at) ?>
                        {{ trans('question.deletedOn', array('date' => $dt->day . ' ' . $monName[$dt->month] . ' พ.ศ. ' . ($dt->year+543))) }}
                    </div>
                    <del class="text-muted">
                @endif
                    <h4>
                        <span style="float:left;">{!! $examQuestionIndex+1 . ') ' !!}</span>
                        <span style="float:left; width: 90%; padding-left: 2%;">{!! $examQuestion->questionTrash->content !!}</span>
                    </h4>
                    <div class="clearfix"></div>
                    <ul style="padding-top: 1%;">
                        <?php $choices = hexdec('0E01'); // First character of the Thai Alphabet ?>
                        <?php

                            // dd($examQuestion->examAnswers->toArray());

                        ?>
                        @foreach ($examQuestion->examAnswers as $answerIndex => $examAnswer)
                            <li class="list-unstyled">
                                <?php
                                    if($answerIndex == 0){
                                        $choices = $choices;
                                    }else if($answerIndex == 2){
                                        $choices = $choices+2;
                                    }else if($answerIndex == 3){
                                        $choices = $choices+3;
                                    }else{
                                        $choices = $choices+1;
                                    }
                                ?>
                                <span class="choice" style="float:left;">
                                    {{ json_decode('"\u0'.dechex($choices).'"') }})
                                </span>
                                <span style="float:left; width: 90%; padding-left: 2%;">
                                    {!! $examAnswer->answer->content !!}
                                </span>
                                <div class="clearfix"></div>
                            </li>
                        @endforeach
                    </ul>
                    </del>
                @if ($examQuestion->questionTrash->deleted_at)
                    <div class="alert alert-danger hidden-print">
                        {{ trans('question.deletedOn', array('date' => $dt->day . ' ' . $monName[$dt->month] . ' พ.ศ. ' . ($dt->year+543))) }}
                    </div>
                    <del class="text-muted">
                @endif
                <hr>
            </div>
        @endforeach
    </div>
</div>
</body>
</html>
@endsection
