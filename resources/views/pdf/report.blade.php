@extends('pdf.template')

@section('title')
    รายงาน
@endsection

@section('sub-title')
    ความถี่ในการเรียกใช้งาน UOC
@endsection

@section('content')
    <?php // include('first.php'); ?>
    @foreach ($industryGroups as $industryGroup)
    <!-- $countEocs -->
    <?php
        foreach($countEocs[$industryGroup->indID] as $index => $uocKey){
            $uocsID[] = $index;
        }
    ?>
        <div class="col-xs-5 width20">
            <strong>{{ trans('keyword.industry') }} :</strong>
        </div>
        <div class="col-xs-7 width80">
            {{ $industryGroup->indName }}
        </div>
        <div class="col-xs-5 width20">
            <strong>{{ trans('keyword.qualification') }} :</strong>
        </div>
        <div class="col-xs-7 width80" style="padding-bottom: 10px;">
            {{ $industryGroup->professionalStandards[0]->levelCompetence[0]->levelName }}
        </div>
        @foreach($industryGroup->professionalStandards[0]->levelCompetence()->whereIn('uocID', $uocsID)->groupBy('uocID')->get() as $uocID)
            <div class="col-xs-5 width20"><strong>{{ trans('keyword.uoc') }} :</strong></div>
            <div class="col-xs-7 width80">{{ $uocID->uoc->stdName }}</div>
            <?php
                foreach($countEocs[$industryGroup->indID][$uocID->uocID] as $index => $uocKey){
                    $eocsID[] = $index;
                }
            ?>
            <div class="row">
                <div class="col-xs-12">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <th>{{ trans('keyword.eoc') }}</th>
                            <th width="10%">{{ trans('report.frequency') }}</th>
                        </thead>
                        @foreach($uocID->uoc->eocs()->whereIn('stdID', $eocsID)->get() as $eoc)
                        <tbody>
                            <tr>
                                <td>{!! $eoc->stdName !!}</td>
                                <td>{{ $countEocs[$industryGroup->indID][$uocID->uocID][$eoc->stdID] }}</td>
                            </tr>
                        </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
        @endforeach
    @endforeach
    @foreach ($industries as $industry)
    <!-- $countEocs -->
    <?php
        foreach($countEocs[$industry->indGRPID] as $index => $uocKey){
            $uocsID[] = $index;
        }
    ?>
        <div class="col-xs-5 width20">
            <strong>{{ trans('keyword.industry') }} :</strong>
        </div>
        <div class="col-xs-7 width80">
            {{ $industry->indGRPName }}
        </div>
        <div class="col-xs-5 width20">
            <strong>{{ trans('keyword.qualification') }} :</strong>
        </div>
        <div class="col-xs-7 width80" style="padding-bottom: 10px;">
            {{ $industry->professionalStandards[0]->levelCompetence[0]->levelName }}
        </div>
        @foreach($industry->professionalStandards[0]->levelCompetence()->whereIn('uocID', $uocsID)->groupBy('uocID')->get() as $uocID)
            <div class="col-xs-5 width20"><strong>{{ trans('keyword.uoc') }} :</strong></div>
            <div class="col-xs-7 width80">{{ $uocID->uoc->stdName }}</div>
            <?php
                foreach($countEocs[$industry->indGRPID][$uocID->uocID] as $index => $uocKey){
                    $eocsID[] = $index;
                }
            ?>
            <div class="row">
                <div class="col-xs-12">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <th>{{ trans('keyword.eoc') }}</th>
                            <th width="10%">{{ trans('report.frequency') }}</th>
                        </thead>
                        @foreach($uocID->uoc->eocs()->whereIn('stdID', $eocsID)->get() as $eoc)
                        <tbody>
                            <tr>
                                <td>{!! $eoc->stdName !!}</td>
                                <td>{{ $countEocs[$industry->indGRPID][$uocID->uocID][$eoc->stdID] }}</td>
                            </tr>
                        </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
        @endforeach
    @endforeach
@endsection