<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/THSarabunPSK.css') }}">
        <meta http-equiv="Content-Type" content="text/html" charset="utf-8" >
        <style>
            .sarabun{
                font-family: 'THSarabunPSK';
            }
            .boxpage{
                height: 100%;
                width: 100%;
            }
            .box-point{
                border: 2px solid #000;
                width: 150px;
                height: 50px;
                border-radius: 10px;
            }
            .under-box{
                width: 150px;
                text-align: center;
                margin-right: 50px;
                padding-top: 10px;
                margin-bottom: 0px;
            }
            .img-center{
                position: relative;
                left: 0;
                right: 0;
                margin:auto;

            }
            .blue{
                margin-top: 10px;
                margin-left:15%;
                width: 70%;
                border: #5099D8 solid 1px;
                /*border-top: 50px;*/
                text-align: center;
                border-radius: 10px;
            }
            .blue p{
                /*border-top: #5099D8 solid 3px;*/
                /*width: 100%;*/
                padding: 5px;
            }
            .box-black{
                margin-top: 20px;
                margin-left:15%;
                border: #000 solid 3px;
                width: 70%;
                padding: 20px;
            }
            .practice{
                padding-top: 15px; 
                /*padding-left: 20px;*/
                font-size: 16px; 
                min-width: 120px;
            }
            .information{
                text-align: left;
            }
            .fill{
                border-bottom: 1px solid;
                height: 18px;
                text-align: center;
            }
            .center{
                text-align: center;
            }
        </style>
    </head>
    <body>
        <div class="boxpage sarabun">
            @if(isset($landscape) && $landscape)
                <div class="row text-center" style="padding-top: 10%;">
                    <img src="{{ asset('tpqi_epd/images/tpqiLogoTH.png') }}" style="max-height: 200px;" class="img img-responsive img-center">
                </div>
            @else
                <div class="row text-center" style="padding-top: 20%;">
                    <img src="{{ asset('tpqi_epd/images/tpqiLogoTH.png') }}" style="max-height: 200px;" class="img img-responsive img-center">
                </div>
            @endif

            <div class="row" style="padding-top: 40px; font-size: 26px;" align="center">
                <p><strong>{{ $reportName }}</strong></p>
                @if($from && $to && !isset($masterExam))<p><strong>ช่วงวันที่ :</strong> {{ $from[2] . ' ' . $monthName[(int)$from[1]] . ' ' . ((int)$from[0] + 543) }} - {{ $to[2] . ' ' . $monthName[(int)$to[1]] . ' ' . ((int)$to[0] + 543) }} <br>@endif
            </div>

            <div class="row" align="center">
            	@if(isset($masterExam))
                <div class="blue" style="max-width: 600px; font-size: 16px;">
                	@if($from && $to)<p><strong>ช่วงวันที่ :</strong> {{ $from[2] . ' ' . $monthName[(int)$from[1]] . ' ' . ((int)$from[0] + 543) }} - {{ $to[2] . ' ' . $monthName[(int)$to[1]] . ' ' . ((int)$to[0] + 543) }} <br>@endif
                	<strong>ชุดคำถามต้นแบบ :</strong> {{ $masterExam->templateHeader->name }}<br><br>
                	<strong>คุณวุฒิวิชาชีพ</strong><br> {{ $masterExam->level_competence_name }}</p>
                </div>
                @endif
            </div>

            <div class="row" style="padding-top: 50px; font-size: 16px; min-width: 120px;" align="center">
                <div class="col-xs-8 col-xs-offset-2 information">
                </div>
            </div>
        </div>
    </body>
</html>