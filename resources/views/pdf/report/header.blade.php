<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/THSarabunPSK.css') }}">
        <meta http-equiv="Content-Type" content="text/html" charset="utf-8" >
        <style>
            .headerq{
                /*padding-top: 20px;*/
                font-family: 'THSarabunPSK';
            }
        </style>
    </head>
    <body>
        <div class="row headerq">
            <div class="col-xs-8">
                <div class="row">
                    <div class="col-md-12">
                        <img src="{{ asset('tpqi_epd/images/paper_header.jpg') }}" class="img img-responsive">
                    </div>
                </div>
            </div>
            <div class="col-xs-4 text-right">
            	วันที่ {{ $date->day . ' ' . $monthName[(int)$date->month] . ' ' . ($date->year+543) }}
            </div>
        </div>
    </body>
</html>