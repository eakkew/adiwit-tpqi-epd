<!DOCTYPE html>
<html>
    <head>
    	<link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/normalize.min.css') }}">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/multiselect.bootstrap.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/font-awesome.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/jquery-ui.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/jquery-ui.structure.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/jquery-ui.structure.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/THSarabunPSK.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/html5.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/partials/style.css') }}">
		<script type="text/javascript" src="{{ asset('tpqi_epd/js/jquery.2.1.4.min.js') }}"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="{{ asset('tpqi_epd/js/multiselect.bootstrap.js') }}"></script>
		<script type="text/javascript" src="{{ asset('tpqi_epd/js/multiselect.collapsible-groups.bootstrap.js') }}"></script>
		<script type="text/javascript" src="{{ asset('tpqi_epd/js/partials/filter.js') }}"></script>
		<script type="text/javascript" src="{{ asset('tpqi_epd/js/restfulizer.js') }}"></script>

    	<script type="text/javascript" src="{{ asset('tpqi_epd/js/jquery-ui.js') }}" defer="defer"></script>
		<script type="text/javascript" src="{{ asset('tpqi_epd/js/report/epdData.js') }}"></script>
		<script type="text/javascript" src="{{ asset('tpqi_epd/js/bootstrap-select.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('tpqi_epd/js/highcharts.js') }}" ></script>
		<script type="text/javascript" src="{{ asset('tpqi_epd/js/highcharts-exporting.js') }}" ></script>
		<script type="text/javascript" src="{{ asset('tpqi_epd/js/highcharts.modules.drilldown.js') }}" ></script>
		<script type="text/javascript" src="{{ asset('tpqi_epd/js/export-th.js') }}" ></script>

        <link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/THSarabunPSK.css') }}">
        <meta http-equiv="Content-Type" content="text/html" charset="utf-8" >
        <style>
            table, tr, td, th, tbody, thead, tfoot {
                page-break-inside: avoid !important;
            }
        </style>
    </head>
    <body>
    <div class="container" style="background-color: white; font-family: 'THSarabunPSK';">
        <div class="row">
			<div class="col-xs-12" align="center" id="graph" style="width: 1024px">
			</div>
			<script type="text/javascript" >

				var graphName = '{{ $graphName }}';
				var graphThing = '{{ $graphThing }}';

			    // Create the chart
			    var chart = new Highcharts.Chart({
			    // $('#graph').highcharts({
			        chart: {
			        	renderTo: 'graph',
			            type: 'column',
			        },
			        "colors": [ 
			        	"#90ed7d",
		            	"#B20000"
		            ],
			        title: {
			            text: graphName
			        },
			        xAxis: {
			            type: 'category'
			        },
			        yAxis: {
		                title: {
		                    text: 'จำนวน'
		                }
		            },

			        legend: {
			            enabled: true,
			        },

			        plotOptions: {
			            series: {
			            	borderWidth: 0,
			                dataLabels: {
			                    enabled: true,
			                },
			                cursor: 'pointer',
			            }
			        },

			        series: [{
			            name: graphThing + 'ที่ใช้งาน',
			            // colorByPoint: true,
			            data: [
			            	@foreach($graphsData as $graphIndex => $data)
		            			{
		            				id: [
		            					@foreach($listGraphsID[$graphIndex]["active"] as $graphID)
		            						{{ $graphID }},
		            					@endforeach
		            				],
		            				type: '{{ $type }}',
		            				typeData: '{{ $typeData }}',
		            				activeData: true,
		            				// name: '{{ $listGraphsCode[$graphIndex] }}',
		            				name: '{{ $listGraphsName[$graphIndex] }}',
		            				y: {{ $data['active'] }},
		            				color: '#90ed7d',
		            			},
			            	@endforeach
			            ]
			        }, {
			            name: graphThing + 'ที่ยกเลิกการใช้งาน',
			            // colorByPoint: true,
			            data: [
			            	@foreach($graphsData as $graphIndex => $data)
		            			{
		            				id: [
		            					@foreach($listGraphsID[$graphIndex]["deleted"] as $graphID)
		            						{{ $graphID }},
		            					@endforeach
		            				],
		            				type: '{{ $type }}',
		            				typeData: '{{ $typeData }}',
		            				activeData: false,
		            				// name: '{{ $listGraphsCode[$graphIndex] }}',
		            				name: '{{ $listGraphsName[$graphIndex] }}',
		            				y: {{ $data['deleted'] }},
		            				color: '#B20000',
		            			},
			            	@endforeach
			            ]
			        }]
			    });
				chart.reflow()
			</script>
        </div>
    </div>
    </body>
</html>