<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/THSarabunPSK.css') }}">
        <meta http-equiv="Content-Type" content="text/html" charset="utf-8" >
        <style>
        	table, tr, td, th, tbody, thead, tfoot {
			    page-break-inside: avoid !important;
			}
        </style>
    </head>
    <body>
    <div class="container" style="background-color: white; font-family: 'THSarabunPSK';">
        <div class="row">
        	<div class="row" style="font-size: 26px;" align="center">
                <strong>{{ $reportName }}</strong><br>
                @if($from && $to)
                	<p><strong>ช่วงวันที่ :</strong> {{ $from[2] . ' ' . $monthName[(int)$from[1]] . ' ' . ((int)$from[0] + 543) }} - {{ $to[2] . ' ' . $monthName[(int)$to[1]] . ' ' . ((int)$to[0] + 543) }} <br>
                @endif
            </div>
            @if(count($examsTables) > 0)
				<div class="col-xs-12">
					<table class="table table-bordered table-hover">
					    <thead>
					      	<tr>
					        	<th style="width: 60%; text-align: left;">{{ trans('keyword.qualification') }}</th>
					        	<th style="width: 20%; text-align: center;">{{ trans('keyword.date') . trans('command.createExam') . trans('keyword.lastest') }}</th>
					        	<th style="width: 20%; text-align: center;">{{ trans('template.amount') . trans('keyword.exam') }}</th>
					      	</tr>
					    </thead>
					    <tbody>
					    @foreach($examsTables as $exam)
					    	<tr data-toggle="modal" data-target="#Modal-{{ $exam->cb_id }}">
					    		<td>{{ $exam->level_competence_name }}</td>
					    		<td class="text-center">{{ $exam->lastest }}</td>
					    		<td class="text-center">{{ $exam->count }}</td>
					    	</tr>
					    @endforeach
					    </tbody>
					</table>
				</div>
			@endif
        </div>
    </div>
    </body>
</html>