<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/bootstrap.min.css') }}">

        <meta http-equiv="Content-Type" content="text/html" charset="utf-8" >
        <style>
        body {
            font-family: sans-serif;
        }
        .title{
            font-size: 25px;
            text-align: center;
        }
        .sub-title{
            font-size: 15px;
            text-align: center;
            color: #808080;
        }
        .width40{
            width: 40%;
        }
        .width20{
            width: 20%;
        }
        .width80{
            width: 80%;
        }
        </style>
    </head>
    <body>
        <div class="title">
            @yield('title')
        </div>
        <div class="sub-title">
            @yield('sub-title')
        </div>
        @yield('content')
    </body>
</html>