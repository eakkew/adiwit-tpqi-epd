@extends('layout.html5')

@section('head')
	<link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/question/create.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/summernote.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/bootstrap-select.min.css') }}">
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
	<script type="text/javascript" src="{{ asset('tpqi_epd/js/question/create.js') }}"></script>
	<script type="text/javascript" src="{{ asset('tpqi_epd/js/bootstrap-select.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('tpqi_epd/js/summernote.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('tpqi_epd/js/summernote-th_TH.js') }}"></script>
  <style type="text/css">
    .dropdown-menu>li>a {
      white-space: normal;
    }
  </style>
@stop
@section('body')
	<div class="container">
		<div class="row">
			<h1 class="text-center">{{ (isset($question))? trans('question.edit') : trans('question.create') }}</h1>
			<hr>
			<form id="formCreate" action="{{ isset($industries)? action('QuestionController@store') : action('QuestionController@update', ['id' => $question->id]) }}" method="post">
				<input id="flagEdit" value="{{ (isset($question))? 1 : 0 }}" hidden>
				@if (isset($question))<input type="hidden" name="_method" value="put">@endif
				{!! csrf_field() !!}
				<div class="form-group col-xs-12 col-md-4">
					<label for="executionTime">{{ trans('keyword.questionType') }} :</label>
					<label class="requiredField">*</label>
					<div>
						<select id="questionType" data-csrf="{{ csrf_token() }}" name="type" class="selectpicker" title="-- {{  trans('keyword.questionType') }} --" data-width="100%" data-size="10" {{ (isset($industries))? '' : 'disabled' }}>
							@if(isset($question))
								<option value='1' {{ ($question->type == 'level')? 'selected' : '' }}> {{ trans('question.questionType1') }} </option>
								<option value='2' {{ ($question->type == 'uoc')? 'selected' : '' }}> {{ trans('question.questionType2') }} </option>
								<option value='3' {{ ($question->type == 'eoc')? 'selected' : '' }}> {{ trans('question.questionType3') }} </option>
							@else
								<option value='1' {{ (isset($getData) && $getData['type'] == 1)? 'selected' : '' }}> {{ trans('question.questionType1') }} </option>
								<option value='2' {{ (isset($getData) && $getData['type'] == 2)? 'selected' : '' }}> {{ trans('question.questionType2') }} </option>
								<option value='3' {{ ((isset($getData) && $getData['type'] == 3) || !isset($getData))? 'selected' : '' }}> {{ trans('question.questionType3') }} </option>
							@endif
						</select>
					</div>
				</div>
				<div class="form-group col-xs-12 col-md-4">
                    <label for="industry">
                    @if( isset($question) && isset($question->industry->industry->indGRPName) )
                        {{ $question->industry->industry->indGRPName }} :
                    @else
                        {{ trans('keyword.industry') }} :
                    @endif
                    </label>
                    <label class="requiredField">*</label>
                    @if (isset($industries))
                        <select id="industryGroup" name="industry_id" data-csrf="{{ csrf_token() }}" class="selectpicker" data-live-search="true" title="-- {{  trans('command.select').trans('keyword.industry') }} --" data-width="100%" data-size="10">
                        	<option value='' disable></option>
                            @foreach ($industries as $industry)
                                <optgroup label="{{ $industry->indGRPName }}">
                                @if(count($industry->industryGroupX) > 0)
                                    @foreach ($industry->industryGroupX as $industryGroupX)
                                        @if(isset($getData))
                                        	<option class="{{ $industry->indGRPID }}" value="{{ $industryGroupX->indID }}" {{ ($getData && $getData['industry_id'] == $industryGroupX->indID)? 'selected' : '' }}>{{ $industryGroupX->indName }}</option>
                                        @else
                                        	<option class="{{ $industry->indGRPID }}" value="{{ $industryGroupX->indID }}">{{ $industryGroupX->indName }}</option>
                                        @endif
                                    @endforeach
                                @else
                                	@if(isset($getData))
                                    	<option class="{{ $industry->indGRPID }}" value="{{ $industry->indGRPID }}" {{ ($getData && $getData['industry_id'] == $industry->indGRPID)? 'selected' : '' }}>{{ $industry->indGRPName }}</option>
                                    @else
                                    	<option class="{{ $industry->indGRPID }}" value="{{ $industry->indGRPID }}" >{{ $industry->indGRPName }}</option>
                                    @endif
                                @endif
                                </optgroup>
                            @endforeach
                        </select>
                    @else
                        <p class="text-muted" id="industryGroup" name="industry_id" value="{{ $question->industry_id }}">{{ ($question->industry)? $question->industry->indName : $question->industryGroup->indGRPName }}</p>
                    @endif
                </div>
                <div class="form-group col-xs-12 col-md-4">
                    <label for="qualification">{{ trans('keyword.qualification') }} :</label>
                    <label class="requiredField">*</label>
                    @if (isset($industries))
                        <select id="qualification" name="level_competence_name" data-csrf="{{ csrf_token() }}" class="selectpicker" title="-- {{  trans('command.select').trans('keyword.qualification') }} --" data-live-search="true" data-width="100%" data-size="10" {{ (isset($lists) && $lists['level'])? '' : 'disabled' }}><option value='' disable></option>
                        @if(isset($lists) && $lists['level'])
                        	@foreach($lists['level'] as $level)
                        		<option value="{{$level->levelName}}" {{ ($getData['level_competence_name'] && $getData['level_competence_name'] == $level->levelName)? 'selected' : '' }}>{{ $level->levelName }}</option>
                        	@endforeach
                        @endif
                        </select>
                    @else
                        <p class="text-muted" id="qualification" name="level_competence_name" value="{{ $question->levelCompetence->levelName }}">{{ $question->levelCompetence->levelName }}</p>
                    @endif
                </div>
				<div class="row">
				    <div class="col-xs-12">
                        @if(isset($question))
                        	<div id="selectUoc" <?=($question->type == 'level')? 'hidden' : ''?> class="form-group col-xs-12 col-md-4">
                        		@if($question->type != 'level')
	                            <div>
		                            <label for="uoc-eoc">
		                                @if( isset($question) && isset($question->eoc->uoc->stdName) )
		                                    {{ trans('keyword.uoc') . " : " . $question->eoc->uoc->stdName }}
		                                @else
		                                    {{ trans('keyword.uoc') . ' / ' . trans('keyword.eoc') }} :
		                                @endif
		                            </label>
	                            	<label class="requiredField">*</label>
	                            </div>
	                                <p class="text-muted" id="uoc" name="eoc_id" value="{{ $question->eoc_id }}">{{ ($question->type == 'uoc')? ( $question->uoc->uocTpqi->uocCode . ' : ' . $question->uoc->stdName ) : $question->eoc->stdName }}</p>
	                            @endif
	                        </div>
                        @else
                        	<div id="selectUoc" class="form-group col-xs-12 col-md-4">
	                            <div>
	                            	<label for="uoc-eoc"> {{ trans('keyword.uoc') . ' / ' . trans('keyword.eoc') }} : </label>
	                            	<label class="requiredField">*</label>
	                            </div>
	                            <select id="uoc" name="eoc_id" data-csrf="{{ csrf_token() }}" class="selectpicker" title="-- {{  trans('command.select').trans('keyword.qualification') }} --" data-live-search="true" data-width="100%" data-size="10" {{ (isset($lists) && isset($lists['uoc']))? '' : 'disabled' }}>
	                            	<option value='' disable></option>
	                            	@if(isset($lists) && isset($lists['uoc']))
	                            		@foreach($lists['uoc'] as $uoc)
	                            			@if($getData['type'] == 2)
	                            				<option value="{{ $uoc->stdID }}" {{ ($getData['eoc_id'] && $getData['eoc_id'] == $uoc->stdID)? 'selected' : '' }}>{{ $uoc->stdName }}</option>
	                            			@else
	                            				<optgroup label="{{ $uoc->stdName }}">
		                            			@foreach($uoc->eocs as $eoc)
		                            				<option value="{{ $eoc->stdID }}" {{ ($getData['eoc_id'] && $getData['eoc_id'] == $eoc->stdID)? 'selected' : '' }}>{{ $eoc->stdName }}</option>
		                            			@endforeach
		                            			</optgroup>
	                            			@endif
	                            		@endforeach
	                            	@endif
	                            </select>
	                        </div>
	                    @endif
	                    <div class="form-group col-xs-12 col-md-4">
							<label for="difficulty">{{ trans('question.difficulty') }} :</label>
							<select id="difficulty" name="difficulty" class="selectpicker" title="-- {{  trans('template.averageDifficulty') }} --" data-width="100%" data-size="10" {{ (isset($industries))? '' : 'disabled' }}>
								@if(isset($question))
									<option value='0' {{ ($question->difficulty == 0)? 'selected' : '' }}> -- {{ trans('question.difficulty-none') }} -- </option>
									<option value='0.8' {{ ($question->difficulty == 0.8)? 'selected' : '' }}>{{ trans('question.difficulty-easy') }}</option>
									<option value='0.6' {{ ($question->difficulty == 0.6)? 'selected' : '' }}>{{ trans('question.difficulty-medium') }}</option>
									<option value='0.2' {{ ($question->difficulty == 0.2)? 'selected' : '' }}>{{ trans('question.difficulty-hard') }}</option>
								@else
									<option value='0' {{ ((isset($getData) && $getData['difficulty'] == 0) || !isset($getData))? 'selected' : ''}}> -- {{ trans('question.difficulty-none') }} -- </option>
									<option value='0.8' {{ (isset($getData) && $getData['difficulty'] == 0.8)? 'selected' : '' }}>{{ trans('question.difficulty-easy') }}</option>
									<option value='0.6' {{ (isset($getData) && $getData['difficulty'] == 0.6)? 'selected' : '' }}>{{ trans('question.difficulty-medium') }}</option>
									<option value='0.2' {{ (isset($getData) && $getData['difficulty'] == 0.2)? 'selected' : '' }}>{{ trans('question.difficulty-hard') }}</option>
								@endif
							</select>
						</div>
						<div class="form-group col-xs-12 col-md-4">
							<label for="executionTime">{{ trans('question.executionTime') }} :</label>
							<div class="input-group">
								@if(isset($getData))
									<input type="number" min="0" name="time_needed" id="executionTime" class="form-control" placeholder="{{ trans('template.maxExecutionTime') }}" value="{{ (isset($getData) && $getData['time_needed'])? $getData['time_needed'] : 0  }}">
								@else
									<input type="number" min="0" name="time_needed" id="executionTime" class="form-control" placeholder="{{ trans('template.maxExecutionTime') }}" value="{{ isset($question->time_needed)? $question->time_needed : 0 }}">
								@endif
								<span class="input-group-addon">{{ trans('question.executionTimeUnit') }}</span>
							</div>
						</div>
				    </div>
				</div>
				<div class="clearfix"></div>
				<hr>
				<div id="questionlist" class="col-xs-12">
					<div class="thumbnail">
						<div class="form-group pull-left">
							<label class="control-label">{{ trans('keyword.question') }} :</label>
							<label class="requiredField">*</label>
						</div>
						<div class="text-right pull-right">
							<label for="trusted">{{ trans('question.trusted')}} :
							@if(!isset($industries) && $question->is_approved)
								<input id="trusted" name="is_approved" type="checkbox" checked value='1'>
							@else
								<input id="trusted" name="is_approved" type="checkbox" value='1'>
							@endif
					</label>
						</div>
						<div class="clearfix"></div>
						@if (isset($industries))
							<textarea id="questionContent" class="summernote" name="content"></textarea>
						@else
							<textarea id="questionContent" class="summernote" name="content">{!! ($question->content)? $question->content : '' !!}</textarea>
						@endif
					</div>
				</div>
				<div id="answerlist">
				@if (!isset($industries) && $question->answers)
					@foreach($question->answers as $index => $answer)
						<div class="col-xs-12 col-sm-6">
							<div class="thumbnail">
							<div class="form-group pull-left">
								<label class="control-label">{{ trans('question.answer') }} :</label>
							</div>
							<div class="text-right pull-right">
								<label>
									@if($answer->is_correct_answer)
										<input type="radio" id="correctAnswer-{{ $index }}" name="is_correct_answer" value="{{ $index }}" onclick="CheckSubmitButton('submitbtn');" checked>
									@else
										<input type="radio" id="correctAnswer-{{ $index }}" name="is_correct_answer" value="{{ $index }}" onclick="CheckSubmitButton('submitbtn');">
									@endif
									{{ trans('question.isCorrectAnswer')}}
								</label>
							</div>
							<div class="clearfix"></div>
							<textarea id="answer-{{ $index }}" class="summernote" correctAnswer="{{ ($answer->is_correct_answer)? 1 : 0 }}" name="answerContent[]">{{ $answer->content }}</textarea>
							<input type="text" name="answerID[]" value="{{ $answer->id }}" hidden>
							</div>
						</div>
					@endforeach
				@endif
				</div>
				<!-- START : Clone this to #answerlist -->
				<div id="newAnswer" class="hidden">
					<div class="col-sm-6 col-xs-12">
						<div class="thumbnail">
							<div class="form-group pull-left">
								<label class="control-label">{{ trans('question.answer') }} :</label>
							</div>
							<div class="text-right pull-right">
								<label>
									<input type="radio" id="correctAnswer-xxixx" name="is_correct_answer" value="xxixx" onclick="CheckSubmitButton('submitbtn');">
									{{ trans('question.isCorrectAnswer')}}
								</label>
							</div>
							<div class="clearfix"></div>
							<textarea id="answer-xxixx" class="xxsummernotexx"></textarea>
						</div>
					</div>
				</div>
				<!-- END : Clone -->
				<div class="col-sm-6 col-xs-12">
					<div id="moreAnswerFrame">
						<div id="moreAnswer" class="text-center" style="display: table;">
							<div style="display: table-cell;">
								<button id="addAnswer" type="button" class="btn btn-success">+ {{ trans('question.moreAnswers') }}</button>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<hr>
				<!-- Saved Modal -->
				<div class="modal fade" id="saveModal" hidden>
				    <div class="modal-dialog" role="document">
				        <div class="modal-content">
				            <div class="modal-header">
				                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				          		<h4 class="modal-title" id="myModalLabel">{{ trans('question.savedQuestion') }} <i class="fa fa-check-square fa-2" style="color: #21b384;"></i></h4>
				        	</div>
				        	<div class="modal-body">
				        		<div id='modalQuestion' style="padding-top: 10px;">
				        			<h4>{{ trans('keyword.question') }}</h4>
				        		</div>
				        		<div id='modalAnswer' style="padding-top: 10px;">
				        			<h4>{{ trans('question.answer') }}</h4>
				        		</div>
				        		<div id='modalSolution' style="padding-top: 10px;">
				        			<h4>{{ trans('keyword.solution') }}</h4>
				        		</div>
				        	</div>
				        	<div class="modal-footer">
				        		<a href="" url="{{ action('QuestionController@index') }}" id="editQuestion" class="btn btn-info">
				        			{{ trans('command.edit') }}
				        		</a>
				        		<a href="{{ action('QuestionController@index') }}" url="{{ action('QuestionController@index') }}" id="indexQuestion" class="btn btn-primary">
				        			{{ trans('command.goIndex') }}
				        		</a>
				        		<a href="" url="{{ action('QuestionController@index') }}" id="nextQuestion" class="btn btn-success">
				        			{{ trans('command.add'). trans('keyword.question') }}
				        		</a>
				        	</div>
				      	</div>
				    </div>
				</div>
				<!-- Show Saved Modal -->
				<div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				</div>
				<div class="col-xs-12 text-center">
					@if(isset($question))
						<button type="submit" id="submitbtn" class="btn btn-primary">{{ trans('question.submit') }}</button>
					@else
						<button type="submit" id="submitbtn" class="btn btn-primary" data-toggle="modal" data-target="#showModal">{{ trans('question.submit') }}</button>
					@endif
				</div>
			</form>
		</div>
	</div>
@stop
