@extends('layout.html5')

@section('head')
	<link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/dataTables.bootstrap.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/question/index.css') }}">
	<script type="text/javascript" src="{{ asset('tpqi_epd/js/jquery-ui.js') }}"></script>
@stop

@section('body')
	<div class="container">
		<div class="row">

			<div class="col-md-8 col-md-offset-2">
				<form id="filter" class="form-inline" action="{{ action('Ajax\FilterController@selectedFilter') }}" method="post">
					<input name='nextTo' value='question' hidden>
					{!! csrf_field() !!}
					@include('partials.filter')
				</form>
			</div>
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<strong style="font-size: 20px;">{{ trans('keyword.question') }}</strong>
						<a href="{{ action('QuestionController@create') }}" class="btn btn-sm btn-default pull-right">&plus; {{ trans('command.add'). trans('keyword.question') }}</a>
					</div>
					<div class="panel-body">
						@foreach($questions as $question)
							<!-- Question -->
							<div class="panel panel-default">
								<div class="panel-body">
									<div class="row">
										<div class="col-sm-12">
											<h4><a href="#" data-toggle="modal" data-target="#e{{ $question->id }}"> {!! $question->content !!}</a><br></h4>
										</div>
									</div>
                                    {{--
	                                    <div class="row">
											<div class="col-xs-5 col-sm-3">
												<strong>{{ trans('keyword.industry') }} :</strong>
											</div>
											<div class="col-xs-7 col-sm-9">
												{{ ($question->industry) ? $question->industry->indName : $question->industryGroup->indGRPName }}
											</div>
	                                    </div>
	                                --}}
                                    <div class="row">
										<div class="col-xs-5 col-sm-3">
											<strong>{{ trans('keyword.qualification') }} :</strong>
										</div>
										<div class="col-xs-7 col-sm-9">
											{{ $question->levelCompetence->levelName or '' }}
										</div>
                                    </div>
                                    @if($question->type == 'level')
                                    	<div class="row">
											<div class="col-xs-5 col-sm-3">
												<strong>{{ trans('keyword.questionType') }} :</strong>
											</div>
											<div class="col-xs-7 col-sm-9">
												{{ trans('question.questionType1') }}
											</div>
	                                    </div>
	                                @elseif($question->type == 'uoc')
	                                	<div class="row">
											<div class="col-xs-5 col-sm-3">
												<strong>{{ trans('keyword.questionType') }} :</strong>
											</div>
											<div class="col-xs-7 col-sm-9">
												{{ trans('question.questionType2') }}
											</div>
	                                    </div>
	                                	<div class="row">
											<div class="col-xs-5 col-sm-3">
												<strong>{{ trans('keyword.uoc') }} :</strong>
											</div>
											<div class="col-xs-7 col-sm-9">
												{{ $question->uoc->stdName or '' }}
											</div>
	                                    </div>
	                                @else
	                                	<!-- after UAT -->
	                                	<div class="row">
											<div class="col-xs-5 col-sm-3">
												<strong>{{ trans('keyword.questionType') }} :</strong>
											</div>
											<div class="col-xs-7 col-sm-9">
												{{ trans('question.questionType3') }}
											</div>
	                                    </div>
	                                	<div class="row">
											<div class="col-xs-5 col-sm-3">
												<strong>{{ trans('keyword.uoc') }} :</strong>
											</div>
											<div class="col-xs-7 col-sm-9">
												{{ $question->eoc->uoc->uocTpqi->uocCode or '' }} : {{ $question->eoc->uoc->stdName or '' }}
											</div>
	                                    </div>
	                                    <div class="row">
											<div class="col-xs-5 col-sm-3">
												<strong>{{ trans('keyword.eoc') }} : </strong>
											</div>
											<div class="col-xs-7 col-sm-9">
												{!! $question->eoc->stdName or '' !!}
											</div>
	                                    </div>
	                                @endif

                                    <div class="row">
										<div class="col-xs-5 col-sm-3">
											<strong>{{ trans('question.executionTime') }} ({{ trans('question.executionTimeUnit') }}) :</strong>
										</div>
										<div class="col-xs-7 col-sm-9">
											{{ $question->time_needed or '' }}
										</div>
                                    </div>
                                    <div class="row">
										<div class="col-xs-5 col-sm-3">
											<strong>{{ trans('question.difficulty') }} :</strong>
										</div>
										<div class="col-xs-7 col-sm-9">
											@if($question->difficulty == 0.8)
												{{ trans('question.difficulty-easy') }}
											@elseif($question->difficulty == 0.6)
												{{ trans('question.difficulty-medium') }}
											@elseif($question->difficulty == 0.2)
												{{ trans('question.difficulty-hard') }}
											@else
												{{ trans('question.difficulty-none') }}
											@endif
										</div>
									</div>
									<div class="row"><hr></div>
									<div class="row text-right">
										<div class="col-md-12">
											<span><a href="#" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#d{{ $question->id }}">{{ trans('command.delete') }}</a></span>
											@if($question->examQuestions->count())
												<span><p class="btn btn-sm btn-info" title="คำถามนี้ถูกใช้ในการออกชุดข้อสอบแล้ว" disabled>{{ trans('command.edit') }}</p></span>
											@else
												<span><a href="{{ action('QuestionController@edit', $question->id) }}" class="btn btn-sm btn-info">{{ trans('command.edit') }}</a></span>
											@endif
										</div>
									</div>
								</div>
							</div>
							<!-- Answer Popup -->
							<div class="modal fade" id="e{{ $question->id }}" style="padding: 15px">
								<div class="modal-dialog modal-lg">
									<div class="modal-content" style="margin-bottom: 15px">
									    <div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h3> คำถาม </h3>
									    </div>
										<div class="modal-body clearfix">
                                            {!! $question->content !!}
										</div>
									</div>

									<div class="modal-content" style="margin-bottom: 15px">
									    <div class="modal-header">
									        <h3> คำตอบ </h3>
									    </div>
										<div class="modal-body clearfix">
											<ul class="choice">
											<?php
											    $thaichar = ['ก','ข','ค','ง','จ','ฉ','ช','ซ','ฌ','ญ','ฐ','ฑ','ฒ'
                                                ,'ณ','ด','ต','ถ','ท','ธ','น','บ','ป','ผ','ฝ','พ','ฟ','ภ','ม','ย','ร','ล'
                                                ,'ว','ศ','ษ','ส','ห','ฬ','อ','ฮ'];
                                                $i =0;
											?>
												@foreach ($question->answers as $index => $answer)
													<li class="clearfix"><span class="choiceNam"> {{$thaichar[$i]}}. </span> <span class="choiceAnswer"> {!! $answer->content !!}</span></li>

													<?php $i++; if($answer->is_correct_answer == 1) $correct = $index; ?>
												@endforeach
												@if(!isset($correct))
													<?php 
														$correct = 1;
													?>
												@endif
											</ul>
										</div>
									</div>

									<div class="modal-content" style="margin-bottom: 15px">
									    <div class="modal-header">
									        <h3> เฉลย </h3>
									    </div>
										<div class="modal-body clearfix">
                                            <span class="choiceNam"> {{$thaichar[$correct]}}. </span> <span class="choiceAnswer"> {!! $question->answers[$correct]->content !!}</span>
										</div>
									</div>
								</div>
							</div>
							<!-- Delete Popup -->
							<div class="modal fade" id="d{{ $question->id }}">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<h4 class="modal-title">
											    {{ trans('command.confirmDeleteHeader') }}
												<button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
											</h4>
										</div>
										<div class="modal-body">
											{!! trans('question.confirmDeleteBody', ['item' => $question->content]) !!}
										</div>
										<div class="modal-footer">
									        <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('command.cancelButton') }}</button>
									        <a href="{{ action('QuestionController@destroy', $question->id) }}" data-csrf="{{ csrf_token() }}" data-method="delete" type="button" class="btn btn-primary">{{ trans('command.confirmButton') }}</a>
										</div>
									</div>
								</div>
							</div>	
						@endforeach

						<?php
							$url =  \Illuminate\Support\Facades\URL::current();
							$lastPage = floor($allDataCount/$perPage);
							$lastPage = ($allDataCount%$perPage)? $lastPage+1 : $lastPage;
						?>
						<ul class="pagination">
						    <li @if($page==1) class="disabled" @endif ><a href="{{$url}}/?page=1" rel="prev">«</a></li>
						    @foreach(range($page-5,$page-1) as $i)
						        @if($i >0)
						            <li><a href="{{$url}}/?page={{$i}}">{{$i}}</a></li>
						        @endif
						    @endforeach
						    <li class="active"><span>{{$page}}</span></li>
						    @foreach(range($page+1,$page+5) as $i)
                                @if($i <= $lastPage || ($i <= $lastPage))
                                    <li><a href="{{$url}}/?page={{$i}}" {{$allDataCount . ' | ' . $perPage . ' | ' . $lastPage}}>{{$i}}</a></li>
                                @endif
                            @endforeach
						    <li @if($page==$lastPage) class="disabled" @endif><a href="{{$url}}/?page={{$lastPage}}" rel="prev">»</a></li>
                        </ul>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop