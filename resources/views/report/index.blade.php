@extends('layout.html5')

@section('head')
@stop

@section('body')
	<div class="container">
		<div class="row">
			<h1 class="text-center">{{ trans('report.title') }}</h1>
			<div class="col-xs-12">
				<ul>
					<li><a href="{{ action('ReportController@getUocFrequency')}}">{{ trans('report.uocFrequency') }}</a></li>
				</ul>
			</div>
		</div>
	</div>
@stop
