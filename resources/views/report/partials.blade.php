@foreach ($industryGroups as $industryGroup)
<!-- $countEocs -->
<?php
    foreach($countEocs[$industryGroup->indID] as $index => $uocKey){
        $uocsID[] = $index;
    }
?>
    <div class="panel panel-default">
        <div class="panel-body">
            @foreach($industryGroup->professionalStandards[0]->levelCompetence()->whereIn('uocID', $uocsID)->groupBy('uocID')->get() as $uocID)

                <div class="row" style="background-color: #D8D8D8; margin-bottom: 5px;">
                    <div class="col-xs-12 col-sm-6">
                        <strong>{{ trans('keyword.industry') }} :</strong>
                        {{ $industryGroup->indName }}
                    </div>
                    <div class="col-xs-12 col-sm-6" style="background-color: #CACACA;">
                        <strong>{{ trans('keyword.qualification') }} :</strong>
                        {{ $industryGroup->professionalStandards[0]->levelCompetence[0]->levelName }}
                    </div>
                </div>
                <div class="row" >
                    <div class="col-xs-12 blue"><strong>{{ trans('keyword.uoc') }} : </strong>{{ $uocID->uoc->stdName }}</div>
                    <?php
                        foreach($countEocs[$industryGroup->indID][$uocID->uocID] as $index => $uocKey){
                            $eocsID[] = $index;
                        }
                    ?>
                    <div class="row" style=" margin-bottom: 20px;">
                        <div class="col-xs-12">
                            <ul>
                                @foreach($uocID->uoc->eocs()->whereIn('stdID', $eocsID)->get() as $eoc)
                                <li class="">
                                    <div class="row" style=" margin-top: -18px; ">

                                        <div class="col-xs-8 col-sm-10">
                                        {!! $eoc->stdName !!}

                                        </div>
                                        <div class="col-xs-4 col-sm-2 " style="    border-left: 1px solid #BDBDBD;">
                                        {{ trans('report.frequency') }} {{ $countEocs[$industryGroup->indID][$uocID->uocID][$eoc->stdID] }} {{ trans('report.times') }}

                                        </div>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endforeach

@foreach ($industries as $industry)
<!-- $countEocs -->
<?php
    foreach($countEocs[$industry->indGRPID] as $index => $uocKey){
        $uocsID[] = $index;
    }
?>
    <div class="panel panel-default">
        <div class="panel-body">
            @foreach($industry->professionalStandards[0]->levelCompetence()->whereIn('uocID', $uocsID)->groupBy('uocID')->get() as $uocID)
                <div class="row" style="background-color: #D8D8D8; margin-bottom: 5px;">
                    <div class="col-xs-12 col-sm-6">
                        <strong>{{ trans('keyword.industry') }} :</strong>
                        {{ $industry->indGRPName }}
                    </div>
                    <div class="col-xs-12 col-sm-6" style="background-color: #CACACA;">
                        <strong>{{ trans('keyword.qualification') }} :</strong>
                        {{ $industry->professionalStandards[0]->levelCompetence[0]->levelName }}
                    </div>
                </div>
                <div class="row" >
                    <div class="col-xs-12 blue"><strong>{{ trans('keyword.uoc') }} : </strong>{{ $uocID->uoc->stdName }}</div>
                    <?php
                        foreach($countEocs[$industry->indGRPID][$uocID->uocID] as $index => $uocKey){
                            $eocsID[] = $index;
                        }
                    ?>
                    <div class="row" style=" margin-bottom: 20px;">
                        <div class="col-xs-12">
                            <ul>
                                @foreach($uocID->uoc->eocs()->whereIn('stdID', $eocsID)->get() as $eoc)
                                <li class="">
                                    <div class="row" style=" margin-top: -18px; ">

                                        <div class="col-xs-8 col-sm-10">
                                        {!! $eoc->stdName !!}

                                        </div>
                                        <div class="col-xs-4 col-sm-2 " style="    border-left: 1px solid #BDBDBD;">
                                        {{ trans('report.frequency') }} {{ $countEocs[$industry->indGRPID][$uocID->uocID][$eoc->stdID] }} {{ trans('report.times') }}

                                        </div>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endforeach