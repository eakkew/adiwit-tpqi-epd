<?php use Carbon\Carbon; ?>

@extends('layout.html5')

@section('head')
	<link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/daterangepicker.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/bootstrap-select.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/report/reportEPDData.css') }}">
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
	<script type="text/javascript" src="{{ asset('tpqi_epd/js/bootstrap-select.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('tpqi_epd/js/jquery-ui.js') }}"></script>
	<script type="text/javascript" src="{{ asset('tpqi_epd/js/report/examDiff.js') }}"></script>
@stop

@section('body')

	<div class="container" style="background-color: white; padding-top: 20px; padding-bottom: 20px;">
		<div class="row">
			<h1 class="text-center">{{ trans('menu.reportDiffExam') }}</h1>
		</div>
		<hr>
		<form class="form-inline" action="{{ action('Ajax\FilterController@selectedFilter') }}" method="post">
			<input name='nextTo' value='reportDiffExam' hidden>
			{!! csrf_field() !!}
			<div class="row">
				<div class="col-xs-6" style="margin-left: 0px; padding-left: 0px">
					<div class="col-xs-12">
						<label class="control-label">{{ trans('keyword.type') }} :</label><br />
						<div id='typeReport' class="btn-group btn-group-justified" data-toggle="buttons">
			                <label class="btn btn-default {{ ($type == 1)? 'active' : '' }}" >
			                	<input data-csrf="{{ csrf_token() }}" type="radio" name="typeReport" value="1" {{ ($type == 1)? 'checked' : '' }}>
			                    {{ trans('keyword.typeLevel') }}
			                </label>
			                <label class="btn btn-default {{ ($type == 2)? 'active' : '' }}" >
			                    <input data-csrf="{{ csrf_token() }}" type="radio" name="typeReport" value="2" {{ ($type == 2)? 'checked' : '' }}>
			                    {{ trans('keyword.typeUOC') }}
			                </label>
			                <label class="btn btn-default {{ ($type == 3 || $type == '')? 'active' : '' }}">
			                    <input data-csrf="{{ csrf_token() }}" type="radio" name="typeReport" value="3" {{ ($type == 3 || $type == '')? 'checked' : '' }}>
			                    {{ trans('keyword.typeEOC') }}
			                </label>
			            </div>
					</div>
				</div>
				<div class="col-xs-6">
					<label class="control-label">{{ trans('keyword.date') }} :</label><br />
					<div class="form-group" style="width:49%; margin-right: 1%">
						<input id="fromDate" name="fromDate" type="text" class="form-control date" placeholder="From Date" style="width:100%; background-color:#ffffff;" readonly>
					</div><div class="form-group" style="width:49%; margin-left: 1%;">
						<input id="toDate" name="toDate" type="text" class="form-control date"  placeholder="To Date" style="width:100%; background-color:#ffffff;" readonly>
					</div>
				</div>
			</div>
			<p></p>
			<div class="row">
				<div class="col-xs-4" style="margin-left: 0px;">
					<label class="control-label">{{ trans('keyword.industry') }} :</label><br />
					<select id="industryGroup" name="industry_id[]" data-csrf="{{ csrf_token() }}" class="multiselect" multiple="multiple" data-non-selected-text="-- {{  trans('command.select').trans('keyword.industry') }} --">
						@foreach ($industries as $industry)
							<optgroup label="{{ $industry->indGRPName }}">
							@if(count($industry->industryGroupX) > 0)				
								@foreach ($industry->industryGroupX as $industryGroupX)
									<option class="{{ $industry->indGRPID }}" value="{{ $industryGroupX->indID }}" {{ ($industriesID && $industriesID[array_search($industryGroupX->indID, $industriesID)] == $industryGroupX->indID)? 'selected' : '' }} >{{ $industryGroupX->indName }}</option>
								@endforeach
							@else
								<option class="{{ $industry->indGRPID }}" value="{{ $industry->indGRPID }}" {{ ($industriesID && $industriesID[array_search($industry->indGRPID, $industriesID)] == $industry->indGRPID)? 'selected' : '' }} >{{ $industry->indGRPName }}</option>
							@endif
							</optgroup>
						@endforeach
					</select>
				</div>
				<div class="col-xs-8">
					<label class="control-label">{{ trans('keyword.qualification') }} :</label><br />
					<select id="qualification" endfilter="Y" name="qualification" data-csrf="{{ csrf_token() }}" class="multiselect" data-non-selected-text="-- {{  trans('command.select').trans('keyword.qualification') }} --" <?=($qualifications && $qualifications != '[]')? '' : 'disabled' ?> >
						@if ($qualifications)
							@foreach ($qualifications as $qualification)
								<option value="{{ $qualification->levelName }}" {{ ($levelsID && $levelsID == $qualification->levelName)? 'selected' : '' }} >{{ $qualification->levelName }}</option>
							@endforeach
						@endif
					</select>
				</div>
			</div>
			<p></p>
			<div class="row text-center">
				<div class="col-xs-12">
					<label class="control-label">{{ 'ชุดคำถามต้นแบบ' }} : </label>
					<select id="masterExam" name="masterExam" data-csrf="{{ csrf_token() }}" class="selectpicker" placeholder="-- เลือกชุดคำถามต้นแบบ --">
					</select>
				</div>
			</div>
			<p></p>
			<div class="row">
				<div class="text-center">
					<button type="submit" id="filterBtn" class="btn btn-primary"><i class="fa fa-search"></i> {{ trans('command.search') }}</button>
				</div>
			</div>
		</form>
		<hr>
		<!-- table -->
		@if(count($exams) > 0)
		<div class="row">
			<div class="col-xs-12">
				<span style="font-size: 12px">
					*ถ้าต้องการให้ข้อสอบซ้ำกันไม่เกิน 50% ข้อสอบที่ควรจะมีในคลังคือ 50 เท่าของจำนวนข้อสอบ ({{$exams->first()->examQuestions->count() * 50}}  ข้อ ตอนนี้มีอยู่ {{$questionCount}} ข้อ)
				</span>
				<a id='clickbtn' class="btn btn-default pull-right"><i class="fa fa-print"> {{ trans('html5.print') }}</i></a>
				<form id='getPDF' target="_blank" action="{{ action('ReportController@reportDiffPDF') }}" method='post' hidden>
					<input name='id' value="{{$masterExam->id}}">
					@if($fromDate && $toDate)
						<input name="from[year]" value="{{ $fromDate['year'] }}">
						<input name="from[month]" value="{{ $fromDate['month'] }}">
						<input name="from[day]" value="{{ $fromDate['day'] }}">
						<input name="to[year]" value="{{ $toDate['year'] }}">
						<input name="to[month]" value="{{ $toDate['month'] }}">
						<input name="to[day]" value="{{ $toDate['day'] }}">
					@endif
					<input name='type' value="reportDiffPDF">
					{!! csrf_field() !!}
				</form>
			</div>
		</div>
		<p></p>
		<script>
			$('#clickbtn').click(function(){
				$('#getPDF').submit();
			});
		</script>
		@endif
		<div class="row">
			@if(count($exams) > 0)
			<div class="col-xs-12">
				<table class="table table-bordered table-hover">
				    <thead>
				      	<tr>
				        	<th style="width: 40%; text-align: left;">{{ 'ชุดคำถามเปรียบเทียบ' }}</th>
				        	<th style="width: 30%; text-align: center;">{{ trans('keyword.date') . trans('keyword.createExam') }}</th>
				        	<th style="width: 30%; text-align: center;">{{ 'ความต่าง' }}</th>
				      	</tr>
				    </thead>
				    <tbody>
				    @foreach($exams as $exam)
				    	<tr data-toggle="modal" data-target="#Modal-{{ $exam->id }}">
				    		<!-- <td>{{ $exam->templateHeader->name . ' ' . trans('keyword.generateNumber') . ' ' . $exam->set . ' ชุดที่ ' . $exam->subset }}</td> -->
				    		<td>{{ $exam->templateHeader->name . ' ' . trans('keyword.generateNumber') . ' ' . $exam->set }}</td>
				    		<td class="text-center">{{ $exam->createAtThai }}</td>
				    		<td class="text-center">{{ number_format($exam->diffPercent, 2, '.', '') }} %</td>
				    	</tr>
				    @endforeach
				    </tbody>
				</table>
			</div>
			@endif
		</div>
		<hr>
		<div class="row">
			<div class="col-xs-4">
				{{ trans('command.printedBy', array('name' => Request::session()->get('tpqi.permission.perName')) ) }}
			</div>
			<div class="col-xs-offset-4 col-xs-4 text-right">
				{{ trans('command.printedOn', array('date' => Carbon::now())) }}
			</div>
		</div>
	</div>
	<!-- Modal -->
	@foreach($exams as $exam)
		<div id="Modal-{{ $exam->id }}" class="modal fade" role="dialog">
		  	<div class="modal-dialog modal-lg">
		    	<div class="modal-content">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal">&times;</button>
		        		<h4 class="modal-title" id='header'>{{ trans('menu.reportDiffExam') }}</h4>
		      		</div>
		      		<div class="modal-body">
		      			<div style="max-height: 400px; overflow: scroll;">
		      				<div class="col-xs-12">
		      					<label class="control-label">{{ 'ชุดคำถามต้นแบบ' }} : </label> {{ $masterExam->templateHeader->name . ' ' . trans('keyword.generateNumber') . ' ' . $masterExam->set }}
		      				</div>
		      				<div class="col-xs-12">
		      					<label class="control-label">{{ 'ชุดคำถามเปรียบเทียบ' }} : </label> {{ $exam->templateHeader->name . ' ' . trans('keyword.generateNumber') . ' ' . $exam->set }}
		      				</div>
		      				<div class="col-xs-12">
		      					<label class="control-label">{{ trans('keyword.date') . trans('keyword.createExam') }} : </label> {{ $exam->createAtThai }}
		      				</div>
		      				<div class="col-xs-12">
		      					<label class="control-label">{{ 'องค์กรรับรองที่นำไปใช้' }} : </label> {{ $exam->organization->orgName }}
		      				</div>
		      				<div class="col-xs-12">
		      					<label class="control-label">{{ trans('keyword.qualification') }} : </label><br>
		      					{{ $exam->lavelCompetence->levelName }}
		      				</div>
		      				<div class="col-xs-12">
		      					<label class="control-label">{{ 'ข้อที่เหมือนกัน (อ้างอิงจากต้นแบบ)' }} : </label><br>
		      					@foreach($exam->sameQuestions as $index => $question)
		      						{{ ($index+1) . ', ' }}
		      					@endforeach
		      				</div>
		      				<div class="col-xs-12">
		      					<label class="control-label">{{ 'ข้อที่ต่างกัน (อ้างอิงจากต้นแบบ)' }} : </label><br>
		      					@foreach($exam->diffQuestions as $index => $question)
		      						{{ ($index+1) . ', ' }}
		      					@endforeach
		      				</div>
				      	</div>
		      		</div>
			      	<div class="modal-footer">
			      		<div id="footer"></div>
			        	<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('command.close') }}</button>
			        	{{--<button type="button" class='btn btn-default pull-right'><i class="fa fa-print"></i></button>--}}
			      	</div>
			    </div>
		  	</div>
		</div>
	@endforeach
	<script type="text/javascript">
		var selectedFromDate = JSON.parse('<?php echo json_encode($fromDate); ?>');
		var selectedToDate = JSON.parse('<?php echo json_encode($toDate); ?>');
		var masterExam = JSON.parse('{{ $masterExam->id or "" }}');
	</script>
@stop
