<?php use Carbon\Carbon; ?>

@extends('layout.html5')

@section('head')
	<link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/daterangepicker.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/bootstrap-select.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/report/reportEPDData.css') }}">
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">

	<script type="text/javascript" src="{{ asset('tpqi_epd/js/highcharts.js') }}" ></script>
	<script type="text/javascript" src="{{ asset('tpqi_epd/js/highcharts-exporting.js') }}" ></script>
	<script type="text/javascript" src="{{ asset('tpqi_epd/js/highcharts.modules.drilldown.js') }}" ></script>
	<script type="text/javascript" src="{{ asset('tpqi_epd/js/export-th.js') }}" ></script>

	<script type="text/javascript" src="{{ asset('tpqi_epd/js/bootstrap-select.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('tpqi_epd/js/jquery-ui.js') }}"></script>
	<script type="text/javascript" src="{{ asset('tpqi_epd/js/report/epdData.js') }}"></script>
@stop

@section('body')

	<div class="container" style="background-color: white; padding-top: 20px; padding-bottom: 20px;">
		<div class="row">
			<h1 class="text-center">{{ trans('menu.reportEPDData') }}</h1>
		</div>
		<hr>
		<form class="form-inline" action="{{ action('Ajax\FilterController@selectedFilter') }}" method="post">
			<input name='nextTo' value='reportEPDData' hidden>
			{!! csrf_field() !!}
			<div class="row">
				<div class="col-xs-6" style="margin-left: 0px; padding-left: 0px">
					<div class="col-xs-6">
						<label class="control-label">{{ trans('report.typeData') }} :</label>
						<select id="typeData" name="typeData" class="selectpicker" title="-- {{  trans('report.typeData') }} --" data-width="70%" data-size="10">
							<option value="question" {{ ($typeData == 'question' || $typeData == '')? 'selected' : '' }}>{{ trans('keyword.question') }}</option>
							<option value="template" {{ ($typeData == 'template')? 'selected' : '' }}>{{ trans('keyword.template') }}</option>
							<option value="exam" {{ ($typeData == 'exam')? 'selected' : '' }}>{{ trans('keyword.exam') }}</option>
						</select>
					</div>
					<div class="col-xs-6">
						<label class="control-label">{{ trans('keyword.type') }} :</label>
						<select id="type" name="type" class="selectpicker" title="-- {{  trans('keyword.type') }} --" data-width="70%" data-size="10">
							<option value="1" {{ ($type == '1' || $type == '')? 'selected' : '' }}>{{ trans('keyword.typeLevel') }}</option>
							<option value="2" {{ ($type == '2')? 'selected' : '' }}>{{ trans('keyword.typeUOC') }}</option>
							<option value="3" {{ ($type == '3')? 'selected' : '' }}>{{ trans('keyword.typeEOC') }}</option>
						</select>
					</div>
				</div>
				<div class="col-xs-6">
					<div class="col-xs-2">
						<label class="control-label pull-right" style="padding-top: 5px;">{{ trans('keyword.date') }} :</label>
					</div>
					<div class="col-xs-5">
						<input id="fromDate" name="fromDate" type="text" class="form-control date" placeholder="{{ trans('keyword.fromDate') }}" style="background-color:#ffffff;" readonly="">
					</div>
					<div class="col-xs-5">
						<input id="toDate" name="toDate" type="text" class="form-control date"  placeholder="{{ trans('keyword.toDate') }}" style="background-color:#ffffff;" readonly>
					</div>
				</div>
			</div>
			<p></p>
			<div class="row">
				<div class="col-xs-4" style="margin-left: 0px;">
					<label class="control-label">{{ trans('keyword.industry') }} :</label>
					<select id="industryGroup" nocheck="Y" name="industry_id[]" data-csrf="{{ csrf_token() }}" class="multiselect" multiple="multiple" data-non-selected-text="-- {{  trans('command.select').trans('keyword.industry') }} --">
					<!-- <select id="industryGroup" name="industry_id" data-csrf="{{ csrf_token() }}" class="selectpicker" data-live-search="true" title="-- {{  trans('command.select').trans('keyword.industry') }} --" data-width="69%" data-size="10"> -->
						@foreach ($industries as $industry)
							<optgroup label="{{ $industry->indGRPName }}">
							@if(count($industry->industryGroupX) > 0)				
								@foreach ($industry->industryGroupX as $industryGroupX)
									<option class="{{ $industry->indGRPID }}" value="{{ $industryGroupX->indID }}" {{ ($industriesID && $industriesID[array_search($industryGroupX->indID, $industriesID)] == $industryGroupX->indID)? 'selected' : '' }} >{{ $industryGroupX->indName }}</option>
								@endforeach
							@else
								<option class="{{ $industry->indGRPID }}" value="{{ $industry->indGRPID }}" {{ ($industriesID && $industriesID[array_search($industry->indGRPID, $industriesID)] == $industry->indGRPID)? 'selected' : '' }} >{{ $industry->indGRPName }}</option>
							@endif
							</optgroup>
						@endforeach
					</select>
				</div>
				<div class="col-xs-8">
					<label class="control-label">{{ trans('keyword.qualification') }} :</label>
					<select id="qualification" nocheck="Y" name="qualification[]" data-csrf="{{ csrf_token() }}" class="multiselect" multiple="multiple" data-non-selected-text="-- {{  trans('command.select').trans('keyword.qualification') }} --" {{ ($qualifications)? '' : 'disabled' }} >
					<!-- <select id="qualification" name="qualification" data-csrf="{{ csrf_token() }}" class="selectpicker" data-live-search="true" title="-- {{  trans('command.select').trans('keyword.qualification') }} --" data-width="86%" data-size="10" disabled> -->
						@if ($qualifications)
							@foreach ($qualifications as $qualification)
								<option value="{{ $qualification->levelName }}" {{ ($levelsID && $levelsID[array_search($qualification->levelName, $levelsID)] == $qualification->levelName)? 'selected' : '' }} >{{ $qualification->levelName }}</option>
							@endforeach
						@endif
					</select>
					<select id="questionType" hidden>
						<option selected value="2" {{ ($type == '2')? 'selected' : '' }}>{{ trans('keyword.typeUOC') }}</option>
					</select>
				</div>
			</div>
			<p></p>
			<div class="row">
				<div class="col-xs-12">
					<label class="control-label">{{ trans('keyword.uoc') }} / {{ trans('keyword.eoc') }} :</label><br />
					<select id="uoc" endfilter="Y" nocheck="Y" name="eocs[]" data-csrf="{{ csrf_token() }}" class="selectpicker" multiple data-max-options="5" data-width="100%" data-live-search="true" data-non-selected-text="-- {{  trans('command.select') }} {{ trans('keyword.uoc') }} / {{ trans('keyword.eoc') }} --" <?=( ($uocs && $uocs != '[]') && $type != 1)? '' : 'disabled' ?> >
						@if ($type != 1)
							@if ($uocs)
								@foreach ($uocs as $uoc)
									<option value="{{ $uoc->stdID }}" {{ ($eocsID && $eocsID[array_search($uoc->stdID, $eocsID)] == $uoc->stdID)? 'selected' : '' }} >{{ $uoc->stdName }}</option>
								@endforeach
							@endif
						@endif
					</select>
				</div>
			</div>
			<p></p>
			<div class="row">
				<div class="text-center">
					<button type="submit" id="filterBtn" class="btn btn-primary"><i class="fa fa-search"></i> {{ trans('command.search') }}</button>
				</div>
			</div>
		</form>
		<hr>
		@if(count($graphsData) > 0)
		<div class="row">
			<div class="col-xs-12">
				<a id="clickbtn" class="btn btn-default pull-right"><i class="fa fa-print"></i> {{ trans('html5.print') }}</a>
				<form id='getPDF' action="{{ action('ReportController@reportEPDDataPDF') }}" method='post' target="_blank" hidden>
					@if($fromDate && $toDate)
						<input name="from[year]" value="{{ $fromDate['year'] }}">
						<input name="from[month]" value="{{ $fromDate['month'] }}">
						<input name="from[day]" value="{{ $fromDate['day'] }}">
						<input name="to[year]" value="{{ $toDate['year'] }}">
						<input name="to[month]" value="{{ $toDate['month'] }}">
						<input name="to[day]" value="{{ $toDate['day'] }}">
					@endif
					@foreach($levelsID as $levelID)
						<input name="qualification[]" value="{{ $levelID }}">
					@endforeach
					<input name="type" value="{{$type}}">
					<input name="typeData" value="{{$typeData}}">
					{!! csrf_field() !!}
				</form>
			</div>
		</div>
		<p></p>
		<script>
			$('#clickbtn').click(function(){
				$('#getPDF').submit();
			});
		</script>
		@endif
		<div class="row">
			<div class="col-xs-12" id="graph"></div>
		</div>
		<hr>
		<div class="row">
			<div class="col-xs-4">
				{{ trans('command.printedBy', array('name' => Request::session()->get('tpqi.permission.perName')) ) }}
			</div>
			<div class="col-xs-offset-4 col-xs-4 text-right">
				{{ trans('command.printedOn', array('date' => Carbon::now())) }}
			</div>
		</div>
	</div>
	<!-- Modal -->
	<div id="questionModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title" id='header'>Modal Header</h4>
	      </div>
	      <div class="modal-body">
	      	<div style="height: 400px; overflow: scroll;">
	      		<table class="table">
	      			<thead>
	      				<tr>
	      					<th style="width: 55%; text-align: center;">คำถาม</th>
	      					<th style="width: 25%; text-align: center;">สถานะ</th>
	      					<th style="width: 20%; text-align: center;">ความถี่</th>
	      				</tr>
	      			</thead>
	      			<tbody id="body">
	      				<tr>
	      					<td>1</td>
	      					<td>เปิดใช้งาน</td>
	      					<td>10</td>
	      				</tr>
	      			</tbody>
	      		</table>
	      	</div>
	      </div>
	      <div class="modal-footer">
	      	<div id="footer"></div>
	        <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('command.close') }}</button>
	      </div>
	    </div>
	  </div>
	</div>
	<div id="graphModal" class="modal fade" role="dialog"></div>
	<script type="text/javascript">
		var selectedFromDate = JSON.parse('<?php echo json_encode($fromDate); ?>');
		var selectedToDate = JSON.parse('<?php echo json_encode($toDate); ?>');

		@if($typeData == 'exam')
			var graphName = 'รายงานสถานะชุดข้อสอบในคลังข้อสอบ';
			var graphThing = 'ชุดข้อสอบ';
		@elseif($typeData == 'template')
			var graphName = 'รายงานสถานะแม่แบบในคลังข้อสอบ';
			var graphThing = 'แม่แบบ';
		@else
			var graphName = 'รายงานสถานะคำถามในคลังข้อสอบ';
			var graphThing = 'คำถาม';
		@endif

	    // Create the chart
	    $('#graph').highcharts({
	        chart: {
	            type: 'column'
	        },
	        "colors": [ 
	        	"#90ed7d",
            	"#B20000"
            ],
	        title: {
	            text: graphName
	        },
	        xAxis: {
	            type: 'category',
	            title: {
	            	text: @if($typeData == 'question' && $type != 1) 'รหัส uoc' @else 'คุณวุฒิวิชาชีพ' @endif
	            }
	        },
	        yAxis: {
                title: {
                    text: 'จำนวน'
                }
            },

	        legend: {
	            enabled: true,
	        },

	        tooltip: {
			    formatter: function() {
			        return this.point.fullName;
			    }
			},

	        plotOptions: {
	            series: {
	            	borderWidth: 0,
	                dataLabels: {
	                    enabled: true,
	                },
	                cursor: 'pointer',
	                point: {
	                    events: {
	                        click: function () {
	                        	var id = this.id;
	                        	var name = this.name;
	                        	var type = this.type;
	                        	var typeData = this.typeData;
	                        	var activeData = this.activeData;
	                        	var token = $('input[name="_token"]').val();
	                        	$.get( "/tpqi_epd/report/1", { id: id, type: type, typeData: typeData, activeData: activeData, token: token } )
	                        	// $.get( "/report/1", { id: id, type: type, typeData: typeData, activeData: activeData, token: token } )
								 .done(function( response ) {
								 	setModal(response, typeData, name);
								 });
	                        }
	                    }
	                }
	            }
	        },

	        series: [{
	            name: graphThing + 'ที่ใช้งาน',
	            // colorByPoint: true,
	            data: [
	            	@foreach($graphsData as $graphIndex => $data)
            			{
            				id: [
            					@foreach($listGraphsID[$graphIndex]["active"] as $graphID)
            						{{ $graphID }},
            					@endforeach
            				],
            				type: '{{ $type }}',
            				typeData: '{{ $typeData }}',
            				activeData: true,
            				name: '{{ $listGraphsCode[$graphIndex] }}',
            				fullName: '{{ $listGraphsName[$graphIndex] }}',
            				y: {{ $data['active'] }},
            				color: '#90ed7d',
            				@if($typeData == 'question' && $type == 3) 
            					drilldown: '{{ $listGraphsCode[$graphIndex] }} (active)' 
            				@endif
            			},
	            	@endforeach
	            ]
	        }, {
	            name: graphThing + 'ที่ยกเลิกการใช้งาน',
	            // colorByPoint: true,
	            data: [
	            	@foreach($graphsData as $graphIndex => $data)
            			{
            				id: [
            					@foreach($listGraphsID[$graphIndex]["deleted"] as $graphID)
            						{{ $graphID }},
            					@endforeach
            				],
            				type: '{{ $type }}',
            				typeData: '{{ $typeData }}',
            				activeData: false,
            				name: '{{ $listGraphsCode[$graphIndex] }}',
            				fullName: '{{ $listGraphsName[$graphIndex] }}',
            				y: {{ $data['deleted'] }},
            				color: '#B20000',
            				@if($typeData == 'question' && $type == 3) 
            					drilldown: '{{ $listGraphsCode[$graphIndex] }} (cancel)' 
            				@endif
            			},
	            	@endforeach
	            ]
	        }]
	    })
	</script>
@stop
