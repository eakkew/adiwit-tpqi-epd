<?php use Carbon\Carbon; ?>

@extends('layout.html5')

@section('head')
	<link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/daterangepicker.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/bootstrap-select.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/report/reportEPDData.css') }}">
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
	<script type="text/javascript" src="{{ asset('tpqi_epd/js/bootstrap-select.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('tpqi_epd/js/jquery-ui.js') }}"></script>
@stop

@section('body')

	<div class="container" style="background-color: white; padding-top: 20px; padding-bottom: 20px;">
		<div class="row">
			<h1 class="text-center">{{ trans('menu.reportFrequencyQuestion') }}</h1>
		</div>
		<hr>
		<form class="form-inline" action="{{ action('Ajax\FilterController@selectedFilter') }}" method="post">
			<input name='nextTo' value='reportFrequencyQuestion' hidden>
			{!! csrf_field() !!}
			<div class="row">
				<div class="col-xs-6" style="margin-left: 0px; padding-left: 0px">
					<div class="col-xs-12">
						<label class="control-label">{{ trans('keyword.type') }} :</label><br />
						<div id='types' class="btn-group btn-group-justified" data-toggle="buttons">
			                <label class="btn btn-default {{ ($type == 1)? 'active' : '' }}" >
			                	<input data-csrf="{{ csrf_token() }}" type="radio" name="type" value="1" {{ ($type == 1)? 'checked' : '' }}>
			                    {{ trans('keyword.typeLevel') }}
			                </label>
			                <label class="btn btn-default {{ ($type == 2)? 'active' : '' }}" >
			                    <input data-csrf="{{ csrf_token() }}" type="radio" name="type" value="2" {{ ($type == 2)? 'checked' : '' }}>
			                    {{ trans('keyword.typeUOC') }}
			                </label>
			                <label class="btn btn-default {{ ($type == 3 || $type == '')? 'active' : '' }}">
			                    <input data-csrf="{{ csrf_token() }}" type="radio" name="type" value="3" {{ ($type == 3 || $type == '')? 'checked' : '' }}>
			                    {{ trans('keyword.typeEOC') }}
			                </label>
			            </div>
					</div>
				</div>
				<div class="col-xs-6">
					<label class="control-label">{{ trans('keyword.date') }} :</label><br />
					<div class="form-group" style="width:49%; margin-right: 1%">
						<input id="fromDate" name="fromDate" type="text" class="form-control date" placeholder="From Date" style="width:100%; background-color:#ffffff;" readonly>
					</div><div class="form-group" style="width:49%; margin-left: 1%;">
						<input id="toDate" name="toDate" type="text" class="form-control date"  placeholder="To Date" style="width:100%; background-color:#ffffff;" readonly>
					</div>
				</div>
			</div>
			<p></p>
			<div class="row">
				<div class="col-xs-4" style="margin-left: 0px;">
					<label class="control-label">{{ trans('keyword.industry') }} :</label><br />
					<select id="industryGroup" name="industry_id[]" data-csrf="{{ csrf_token() }}" class="multiselect" multiple="multiple" data-non-selected-text="-- {{  trans('command.select').trans('keyword.industry') }} --">
						@foreach ($industries as $industry)
							<optgroup label="{{ $industry->indGRPName }}">
							@if(count($industry->industryGroupX) > 0)
								@foreach ($industry->industryGroupX as $industryGroupX)
									<option class="{{ $industry->indGRPID }}" value="{{ $industryGroupX->indID }}" {{ ($industriesID && $industriesID[array_search($industryGroupX->indID, $industriesID)] == $industryGroupX->indID)? 'selected' : '' }} >{{ $industryGroupX->indName }}</option>
								@endforeach
							@else
								<option class="{{ $industry->indGRPID }}" value="{{ $industry->indGRPID }}" {{ ($industriesID && $industriesID[array_search($industry->indGRPID, $industriesID)] == $industry->indGRPID)? 'selected' : '' }} >{{ $industry->indGRPName }}</option>
							@endif
							</optgroup>
						@endforeach
					</select>
				</div>
				<div class="col-xs-8">
					<label class="control-label">{{ trans('keyword.qualification') }} :</label><br />
					<select id="qualification" name="qualification[]" data-csrf="{{ csrf_token() }}" class="multiselect" multiple="multiple" data-non-selected-text="-- {{  trans('command.select').trans('keyword.qualification') }} --" <?=($qualifications && $qualifications != '[]')? '' : 'disabled' ?> >
						@if ($qualifications)
							@foreach ($qualifications as $qualification)
								<option value="{{ $qualification->levelName }}" {{ ($levelsID && $levelsID[array_search($qualification->levelName, $levelsID)] == $qualification->levelName)? 'selected' : '' }} >{{ $qualification->levelName }}</option>
							@endforeach
						@endif
					</select>
				</div>
			</div>
			<p></p>
			<div class="row">
				<div class="col-xs-12">
					<label class="control-label">{{ trans('keyword.uoc') }} / {{ trans('keyword.eoc') }} :</label><br />
					<select id="uoc" name="eocs[]" data-csrf="{{ csrf_token() }}" class="multiselect" multiple="multiple" data-non-selected-text="-- {{  trans('command.select') }} {{ trans('keyword.uoc') }} / {{ trans('keyword.eoc') }} --" <?=( ($uocs && $uocs != '[]') && $type != 1)? '' : 'disabled' ?> >
						@if ($type == 2)
							@if ($uocs)
								@foreach ($uocs as $uoc)
									<option value="{{ $uoc->stdID }}" {{ ($eocsID && $eocsID[array_search($uoc->stdID, $eocsID)] == $uoc->stdID)? 'selected' : '' }} >{{ $uoc->stdName }}</option>
								@endforeach
							@endif
						@elseif ($type == 3 || $type == '')
							@if ($uocs)
								@foreach ($uocs as $uoc)
									<optgroup label="{{ $uoc->stdName }}">
									@foreach ($uoc->eocs as $eoc)
										<option value="{{ $eoc->stdID }}" {{ ($eocsID && $eocsID[array_search($eoc->stdID, $eocsID)] == $eoc->stdID)? 'selected' : '' }} >{{ $eoc->stdName }}</option>
									@endforeach
									</optgroup>
								@endforeach
							@endif
						@endif
					</select>
				</div>
			</div>
			<p></p>
			<div class="row">
				<div class="col-xs-12">
				<?php
					echo "<pre>";
					print_r($orderType);
					echo "</pre>";
				?>
					<label class="control-label">{{ 'รูปแบบการแสดงผล' }} :</label>
					<input type="radio" style="padding-left: 5px;" name="orderType" value="desc" @if($orderType == 'desc') {{ 'checked' }} @elseif(!$orderType) {{ 'checked' }} @endif> มากไปน้อย</input>
					<input type="radio" style="padding-left: 5px;" name="orderType" value="asc" @if($orderType == 'asc') {{ 'checked' }} @endif> น้อยไปมาก
					<input type="radio" style="padding-left: 5px;" name="orderType" value="none" @if($orderType == 'none') {{ 'checked' }} @endif> เฉพาะคำถามที่ไม่ถูกใช้งาน
				</div>
			</div>
			<div class="row">
				<div class="text-center">
					<button type="submit" id="filterBtn" class="btn btn-primary"><i class="fa fa-search"></i> {{ trans('command.search') }}</button>
				</div>
			</div>
		</form>
		<hr>
		<!-- table -->
		@if(count($questions) > 0)
		<div class="row">
			<div class="col-xs-12">
				<a id='clickbtn' class="btn btn-default pull-right"><i class="fa fa-print"> {{ trans('html5.print') }}</i></a>
				<form id='getPDF' target="_blank" action="{{ action('ReportController@reportFrequencyQuestionPDF') }}" method='post' hidden>
					@if($fromDate && $toDate)
						<input name="from[year]" value="{{ $fromDate['year'] }}">
						<input name="from[month]" value="{{ $fromDate['month'] }}">
						<input name="from[day]" value="{{ $fromDate['day'] }}">
						<input name="to[year]" value="{{ $toDate['year'] }}">
						<input name="to[month]" value="{{ $toDate['month'] }}">
						<input name="to[day]" value="{{ $toDate['day'] }}">
					@endif
					@if($type == 1)
						@foreach($levelsID as $levelID)
							<input name="qualification[]" value="{{ $levelID }}">
						@endforeach
					@elseif($type == 2 || $type == 3)
						@foreach($eocsID as $eocID)
							<input name="eocs[]" value="{{ $eocID }}">
						@endforeach
					@endif
					<input name="type" value="{{$type}}">
					{!! csrf_field() !!}
				</form>
			</div>
		</div>
		<p></p>
		<script>
			$('#clickbtn').click(function(){
				$('#getPDF').submit();
			});
		</script>
		@endif
		<div class="row">
			@if(count($questions) > 0)
			<div class="col-xs-12">
				<table class="table table-bordered table-hover">
				    <thead>
				      	<tr>
				        	<th style="width: 60%; text-align: left;">{{ trans('keyword.question') }}</th>
				        	<th style="width: 20%; text-align: center;">{{ trans('keyword.date') . trans('question.createQuestion') }}</th>
				        	<th style="width: 20%; text-align: center;">{{ 'จำนวนครั้งที่ใช้งาน' }}</th>
				      	</tr>
				    </thead>
				    <tbody>
				    @foreach($questions as $question)
				    	<tr data-toggle="modal" data-target="#Modal-{{ $question->id }}">
				    		<td>{!! $question->content !!}</td>
				    		<td class="text-center">{{ $question->createAtThai }}</td>
				    		<td class="text-center">{{ $question->examQuestionsTrash->count() }}</td>
				    	</tr>
				    @endforeach
				    </tbody>
				</table>
				<span class="pull-right">{!! $questions->render() !!}</span>
			</div>
			@endif
		</div>
		<hr>
		<div class="row">
			<div class="col-xs-4">
				{{ trans('command.printedBy', array('name' => Request::session()->get('tpqi.permission.perName')) ) }}
			</div>
			<div class="col-xs-offset-4 col-xs-4 text-right">
				{{ trans('command.printedOn', array('date' => Carbon::now())) }}
			</div>
		</div>
	</div>
	<!-- Modal -->
	@foreach($questions as $question)
		<div id="Modal-{{ $question->id }}" class="modal fade" role="dialog">
		  	<div class="modal-dialog modal-lg">
		    	<div class="modal-content">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal">&times;</button>
		        		<h4 class="modal-title" id='header'>{{ trans('menu.reportFrequencyQuestion') }}</h4>
		      		</div>
		      		<div class="modal-body">
		      			<div class="row">
		      				<div class="col-xs-12">
		      					<label class="control-label">{{ trans('keyword.question') }} :</label> {!! $question->content !!} <br>
								<label class="control-label">{{ trans('keyword.industry') }} :</label> {{ ($question->industryGroup)? $question->industryGroup->indGRPName : $question->industry->indName }} <br>
								<label class="control-label">{{ trans('keyword.qualification') }} :</label> {{ $question->levelCompetence->levelName }} <br>
								@if($question->type == 'eoc')
									<label class="control-label">{{ trans('keyword.uoc') }} :</label> {{ $question->eoc->uoc->stdName }} <br>
									<label class="control-label">{{ trans('keyword.eoc') }} :</label> {{ $question->eoc->stdName }} <br>
								@elseif($question->type == 'uoc')
									<label class="control-label">{{ trans('keyword.uoc') }} :</label> {{ $question->uoc->stdName }} <br>
								@endif
								<label class="control-label">{{ trans('keyword.date') . trans('question.createQuestion') }} :</label> {{ $question->createAtThai }} <br>
								<label class="control-label">จำนวนครั้งที่ใช้งาน :</label> {{ $question->examQuestionsTrash->count() }} <br>
							</div>
		      			</div>
		      			@if($question->examQuestionsTrash->count() > 0)
		      			<div style="max-height: 400px; overflow: scroll;">
				      		<table class="table">
				      			<thead>
				      				<tr>
				      					<th style="width: 60%; text-align: center;">ชุดคำถามที่ใช้</th>
				      					<th style="width: 15%; text-align: center;">วันที่สร้าง</th>
				      					<th style="width: 25%; text-align: center;">สถานะ</th>
				      				</tr>
				      			</thead>
				      			<tbody id="body">
				      				@foreach($question->examQuestionsTrash as $examQuestion)
				      					<tr>
				      						<td>{{ $examQuestion->examTrash->templateHeader->name . ' ' . trans('keyword.generateNumber') . ' ' . $examQuestion->examTrash->set . ' ชุดที่ ' . $examQuestion->examTrash->subset }}</td>
				      						<td style="text-align: center;">{{ $examQuestion->examTrash->createAtThai }}</td>
				      						<td style="text-align: center;">{{ ($examQuestion->examTrash->deleteAtThai)? 'ยกเลิกการใช้งาน' : 'เปิดใช้งาน' }}</td>
				      					</tr>
				      				@endforeach
				      			</tbody>
				      		</table>
				      	</div>
				      	@endif
		      		</div>
			      	<div class="modal-footer">
			      		<div id="footer"></div>
			        	<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('command.close') }}</button>
			      	</div>
			    </div>
		  	</div>
		</div>
	@endforeach
	<script type="text/javascript">
		var selectedFromDate = JSON.parse('<?php echo json_encode($fromDate); ?>');
		var selectedToDate = JSON.parse('<?php echo json_encode($toDate); ?>');
	</script>
@stop
