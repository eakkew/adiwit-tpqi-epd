<?php use Carbon\Carbon; ?>

@extends('layout.html5')

@section('head')
	<link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/daterangepicker.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/bootstrap-select.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('tpqi_epd/css/report/reportEPDData.css') }}">
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">

	<script type="text/javascript" src="{{ asset('tpqi_epd/js/highcharts.js') }}" ></script>
	<script type="text/javascript" src="{{ asset('tpqi_epd/js/highcharts-exporting.js') }}" ></script>
	<script type="text/javascript" src="{{ asset('tpqi_epd/js/highcharts.modules.drilldown.js') }}" ></script>
	<script type="text/javascript" src="{{ asset('tpqi_epd/js/export-th.js') }}" ></script>

	<script type="text/javascript" src="{{ asset('tpqi_epd/js/bootstrap-select.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('tpqi_epd/js/jquery-ui.js') }}"></script>
	<script type="text/javascript" src="{{ asset('tpqi_epd/js/report/examByCBD.js') }}"></script>
@stop

@section('body')

	<div class="container" style="background-color: white; padding-top: 20px; padding-bottom: 20px;">
		<div class="row">
			<h1 class="text-center">{{ trans('menu.reportNumberExamByCBD') }}</h1>
		</div>
		<hr>
		<form class="form-inline" action="{{ action('Ajax\FilterController@selectedFilter') }}" method="post">
			<input name='nextTo' value='reportNumberExamByCBD' hidden>
			{!! csrf_field() !!}
			<div class="row">
				<div class="col-xs-4" style="margin-left: 0px; padding-left: 0px">
					<div class="col-xs-12">
						<label class="control-label">{{ trans('keyword.type') }} :</label>
						<select id="type" name="type" class="selectpicker" title="-- {{  trans('keyword.type') }} --" data-width="70%" data-size="10">
							<option value="1" {{ ($type == '1')? 'selected' : '' }}>{{ trans('keyword.typeLevel') }}</option>
							<option value="2" {{ ($type == '2')? 'selected' : '' }}>{{ trans('keyword.typeUOC') }}</option>
							<option value="3" {{ ($type == '3' || $type == '')? 'selected' : '' }}>{{ trans('keyword.typeEOC') }}</option>
						</select>
					</div>
				</div>
				<div class="col-xs-6">
					<div class="col-xs-2">
						<label class="control-label pull-right" style="padding-top: 5px;">{{ trans('keyword.date') }} :</label>
					</div>
					<div class="col-xs-5">
						<input id="fromDate" name="fromDate" type="text" class="form-control date" placeholder="{{ trans('keyword.fromDate') }}" style="background-color:#ffffff;" readonly="">
					</div>
					<div class="col-xs-5">
						<input id="toDate" name="toDate" type="text" class="form-control date"  placeholder="{{ trans('keyword.toDate') }}" style="background-color:#ffffff;" readonly>
					</div>
				</div>
			</div>
			<p></p>
			<div class="row">
				<div class="col-xs-4" style="margin-left: 0px;">
					<label class="control-label">{{ trans('keyword.industry') }} :</label>
					<select id="industryGroup" name="industry_id[]" data-csrf="{{ csrf_token() }}" class="multiselect" multiple="multiple" data-non-selected-text="-- {{  trans('command.select').trans('keyword.industry') }} --">
						@foreach ($industries as $industry)
							<optgroup label="{{ $industry->indGRPName }}">
							@if(count($industry->industryGroupX) > 0)				
								@foreach ($industry->industryGroupX as $industryGroupX)
									<option class="{{ $industry->indGRPID }}" value="{{ $industryGroupX->indID }}" {{ ($industriesID && $industriesID[array_search($industryGroupX->indID, $industriesID)] == $industryGroupX->indID)? 'selected' : '' }} >{{ $industryGroupX->indName }}</option>
								@endforeach
							@else
								<option class="{{ $industry->indGRPID }}" value="{{ $industry->indGRPID }}" {{ ($industriesID && $industriesID[array_search($industry->indGRPID, $industriesID)] == $industry->indGRPID)? 'selected' : '' }} >{{ $industry->indGRPName }}</option>
							@endif
							</optgroup>
						@endforeach
					</select>
				</div>
				<div class="col-xs-8">
					<label class="control-label">{{ trans('keyword.qualification') }} :</label>
					<select id="qualification" endfilter="Y" name="qualification[]" data-csrf="{{ csrf_token() }}" class="multiselect" multiple="multiple" data-non-selected-text="-- {{  trans('command.select').trans('keyword.qualification') }} --" {{ ($qualifications)? '' : 'disabled' }} >
						@if ($qualifications)
							@foreach ($qualifications as $qualification)
								<option value="{{ $qualification->levelName }}" {{ ($levelsID && $levelsID[array_search($qualification->levelName, $levelsID)] == $qualification->levelName)? 'selected' : '' }} >{{ $qualification->levelName }}</option>
							@endforeach
						@endif
					</select>
				</div>
			</div>
			<p></p>
			<div class="row">
				<div class="text-center">
					<button type="submit" id="filterBtn" class="btn btn-primary"><i class="fa fa-search"></i> {{ trans('command.search') }}</button>
				</div>
			</div>
		</form>
		<hr>
		<!-- table -->
		@if(count($examsTables) > 0)
		<div class="row">
			<div class="col-xs-12">
				<a id='clickbtn' class="btn btn-default pull-right"><i class="fa fa-print"> {{ trans('html5.print') }}</i></a>
				<form id='getPDF' target="_blank" action="{{ action('ReportController@reportNumberExamByCBDPDF') }}" method='post' hidden>
					@if($fromDate && $toDate)
						<input name="from[year]" value="{{ $fromDate['year'] }}">
						<input name="from[month]" value="{{ $fromDate['month'] }}">
						<input name="from[day]" value="{{ $fromDate['day'] }}">
						<input name="to[year]" value="{{ $toDate['year'] }}">
						<input name="to[month]" value="{{ $toDate['month'] }}">
						<input name="to[day]" value="{{ $toDate['day'] }}">
					@endif
					@foreach($levelsID as $levelID)
						<input name="qualification[]" value="{{ $levelID }}">
					@endforeach
					<input name="type" value="{{$type}}">
					{!! csrf_field() !!}
				</form>
			</div>
		</div>
		<p></p>
		<script>
			$('#clickbtn').click(function(){
				$('#getPDF').submit();
			});
		</script>
		@endif
		<div class="row">
			@if(count($examsTables) > 0)
			<div class="col-xs-12">
				<table class="table table-bordered table-hover">
				    <thead>
				      	<tr>
				        	<th class="col-md-6">{{ trans('keyword.certifiedBody') }}</th>
				        	<th class="col-md-3">{{ trans('keyword.date') . trans('command.createExam') . trans('keyword.lastest') }}</th>
				        	<th class="col-md-3">{{ trans('template.amount') . trans('keyword.exam') }}</th>
				      	</tr>
				    </thead>
				    <tbody>
				    @foreach($examsTables as $exam)
				    	<tr data-toggle="modal" data-target="#Modal-{{ $exam->cb_id }}">
				    		<td>{{ $exam->organization->orgName }}</td>
				    		<td>{{ $exam->lastestTH }}</td>
				    		<td>{{ $exam->count }}</td>
				    	</tr>
				    @endforeach
				    </tbody>
				</table>
				<span class="pull-right">{!! $examsTables->render() !!}</span>
			</div>
			@endif
		</div>
		<hr>
		<div class="row">
			<div class="col-xs-4">
				{{ trans('command.printedBy', array('name' => Request::session()->get('tpqi.permission.perName')) ) }}
			</div>
			<div class="col-xs-offset-4 col-xs-4 text-right">
				{{ trans('command.printedOn', array('date' => Carbon::now())) }}
			</div>
		</div>
	</div>
	<!-- Modal -->
	@foreach($examsTables as $examGroup)
		<div id="Modal-{{ $examGroup->cb_id }}" class="modal fade" role="dialog">
		  	<div class="modal-dialog">
		    	<div class="modal-content">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal">&times;</button>
		        		<h4 class="modal-title" id='header'>{{ $examGroup->organization->orgName }}</h4>
		      		</div>
		      		<div class="modal-body">
		      			<div style="height: 400px; overflow: scroll;">
				      		<table class="table">
				      			<thead>
				      				<tr>
				      					<th style="width: 55%; text-align: center;">ชุดคำถาม</th>
				      					<th style="width: 25%; text-align: center;">สถานะ</th>
				      					<th style="width: 20%; text-align: center;">สถานที่สอบ</th>
				      				</tr>
				      			</thead>
				      			<tbody id="body">
				      				@foreach($exams->where('cb_id', $examGroup->cb_id) as $exam)
				      					<tr>
				      						<td><a href="{{ action('ExamController@edit', $exam->id) }}">{{ $exam->templateHeader->name . ' ' . trans('keyword.generateNumber') . ' ' . $exam->set . ' ชุดที่ ' . $exam->subset }}</a></td>
				      						<td style="text-align: center;">{{ ($exam->deleted_at)? 'ยกเลิกการใช้งาน' : 'เปิดใช้งาน' }}</td>
				      						<td style="text-align: center;">{{ $exam->location }}</td>
				      					</tr>
				      				@endforeach
				      			</tbody>
				      		</table>
				      	</div>
		      		</div>
			      	<div class="modal-footer">
			      		<div id="footer"></div>
			        	<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('command.close') }}</button>
			      	</div>
			    </div>
		  	</div>
		</div>
    @endforeach
	<div id="graphModal" class="modal fade" role="dialog"></div>
	<script type="text/javascript">
		var selectedFromDate = JSON.parse('<?php echo json_encode($fromDate); ?>');
		var selectedToDate = JSON.parse('<?php echo json_encode($toDate); ?>');
	</script>
@stop
