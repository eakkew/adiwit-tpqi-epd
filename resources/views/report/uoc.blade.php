<?php use Carbon\Carbon; ?>

@extends('layout.html5')

@section('head')
	<style>
		{!! File::get(public_path() . '/css/dataTables.bootstrap.css') !!}
	</style>
	<script type="text/javascript">
		{!! File::get(public_path() . '/js/dataTables.min.js') !!}
		{!! File::get(public_path() . '/js/dataTables.bootstrap.js') !!}
		{!! File::get(public_path() . '/js/report/uoc.js') !!}
	</script>
	<style>
	    .blue{
	        color: #23527c;
	    }
	</style>
@stop

@section('body')

	<div class="container" style="background-color: white; padding-top: 20px; padding-bottom: 20px;">
	    <div class=" hidden-print">
            <div class="col-xs-12 text-right">
				<a href="{{ action('ReportController@show', [ 'id' => '1', 'type' => 'reportUocFrequencyPDF' ]) }}" class="btn btn-primary">{{ trans('html5.print') }}</a>
            </div>
        </div>
		<div class="row">
			<h1 class="text-center">{{ trans('report.title') }}</h1>
			<!-- <h4 class="text-muted text-center">{{-- trans('report.uocFrequency') --}}</h4> -->
			<h4 class="text-muted text-center">{{ 'จำนวนชุดข้อสอบ แยกตามหน่วยสมรรถนะย่อย' }}</h4>
		</div>
		<hr>
		<div class="row">
			<div class="col-xs-12">
                @include('report.partials')
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-xs-4">
				{{ trans('command.printedBy', array('name' => Request::session()->get('tpqi.permission.perName')) ) }}
			</div>
			<div class="col-xs-offset-4 col-xs-4 text-right">
				{{ trans('command.printedOn', array('date' => Carbon::now())) }}
			</div>
		</div>
	</div>
	<div class="container" style="margin-top: 20px;">
		<div class="row">
			<div class="col-xs-12 text-center">
				<a href="{{ action('ReportController@show', [ 'id' => '1', 'type' => 'reportUocFrequencyPDF' ]) }}" class="btn btn-primary">{{ trans('html5.print') }}</a>
			</div>
		</div>
	</div>
@stop
