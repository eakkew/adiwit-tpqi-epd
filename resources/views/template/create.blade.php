@extends('layout.html5')

@section('head')
	<style>
		{!! File::get(public_path() . '/css/template/create.css') !!}
		{!! File::get(public_path() . '/css/bootstrap-select.min.css') !!}
	</style>
	<script type="text/javascript">
		{!! File::get(public_path() . '/js/bootstrap-select.min.js') !!}
		{!! File::get(public_path() . '/js/template/create.js') !!}
	</script>
@stop

@section('body')
	<div class="container">
		<div class="row">
			<h1 class="text-center">{{ (isset($templateHeader))? trans('template.editTemplate') : trans('template.create') }}</h1>
			<form id="addTemplate" action="{{ isset($industries)? action('TemplateController@store') : action('TemplateController@update', ['id' => $templateHeader->id]) }}" method="post">
				@if (isset($templateHeader))				
					<input type="hidden" name="_method" value="put">
				@endif
				{!! csrf_field() !!}
				<div class="col-md-3">
					<div class="panel panel-default">
						<div class="panel-heading"></div>
						<div class="panel-body">
							<p>
								@if (isset($templateHeader))
									<input type="text" name="templateID" id="templateID" value="{{ $templateHeader->id }}" hidden disabled>
								@endif
								<label for="templateName">{{ trans('template.templateName') }} :</label>
								<label class="requiredField">*</label>
								<input type="text" name="name" id="templateName" class="form-control" placeholder="{{ trans('template.templateName') }}" value="{{ isset($templateHeader->name)? $templateHeader->name : '' }}">
							</p>
							<p>
								<label for="difficulty">{{ trans('template.Difficulty') }} :</label>
								<select id="difficulty" name="max_difficulty" class="selectpicker" title="-- {{  trans('template.averageDifficulty') }} --" data-width="100%" data-size="10" {{ (isset($industries))? '' : 'disabled' }}>
									@if(isset($templateHeader->max_difficulty))
										<option value='0' {{ ($templateHeader->max_difficulty == 0)? 'selected' : '' }}> -- {{ trans('template.difficulty-none') }} -- </option>
										<option value='1' {{ ($templateHeader->max_difficulty == 1)? 'selected' : '' }}>{{ trans('template.difficulty-easy') }}</option>
										<option value='2' {{ ($templateHeader->max_difficulty == 2)? 'selected' : '' }}>{{ trans('template.difficulty-medium') }}</option>
										<option value='3' {{ ($templateHeader->max_difficulty == 3)? 'selected' : '' }}>{{ trans('template.difficulty-hard') }}</option>
									@else
										<option value='0' selected> -- {{ trans('template.difficulty-none') }} -- </option>
										<option value='1'>{{ trans('template.difficulty-easy') }}</option>
										<option value='2'>{{ trans('template.difficulty-medium') }}</option>
										<option value='3'>{{ trans('template.difficulty-hard') }}</option>
									@endif
								</select>
							</p>
							<p>
								<label for="executionTime">{{ trans('template.maxExecutionTime') }} ({{ trans('template.maxExecutionTimeUnit') }}) :</label>
								<div class="input-group">
									<input type="number" min="0" name="max_time_needed" id="executionTime" class="form-control" placeholder="{{ trans('template.maxExecutionTime') }}" value="{{ isset($templateHeader->max_time_needed)? $templateHeader->max_time_needed : 0 }}">
									<span class="input-group-addon">{{ trans('template.maxExecutionTimeUnit') }}</span>
								</div>
							</p>
							<!-- after UAT -->
							<p>
								<label for="templateType">{{ 'ประเภทการสร้างข้อสอบ' }} :</label>
								<label class="requiredField">*</label>
								<div class="input-group">
									<select class="selectpicker" id="templateType" name="type" data-csrf="{{ csrf_token() }}" title="-- {{  trans('keyword.templateType') }} --" data-width="100%" data-size="10" {{ (!isset($templateHeader))? '' : 'disabled' }}>
										@if(isset($templateHeader))
											<option value='1' {{ ($templateHeader->type == 'level')? 'selected' : '' }}> {{ trans('template.templateType1') }} </option>
											<option value='2' {{ ($templateHeader->type == 'uoc')? 'selected' : '' }}> {{ trans('template.templateType2') }} </option>
											<option value='3' {{ ($templateHeader->type == 'eoc')? 'selected' : '' }}> {{ trans('template.templateType3') }} </option>
										@else
											<option value='1'> {{ trans('template.templateType1') }} </option>
											<option value='2'> {{ trans('template.templateType2') }} </option>
											<option value='3' selected> {{ trans('template.templateType3') }} </option>
										@endif
									</select>
								</div>
							</p>
							<label for="industry">
								@if( isset($templateHeader) && isset($templateHeader->industry->industry->indGRPName) ) 
									{{ $templateHeader->industry->industry->indGRPName }} :
								@else
									{{ trans('keyword.industry') }} : 
								@endif 
							</label>
							<label class="requiredField">*</label>

							@if (isset($industries))
								<select id="industryGroup" name="industry_id" data-csrf="{{ csrf_token() }}" class="selectpicker" data-live-search="true" title="-- {{  trans('command.select').trans('keyword.industry') }} --">
									@foreach ($industries as $industry)
										<optgroup label="{{ $industry->indGRPName }}">
										@if(count($industry->industryGroupX) > 0)				
											@foreach ($industry->industryGroupX as $industryGroupX)
												<option class="{{ $industry->indGRPID }}" value="{{ $industryGroupX->indID }}">{{ $industryGroupX->indName }}</option>
											@endforeach
										@else
											<option class="{{ $industry->indGRPID }}" value="{{ $industry->indGRPID }}">{{ $industry->indGRPName }}</option>
										@endif
										</optgroup>
									@endforeach
								</select>
							@else
								<p class="text-muted" id="industryGroup" name="industry_id" value="{{ $templateHeader->industry_id }}">{{ ($templateHeader->industry)? $templateHeader->industry->indName : $templateHeader->industryGroup->indGRPName }}</p>
							@endif


							<label for="qualification">{{ trans('keyword.qualification') }} :</label>
							<label class="requiredField">*</label>
							@if (isset($industries))
								<select id="qualification" name="level_competence_name" data-csrf="{{ csrf_token() }}" endfilter="Y" class="selectpicker" title="-- {{  trans('command.select').trans('keyword.qualification') }} --" data-live-search="true" data-width="100%" data-size="10" disabled></select>
							@else
								<p class="text-muted" id="qualification" name="level_competence_name" value="{{ $templateHeader->levelCompetence->levelName }}">{{ $templateHeader->levelCompetence->levelName }}</p>
							@endif
						</div>
					</div>
				</div>

				<!-- Template by LevelCompetence -->
				<div class="col-md-9">
					<div id="templateLevel" class="panel panel-default" {{ ( (isset($templateHeader) && ($templateHeader->type == 'level')) )? '' : 'hidden' }}>
						<div class="panel-heading">{{ trans('template.templateType1') }}</div>
						<div class="panel-body">
							<div id="templateLevellist">
								@if(isset($templateHeader) && $templateHeader->type == 'level')
									<div class="col-xs-1">
										{{ trans('template.amount') }}
									</div>
									<div class="col-xs-2">
										<input type="number" id="byLevelAmount" onChange="CheckSubmitButton()" class="form-control" placeholder="-- {{ trans('template.amount') . trans('template.amountUnit') }} --" value="{{ $templateHeader->templateItems()->first()->amount or '' }}" name="amount">
										<input id="byLevelID" value="{{ $templateHeader->level_competence_name }}" hidden>
									</div>
									<div class="col-xs-1">
										{{ trans('template.amountUnit') }}
									</div>
								@endif
							</div>

							<!-- Clone this block for UOC list -->
							<div id="newlevel" style="display:none">
								<div class="col-xs-1">
									{{ trans('template.amount') }}
								</div>
								<div class="col-xs-2">
									<input type="number" id="byLevelAmount" onChange="CheckSubmitButton()" class="form-control" placeholder="-- {{ trans('template.amount') . trans('template.amountUnit') }} --" value="">
									<input id="byLevelID" value="xxLevelIDxx" hidden>
								</div>
								<div class="col-xs-1">
									{{ trans('template.amountUnit') }}
								</div>
							</div>
							<!-- End Clone -->
							
							<div class="clearfix"></div>
							<hr class="uocSeparator">
							<hr class="uocSeparator">
							<div class="col-xs-12 text-center">
								<button type="submit" templateType='1' id="submitTemplateLevel" class="btn btn-primary">{{ isset($industries)? trans('template.create') : trans('template.edit') }}  </button>
							</div>
						</div>
					</div>
				</div>

				<!-- Template by UOC -->
				<div class="col-md-9">
					<div id="templateUoc" class="panel panel-default" {{ ( (isset($templateHeader) && ($templateHeader->type == 'uoc')) )? '' : 'hidden' }}>
						<div class="panel-heading">{{ trans('template.templateType2') }}</div>
						<div class="panel-body">
							<div id="templateUOClist">
								@if (isset($templateHeader) && $templateHeader->type == 'uoc')
									@foreach($uocs as $index => $uoc)
										<div class="col-xs-8">
											<p>{{ $uoc->uocTpqi->uocCode }} :: {{ $uoc->stdName }}</p>
											<input id="UOCid-{{ $index }}" value="{{ $uoc->stdID }}" hidden>
										</div>
										<div class="col-xs-1">
											{{ trans('template.amount') }}
										</div>
										<div class="col-xs-2">
											<input type="number" id="UOCamount-{{ $uoc->stdID . '-' . $index }}" name="amount[]" onchange="CheckAmountEOC('UOCamount')" class="form-control" placeholder="-- {{ trans('template.amount') . trans('template.amountUnit') }} --" value="{{ $templateHeader->templateItems()->where('uoc_id', '=', $uoc->stdID)->first()->amount or '' }}">
											<input id="UOCid-{{ $uoc->stdID }}" value="{{ $uoc->stdID }}" name="uoc_id[]" hidden>
										</div>
										<div class="col-xs-1">
											{{ trans('template.amountUnit') }}
										</div>
									@endforeach
								@endif
							</div>
							<!-- Clone this block for UOC list -->
							<div id="newuoc" style="display:none">
								<div class="col-xs-8">
									<p>{{ trans('keyword.uoc') }} :: xxUOCidxx xxUOCdataxx</p>
									<input id="UOCid-xxixx" value="xxUOCidxx" hidden>
								</div>
								<div class="col-xs-1">
									{{ trans('template.amount') }}
								</div>
								<div class="col-xs-2">
									<input type="number" id="UOCamount-xxUOCidxx-xxidxx" onchange="CheckAmountEOC('UOCamount')" class="form-control" placeholder="-- {{ trans('template.amount') . trans('template.amountUnit') }} --" value="">
									<input id="UOCid-xxUOCidxx" value="xxUOCidxx" hidden>
								</div>
								<div class="col-xs-1">
									{{ trans('template.amountUnit') }}
								</div>
							</div>
							<!-- End Clone -->
							<?php $totalUOC = 0; ?>
							<div class="col-xs-offset-9 col-xs-3 text-right">
								<p class="form-control-static">
									{{ trans('template.totalUOC') }}
									<b id='totalUOC'>{{ $totalUOC }}</b>
									{{ trans('template.amountUnit') }}
								</p>
							</div>
							<div class="clearfix"></div>
							<hr class="uocSeparator">
							<hr class="uocSeparator">
							<div class="col-xs-12 text-center">
								<button type="submit" templateType='2' id="submitTemplateUoc" class="btn btn-primary">{{ isset($industries)? trans('template.create') : trans('template.edit') }}  </button>
							</div>
						</div>
					</div>
				</div>

				<!-- Template by EOC -->
				<div class="col-md-9">
					<div id="templateEoc" class="panel panel-default" {{ ( (isset($templateHeader) && ($templateHeader->type == 'eoc')) || !isset($templateHeader) )? '' : 'hidden' }}>
						<div class="panel-heading">{{ trans('template.templateType3') }}</div>
						<div class="panel-body">
							<div id="templateEOClist">
								<!-- load data for edit -->
								@if (isset($templateHeader) && $templateHeader->type == 'eoc')
									@foreach ($uocs as $index => $uoc)
										<div id="AddUOC{{ $index }}">
											<div class="col-xs-10">
												<p><strong><span class="uoc_id" data-uoc="{{ $uoc->stdID }}">{{ $uoc->stdID }}</span> # {{ $uoc->stdName }}</strong></p>
											</div>
											<div class="col-xs-2 text-right"><p class="form-control-static">จำนวน <b id="totalEOC-{{ $index }}">0</b> ข้อ</p></div>
											@foreach ($uoc->eocs as $eoc)
												<div id="listeoc{{ $index }}">
													<div class="col-xs-offset-1 col-sm-7 col-xs-11" style="margin-top: 4px;">
														{{ $eoc->stdID }} # {!! $eoc->stdName !!}
													</div>
													<div class="col-sm-1 col-xs-2" style="margin-top: 4px;">จำนวน</div>
													<div class="col-xs-4 col-sm-2">
														<input type="number" id="EOCamount-{{ $uoc->stdID }}-{{ $eoc->stdID }}-{{ $index }}" onchange="CheckAmountEOC('EOCamount')" class="form-control input-sm" placeholder="-- จำนวนข้อ --" value="{{ $templateHeader->templateItems()->where('eoc_id', '=', $eoc->stdID)->first()->amount or '' }}" name="amount[]">
														<input id="EOCid-{{ $uoc->stdID }}" value="{{ $eoc->stdID }}" hidden="" name="eoc_id[]">
													</div>
													<div class="col-xs-1" style="margin-top: 4px;">ข้อ</div>
													<p class="clearfix"></p>
												</div>
											@endforeach
										</div>
										<div class="clearfix"><hr class="uocSeparator"></div>
									@endforeach
								@endif
							</div>
							<!-- Clone this block for EOC list -->
								<div id="neweoc" style="display:none">
									<div class="col-xs-10">
										<p>xxUocCodexx :: xxUOCdataxx</p>
										<input id="UOCid-xxixx" value="xxUOCidxx" hidden>
									</div>
									<div class="col-xs-2 text-right"><p class="form-control-static">{{ trans('template.amount') }} <b id='totalEOC-xxixx'>0</b> {{ trans('template.amountUnit') }}</p></div>
									<div id="listeocxxixx">
										<div class="col-xs-offset-1 col-xs-7">
											# xxEOCdataxx
											{{-- <p>{{ trans('keyword.eoc') . ' :: ' . "xxEOCidxx" }}</p> --}}
										</div>
										<div class="col-xs-1">
											{{ trans('template.amount') }}
										</div>
										<div class="col-xs-2">
											<input type="number" id="EOCamount-xxUOCidxx-xxEOCidxx-xxidxx" onchange="CheckAmountEOC('EOCamount')" class="form-control" placeholder="-- {{ trans('template.amount') . trans('template.amountUnit') }} --" value="">
											<input id="EOCid-xxEOCidxx" value="xxEOCidxx" hidden>
										</div>
										<div class="col-xs-1">
											{{ trans('template.amountUnit') }}
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="clearfix"><hr class="uocSeparator"></div>
								</div>
							<!-- End Clone -->
							<?php $totalUOC = 0; ?>
							<div class="col-xs-offset-9 col-xs-3 text-right">
								<p class="form-control-static">
									{{ trans('template.totalUOC') }}
									<b id='totalEOC'>{{ $totalUOC }}</b>
									{{ trans('template.amountUnit') }}
								</p>
							</div>
							<div class="clearfix"></div>
							<hr class="uocSeparator">
							<hr class="uocSeparator">
							<div class="col-xs-12 text-center">
								<button type="submit" templateType='3' id="submitTemplateEoc" class="btn btn-primary">{{ 'บันทึก' }}  </button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>

@stop