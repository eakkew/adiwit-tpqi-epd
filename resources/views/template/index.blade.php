@extends('layout.html5')

@section('head')
	<link rel="stylesheet" type="text/javascript" href="{{ asset('tpqi_epd/css/template/index.css') }}">
	<script type="text/javascript" src="{{ asset('tpqi_epd/js/jquery-ui.js') }}"></script>
	<script type="text/javascript" src="{{ asset('tpqi_epd/js/template/index.js') }}"></script>
@stop

@section('body')
	<div class="container">
		<div class="row">

			<div class="col-md-8 col-md-offset-2">
				<form id="filter" class="form-inline" action="{{ action('Ajax\FilterController@selectedFilter') }}" method="post">
					<input name='nextTo' value='template' hidden>
					{!! csrf_field() !!}
					@include('partials.filter')
				</form>
			</div>
			<div class="col-md-12">
				<div id="templates" class="panel panel-default">
					<div class="panel-heading">
						<strong style="font-size: 20px;">{{ trans('keyword.template') }}</strong>
						<a href="{{ action('TemplateController@create') }}" class="btn btn-sm btn-default pull-right">&plus; {{ trans('command.add'). trans('keyword.template') }}</a>
					</div>
					<div class="panel-body">
						<!-- Real Data -->
						@foreach ($templateHeaders as $templateHeader)
							<?php $totalAmount = 0; ?>
							<div class="panel panel-default">
								<div class="panel-body">
									<div class="row">
										<div class="col-sm-12">
											<h4>{{ trans('keyword.template') . ' ' . $templateHeader->name }}</h4><br />
										</div>
									</div>
										@if($templateHeader->industry)
										<div class="row">
											<div class="col-xs-4 col-sm-3">
												<strong>{{ trans('keyword.industry') }} :</strong>
											</div>
											<div class="col-xs-8 col-sm-9">
												{{ ($templateHeader->industry)? $templateHeader->industry->indName : $templateHeader->industryGroup->indGRPName }}
												{{-- $templateHeader->industry->industry->indGRPName --}}
											</div>
                                        </div>
										@endif
                                    <div class="row" hidden>
										<div class="col-xs-4 col-sm-3">
											<strong>{{ trans('keyword.profession') }} :</strong>
										</div>
										<div class="col-xs-8 col-sm-9">
											{{ ($templateHeader->industry)? $templateHeader->industry->indName : $templateHeader->industryGroup->indGRPName }}
										</div>
                                    </div>
                                    <div class="row">
										<div class="col-xs-4 col-sm-3">
											<strong>{{ trans('keyword.qualification') }} :</strong>
										</div>
										<div class="col-xs-8 col-sm-9">
											{{ $templateHeader->levelCompetence->levelName }}
										</div>
                                    </div>
                                    <div class="row">
										<div class="col-xs-4 col-sm-3">
											<strong>{{ trans('template.type') }} :</strong>
										</div>
										<div class="col-xs-8 col-sm-9">
											{{ trans('template.templateType' . $templateHeader->type) }}
										</div>
                                    </div>
                                    <div class="row">
										<div class="col-xs-4 col-sm-3">
											<strong>{{ trans('template.averageDifficulty') }} :</strong>
										</div>
										<div class="col-xs-8 col-sm-9">
											{{ number_format($templateHeader->max_difficulty, 2) }}
										</div>
                                    </div>
                                    <div class="row">
										<div class="col-xs-4 col-sm-3">
											<strong>{{ trans('template.maxExecutionTime') }} :</strong>
										</div>
										<div class="col-xs-8 col-sm-9">
											{{ $templateHeader->max_time_needed }} {{ trans('template.maxExecutionTimeUnit') }}
										</div>
                                    </div>
                                    <div class="row">
										<div class="col-xs-4 col-sm-3">
											<strong>{{ trans('template.questionCount') }} :</strong>
										</div>
										<div class="col-xs-8 col-sm-9">
											{{ $templateHeader->templateItems()->sum('amount') }} {{ trans('template.amountUnit') }}
										</div>
                                    </div>
                                    @if($templateHeader->type != 'level')
                                    	<div class="row">
											<div class="col-xs-4 col-sm-3">
												<strong>{{ trans('keyword.uoc') }} :</strong>
											</div>
											<div class="col-xs-8 col-sm-9">
												<ul>
													<?php $templateItems = $templateHeader->templateItems->groupBy('uoc_id'); ?>
													@foreach ($templateItems as $templateItem)
														<?php $uocAmount = 0; ?>
														@foreach ($templateItem as $eoc)
															<?php 
																$totalAmount += $eoc->amount; 
																$uocAmount += $eoc->amount; 
																$uocName = $eoc->uoc->stdName;
															?>												
														@endforeach
														<li>
															 {{ $uocName }} ( {{ trans('template.amount') }} {{ $uocAmount }} {{ trans('template.amountUnit') }} )
														</li>
													@endforeach
												</ul>
											</div>
										</div>
									@endif
									<div class="row"><hr></div>
									<div class="row text-right">
										<div class="col-md-12">
											<span><a href="#" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#d{{ $templateHeader->id }}">{{ trans('command.delete') }}</a></span>
											@if($templateHeader->exams->count())
												<span><a class="btn btn-sm btn-info" title="แม่แบบนี้ถูกใช้ในการออกชุดข้อสอบแล้ว" disabled>{{ trans('command.edit') }}</a></span>
											@else
												<span><a href="{{ action('TemplateController@edit', $templateHeader->id) }}" class="btn btn-sm btn-info">{{ trans('command.edit') }}</a></span>
											@endif
										</div>
									</div>
							</div>
							<div class="modal fade" id="d{{ $templateHeader->id }}">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<h4 class="modal-title">{{ trans('command.confirmDeleteHeader') }}
												<button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
											</h4>
										</div>
										<div class="modal-body">
											{{ trans('command.confirmDeleteBody', ['item' => trans('keyword.template') . ' ' . $templateHeader->name]) }}
										</div>
										<div class="modal-footer">
									        <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('command.cancelButton') }}</button>
									        <a href="{{ action('TemplateController@destroy', $templateHeader->id) }}" data-csrf="{{ csrf_token() }}" data-method="delete" type="button" class="btn btn-primary">{{ trans('command.confirmButton') }}</a>
										</div>
									</div>
								</div>
							</div>
                        </div>
						@endforeach
						<?php 
							$url =  \Illuminate\Support\Facades\URL::current(); 
							$lastPage = floor($allDataCount/$perPage);
							$lastPage = ($allDataCount%$perPage)? $lastPage+1 : $lastPage;
						?>
						<ul class="pagination">
						    <li @if($page==1) class="disabled" @endif ><a href="{{$url}}/?page=1" rel="prev">«</a></li>
						    @foreach(range($page-5,$page-1) as $i)
						        @if($i >0)
						            <li><a href="{{$url}}/?page={{$i}}">{{$i}}</a></li>
						        @endif
						    @endforeach
						    <li class="active"><span>{{$page}}</span></li>
						    @foreach(range($page+1,$page+5) as $i)
                                @if($i <= $lastPage || ($i <= $lastPage))
                                    <li><a href="{{$url}}/?page={{$i}}" {{$allDataCount . ' | ' . $perPage . ' | ' . $lastPage}}>{{$i}}</a></li>
                                @endif
                            @endforeach
						    <li @if($page==$lastPage) class="disabled" @endif><a href="{{$url}}/?page={{$lastPage}}" rel="prev">»</a></li>
                        </ul>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop
